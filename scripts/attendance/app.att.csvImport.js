var appAttCsvImport = angular.module("app.att.csvImport", []);

appAttCsvImport.controller("scheduleImportCtrl", ["logger","$cookieStore","ApiURL","$scope","$routeParams","$http","fileUpload", function(logger,$cookieStore,api,$scope,$routeParams,$http,fileUpload) {
		
		// FUNCTION UPLOAD FILE CSV
		
		$scope.uploadFile = function(){
			var file = $scope.myFile;
			$scope.myFile = '';
			var uploadUrl = api.url + '/api/module/import-csv?key=' + $cookieStore.get('key_api');
			fileUpload.uploadFileToUrl(file, uploadUrl);
		};
    	
		
		// LINK DOWNLOAD CSV
		
		$scope.e = api.url +  '/api/module/import-csv-getfile?key=' + $cookieStore.get('key_api');
		
}]);


appAttCsvImport.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


appAttCsvImport.service('fileUpload', ['$http','logger', function ($http,logger) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        
		}).success(function(data,status,headers,config){
			logger.logSuccess(data.header.message);
        }).error(function(data,status,headers,config){
			logger.logError(data.header.message);
        });
    }
}]);
