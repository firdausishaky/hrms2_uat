(function(){
	
	angular.module("app.att.attendancecutoff",[])
	
	.factory('attCutOffFactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,$route){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['name','Name'],
					['Date','date'],
					['Schedule Time','schedule'],
					['Time In','timein'],
					['Time Out','time_out'],
					['Work Hours (Hrs)','workhour'],
					['Overtime (Hrs)','overtime'],
					['Overtime Rest Day (Hrs)','retsday'],
					['Short (Mins)','short'],
					['Late (Mins)','late'],
					['Early Out (Mins)','early_out']
					);
				this.setactiveheader(this.headers[0],false)	
			},getaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/cutOff/view?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setform(res.data.data);
						logger.logSuccess('Access Granted');
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},setform:function(data){
				self.depart = data.department;
				self.jobs = data.job;
				self.dataperiod=data.period;
			},view : function(){
				console.log(self.form);
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
					data :self.form,
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					self.setdata(res.data.data);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},cutOff : function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data :self.form,
					headers: {
						"Content-Type": "application/json"
					}
					
				}).then(function(res){
					self.setdata([]);
					logger.logSuccess('Success cut off data');
					$route.reload();
				},function(res){
					res.data.header ? logger.logError('Error cut off data') : logger.logError("Error cut off data")
				});
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			
		}
	})
	.controller('attCutOffController',function(attCutOffFactory,ApiURL,$cookieStore,$scope,$http){
		var self = this;
		self.form={}
		self.handler = attCutOffFactory;
		self.handler.getaccess()
	})
	
	/** FUNCTION SET HEADER TABLES ATT CUT-OFF **/
	.run(function(attCutOffFactory){
		attCutOffFactory.setheaders();
	})
	
}())
