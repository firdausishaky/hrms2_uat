(function(){
	
	/** MODULE **/ 
	angular.module("app.att.changeShift",[])
	
	
	/** FACTORY **/ 
	.factory('changeShiftfactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore){
		
		var self;
		
		return self = {
			
			save: function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/change-shift/request?key=' + $cookieStore.get('key_api'),
					headers : {'Content-Type' : 'application/x-www-form-urlencoded',},
					data : $.param(self.form) 
				}).then(function(res){
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header.message ? logger.logError(res.data.header.message) : logger.logError("Adding Change Shift Failed")
				});
			},
			
			
			/** FUNCTION GET CHANGE SHIFT **/
			update : function(){
				self.form.name = self.label;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/change-swap-shift/work-hour?key=' + $cookieStore.get('key_api'),
					headers : {'Content-Type' : 'application/x-www-form-urlencoded',},
					data : $.param(self.form) 
				}).then(function(res){
					console.log(res.data)
					logger.logSuccess(res.data.header.message);
				},function(res){
					logger.logError(res.data.header.message);
				});
				
			},
			
			
			/** FUNCTION SEARCHING NAME **/
			onSelect : function ($item, $model, $label) {
					self.subordinate = $item.name;
					self.model = $item.name;
					self.label = $item.employee_id;
			},
			getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name +'?&key=' + $cookieStore.get('key_api'),{
				  params: {
					address : val,
				  }
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						  return a.result.map(function(item){
							return item;
						  });
					}  
				});
			},
			
			
			
			/** FUNCTION SEARCHING SWAP WITH  **/
			onSwap : function ($item, $model, $label) {
					self.subordinate = $item.name;
					self.model = $item.name;
					self.label = $item.employee_id;
			},
			getSwap : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.swap +'?&key=' + $cookieStore.get('key_api'),{
				  params: {
					address : val,
				  }
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						  return a.result.map(function(item){
							return item;
						  });
					}  
				});
			},
			
			
			/** FUNCTION RESET FORM **/
			cancel : function(){
				self.form = {};
				self.form.type = '';
			},
				
		}
	})
	
	
	/** MAIN CONTROLLER **/  
	.controller('changeShiftcontroller',function(changeShiftfactory,ApiURL,$cookieStore,$scope,$http,logger){
		var self = this;
			self.handler = changeShiftfactory;
			self.form = {};
			
			/** FUNCTION ADD CHANGE SHIFT **/
			this.addChange = function(a){
				var a = {
						date:null,
						orirginal:null,
						newShift: null
					}	
				this.form.subChange.push(a);
			}
			this.form.subChange = [];
			this.addChange();
			
			
			/** FUNCTION ADD SWAP SHIFT **/
			this.addsubSwap = function(){
				var b = {
						date:null,
						name1:null,
						swap: null,
						name2:null
					}	
				this.form.subSwap.push(b);
			}
			this.form.subSwap = [];
			this.addsubSwap();
		
		
	})
	
	   
}())

/**
this.update = function(){
	self.form.subChange[0].date = self.handler.form;
	console.log(self.handler.form)
	$http({
		method : 'POST',
		url : ApiURL.url + '/api/attendance/change-swap-shift/work-hour?key=' + $cookieStore.get('key_api'),
		headers : {'Content-Type' : 'application/x-www-form-urlencoded',},
		data : $.param(self.handler.form) 
	}).then(function(res){
		console.log(res.data)
		logger.logSuccess(res.data.header.message);
	},function(res){
		logger.logError(res.data.header.message);
	});
},
**/
			