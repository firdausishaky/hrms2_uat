(function(){

		function employeeData(a){
			this.name = a;
		}

		employeeData.prototype.update = function(a){
			this.name = a;
		}
		
		
		function getDaysInMonth(month, year) {
         
			var date = new Date(year, month, 1);
			var days = [];
			 
			 while (date.getMonth() === month) {
				days.push(new Date(date));
				date.setDate(date.getDate() + 1);
			 }
			 
			return days;
		}
		
		
		
		
		

	angular.module('app.att.unApproved',[])
		
		.factory('pagination', function() {
			return {
				option: [5,10,20]
			}
			 
		})
		
		
		.factory('unApprovedFactory',function(pagination,$filter,$modal,$http,logger){
			function createheaders(){
				var temp = [];
				for(var i=0;i<arguments.length;i++){
					var x = {};
					x.id = i;
					x.name =arguments[i][0];
					x.sort =arguments[i][1];
					x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
					x.order =false;
					temp.push(x);
				}
				return temp;
			}
			var self;
			return self = {
				setheaders: function(){
				
					self.date = [];
					var temp = getDaysInMonth(1, 2015);
					for(a in temp){
						var b = temp[a];
						var c = $filter('date')(b,'dd');
						self.date.push(c);
						
					}
					console.log(self.date);
					
					self.a = [];
					for(i = 0; i < self.date.length ; i++){
						 var a = [];
						 a = self.date[i];
						 console.log(a);
					};
					
					
					this.headers = new createheaders(
						['nama'],['number']//self.test
					);
					this.setactiveheader(this.headers[0],false)
				},
				
				adddata: function(a){
					this.maindata.push(a);
				},
				
				setdata: function(a){
					this.maindata = a;
				},

				/*** Function get data ****/
				getdata: function(){
					self.filtered = $filter('filter')(this.maindata,{$:self.search});
					self.select(self.currentPage);
					return self.paginated;
				},
				
				/*** Function Delete ****/
				deldata: function(a){
					for (i = 0; i < this.maindata.length; ++i) {
						this.maindata[i]==a ? this.maindata.splice(i,1) : 0
					}
				},
				
				/*** order by ****/
				setactiveheader:function(a,bool){
					this.activeheader = a;
					this.activeheader.order = bool;
				},
				
				
				/*** Pagination ****/
				numPerPageOpt	: pagination.option,
				numPerPage 		: pagination.option[0],
				currentPage		: 1,
				search			: '',
				select			: function(page){
					if(self.filtered){
						start = (page-1)*self.numPerPage;
						end = start + self.numPerPage;
						self.paginated = self.filtered.slice(start, end) || false;
						self.filteredlength = self.paginated.length || 0;

					}
				},
				
				
				
				/*** Modal Handler ****/
				modals: {
				
					/*** Modal Change Workshift ****/
					edit:{
						animation: true,
						templateUrl: 'unAppModal',
						controller: 'editemployeModal',
						controllerAs: 'unAppModal',
						size:'',
						backdrop:'static',
						resolve:{
							data:function(){
								return self.datatoprocess;
							},
							title:function(){
								return "Edit"
							}
						}
					}
				},
				openmodal: function(a,b){
					self.datatoprocess = b
					$modal.open(self.modals[a]).result.then(function (data) {
						switch (a) {
							case 'add':
								$http.post('/addmanufacturerAPI',data).success(function(res){
									
									logger.logSuccess(res.msg)
									self.adddata(new employeeData(data.name));
									
								}).error(function(){
									//logger.logError("Adding Manufacturer Failed")
									self.adddata(new employeeData(data.name));
								});
								break;
								
							case 'edit':
								
								$http.post('/editmanufacturerAPI',data.b).success(function(res){
									
									logger.logSuccess(res.msg)
									data.a.update(data.b.name)
									
								}).error(function(){
									
									//logger.logError("Updating Manufacturer Failed")
									data.a.update(data.b.name)
									
								});
								break;	
						}
						
						
					});
				},
			
			}
		}) 

		.controller('unApprovedController',function(unApprovedFactory){
			//console.log(employeeFactory, "<-result of post ? if error the apipost request have error");
			
			//** should be removed upon api connection
			serverdata = [];
			serverdata.push(new employeeData('Training','Training 1','Training 2'));
			serverdata.push(new employeeData('Test'));
			serverdata.push(new employeeData('Permanent'));
			serverdata.push(new employeeData('contract'));
			serverdata.push(new employeeData('Training'));
			serverdata.push(new employeeData('Test'));
			serverdata.push(new employeeData('Permanent'));
			serverdata.push(new employeeData('contract'));
			serverdata.push(new employeeData('Training'));
			serverdata.push(new employeeData('Test'));
			serverdata.push(new employeeData('Permanent'));
			serverdata.push(new employeeData('contract'));
				
			this.handler = unApprovedFactory;
			unApprovedFactory.setdata(serverdata)
			
			
		})


		.controller('editemployeModal',function(unApprovedFactory,$modalInstance,$timeout,data,title){
			
				var self = this;
				
				self.title = title;
				
				self.submit = function(){
					$modalInstance.close(
							{a:data,b:self.form}
					);
				}
				
				self.form = {};
				
				self.form = angular.copy(data);
					
				self.close = function(){
					$modalInstance.dismiss("shuwwaaa")
				}
		
})


		.run(function(unApprovedFactory){
			unApprovedFactory.setheaders();
		})

}())		