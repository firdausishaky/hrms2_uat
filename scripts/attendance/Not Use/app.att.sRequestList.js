var appRequestList = angular.module("app.att.sRequestList",[]);

	
appRequestList.controller("sRequestListCtrl", ["$scope","$filter","$http","$modal","$log","$cookieStore","ApiURL","$routeParams","filterFilter","logger", function($scope,$filter,$http,$modal,$log,$cookieStore,api,$routeParams,filterFilter,logger) {		
		
		
		/*
			$http.get(api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api') ).success(function(res){
				
				// FUNCTION GT ID FOR DELETE
				$scope.check = {
					currentPageStores: []
				}
				
				var end, start;
				start =0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
				
				
			}).success(function(data, status, header, config) {
					$scope.role = true;
					logger.logSuccess(data.header.message);
			}).error(function(data, status, headers, config) {
					$scope.role = false;
					logger.logError(data.header.message);
			});
			
		*/	

		
			$http.get("json/json/sRequestList.json").success(function(res){
				
				// FUNCTION GT ID FOR DELETE
				$scope.check = {
					currentPageStores: []
				}
				
				var end, start;
				start =0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
				
				
			});
			
		
			// FUNCTION TABLE LEAVE 
			
			var init;
            $scope.numPerPageOpt = [3, 5, 10, 20]; 
            $scope.numPerPage = $scope.numPerPageOpt[2]; 
            $scope.currentPage = 1; 
            $scope.currentPageStores = []; 
            $scope.searchKeywords = ""; 
            $scope.filteredStores = []; 
            $scope.row = "" ;
            $scope.stores =[];
            $scope.select = function(page) {
                 var end, start;
                 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
            };
            $scope.onFilterChange = function() {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
            };
            $scope.onNumPerPageChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.onOrderChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.search = function() {
                return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
            };
            $scope.order = function(rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
            };
            init = function() {
                return $scope.search(), $scope.select($scope.currentPage)
            };
			
			
			
			
			//################ FUNCTION MODAL SCHEDULE REQUEST DETAILS  ################################################################
			
			$scope.edit = function(size,store) {
					
					var modal;
					
					for(modal in $scope.stores){
					
						
						if($scope.stores[modal].request == "Change Shift") {
						
							console.log($scope.stores[modal].request)
							
							modal = $modal.open({
								templateUrl: "swapShift.html",
								controller: "swapShiftCtrl",
								backdrop : "static",
								size: size,
								resolve: {
									items: function(){
										return store;
									}
								}
							});
							
							
						}else{
							
							/*
							
								modal = $modal.open({
									templateUrl: "timeInOut.html",
									controller: "timeInOutCtrl",
									backdrop : "static",
									size: size,
									resolve: {
										items: function(){
											return store;
										}
									}
								});
							
							
								modal = $modal.open({
									templateUrl: "changeShift.html",
									controller: "changeShiftCtrl",
									backdrop : "static",
									size: size,
									resolve: {
										items: function(){
											return store;
										}
									}
								});
								
								
								modal = $modal.open({
									templateUrl: "overtime.html",
									controller: "overtimeModalCtrl",
									backdrop : "static",
									size: size,
									resolve: {
										items: function(){
											return store;
										}
									}
								});
								
								
								modal = $modal.open({
									templateUrl: "undertime.html",
									controller: "undertimeModalCtrl",
									backdrop : "static",
									size: size,
									resolve: {
										items: function(){
											return store;
										}
									}
								});
								
								
								modal = $modal.open({
									templateUrl: "lateEarly.html",
									controller: "lateEarlyModalCtrl",
									backdrop : "static",
									size: size,
									resolve: {
										items: function(){
											return store;
										}
									}
								});
								
								
								
								modal = $modal.open({
									templateUrl: "trainingModal.html",
									controller: "trainingCtrl",
									backdrop : "static",
									size: size,
									resolve: {
										items: function(){
											return store;
										}
									}
								});
								
								
								modal = $modal.open({
									templateUrl: "swapShift.html",
									controller: "swapShiftCtrl",
									backdrop : "static",
									size: size,
									resolve: {
										items: function(){
											return store;
										}
									}
								});
						
							*/
						}
					};
					
			};
			
			
			
				
}]);


		// #########################################	TIME-IN/TIME-OUT CONTROLLER		##############################################################################
		
appRequestList.controller("timeInOutCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
		
				if(items){
					$scope.formData = items;
				}
								
						
				$scope.approve = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;				
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.reject = function() {
					
				}
				
				
				$scope.back = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);


		// #########################################	CHANGE SHIT CONTROLLER		##############################################################################
		
appRequestList.controller("changeShiftCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
		
				if(items){
					$scope.formData = items;
				}
								
						
				$scope.approve = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;				
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.reject = function() {
					
				}
				
				
				$scope.back = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);


	// #########################################	OVERTIME CONTROLLER		##############################################################################
		
appRequestList.controller("overtimeModalCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
		
				if(items){
					$scope.formData = items;
				}
								
						
				$scope.approve = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;				
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.reject = function() {
					
				}
				
				
				$scope.back = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);



	// #########################################	UNDERTIME CONTROLLER		##############################################################################
		
appRequestList.controller("undertimeModalCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
		
				if(items){
					$scope.formData = items;
				}
								
						
				$scope.approve = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;				
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.reject = function() {
					
				}
				
				
				$scope.back = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);




	// #########################################	LATE EARLY CONTROLLER		##############################################################################
		
appRequestList.controller("lateEarlyModalCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
		
				if(items){
					$scope.formData = items;
				}
								
						
				$scope.approve = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;				
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.reject = function() {
					
				}
				
				
				$scope.back = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);




	// #########################################	TRAINING CONTROLLER		##############################################################################
		
appRequestList.controller("trainingCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
		
				if(items){
					$scope.formData = items;
				}
								
						
				$scope.approve = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;				
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.reject = function() {
					
				}
				
				
				$scope.back = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);


	// #########################################	SWAFT SHIFT CONTROLLER		##############################################################################
	
appRequestList.controller("swapShiftCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
		
				if(items){
					$scope.formData = items;
				}
								
						
				$scope.approve = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;				
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.reject = function() {
					
				}
				
				
				$scope.back = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);

