(function(){
	
	
	angular.module("app.att.attrecord",[])
	
	.factory('attrecordfactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		
		var self;
		return self = {
			tmp_array : [],
			tmp_header:function(){
				fileName = "Report"
				
				tmp = []
				
				// --------------------------------------------------------------------------------------------
				headersStart = this.headers
				for (var i = 1; i < headersStart.length; i++) {
					if (headersStart[i]) {
						if ([i] <= 2) {

							x = headersStart[i].name
							tmp.push(x)
						}
					}
				}

				// --------------------------------------------------------------------------------------------

				dates = self.getdate

				for (var i = 0; i < dates.length; i++) {
					if (dates[i].date) {
						x = dates[i].date.split("-")
						tmp.push(x[2])
					}
				}

				// --------------------------------------------------------------------------------------------

				for (var i = 0; i < headersStart.length; i++) {
					if (headersStart[i]) {
						if (2 < [i] ) {

							x = headersStart[i].name
							tmp.push(x)
						}
					}
				}

				self.tmp_array.push(tmp)
			},
			check_data : function(a,idx){
				var arr = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','aa','ab','ac','ad','ae'];
				tmpMainDate = []
				t = [];

				// ====================================================================================

				self.listTable = "";
				if(self.getdate){				
					
					var len = self.getdate.length;					
					if(len > 0){
						self.listTable += 
						"<td>"+idx+"</td>"+
						"<td>"+a.last_name+"</td>"+
						"<td>"+a.first_name+"</td>";	
						t.push(a.last_name);
						t.push(a.first_name);
						
						var r = [];
						for(var i=0; i< len; i++){
							r.push(a[arr[i]]);
							if(a[arr[i]] != undefined){
								
								var keys = arr[i];
								var st = 'status'+keys;
								if(a[st] == 'green'){
									a[st] = '#ccffcc';
								}
								self.listTable +='<td style="text-align:center;" '+

								"bgcolor='"+a[st]+"' "+
								"><b>"+a[arr[i]]+'</b></td>';
							}else if(a[arr[i]] == undefined){
								var keys = '';
								var st = 'yellow';
								self.listTable +='<td style="text-align:left;"'+
								"bgcolor='yellow'"+
								"><b>"+keys+'</b></td>';

								r.push(keys);
							}
						}
						for(var x in r){
							t.push(r[x]);
						}


						self.listTable += "<td>"+a.tar+"</td>"+
						"<td>"+a.abs+"</td>"+
						"<td>"+a.nd+"</td>"+
						"<td>"+a.ndot+"</td>"+
						"<td>"+a.rot+"</td>"+
						"<td>"+a.rotdo+"</td>";
						t.push(a.tar);
						t.push(a.abs);
						t.push(a.nd);
						t.push(a.ndot);
						t.push(a.rot);
						t.push(a.rotdo);
					}
					if(self.tmp_array.length < self.showDatas()){
						self.tmp_header()
						self.tmp_array.push(t);
					}
				}


				if(self.listTable){
					return true;
				}else{
					return false;
				}

			},
			tmpDataTable:function(a){
				x = []
				x.push(a)
				this.tmpDataTabless = x 
			},
			tmpDateTable:function(){
				return this.tmpDataTabless
			},setheaders: function(){
				this.headers = new createheaders(
					['No','no'],
					['Last','last'],
					['First Name','first'],
					['TAR(Mins)','tar'],
					['ABS(Day)','abs'],
					['ND(Hrs)','nd'],
					['ND(OT)(Hrs)','ndot'],
					['ROT(Hrs)','rot'],
					['ROT DO(Hrs)','rotdo']
					);
				this.setactiveheader(this.headers[0],false)	
			},getaccess : function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/cutOff/index?key='  + $cookieStore.get('key_api')
				}).then(function(res){
					$http({
						method : 'GET',
						url : ApiURL.url + '/api/attendance/cutOff/view?key=' + $cookieStore.get('key_api')
					}).then(function(res){		
						self.department =  res.data.data.department;
						self.job = res.data.data.job
					},function(res){
						//res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
					});
					self.access(res.data.header.access)
					self.dataperiod = res.data.data;
					logger.logSuccess('Access Granted');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError(" Get Access Failed")
				});		
			},access:function(a){
				if(a){this.mainaccess=a}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},view:function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/views?key=' + $cookieStore.get('key_api'),
					data : self.form

				}).then(function(res){

					if(res.data.status == 500){
						return logger.logError(res.data.message);
					}
					if(res.data.data == ''){
						self.setdata([])
						self.setformcomment([]);
						// self.check_data()
					}else{
						self.setdate(res.data.data.date)
						self.countData(res.data.data[0].length)
						self.setformcomment(res.data.data.comment);
						self.setdata(res.data.data[0]);
						// self.check_data()
					}
					console.log(self.tmp_array)
					console.log(self.showDatas())
					logger.logSuccess(res.data.header.message);
				},function(res){
					console.log(self.showDatas())
					//logger.logError(res.data.message);
					res.data.header ? logger.logError(res.data.message) : logger.logError("View Attendance Record Failed")
				});
			},
			countData:function(a){
				this.countDatas = a
			},
			showDatas:function(){
				return this.countDatas

			},
			savecomment:function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/insert_comment?key=' + $cookieStore.get('key_api'),
					data : self.form
				}).then(function(res){
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Note Failed")
				});
			},setformcomment:function(a){
				if(a[0]){
					self.form.comment = a[0].comment;
				}
			},
			setdate:function(date){
				self.getdate=date;
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},

			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			// numPerPageNew : self.showDatas(),
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.showDatas();
					end = start + self.showDatas();
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},
			in_date : function(date_x,idx){
				arr = []
				for(var prop in date_x){
					var splix =  date_x[prop].date.split('-');
					//console.log()(splix)
					if(parseInt(splix[2]) == idx){
						arr.push({status : true},{date : date_x[prop].date})
						//console.log()(arr)
						return arr
					}else{
						arr.push({status : false},{date : date_x[prop].date})
						//console.log()(arr)
						return arr
					}
				}
			},
			in_array :  function (arg,arg1,date_x){
				//console.log(arg,arg1,date_x)
				//self.exports();
				var count  = arg.length;
				var arr = [];
				/*for(var i = 0; i < count; i++){
					var undefined  =  false;
					// console.log(arg[i])
					if(arg1.hasOwnProperty(arg[i]) == true){
						this.in_date(date_x,i,(result)=>{

							console.log()(result)
						});
					}

					if(i == (count - 1)){
						return arr;
					}
				}*/
			},
			export : function(){
				// var dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];
				// console.log(this.maindata,'========================');
				// for(var prop in this.maindata){
				// 	this.in_array (dictionary,this.maindata[prop], self.getdate,(test)=>{
				// 		console.log(test);
				// 	})
				// }		

				// fileName = "Report"
				
				// tmp = []
				
				// // --------------------------------------------------------------------------------------------
				// headersStart = this.headers
				// for (var i = 1; i < headersStart.length; i++) {
				// 	if (headersStart[i]) {
				// 		if ([i] <= 2) {

				// 			x = headersStart[i].name
				// 			tmp.push(x)
				// 		}
				// 	}
				// }

				// // --------------------------------------------------------------------------------------------

				// dates = self.getdate

				// for (var i = 0; i < dates.length; i++) {
				// 	if (dates[i].date) {
				// 		x = dates[i].date.split("-")
				// 		tmp.push(x[2])
				// 	}
				// }

				// // --------------------------------------------------------------------------------------------

				// for (var i = 0; i < headersStart.length; i++) {
				// 	if (headersStart[i]) {
				// 		if (2 < [i] ) {

				// 			x = headersStart[i].name
				// 			tmp.push(x)
				// 		}
				// 	}
				// }

				// self.tmp_array.push(tmp)

				// --------------------------------------------------------------------------------------------
				// 
				
				// mainDataTable = self.tmp_array
				// console.log(mainDataTable)
				// console.log(mainDataTable)
				// self.tmp_array = []
				// tmpMainData =[]
				// tmpData = []
				// tmpDataDate = [] 
				// var arr = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','aa','ab','ac','ad','ae'];
				// var len = self.getdate.length
				
				// test = self.tmpDateTable()
				// console.log(test)


				// angular.forEach(mainDataTable,function(value,key){
				// 	tmpData.push([key+1,value.last_name,value.first_name])
				// 	for (var i = 0; i < len; i++) {
				// 		tmpDataDate.push(value[arr[i]])
				// 	}

				// })
				// x = []
				// for (var i = 0 ; i < len; i++) {
				// 	if (0<tmpDataDate) {
				// 		x.push(i)
				// 	}
				// }
				// console.log(x)

				// console.log(tmpDataDate)

				// alasql('SELECT * INTO XLSX("Shipping Delivered Order.xlsx",{headers:true}) FROM ?',[this.maindata]);
			   //console.log()(this.maindata,"GENERATE XLSX")
			},
			/** function set color in table **/
			getcolor:function(a){
				if(self.maindata){
					switch(a){
						case 'j':
							//console.log()('a')
							break;
							case 'k':
							//console.log()('b')
							break;
							case 'l':
							//console.log()('c')
							break;
						}
					}
				}
			}
		})


.controller('attrecordcontroller',function(attrecordfactory,$cookieStore,$scope){
	var self = this;
	self.handler = attrecordfactory;
	self.handler.getaccess()

	$scope.fileName = "report"
	$scope.exportData = self.handler.tmp_array

})

.run(function(attrecordfactory){
	attrecordfactory.setheaders();
})

}())
