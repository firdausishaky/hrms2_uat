(function(){
	
	angular.module("app.att.changeShift",["ngFileUpload"])
	
	.factory('changeShiftfactory',function(Upload,$filter,$modal,$http,logger,ApiURL,$cookieStore){
		var self;
		return self = {
			getaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/change-swap-shift/default-change-shift?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					console.log(res)
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
						self.addDisabled =  true;
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data);
						//self.empx =
						self.addDisabled =  true;
						logger.logSuccess('Access Granted');
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
				});
			},callback:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/change-swap-shift/default-change-shift?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setdata(res.data.data);

				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},setdata:function(res){
				//console.log(res)
				self.form = {};
				if(res.requestType === 'Change Shift'){
					self.form.requestType = 'Change Shift';
				}else{
					self.form.requestType = 'Swap Shift / Day Off';
				}
				console.log(res);
				if(res.role.role  == "User"){
					self.hides = false
					self.form.name  = res.name;
					$cookieStore.put('userChangeShift',res.name)
					//console.log(self.hides)
				}else{
					self.hides = true
					//self.form.name  = res.name;
					$cookieStore.put('userChangeShift',res.name)
					//console.log(self.form)
				}
			},find:function(){
				self.form.name = self.nameId;
				self.form.nameSwap = self.swapId;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/change-swap-shift/swap_value?key=' + $cookieStore.get('key_api'),
					data :self.form,
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					self.form.name = self.name;
					self.form.nameSwap = self.nameSwap;
					self.setnorequest(res.data.data[0])
					logger.logSuccess(res.data.header.message);
				},function(res){
					//self.form.name = self.model;
					self.form.name = self.nameId;
					self.form.nameSwap = self.nameSwap;
					logger.logError(res.data.header.message);
				});
			},setnorequest:function(a){
				self.form.NoRequestForTheMonth = a.NoRequestForTheMonth;
				self.form.NoWithForTheMonth = a.NoWithForTheMonth;
			},
			
			/** 
				=============================
				ALL FUNCTION FOR CHANGE SHIFT
				=============================
				**/
				addChange: function(form){
					var a = {};
					a.date = '';
					a.original = '';
					a.new = '';
					form.subChange.push(a);
				},removesubChange: function(form,a){
					form.subChange.splice(a,1);
				},onSelect : function ($item,$model,$label){
					console.log($item)
					self.subordinate = $item.employee_id;
					self.model = $item.name;
					self.nameId = $item.employee_id;
					self.name = $item.name;
					this.nameid = $item.employee_id;
				},getLocation : function(val){
					return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name +'?&key=' + $cookieStore.get('key_api'),{
						params: {
							address : val,
						}
					}).then(function(response){
						if(response.data.data === null){
							logger.logError(response.data.header.message);
							var a = {};
							a.result = [];
							return a.result.map(function(item){
								return item;
							});
						}else if(response.data != null){
							console.log(response.data)
							var a = {};
							a.result = response.data;
							return a.result.map(function(item){
								return item;
							});
						}  
					});
				},getchange : function(form){
					console.log(self.form,form)
					self.form.date = form.subChange[form.subChange.length-1].date;
					self.form.name = self.nameId;

					if(self.form.name ==  null){
						self.form.name = $cookieStore.get('NotUser');
					}
					console.log(self.form.name )
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/attendance/change-swap-shift/work-hour?key=' + $cookieStore.get('key_api'),
						data : self.form,
						headers: {
							"Content-Type": "application/json"
						} 
					}).then(function(res){
						self.setdatasub(res.data.data,form)
						self.form.name = self.name || $cookieStore.get('userChangeShift');
						logger.logSuccess(res.data.header.message);
					},function(res){
						self.form.name = self.name || $cookieStore.get('userChangeShift');
						logger.logError(res.data.header.message);
					});
				},setdatasub:function(a,form){
					form.subChange[form.subChange.length-1].stores =a.new;
					form.subChange[form.subChange.length-1].original = a.original;
				},setdatachange:function(form,self){
					var data = form.subChange;
					for(b in data){delete data[b].stores}
						self.subChange = form.subChange;
					self.name =this.nameid;
					if(self.name ==  null){
						self.name = $cookieStore.get('NotUser'); } return self;
					},

			/** 
				=============================================
				FUNCTION FOR SAVE REQUEST CHANGE / SWAP SHIFT
				=============================================
				**/
				save: function(form,file){
					if(self.form.requestType == 'Change Shift'){
						self.setdatachange(form,self.form)
					}else{
						self.setdataswap(form,self.form)
					}
					delete self.form.date
					if(file){
						file.upload = Upload.upload({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/change-swap-shift/request?key=' + $cookieStore.get('key_api'),
							data :{data:self.form},
							file: file,
							headers: {
								"Content-Type": "application/json"
							}
						}).then(function(res){
							self.callback();
							form.subChange=[];
							form.subSwap=[];
							self.files=false;
							logger.logSuccess(res.data.header.message);
						},function(res){
							self.callback()
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
						});
					}else{
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/change-swap-shift/request?key=' + $cookieStore.get('key_api'),
							data : self.form,
							headers: {
								"Content-Type": "application/json"
							}   
						}).then(function(res){
							self.callback()
							form.subChange=[];
							form.subSwap=[];
							logger.logSuccess(res.data.header.message);
						},function(res){
							self.callback()
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
						});
					}
				},
				

			/** 
				=======================
				ALL FUNCTION SWAP SHIFT
				=======================
				**/
				onSwap : function ($item,$model,$label){

					self.form.arg1 = $cookieStore.get('NotUser'); 
					self.form.arg2 = $item.employee_id;
					self.model = $item.name;
					self.swapId = $item.employee_id;
					self.nameSwap = $item.name;
					this.swapid = $item.employee_id;
				//self.form.name=$cookieStore.get('userChangeShift')
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/change-swap-shift/swap_value?key=' + $cookieStore.get('key_api'),
					data : self.form,
					headers: {
						"Content-Type": "application/json"
					}  
				}).then(function(res){
					console.log(res)
					self.addDisabled = res.data.data.result
						//self.callback()
						//form.subChange=[];
						//form.subSwap=[];
						logger.logSuccess(res.data.header.message);
					},function(res){
						self.addDisabled = res.data.data.result
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
					});
				/** find no request **/
				//self.find()
			},getSwap : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.nameSwap +'?&key=' + $cookieStore.get('key_api'),{
					params: {
						address : val,
					}
				}).then(function(response){
					//return console.log(response,"10000000");
					if(response.data == null){
						var res = 'Error employee not found';
						if(response.data.header.message){
							res = response.data.header.message;
						}
						logger.logError(res);
						var a = {};
						a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else{
						var a = {};
						console.log(response.data,"999999999999999999999999999999999999999999999999999999")
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},addSwap: function(swap){
				var a = {};
				a.date = '';
				a.original = '';
				a.new = '';

				swap.subSwap.push(a);
			},removesubSwap: function(swap,a){
				swap.subSwap.splice(a,1);
			},setdataswap:function(form,self){
				self.subSwap = form.subSwap;
				self.name = this.nameid;
				self.nameSwap = this.swapid;
				//console.log(self.name)
				return self;
			},updateSwap : function(form){
				console.log(self.form);
				self.form.date = form.subSwap[form.subSwap.length-1].date;
				self.form.name = self.nameId;
				self.form.nameSwap = self.swapId;

				if(self.form.name ==  null){
					self.form.name = $cookieStore.get('NotUser');
				}
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/change-swap-shift/work-hour?key=' + $cookieStore.get('key_api'),
					data : self.form,
					headers: {
						"Content-Type": "application/json"
					} 
				}).then(function(res){
					var a = res.data.data;
					form.subSwap[form.subSwap.length-1].original = a.original;
					form.subSwap[form.subSwap.length-1].new = a.new;
					self.form.name = self.name || $cookieStore.get('userChangeShift');
					self.form.nameSwap = self.nameSwap;
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.name = self.name || $cookieStore.get('userChangeShift');
					self.form.nameSwap = self.nameSwap;
					logger.logError(res.data.header.message);
				});
			},
			
			/** 
				======================
				FUNCTION CANCEL RQUEST
				======================
				**/
				cancel : function(){
					self.callback()
				}

			}
		})


.controller('changeShiftcontroller',function(changeShiftfactory,ApiURL,$cookieStore,$scope,$http,logger,Upload){
	var self = this;
	self.handler = changeShiftfactory;
	self.handler.getaccess();
	self.form = {};
	self.form.subChange = [];
	self.form.subSwap = [];
})


}())

