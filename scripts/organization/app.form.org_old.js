var appFormOrg = angular.module("app.form.org", ['ui.mask','ngFileUpload','ui.bootstrap.contextMenu']);


	/** General Information Ctrl **/
	appFormOrg.factory('permissionGi', function() {
		var self;
			return self = {
				data:function(a){
					self.access = a;
					//console.log(a)
				}
			}
	});
	appFormOrg.controller("generalCtrl", function(logger,$cookieStore,ApiURL,$scope,$routeParams,$http,Upload,$window,$rootScope,$timeout,GI,permissionGi) {
			
			$http.get(ApiURL.url + '/api/organization/general-information?key=' + $cookieStore.get('key_api') ).success(function(res){	
				if(res.header.message == 'Unauthorized'){
					permissionGi.data(res.header.access);
					logger.logError('Access Unauthorized')
				}else{
					$scope.genInfo = res.data;
					permissionGi.data(res.header.access);
					if($scope.genInfo.filename == null){
						$scope.genInfo.filename = "Not Defined";
					}	
					$scope.files=[];
					var g = new File([""], $scope.genInfo.filename);
					$scope.files.push(g);
					if($scope.genInfo.filename == "Not Defined"){
						var logo = 'images/no-logo.png';
						GI.setData(logo);
					}else{
						var logo = $scope.genInfo.filename;
						GI.setData(ApiURL.url + '/get-images/company/' + logo);
					}
					logger.logSuccess(res.header.message)
				}			
			}).error(function(data){
					data.header ? logger.logError(data.header.message) : logger.logError("Show Data Failed")
			});	
			$scope.edit = function(){
				if($scope.genInfo.filename === "Not Defined"){
					$scope.bfile = false;
					$scope.open =true;
					$scope.genInfo.radio = 1;
				}else{
					$scope.open = true;
					$scope.genInfo.radio = 1;
				}
			};
			$scope.replaceFile = function(){
				$scope.bfile = true;
			};
			$scope.hidebutton = function(a){
				var data = permissionGi.access;
				if(data){
					switch(a){
						case 'create':
							if(data.create == 1){return true}
						break;
						case 'delete':
							if(data.delete == 1){return true}
						break;
					}
				}
			};
			function getData(){	
					$http.get(ApiURL.url + '/api/organization/general-information?key=' + $cookieStore.get('key_api') ).success(function(res){
						if(res.header.message == 'Unauthorized'){
							logger.logError(res.header.message)
						}else{
							$scope.genInfo = res.data;
							if($scope.genInfo.filename == null){
								$scope.genInfo.filename = "Not Defined";
							}	
							$scope.files=[];
							var g = new File([""], $scope.genInfo.filename);
							$scope.files.push(g);
							if($scope.genInfo.filename == "Not Defined"){
								var logo = 'images/no-logo.png';
								GI.setData(logo);
							}else{
								var logo = $scope.genInfo.filename;
								GI.setData(ApiURL.url + '/get-images/company/' + logo);
							}
						}	
				});			
			}
			function send_upload(file,id){
				$scope.genInfo = JSON.stringify($scope.genInfo);
				file.upload = Upload.upload({
				  url : ApiURL.url + '/api/organization/general-information/update/' + id + '?key=' + $cookieStore.get('key_api'), 
				  method: 'POST',
				  headers: {'Content-Type' : 'application/form-data',},
				  data : JSON.parse($scope.genInfo,$scope.genInfo.country),
				  file: file
				}).success(function(data){
						$scope.genInfo = getData();
						$scope.open = false;
						$scope.button=false;
						$scope.bfile=false;
						logger.logSuccess(data.header.message);	
				}).error(function(data) {
						$scope.genInfo = getData();
						data.header ? logger.logError(data.header.message) : logger.logError("Save Failed")
				});	
			}	
			function send_string(file,id){
				$scope.genInfo = JSON.stringify($scope.genInfo);	
				file.upload = Upload.upload({
				  url : ApiURL.url + '/api/organization/general-information/update/' + id + '?key=' + $cookieStore.get('key_api'), 
				  method: 'POST',
				  headers: {'Content-Type' : 'application/form-data',},
				  data : JSON.parse($scope.genInfo,$scope.genInfo.country)
				}).success(function(data) {
						$scope.open = false;
						$scope.genInfo = getData();
						$scope.button=false;
						logger.logSuccess(data.header.message);	
					}).error(function(data) {
						$scope.genInfo = getData();
						data.header ? logger.logError(data.header.message) : logger.logError("Save Failed")
					});
			}
			
			function getID(scope,scope_form){
					var a = [];
					for(a in scope) {
						if(scope[a].title == scope_form) {
								scope_form = scope[a].id;
						};
					};
					return scope_form
			}
			$scope.save = function(file,id){
				$scope.genInfo.country = getID($scope.jobs,$scope.genInfo.country);
				if($scope.files){
					if($scope.files[0].name == $scope.genInfo.filename){
						send_string(file,id);
						//console.log('string')
					}else{
						send_upload(file,id);
						//console.log('upload')
					}
				}else{
					send_string(file,id);
				}		
			};	
			$scope.test = function(){
				var logo = ApiURL.url +  '/api/organization/general-information/get_file/download?key=' + $cookieStore.get('key_api');
				$window.open(logo);
			};	
	});

	
	
	
	/** permission sctructure **/
	appFormOrg.factory('permissionstructure', function() {
		var self;
			return self = {
				data:function(a){
					self.access = a;
				}
			}
	});
	
	appFormOrg.controller("structureCtrl", function(logger,$cookieStore,ApiURL,$scope,$routeParams,$http,$modal,filterFilter,permissionstructure){	
		
		/** get all data **/
		$scope.list = [];
		$http({
			method : 'GET',
			url : ApiURL.url +'/api/organization/structure?key=' + $cookieStore.get('key_api')
		}).then(function(res){
			var list = res.data.data.organization;
			permissionstructure.data(res.data.header.access);
			$scope.list.push(list);
			logger.logSuccess(res.data.header.message);
		},function(res){
			res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Organization Structure Failed")
		});
		
		/** hide button **/
		$scope.button = function(a){
			var data = permissionstructure.access;
			if(data){
				switch(a){
					case 'create':
						if(data.create == 1){return true}
					break;
					case 'delete':
						if(data.delete == 1){return true}
					break;
					case 'update':
						if(data.update == 1){return true}
					break;
				}
			}
		};
		
		

		/** modal handler **/
		$scope.open = function(size,item) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalStructure.html",
				controller: "structureAddCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return item;
					}
				}
			});	
			modal.result.then(function (newjob) {
				if (newjob.parent_id == 1){		
						if($scope.list[0].sub == null){
							$scope.list[0].sub = {};
							$scope.list[0].sub[newjob.sub_unit] = newjob;				
						}else{
							$scope.list[0].sub[newjob.sub_unit] = newjob;				
						}
				}else{
						var s = [];
						for(s in $scope.list[0].sub){
							if($scope.list[0].sub[s].id == newjob.parent_id){
								$scope.list[0].sub[s].sub.push(newjob)
							}
						}
				}
				
			});
			
		};
		$scope.edit = function(size,item) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditStructure.html",
				controller: "editCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return item;
					}
				}
			});
		};
		$scope.openDelete = function(size,item) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalDelete.html",
				controller: "deleteStrucCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return item;
					}
				}
			});
			modal.result.then(function (newjob) {
				if(newjob.parent_id == 1 ){
					var s = [];
					for(s in $scope.list[0].sub){
							if($scope.list[0].sub[s].id == newjob.id){
								delete $scope.list[0].sub[s];
							}
							
					};
				}else{
					for(s in $scope.list[0].sub){								
						if($scope.list[0].sub[s].id == newjob.parent_id){
							for (a in $scope.list[0].sub[s].sub){
								if($scope.list[0].sub[s].sub[a].id == newjob.id){
									$scope.list[0].sub[s].sub.splice(a,1);	
								}
							}
						}																
					}				
				}					
			});

			
		};
	
	});
	
	appFormOrg.controller("structureAddCtrl", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$location) {
		
		if(items){
		}
		$scope.parent_id = items.id;
		$scope.sub = items;	
		
		/** function get select department **/		
		$http.get(ApiURL.url+'/api/organization/department?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.jobs = res.data;
		});
		
		/** function save **/
		$scope.save = function(){
			$http({
				method 			: 'POST',
				url  			: ApiURL.url+'/api/organization/structure?key=' + $cookieStore.get('key_api'),
				headers 		: { 'Content-Type' : 'application/x-www-form-urlencoded', },
				transformRequest: function(obj) {
								  var str = [];
								  for(var p in obj)
								  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
								  return str.join("&");		
								  },
				data 			: {sub_unit:$scope.sub_unit,descript:$scope.descript,parent_id:$scope.parent_id}
			}).success(function(data) {
				var temp = {};
					temp.descript = $scope.descript;
					temp.parent_id = $scope.parent_id;
					temp.id = data.data;
					/** data departement name **/
					var a, b, c = $scope.sub_unit;
					for(var i = 0; i < $scope.jobs.length; i++) {
						a = $scope.jobs[i];
						if(a.id == c) {
							b = a.name;
						};
					};
					temp.sub_unit = b;						
				logger.logSuccess(data.header.message);
				$modalInstance.close(temp);
			}).error(function(data) {
				$modalInstance.close('dismiss');
				logger.logError(data.header.message);
			});
		};
		
		/** close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}
	});

	appFormOrg.controller("editCtrl",function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,struc) {	
		var a = String(items.sub_unit);
		var b = String(items.descript);
		if(items){
			$scope.formData = items;
		}				
		/** function get select department **/
		$http.get(ApiURL.url+'/api/organization/department?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.jobs = res.data;
		});
				
		/** function save **/		
		$scope.save = function(){
			$http({
				method : 'POST',
				url : ApiURL.url+'/api/organization/structure/' + items.id +'?key=' + $cookieStore.get('key_api'),
				headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
				data : $.param($scope.formData)
			}).success(function(data) {
					var temp = {}; 
						temp = $scope.formData;
						/** data departement name **/
						var a, b, c = $scope.formData.sub_unit;
						for(var i = 0; i < $scope.jobs.length; i++) {
							a = $scope.jobs[i];
							if(a.id == c) {
								b = a.name;
							};
						};
						temp.sub_unit = b;	
				logger.logSuccess(data.header.message);
				$modalInstance.close(temp);	
			}).error(function(data){
				$modalInstance.close('dismiss');
				logger.logError(data.header.message);
			});
		};		
		$scope.cancel = function() {
			$scope.formData.sub_unit = a;
			$scope.formData.descript = b;
			$modalInstance.dismiss("cancel")
		}	
	});
	appFormOrg.controller("deleteStrucCtrl",function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL) {
						
		if(items){
			$scope.formData = items;
		}
		$scope.sub = items;	
		$scope.remove = function(){
			$http({
				method : 'DELETE',
				url : ApiURL.url+'/api/organization/structure/' + items.id +'?key=' + $cookieStore.get('key_api'),
			}).success(function(data){
				var temp = {};
					temp = $scope.sub;
				$modalInstance.close(temp);
				logger.logSuccess(data.header.message);
			}).error(function(data){
				$modalInstance.close('dismiss');
				logger.logError(data.header.message);
			});
		};					
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}	
});



