(function(){
			
	angular.module('app.org.depart',['checklist-model'])

	.factory('departmentfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Department Name','name',100]
				);
				this.setactiveheader(this.headers[0],false)
			},getdatadepartment:function(){
				$http({
					url : ApiURL.url +  '/api/organization/department?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data)
						logger.logSuccess(res.data.header.message);
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Department Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/organization/department?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalDepartment',
					controller: 'modalDepartment',
					controllerAs: 'modalDepartment',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalEditDepart',
					controller: 'modalEditDepart',
					controllerAs: 'modalEditDepart',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
								$http({
									method : 'POST',
									url : ApiURL.url +  '/api/organization/department?key=' + $cookieStore.get('key_api'),
									data : data,
								}).then(function(res){
									var temp=data;
										temp.id=res.data.data
										self.adddata(temp)
									//self.callback()
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Department Failed")
								});
							break;
						case 'edit':
								$http({
									method : 'POST',
									url : ApiURL.url +  '/api/organization/department/' + b.id + '?key=' + $cookieStore.get('key_api'),
									data : data.b
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError(" Updating Department Failed ")
								});			
							break;
					}
					
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/organization/department/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Delete Department Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError(" Delete Department Failed ")
				})
			}
		}
	})

	.controller('departmentCtrl',function(departmentfactory){
		var self = this;
		self.handler = departmentfactory;
		self.handler.getdatadepartment()
	})
	.controller('modalDepartment',function(departmentfactory,$modalInstance){	
		var self = this;
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})
	.controller('modalEditDepart',function(departmentfactory,$modalInstance,$timeout,data,time){	
		var self = this;
			self.handler = departmentfactory;
			self.handler.button();
			self.form = data;
			self.form = angular.copy(data);
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})
	.run(function(departmentfactory){
		departmentfactory.setheaders();
	})

}())
