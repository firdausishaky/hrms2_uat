var appDashboard = angular.module("app.dashboard", []);

appDashboard.controller("dashboardCtrl", function($scope,$cookieStore,$location,GI,ApiURL,GetName) {
		
		/** set data logo **/
		GI.setData(ApiURL.url + '/get-images/company/' + $scope.test);
		
		/** set name **/
		$scope.onlyUser = $cookieStore.get('UserAccess');
		$scope.notUser = $cookieStore.get('NotUser');
		
		$scope.open = function(){
			if($scope.notUser==null){
				$location.path('/employee/personalDetail/' + $scope.onlyUser.id)	
			}else{
				$location.path('/employee/personalDetail/' + $scope.notUser)	
			}
		};
});
