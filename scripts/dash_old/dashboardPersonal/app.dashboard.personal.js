var appPersonalDashboard = angular.module('app.dashboard.personal',["ui.mask","countrySelect","ngFileUpload","checklist-model"])


appPersonalDashboard.factory('personalDashboardFactory',function(){
	var self;
	return self = {
		data:function(a){
			self.access = a;
		},

		// coba: function(){
		// 	console.log('test')
		// }
	}
});


appPersonalDashboard.controller("personalCtrlDashboard",function($scope,$http,ApiURL,logger,$cookieStore,filterFilter,$modal,$routeParams,personalDashboardFactory,$location) {
	// var init;
	// var self = this;
	// self.handler = personalDashboardFactory

	// const onlyUser = $cookieStore.get('UserAccess')
	// const notUser = $cookieStore.get('NotUser')
	const onlyUser = $cookieStore.get('UserAccess')
	const notUser = $cookieStore.get('NotUser')
	function getID(){
		if (notUser == null) {
			return(onlyUser.id)	
		}else{
			return(notUser)
		}
	}
	$scope.id = getID()

	$http.get(ApiURL.url + '/api/employee-details/personal/' + $scope.id + '?key=' + $cookieStore.get('key_api')).success(function(res){
		// console.log(res)
		$scope.perInfo = res.data;
		personalDashboardFactory.data(res.header.access);
		/** function get image profile **/
		$scope.name = $scope.perInfo.first_name +" "+ $scope.perInfo.last_name;
		$scope.localIT =  res.data.local_it;
		if (res.pictures == null ){
			$scope.pic = "images/user2.jpg";
		}else{
			$scope.pic = ApiURL.url + "/images/" + $scope.id + '/' + res.pictures;
			console.log($scope.pic)	
		}
		if($scope.perInfo.date_of_birth === '0000-00-00'){
			$scope.perInfo.date_of_birth = '';
		}
		$scope.$watch('$scope.perInfo',function(){
			if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Single'){
				$scope.maiden = false;
			}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Married'){
				$scope.maiden = true;
			}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Separated'){
				$scope.maiden = true;
			}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Widow'){
				$scope.maiden = true;
			}
			if($scope.perInfo.gender == 'male'){
				$scope.maiden = false;
			}
		});
		/** get select nationalities **/	
		$http.get(ApiURL.url+'/api/nationalities/form-nationalities?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.jobs = res.data;
		});
	}).success(function(data){
		logger.logSuccess(data.header.message);
	}).error(function(data){
		data.header ? logger.logError(data.header.message) : logger.logError(" Show Personal Details Failed")
	});

	/** function hidden button **/
	$scope.hide = function(a){
		var data = personalDashboardFactory.access;
		if(data){
			switch(a){
				case 'create':
				if(data.create == 1){return true}
					break;
				case 'read':
				if(data.read == 1){return true}
					break;
			}
		}
	};

	/** function get maiden name **/
	$scope.showMaiden = function(){
		if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Single'){
			$scope.maiden = false;
		}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Married'){
			$scope.maiden = true;
		}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Separated'){
			$scope.maiden = true;
		}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Widow'){
			$scope.maiden = true;
		}
	};
	$scope.selectMale  = function(){
		if($scope.perInfo.gender == 'male'){
			$scope.maiden = false;
			$scope.perInfo.marital_status = '';
		}
	};

	/** function save **/
	$scope.save = function(){
		for(a in $scope.jobs) {
			if($scope.jobs[a].title === $scope.perInfo.nationality) {
				$scope.perInfo.nationality = $scope.jobs[a].id;
			};
		};
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/personal/' +  $scope.id + '?key=' + $cookieStore.get('key_api'), 
			data :$scope.perInfo 
		}).success(function(data){
			console.log(data);
			$scope.getdata()	
			logger.logSuccess(data.header.message);
			$scope.button=false;
		}).error(function(data){
			data.header ? logger.logError(data.header.message) : logger.logError(" Saving Personal Details Failed")
		});
	};

	/** function get employee **/				
	$scope.onSelect = function($item,$model,$label) {
		$location.path('/employee/personalDetail/' + $item.employee_id)
	};
	$scope.getLocation = function(val) {
		return $http.get(ApiURL.url + '/api/module/employee-info-search?search=' +$scope.employee + '&key=' + $cookieStore.get('key_api'), {
			params: {
				address : val,
			}
		}).then(function(response){
			var a = {};
			a.result = response.data;
			return a.result.map(function(item){
				return item;
			});
		});
	};

	/** function refresh data**/
	$scope.getdata = function(){
		$http.get(ApiURL.url + '/api/employee-details/personal/' + $scope.id + '?key=' + $cookieStore.get('key_api')).success(function(res){
			console.log(res);
			$scope.perInfo = res.data;
			personalDashboardFactory.data(res.header.access);
			$scope.name = $scope.perInfo.first_name +" "+ $scope.perInfo.last_name;
			if (res.pictures == null ){
				$scope.pic = "images/user2.jpg";
			}else{
				$scope.pic = ApiURL.url + "/images/" + $scope.id + '/' + res.pictures;
			}
			if($scope.perInfo.date_of_birth === '0000-00-00'){
				$scope.perInfo.date_of_birth = '';
			}
			$scope.$watch('$scope.perInfo',function(){
				if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Single'){
					$scope.maiden = false;
				}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Married'){
					$scope.maiden = true;
				}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Separated'){
					$scope.maiden = true;
				}else if($scope.perInfo.gender == 'female' && $scope.perInfo.marital_status == 'Widow'){
					$scope.maiden = true;
				}
				if($scope.perInfo.gender == 'male'){
					$scope.maiden = false;
				}
			});
			$http.get(ApiURL.url+'/api/nationalities/form-nationalities?key=' + $cookieStore.get('key_api') ).success(function(res){
				$scope.jobs = res.data;
			});
		});
	};

	/** hide searching for user **/
	$scope.useracess=function(a){
		var access=$cookieStore.get('UserAccess');
		if(access){
			switch(a){
				case 'user':
				if(access.name == 'user'){return true}
					break;
			}
		}
	};


});

appPersonalDashboard.controller("attachCtrlDashboard",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,$window,personalDashboardFactory) {

	/** employee id **/		
	// $scope.id = $cookieStore.get('NotUser')
	const onlyUser = $cookieStore.get('UserAccess')
	const notUser = $cookieStore.get('NotUser')
	function getID(){
		if (notUser == null) {
			return(onlyUser.id)	
		}else{
			return(notUser)
		}
	}
	$scope.id = getID()


	$http.get(ApiURL.url + '/api/employee-details/personal/attach/' + $scope.id + '?key=' + $cookieStore.get('key_api') ).success(function(res){
		$scope.study=res.data;
		$scope.check = {
			study: []
		}
		/** function get file **/
		$scope.test = function(a){
			var e = ApiURL.url +  '/api/employee-details/personal/attach/'  + $scope.id +  '/' + a.target.innerText + '?key=' + $cookieStore.get('key_api');
			$window.open(e);
		};
	}).success(function(data){
		logger.logSuccess(data.header.message);
	}).error(function(data){
			//logger.logError(data.header.message);
		});

	/** function hidden button **/
	$scope.hide = function(a){
		var data = personalDashboardFactory.access;
		if(data){
			switch(a){
				case 'create':
				if(data.create == 1){return true}
					break;
				case 'delete':
				if(data.delete == 1){return true}
					break;
				case 'read':
				if(data.read == 1){return true}
					break;
			}
		}
	};


	/** modal handler **/
	$scope.open = function(size) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalAttac.html",
			controller: "modalAttacCtrl",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return;
				}
			}
		});
		modal.result.then(function(newjob){
			if($scope.study){
				$scope.study.push(newjob);
			}else{
				$scope.study = [];
				$scope.study.push(newjob);
			}
		});
	};
	$scope.edit = function(size,student) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalEditAttac.html",
			controller: "AttacEditCtrl",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return student;
				}
			}
		});
		modal.result.then(function(newjob){
			for(a in $scope.study){
				if($scope.study[a].id==newjob.id){
					$scope.study[a]=newjob;
				}
			}
		});
	};

	/** function delete **/
	$scope.remove = function(id){
		$http({
			method : 'DELETE',
			url : ApiURL.url + '/api/employee-details/personal/attach/' + id + '?key=' + $cookieStore.get('key_api')
		}).success(function(data){
			var i;
			for( i = 0; i < id.length; i++) {
				$scope.study = filterFilter($scope.study, function (student) {
					return student.id != id[i];
				});
			};
			$scope.check.study = [];
			logger.logSuccess(data.header.message);
		}).error(function(data){
			data.header ? logger.logError(data.header.message) : logger.logError(" Delete Attachments Failed")
		});
	};

});


appPersonalDashboard.controller("modalAttacCtrlDashboard", function(filterFilter,$http,$scope,$modalInstance,items,logger,ApiURL,$cookieStore,Upload,$routeParams) {

	/** employee id **/		
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()

		
		/** copy data **/
		if(items){
			$scope.perInfo=items;
		}	
		
		$scope.saveButton = false;
		/** function save **/
		$scope.save = function(file){
			if(file){
				/** function upload **/
				file.upload = Upload.upload({
					method : 'POST',
					url : ApiURL.url +  '/api/employee-details/personal/attach/store1/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
					data :$scope.form,
					file: file
				}).then(function(res){
					console.log(res.data.data)
					$modalInstance.close(res.data.data); 
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
				});
			}else{
				$http({
					method : 'POST',
					url : ApiURL.url +  '/api/employee-details/personal/attach/store1/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
					data :$scope.form     
				}).then(function(res){
					console.log(res.data.data)
					$modalInstance.close(res.data.data);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
				});
			}
		};

		/** function close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}

	});


appFormPer.controller("AttacEditCtrlDashboard",function($scope,$modalInstance,items,logger,$cookieStore,$http,ApiURL,Upload,$routeParams,personalDashboardFactory){

	/** employee id **/			
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** function copy data **/
		if(items){
			$scope.form=items;
			$scope.form=angular.copy(items);
		}
		
		/** function change name file **/
		$scope.form.files=[];
		var g = new File([""], items.filename);
		var count = g.length;
		if(count > 30)
		{
			var minus = count - 1;
			var startMinus = minus - 10;
			var subG = g.substring(0,9)+'...'+g.substring(startMinus,minus);	
		}else{
			var subG = g
		}
		$scope.form.files.push(subG);
		

		/** function save **/		
		$scope.save = function(file){
			console.log(file)
			if(file){
				/** function upload **/
				file.upload = Upload.upload({
					method : 'POST',
					url : ApiURL.url +  '/api/employee-details/personal/attach/update/' + items.id + '?key=' + $cookieStore.get('key_api'),
					data :$scope.form,
					file: file
				}).then(function(res){
					console.log(res.data.data)
					$modalInstance.close(res.data.data); 
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
				});
			}else{				
				$http({
					method : 'POST',
					url : ApiURL.url +  '/api/employee-details/personal/attach/update/' + items.id + '?key=' + $cookieStore.get('key_api'),
					data :$scope.form     
				}).then(function(res){
					console.log(res.data.data)
					$modalInstance.close(res.data.data);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
				});
			}
		};
		
		
		/** function hide button **/
		$scope.hide = function(a){
			var access  = personalDashboardFactory.access;				
			if(access){
				switch(a){
					case 'update':
					if(access.update == 1){
						return true
					}
					break;
				}
			}
		};


		/** function close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss();
		}


	});
