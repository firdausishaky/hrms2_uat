var appTask = angular.module("app.task", []);

appTask.factory("taskStorage", function() {
    var DEMO_TASKS = "[]"; 
	var STORAGE_ID = "tasks";
	var factory = {};
	
	factory.get = function(){
		return JSON.parse(localStorage.getItem(STORAGE_ID) || DEMO_TASKS)
	};
	
	factory.put = function(tasks){
		localStorage.setItem(STORAGE_ID, JSON.stringify(tasks));
	};
	
	return factory;
});

appTask.directive("taskFocus", ["$timeout", function($timeout) {
    return {
        link: function(scope, ele, attrs) {
            return scope.$watch(attrs.taskFocus, function(newVal) {
                return newVal ? $timeout(function() {
                    return ele[0].focus()
                }, 0, !1) : void 0
            })
        }
    }
}]);

appTask.controller("taskCtrl", ["$scope", "taskStorage", "filterFilter", "$rootScope", "logger", function($scope, taskStorage, filterFilter, $rootScope, logger) {
    var tasks;
	tasks = $scope.tasks = taskStorage.get();
	$scope.newTask = "";
	$scope.remainingCount = filterFilter(tasks, {completed: false}).length;
	$scope.editedTask = null; 
    $scope.statusFilter = {completed: false};
	$scope.filter = function(filter){
        switch (filter) {
            case "all":
                return $scope.statusFilter = "";
            case "active":
                return $scope.statusFilter = {completed: false};
            case "completed":
                return $scope.statusFilter = {completed: true};
        }
    }; 
	$scope.add = function() {
        var newTask;
        newTask = $scope.newTask.trim();
		if(newTask.length !== 0){
			tasks.push({
				title: newTask,
                completed: false
            });
			logger.logSuccess('New task: "' + newTask + '" added');
			taskStorage.put(tasks);
			$scope.newTask = "";
			$scope.remainingCount++;
		}
	};	
	$scope.edit = function(task) {
        return $scope.editedTask = task
    }; 	
	$scope.doneEditing = function(task) {
		$scope.editedTask = null; 
		task.title = task.title.trim();
		task.title ? logger.log("Task updated") : $scope.remove(task)
        taskStorage.put(tasks)
    };	
	$scope.remove = function(task) {
        var index;
        $scope.remainingCount -= task.completed ? 0 : 1;
		index = $scope.tasks.indexOf(task);
		$scope.tasks.splice(index, 1);
		taskStorage.put(tasks);
		logger.logError("Task removed");
    }
	 $scope.completed = function(task) {
        $scope.remainingCount += task.completed ? -1 : 1;
		taskStorage.put(tasks);
		task.completed ? $scope.remainingCount > 0 ? logger.log(1 === $scope.remainingCount ? "Almost there! Only " + $scope.remainingCount + " task left" : "Good job! Only " + $scope.remainingCount + " tasks left") : logger.logSuccess("Congrats! All done :)") : void 0
    }
	$scope.clearCompleted = function() {
        $scope.tasks = tasks = tasks.filter(function(val) {
            return !val.completed
        });
		taskStorage.put(tasks);
    }
	$scope.markAll = function(completed) {
        tasks.forEach(function(task) {
            task.completed = completed
        });
		$scope.remainingCount = completed ? 0 : tasks.length;
		taskStorage.put(tasks);
		completed ? logger.logSuccess("Congrats! All done :)") : void 0
    }
	$scope.$watch("remainingCount == 0", function(val) {
        $scope.allChecked = val
    })
	$scope.$watch("remainingCount", function(newVal) {
        $rootScope.$broadcast("taskRemaining:changed", newVal)
    }) 
}]);

