(function(){
	
	angular.module("app.salary.performance_bonus",["ngFileUpload"])
	
	.factory('performanceBonusFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.min =arguments[i][0];
				x.max =arguments[i][1];
				x.bonus_rate = arguments[i][2];
				x.width =arguments[i][3] ? arguments[i][3]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Min','Max','Bonus Rate (%)',33]
					);
				this.setactiveheader(this.headers[0],false)
			},getDataPerformance:function(){
				$http({
					url : ApiURL.url + '/payroll/performance_bonus_rate/get/0' ,
					method: 'GET'
				}).then(function(res){
					console.log(res)
					const status = res.status

					if (status !== 200) {
						logger.logError("View Data Failed");
					}else{

						self.setdata(res.data.data)
						logger.logSuccess("View Data Success");
					}
				},function(res){
					res.data.header ? logger.logError(res.data.message) : logger.logError("Show Incentive Allowance Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					// url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),

					
				}).then(function(res){
					self.setdata(res.data.data)
					console.log(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
				console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;

			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addModalPerformanceBonus',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditemployee',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				// console.log(b)
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						// console.log(data)
						$http({
							method : 'POST',
									// url : ApiURL.url + '/incentive_allowance_config/insert',
									url : ApiURL.url + '/payroll/performance_bonus_rate/insert',
									data : data,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									console.log(res)
									self.adddata(res.data.data[0])
									logger.logSuccess("Adding Performance Bonus Rate Success");
									self.getDataPerformance()
								},function(res){
									logger.logError("Adding Performance Bonus Rate Failed")
								});
								break;
								case 'edit':
								$http({
									method : 'POST',
									// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									url : ApiURL.url + '/payroll/performance_bonus_rate/update/' + b.id,
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.message);
									self.getDataPerformance()
								},function(res){
									logger.logError("Updating Allowance Failed")
								});			
								break;
							}
						});
			},
			deldata: function(id){
				$http({
					// url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
					url : ApiURL.url + '/payroll/performance_bonus_rate/delete/' + id,
					method: 'GET'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Success');
					self.getdataAllowance()
				},function(res){
					logger.logError("Deleting Failed")
				})
			}
		}
	})


.controller('performanceBonusCtrl',function(performanceBonusFactory,ApiURL,$cookieStore,$scope,$http,logger,Upload){
	var self = this;
	self.handler = performanceBonusFactory;
	self.handler.getDataPerformance()
})
.controller('addModalPerformanceBonus',function(performanceBonusFactory,$modalInstance,data){	
	// console.log("xxx")
	var self = this;
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.controller('modaleditemployee',function(performanceBonusFactory,$modalInstance,data){	
	var self = this;
	self.handler = performanceBonusFactory;
	self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(performanceBonusFactory){
	performanceBonusFactory.setheaders();
})

}())

