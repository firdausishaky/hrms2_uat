// app.salary.monthlyExpatPayroll


(function(){

	angular.module('app.salary.monthlyExpatPayroll',['checklist-model'])

	.factory('monthlyExpatPayrollFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(

					['Employee Name','employee_name'],
					['Department','department'],
					['Basic','basic'],
					['Ecola','ecola'],

					['Total','total'],
					['Government Reimbursment','government_reimbursment'],
					['SSS','sss'],
					['Philhealth','philhealth'],
					['HDMF','hdmf'],
					['without Tax','without_tax'],
					['Total','Total'],
					['Net Pay','netpay'],

					);
				this.setactiveheader(this.headers[0],false)
			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},
			onSelect : function ($item,$model,$label){

				self.model = $item.name;
				self.label = $item.employee_id;
				self.getIDEmployee(self.label)

			},

			searchDatas: function(a,b,c){
				console.log(a,b,c)
				// console.log(self.handler.employee_id)

				if (a == null || a==undefined) {
					a = 0
				}
				if (b == null || b==undefined) {
					b = 0
				}
				if (c == null || c==undefined) {
					logger.logError('Please select Cut Off Periode')
				}

				
				// console.log(self.form.employee_id)
				datas = {
					employee_id:a,
					department: b[0],
					cutoff_periode: c
				}
				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/monthly_expat_payroll/get',
					data : datas,
					headers: {
						"Content-Type": "application/json"
					}
				}).success(function(res){
					logger.logSuccess("Viewing Data Success");
					self.setdata(res.data)
						// console.log(res)
						
					}).error(function(res){
						logger.logError("Viewing Data Error");

					})
				},
				getDepartment:function(){
					$http({
						method : 'GET',
						url : ApiURL.url + '/cutoff_perpayroll/'
					}).success(function(res){
						self.setdataDepartment(res.data[0].department)
						self.cutoffDate(res.data[0].cutoff_periode)
						logger.logSuccess('Get Data Success!')
					}).error(function(res){
						// console.log(res)
						logger.logError('Get Data Failed!')
					})



					// then(function(res){

					// 	if (res.status !==200 ) {
					// 		logger.logError("Get Data Failed")
					// 	}else{
					// 		logger.logSuccess("Get Data Success")
					// 		self.setdataDepartment(res.data.data[0].department)
					// 		self.cutoffDate(res.data.data[0].payout)
					// 	}
					// });
				},
				cutoffDate:function(x){
					// consoe.log()
					for (var i = 0; i < x.length; i++) {
						x[i].payout_date = x[i].payout_date.split("T")
						x[i].payout_date = x[i].payout_date[0]
					}

					this.cutoff_date = x

				},


				setdataDepartment:function(x){
					this.setdataDepartment = x
				},

				getdataAllowance:function(){
					$http({
						url : ApiURL.url + '/payroll/incentive_allowance_config/get/0' ,
						method: 'GET'
					}).success(function(res){
						self.setdata(res.data)
						logger.logSuccess("View Data Success");
					}).error(function(res){
						logger.logError("Show Incentive Allowance Failed")
					})

					// then(function(res){
					// 	console.log(res)
					// 	const status = res.status

					// 	if (status !== 200) {
					// 		logger.logError("Access Unauthorized");
					// 	}else{

					// 		self.setdata(res.data.data)
					// 		logger.logSuccess("View Data Success");
					// 	}
					// },function(res){
					// 	res.data.header ? logger.logError(res.data.message) : logger.logError("Show Incentive Allowance Failed")
					// })
				},getpermission:function(active){
					if(active){
						this.mainaccess = active;
					}
				},button:function(a){
					if(this.mainaccess){
						switch(a){
							case 'create':
							if(this.mainaccess.create == 1){return true}
								break;
							case 'delete':
							if(this.mainaccess.delete == 1){return true}		
								break;
							case 'update':
							if(this.mainaccess.update == 1){return true}		
								break;
						}
					}
				},callback:function(){
					$http({
						method : 'GET',
					// url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
					console.log(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){

				this.maindata = a;
				console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
				
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodalemployee',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditemployee',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				console.log(b)
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						console.log(data)
						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
							data : data,
							headers : {	'Content-Type' :'application/json' }
						}).then(function(res){
							console.log(res)
							self.adddata(res.data.data[0])
							logger.logSuccess("Adding Incentive Allowance Success");
							self.getdataAllowance()
						},function(res){
							res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						});
						break;
						case 'edit':
						$http({
							method : 'POST',
									// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.message);
									self.getdataAllowance()
								},function(res){
									res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
								});			
								break;
							}
						});
			},
			deldata: function(id){
				$http({
					// url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
					url : ApiURL.url + '/payroll/incentive_allowance_config/delete/' + id,
					method: 'GET'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Success');
					self.getdataAllowance()
				},function(res){
					console.log(res.data.header)
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Failed")
				})
			},

			cutOffDatas: function(){
				y = this.maindata
				// x = y[0]
				// console.log(x[0])

				for(let i = 0; i < y.length; i++){

					newDatas = {
						employee_id : x.employee_id, 
						department_id : x.department_id,
						basic : x.basic,
						ecola: x.ecola,
						total_monthly_salary: x.total_monthly_salary,
						government_reimbursment: x.government_reimbursment,
						sss: x.sss,
						philhealth: x.philhealth,
						hdmf: x.hdmf,
						withholding_tax: x.withholding_tax,
						total_deductions: x.total_deductions,
						total_net_pay: x.total_net_pay,
						cut_off:  "2019-01-01"
					}
					$http({
						method : 'POST',
						url : ApiURL.url + '/payroll/monthly_expat_payroll/insert',
						data : newDatas,
						headers: {
							"Content-Type": "application/json"
						}
					}).success(function(res){
						logger.logSuccess("Saving Success")
					}).error(function(res){
						logger.logSuccess("Saving Success")

					})			
				}	
				
			},

		}
	})

.controller('monthlyExpatPayrollCtrl',function(monthlyExpatPayrollFactory,ApiURL,$cookieStore){
	console.log("xxxx")
	var self = this;
	self.handler = monthlyExpatPayrollFactory;
	// self.handler.getdataAllowance()
	self.handler.getDepartment()
})
.controller('addmodalemployee',function(monthlyExpatPayrollFactory,$modalInstance,data){	

	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.controller('modaleditemployee',function(monthlyExpatPayrollFactory,$modalInstance,data){	
	console.log("test")
	var self = this;
	self.handler = monthlyExpatPayrollFactory;
	self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(monthlyExpatPayrollFactory){
	monthlyExpatPayrollFactory.setheaders();
})

}())