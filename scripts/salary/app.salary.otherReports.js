(function(){
	
	angular.module("app.salary.otherReports",[])
	
	.factory('other_reportsFactory',function($interval,$filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,$route,adjustmentHistoryFactory){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Changed Date','changed_date'],
					['Name','name'],
					['Department','deparment'],
					['Salary Basic','salary_basic'],
					['Salary Ecola','salary_ecola'],
					['Deduction','deduction'],
					['Cut Off Period','cut_off_period'],
					['Remarks','remarks'],

					);
				this.setactiveheader(this.headers[0],false)	
			},getDataDropdown:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/payroll/adjustment_payroll?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					const status = res.status

					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						self.setDataDateCutOff(res.data.data[0].period)
						self.setDataDepartment(res.data.data[0].department)
						logger.logSuccess("View Data Success");
					}
				})
			},setDataDepartment:function(a){
				this.depart = a
			},
			
			setDataDateCutOff:function(a){
				this.dataCutOff = a
			},
			getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},saveData:function(a){
				const splitDate = a.cutoff_date.split('T')
				console.log(splitDate[0])
				datas = {
					employee_id: a.employee_id,
					cutoff_date: a.cutoff_date,
					adjust_salary_basic: a.adjust_salary_basic,
					adjust_salary_ecola: a.adjust_salary_ecola,
					deduction_contribute_sss: a.adjust_contribute_sss,
					deduction_contribute_philhealth: a.adjust_contribute_philhealth,
					deduction_contribute_hdmf: a.adjust_contribute_hdmf,
					deduction_loan_sss: a.adjust_loan_sss,
					deduction_loan_hdmf: a.adjust_loan_hdmf,
					deduction_loan_hmo: a.adjust_loan_hmo,
					remarks_basic: a.remarks_basic,
					remarks_contribute_hdmf: a.remarks_contribute_hdmf,
					remarks_contribute_philhealth: a.remarks_contribute_philhealth,
					remarks_contribute_sss: a.remarks_contribute_sss,
					remarks_ecola: a.remarks_ecola,
					remarks_loan_hdmf: a.remarks_loan_hdmf,
					remarks_loan_hmo: a.remarks_loan_hmo,
					remarks_loan_sss: a.remarks_loan_sss
				}


				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/adjustment_payroll/insert',
					data : datas,
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					const status = res.status
					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						logger.logSuccess("View Data Success");
						self.searchData(self.form)
						adjustmentHistoryFactory.getdataHistory()

					}

				})
			},
			searchData:function(datas){

				if(datas == null || datas == undefined){
					return logger.logError("Please Input form First")

				}
				if (datas.employee_id == null || datas.employee_id == undefined) {
					return logger.logError("Please Input Name First")
				}
				if (datas.typeReport == null || datas.typeReport== undefined) {
					return logger.logError("Please Input Type Report First")
				}
				if (datas.name_signatory == null || datas.name_signatory== undefined) {
					return logger.logError("Please Input Name Signatory First")
				}
				if (datas.document_number == null || datas.document_number== undefined) {
					return logger.logError("Please Input Type Document Number  First")
				}
				logger.logSuccess("Viewing Report Success")

				ii = new Date()
				yy = ii.toISOString()
				yyy = yy.split("T")

				datas.dateNow = yyy[0]

				console.log(datas,"===========")
				this.template_datas = datas
				employeeReq = self.label
				

				self.findTerminateDate(employeeReq)
				self.findIncentivePersonal(employeeReq)
				// datas.terminate_data = this.terminate_data
				// console.log(xxx)
				

				employeeSign = self.label_signatory


				$http({
					method : 'GET',
					url : ApiURL.url + '/api/employee-details/job-history/emp/' + employeeReq
				}).success(function(res){
					logger.logSuccess("Viewing Job Success")
					// console.log(res.data.job_history.current_data)
					self.doc1(res.data.job_history.current_data)
					// console.log(res.data.job_history.current_data)
					// this.templateDoc = res.data.job_history.current_data

				}).error(function(res){
					
					logger.logError("Viewing Job Failed")

				})

				// self.findTerminateDate()


				$http({
					method : 'GET',
					url : ApiURL.url + '/api/employee-details/job-history/emp/' + employeeSign
				}).success(function(res){
					// console.log(res.data.job_history.current_data)

					logger.logSuccess("Viewing Job Success")
					self.doc2(res.data.job_history.current_data)

					// this.templateReqDoc = res.data.job_history.current_data

				}).error(function(res){
					
					logger.logError("Viewing Job Failed")

				})



				self.hideShowDoc(datas.typeReport)


			},
			findIncentivePersonal:function(a){
				newObj = {
					"employee_id" : a
				}

				$http({
					method : 'POST',
					url : ApiURL.url +  '/payroll/personal_incentive_allowance/get',
					data : newObj
				}).success(function(res){
					self.getIncentivePersonal(res.data)
					logger.logSuccess('Get data Success')
				}).error(function(res){
					logger.logError('Get data failed!')
				})
			},
			getIncentivePersonal:function(x){
				this.totalIncentiveAllowance = 0
				for(let i = 0; i < x.length; i++){
					this.totalIncentiveAllowance = this.totalIncentiveAllowance+x[i].amount
				}
				console.log(this.totalIncentiveAllowance,"---------------------")
				this.maindataIncentivePersonal = x
			},

			findTerminateDate:function(x){
				newObj = {
					employee_id : x
				}
				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/terminate_data/get',
					data : newObj,
					headers: {
						"Content-Type": "application/json"
					}
				}).success(function(res){

					self.tmpTerminateData(res.data[0])
					logger.logSuccess("Viewing Data Success")

				}).error(function(res){
					logger.logError("Viewing Data Failed")
				})
			},
			tmpTerminateData:function(x){
				try {
					y = x.termination_date
					yy = y.split("T")
					x.termination_date = yy[0]
					this.terminate_data = x
				} catch(e) {

				}
				
			},
			doc1:function(x){
				// self.findTerminateDate(x.employee_id)
				this.templateDoc = x

			},
			doc2:function(x){
				this.templateReqDoc = x
			},
			view : function(){
				$http({
					method : 'POST',
					url : ApiURL.url + 'payroll/adjustment_payroll/get?key=' + $cookieStore.get('key_api'),
					data :self.form,
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					console.log(res)
					// if(res.data.data  ==  []){
					// 	console.log(2)
					// 	self.cutoff_disabled   = true;
					// 	self.talesnotale =    res.data.data
					// }else{	
					// 	console.log(3)
					// 	self.cutoff_disabled   =  false;
					// }

					// var tmp=[];
					// if(res.data.data.length > 0){
					// 	for(var i=0;i<res.data.data.length;i++){
					// 		var obj = Object.keys(tmp);
					// 		var t = obj.findIndex((str)=>{
						
					// 			return str==res.data.data[i].date;
					// 		});
					// 		if(t != -1){
					// 			if(res.data.data[i].employee_id){
					// 				tmp[obj[t]].push(res.data.data[i]);
					// 			}
					// 		}else{
					// 			if(res.data.data[i].employee_id){
					// 				tmp[res.data.data[i].date] = [res.data.data[i]];
					// 			}
					// 		}
					// 	}
					// }
					// console.log(tmp,"asdsadsad");
					// self.keyss = Object.keys(tmp);
					// self.group=tmp;
					// $cookieStore.put('option',self.form);

					// console.log($cookieStore.get('option'))
					
					// self.setdata(self.group[self.keyss[0]]);
					// self.setrange(self.form)
					// //self.setdata(res.data.data);
					// //$http.post('/data.json',res.data.data);

					// logger.logSuccess(res.data.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
					if(res.data.header.status == 504) {
						logger.logError("Please Input Job Title & Department")
					}
				});
			},setrange:function(form){
				//console.log(JSON.stringify(form,null,4),"+++++++++++++++++++++++++++++++")
				var base_a = form.attendance.slice(0,10);
				var base_b = form.attendance.slice(11,21);
				
				var a = new Date(base_a);
				var  b  =  new Date(base_b)
				
				var n1 = b.getDate();
				var split_a = base_a.split('/');
				var split_b = base_b.split('/');

				var lastDay = new Date(parseInt(base_a[0]),parseInt(base_a[1]), 0);
				var n = lastDay.getDate();
				
				var str = '';

				if(split_a[1] != split_b[1]){
					for(var i = parseInt(split_a[2]);  i <= parseInt(n); i++){
						var ix =  i
						str = str+'.'+ ix
						if(i == n){
							for(var j = parseInt(1);  j <= parseInt(split_b[2]); j++){
								var ij =  j
								str = str +'.'+ ij
							}
						}
					} 
				}else{
					for(var i = parseInt(split_a[2]); i <= parseInt(split_b[2]); i++){
						var ix =  i
						str = str +'.'+ ix 
					}
				}

				var getDatess = function(startDate, endDate) {
					var dates = [],
					currentDate = startDate,
					addDays = function(days) {
						var date = new Date(this.valueOf());
						date.setDate(date.getDate() + days);
						return date;
					};
					while (currentDate <= endDate) {
						dates.push(currentDate);
						currentDate = addDays.call(currentDate, 1);
					}
					return dates;
				};

				// Usage
				var tmp2=[];
				var dates = getDatess(new Date(base_a), new Date(base_b));
				dates.forEach(function(date) {
					tmp2.push(date.getDate().toString());
				});

				self.dataTabs = []
				self.datarange = /*str.split('.')*/ tmp2;
				//delete self.datarange[0];
				var arr = [];
				var len = self.datarange.length;
				console.log(len);
				var j=0;var keys=self.keyss;
				for (var i = 0; i < len; i++) {

					try{
						var tmp_tgl = keys.findIndex((idx)=>{

							var tgl  	= idx.split('-');
							tgl[2]  = parseInt(tgl[2]);
							if(tgl[2].toString() == self.datarange[i]){ return true; }
							else{ return false; }
						})

						if(tmp_tgl < 0){
							arr.push({
								tab:self.datarange[i],
								date:[],
							});
						}else{
							arr.push({
								tab:self.datarange[i],
								date:keys[tmp_tgl],
							});
						}
						/*var tgl  = keys[j].split('-');
						if(tgl[2] != self.datarange[i] && self.datarange[i] == '31'){
							
						}else{
						    arr.push({
						        tab:self.datarange[i],
						        date:keys[j],
						    });
						    j++;
						}*/
					}catch(e){

					}
				}
				self.dataRange = arr
				console.log(self.dataRange,'range')
				
			},tabIndex:0,
			setIndex:function(index){
				console.log(index)
				self.tabIndex = index;
				self.setNextData(self.dataRange[self.tabIndex])
				

			},next : function(){
				console.log(self.tabIndex)
				if(self.tabIndex !== self.dataRange.length - 1){
					self.dataRange[self.tabIndex].active = false;
					self.tabIndex++
					self.dataRange[self.tabIndex].active = true;
				}   
			},prev:function(){
				if(self.tabIndex > 0){
					self.dataRange[self.tabIndex].active = false;
					self.tabIndex--
					self.dataRange[self.tabIndex].active = true;
				}else{
					void 0
				}	
			},getdatabyIndex:function(i,j){
				self.tabIndex = j;
				self.form.tab =  i;

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
					data :self.form,
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					self.setdata(res.data.data);
					//$http.post('/data.json',res.data.data);
					self.tabIndex = j;
					console.log(res.data.data)
					self.setrange(self.form)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
					if(res.data.header.status == 504) {
						logger.logError("Please Input Job Title & Department")
					}
				});
			},testMasuk : function(data,i){
				
				self.form.tabActive = data;
				$http({		
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
					data :self.form,
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					console.log(i)
					self.setIndex =  i
					self.setdata(res.data.data);
					//$http.post('/data.json',res.data.data);

					self.setrange(self.form)
					console.log(res)
					logger.logSuccess(res.data.message);
				},function(res){
					res.data.header ? logger.logError(res.data.message) : logger.logError("View Attendance Cut Off Failed")
				});
			},setNextData:function(data){

				//console.log(self.form.tab)
				// $http({		
				// 	method : 'POST',
				// 	url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
				// 	data :self.form
				// }).then(function(res){

				// 	self.setdata(res.data.data);
				// 	//$http.post('/data.json',res.data.data);

				// 	self.setrange(self.form)
				// 	logger.logSuccess(res.data.header.message);
				// },function(res){
				// 	res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
				// });

				/*$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.setdata([]);
					logger.logSuccess('Success find data');
					$route.reload();
				},function(res){
					res.data.header ? logger.logError('Error cut off data') : logger.logError("Error cut off data")
				});*/
			},adddata: function(a){
				this.maindata.push(a);
			},openmodal:function(a){
				//check short exists  
				

				var thanos  =  false;
				var collect  = [];
				var i  =   0;
				for(var prop  in self.maindata){
					if(self.maindata[prop].Short != '-'){

						collect[i]  = self.maindata[prop] ;
						i += 1;
						thanos  =    true;

					}
				}

				if(thanos  ==  true){
					self.collect  =  collect
					//self.context.cancel();
					console.log(self.context)
					$modal.open(self.modals[a]).result.then(function(data){
						switch (a) {
							case 'open':
							self.collect  =  collect
							break;
							case  'open1':


							break;
						}
					});
				}else{
					var a   =   'open1'
					$modal.open(self.modals[a]).result.then(function(data){
						switch (a) {
							case  'open1':


							break;
						}
					});
				}
			},modals : {
				open:{
					animation: true,
					templateUrl: 'open',
					controller: 'cutoffmodal',
					controllerAs: 'open',
					size:'lg',
					resolve:{
						data:function(){
							return self.collect;
						}					
					}
				},
				open1:{
					animation: true,
					templateUrl: 'open1',
					controller: 'cutoffmodal1',
					controllerAs: 'open1',
					size:'lg',
					resolve:{
						data:function(){
							return self.form;
						}					
					}
				}
			},setdata: function(a){
				console.log(a)
				this.maindata = a

			},
			getdata: function(){
				if(self.dataRange){
					
					var t=self.dataRange.findIndex(function(dt){
						return dt.active==true;
					});
					if(t!=-1){
						this.maindata = self.group[self.dataRange[t].date];
					}else{
						this.maindata = [];
					}
				}
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},getLocation : function(val) {
				
				if (val.length < 4) {
					return ""
				}
				newObj= {
					name :val
				}
				setTimeout(function(){
					// console.log("xxxx")
					$http({
						method : 'POST',
						url : ApiURL.url + '/last_pay_sheet/search_by_name' ,
						data : newObj,
						headers: {
							"Content-Type": "application/json"
						}
					// params : {add}
				}).success(function(res){
					if(res.data === null){
						logger.logError("Data Empty");
						var a = {};
						a.result = [];
						self.tmpType(res.data)
						return a.result.map(function(item){
							return item;
						});  
					}else if(res.data != null){

						var a = {};
						// console.log(res.data)
						a.result = res.data;
						self.tmpType(res.data)
						return a.result.map(function(item){
							return item;
						});
					}  
				})
			},1000)

				// $http({
				// 	method : 'POST',
				// 	url : ApiURL.url + '/last_pay_sheet/search_by_name' ,
				// 	data : newObj,
				// 	// params : {add}
				// }).success(function(res){
				// 	if(res.data === null){
				// 		logger.logError("Data Empty");
				// 		var a = {};
				// 		a.result = [];
				// 		self.tmpType(res.data)
				// 		return a.result.map(function(item){
				// 			return item;
				// 		});  
				// 	}else if(res.data != null){

				// 		var a = {};
				// 		console.log(res.data)
				// 		a.result = res.data;
				// 		self.tmpType(res.data)
				// 		return a.result.map(function(item){
				// 			return item;
				// 		});
				// 	}  
				// })
				// $http.get(
				// 	url :ApiURL.url + '/last_pay_sheet/search_by_name' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),
						// params:{address : val,}

				// 	{}).then(function(response){
				// 		if(response.data.data === null){
				// 			logger.logError(response.data.header.message);
				// 			var a = {};a.result = [];
				// 			return a.result.map(function(item){
				// 				return item;
				// 			});  
				// 		}else if(response.data != null){
				// 			var a = {};
				// 			a.result = response.data;
				// 			return a.result.map(function(item){
				// 				return item;
				// 			});
				// 		}  
				// 	});
			},
			tmpType : function(x){

				this.data1 = x

			},
			onSelect : function ($item,$model,$label){
				self.model = $item.name;
				self.label = $item.employee_id;
			},

			getLocation_2 : function(val) {
				if (val.length < 4) {
					return ""
				}
				newObj= {
					name :val
				}
				setTimeout(function(){
					// console.log("xxxx")
					$http({
						method : 'POST',
						url : ApiURL.url + '/last_pay_sheet/search_by_name' ,
						data : newObj,
						headers: {
							"Content-Type": "application/json"
						}
					// params : {add}
				}).success(function(res){
					if(res.data === null){
						logger.logError("Data Empty");
						var a = {};
						a.result = [];
						self.tmpType_2(res.data)
						return a.result.map(function(item){
							return item;
						});  
					}else if(res.data != null){

						var a = {};
						console.log(res.data)
						a.result = res.data;
						self.tmpType_2(res.data)
						return a.result.map(function(item){
							return item;
						});
					}  
				})
			},1000)


				// return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name_signatory +'?&key=' + $cookieStore.get('key_api'),{
				// 	params:{address : val,}
				// }).then(function(response){
				// 	if(response.data.data === null){
				// 		logger.logError(response.data.header.message);
				// 		var a = {};a.result = [];
				// 		return a.result.map(function(item){
				// 			return item;
				// 		});  
				// 	}else if(response.data != null){
				// 		var a = {};
				// 		a.result = response.data;
				// 		return a.result.map(function(item){
				// 			return item;
				// 		});
				// 	}  
				// });
			},onSelect_2 : function ($item_signatory,$model_signatory,$label_signatory){
				// console.log($item_signatory,$model_signatory,$label_signatory)
				self.model_signatory = $item_signatory.name;
				self.label_signatory = $item_signatory.employee_id;
			},
			tmpType_2:function(x){
				this.data2 = x

			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[4],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},

			hideShowReport : function(){

				// console.log(x)
				x = self.form.typeReport
				if (x == 'coe_resign' || x == 'coe_compensation' || x == 'coe_without_compensation') {
					if (self.form_1 = true){
						return (self.form_2 =false)
					}

				}
				if (x == 'philhealth') {
					if (self.form_1 = true){
						return (self.form_2 =true)
					}
				}


			},

			hideShowDoc:function(y){
				x = y

				if (x == 'coe_resign') {
					if (self.coe_resign_doc = true) {

						// console.log("X")
						return ((self.coe_compensation_doc = false) ||(self.coe_without_compensation_doc = false) ||(self.philhealth_doc = false) )
					}
					
					
				}

				if (x == 'coe_compensation') {
					if (self.coe_compensation_doc = true) {
						// console.log("Y")
						return ((self.coe_resign_doc = false) ||(self.coe_without_compensation_doc = false) ||(self.philhealth_doc = false) )
					}
				}

				if (x == 'coe_without_compensation') {
					if (self.coe_without_compensation_doc = true) {
						// console.log("z")
						return ((self.coe_compensation_doc = false) ||(self.coe_resign_doc = false) ||(self.philhealth_doc = false) )
					}
				}


				if (x == 'philhealth') {
					if (self.philhealth_doc= true) {
						// console.log("z")
						return ((self.coe_compensation_doc = false) ||(self.coe_resign_doc = false) ||(self.coe_without_compensation_doc = false) )
					}
				}

			},
			generate_excel:function(){
				newObj = {
					"document_number" :  self.template_datas.document_number,
					"name_request" : self.template_datas.employee_id,
					"contract_start" : self.templateDoc.contract_start_date,
					"termination_date" : self.terminate_data.termination_date,
					"job" : self.templateDoc.job,
					"date_now" : self.template_datas.dateNow,
					"name_signatory" : self.template_datas.name_signatory,
					"sub_unit_signatory" : self.templateReqDoc.sub_unit

				}

				console.log(newObj)

				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/other_reports/resigned/generate_excel',
					data : newObj
				}).success(function(res){
					logger.logSuccess('Convert to Doc Success!')
				}).error(function(res){
					logger.logError('Convert to Doc Success!')

				})

			},
			generate_excel_compensation:function(){
				newObj = {
					"document_number" :  self.template_datas.document_number,
					"name_request" : self.template_datas.employee_id,
					"contract_start" : self.templateDoc.contract_start_date,
					"job" : self.templateDoc.job,
					"date_now" : self.template_datas.dateNow,
					"name_signatory" : self.template_datas.name_signatory,
					"sub_unit_signatory" : self.templateReqDoc.sub_unit,
					"incentive_compensation" : this.maindataIncentivePersonal

				}
				console.log(newObj)
				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/other_reports/compensation/generate_excel',
					data : newObj	
				}).success(function(res){
					logger.logSuccess("YEY BISA")
				}).error(function(res){
					logger.logError('YAH GABISA')
				})


			},
			generate_excel_without_compensation:function(){
				newObj={
					"document_number" :  self.template_datas.document_number,
					"name_request" : self.template_datas.employee_id,
					"contract_start" : self.templateDoc.contract_start_date,
					"termination_date" : self.terminate_data.termination_date,
					"job" : self.templateDoc.job,
					"date_now" : self.template_datas.dateNow,
					"name_signatory" : self.template_datas.name_signatory,
					"sub_unit_signatory" : self.templateReqDoc.sub_unit

				}
				console.log(newObj)

				$http({
					url : ApiURL.url + '/payroll/other_reports/without_compensation/generate_excel',
					method : 'POST',
					data : newObj
				}).success(function(res){
					logger.logSuccess("Working!")
				}).error(function(res){
					logger.logError('Oh no!')
				})
			}

		}
	})
.controller('otherReportsController',function(other_reportsFactory,ApiURL,$cookieStore,$scope,$http){
	var self = this;
	self.form={}
	self.handler = other_reportsFactory;
	// self.handler.getDataDropdown()
	
})

.controller('cutoffmodal',function(other_reportsFactory,ApiURL,$cookieStore,$modalInstance,$scope,$http,data,$route,logger){
	console.log(data)
	var self = this;
	self.form={}
	self.handler = other_reportsFactory;
	self.handler.context = this
	self.save = function(){

		var final_data =  $cookieStore.get('option');
		var k  =  0;
		var arr  = [];
		for(var prop in self.handler.collect){
			result   =    self.handler.collect[prop]
			arr[k]   = {'employee_id' : result.employee_id,  'date' : result.date, 'short' :  result.Short }
			k  += 1
		}

		console.log(self.handler.maindata)
		var obj   =  self.handler.maindata;
		for(var prop in  obj){
			if((self.maindata[prop].timeIn == '-' && self.maindata[prop].timeOut == '-') || (self.maindata[prop].timeIn == '-' || self.maindata[prop].timeOut == '-')){
				logger.logError(`Error cut off Time In or Time Out doesn't exists  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;
			}
			else if(self.maindata[prop].C_TimeIn ==  'red' || self.maindata[prop].C_TimeIn ==  'orange'){
				logger.logError(`Error cut off Time In or Time Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].OvertimeRestDay_color ==  'red' || self.maindata[prop].OvertimeRestDay_color ==  'orange'){
				logger.logError(`Error cut off Overtime Rest Day not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].colorLate ==  'red' || self.maindata[prop].colorLate ==  'orange'){
				logger.logError(`Error cut off Late not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].early_c ==  'red' || self.maindata[prop].early_c ==  'orange'){
				logger.logError(`Error cut off Early Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].new_color_overtime==  'red' || self.maindata[prop].new_color_overtime==  'orange'){
				logger.logError(`Error cut off Overtime not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else{

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data : {form  :  final_data, short_person : arr},
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					var  base_data  = [];
					self.handler.setdata(base_data);
					self.handler.dataRange = [];
					self.handler.form =  []
					// var b = res.data.data;
					// console.log(b)
					// for(s in self.maindata){
					// 	if(b.date === self.maindata[s].date){
					// 		self.maindata[s] = res.data.data;
					// 	}
					// }

					$route.reload();
					logger.logSuccess('Success cut off data');
				},function(res){
					logger.logError('Failed cut off data')
				})
			}
		}


		$modalInstance.close(self.form);
	},
	self.cancel = function(){
		console.log('test_cancle')
		$modalInstance.close()
		self.handler.openmodal('open1')
	}
})


.controller('cutoffmodal1',function(other_reportsFactory,ApiURL,$cookieStore,$modalInstance,$scope,$http,data,$route,logger){
	var self = this;
	self.form={}
	self.handler = other_reportsFactory;

	self.save1 = function(file){
		var final_data =  $cookieStore.get('option');

		var k  =  0;
		var arr  = [];


		console.log(self.handler.maindata)
		var obj   =  self.handler.maindata;
		for(var prop in  obj){
			if((self.maindata[prop].timeIn == '-' && self.maindata[prop].timeOut == '-') || (self.maindata[prop].timeIn == '-' || self.maindata[prop].timeOut == '-')){
				logger.logError(`Error cut off Time In or Time Out doesn't exists  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;
			}
			else if(self.maindata[prop].C_TimeIn ==  'red' || self.maindata[prop].C_TimeIn ==  'orange'){
				logger.logError(`Error cut off Time In or Time Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].OvertimeRestDay_color ==  'red' || self.maindata[prop].OvertimeRestDay_color ==  'orange'){
				logger.logError(`Error cut off Overtime Rest Day not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].colorLate ==  'red' || self.maindata[prop].colorLate ==  'orange'){
				logger.logError(`Error cut off Late not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].early_c ==  'red' || self.maindata[prop].early_c ==  'orange'){
				logger.logError(`Error cut off Early Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].new_color_overtime==  'red' || self.maindata[prop].new_color_overtime==  'orange'){
				logger.logError(`Error cut off Overtime not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}else{


				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data : {form  :  final_data, short_person : arr},
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					$route.reload();
					var  base_data  = [];
					self.handler.setdata(base_data);
					self.handler.dataRange = [];
					self.handler.form =  []
					logger.logSuccess("Success cut off data");
				},function(res){
					logger.logError("Failed   cut off   data")
				})
			}
		}

		$modalInstance.close(self.form);
	},
	self.cancel1 = function(){
		console.log('test_cancle')
		$modalInstance.dismiss("Close")
	}
})


/** FUNCTION SET HEADER TABLES ATT CUT-OFF **/
.run(function(other_reportsFactory){
	other_reportsFactory.setheaders();
})

}())
