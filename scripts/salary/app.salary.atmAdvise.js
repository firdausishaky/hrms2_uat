

(function(){

	angular.module('app.salary.atmAdvise',['checklist-model'])

	.factory('atmAdviseFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.title =arguments[i][0];
				x.amount =arguments[i][1];
				x.netpay =arguments[i][2];
				x.remarks =arguments[i][3];
				x.width =arguments[i][4] ? arguments[i][4]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Employee Name','Account Number','Net Pay For Credit','Remarks',25]
					);
				this.setactiveheader(this.headers[0],false)
			},getdataAllowance:function(){
				$http({
					url : ApiURL.url + '/payroll/incentive_allowance_config/get/0' ,
					method: 'GET'
				}).then(function(res){
					console.log(res)
					const status = res.status

					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						self.setdata(res.data.data)
						logger.logSuccess("View Data Success");
					}
				},function(res){
					res.data.header ? logger.logError(res.data.message) : logger.logError("Show Incentive Allowance Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					// url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
					console.log(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
				console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
				
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				// console.log(page,"y")
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodalIncen',
					controller: 'addModalIncentiveAllowance',
					controllerAs: 'addmodalincen',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},

				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditemployee',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						console.log("xxxxxx")
						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
							data : data,
							headers : {	'Content-Type' :'application/json' }
						}).then(function(res){
							console.log(res)
							self.adddata(res.data.data[0])
							logger.logSuccess("Adding Incentive Allowance Success");
							self.getdataAllowance()
						},function(res){
							res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						});
						break;
						case 'edit':
						$http({
							method : 'POST',
									// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.message);
									self.getdataAllowance()
								},function(res){
									res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
								});			
								break;
							}
						});
			},
			deldata: function(id){
				$http({
					// url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
					url : ApiURL.url + '/payroll/incentive_allowance_config/delete/' + id,
					method: 'GET'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Success');
					self.getdataAllowance()
				},function(res){
					console.log(res.data.header)
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Failed")
				})
			},

			change_payroll_slips:function(){
				x = self.form.local_it 
				// console.log(x)
				if (x == 1) {
					if (self.expat_dropdown = true) {}
						return self.local_dropdown = false
				}
				if (x == 2) {
					if (self.local_dropdown = true) {
						return self.expat_dropdown = false

					}
					// self.local_drodown = true
				}
			},

			dropdownAtmAdvise : function(){
				$http({
					url: ApiURL.url + '/atm_adsive_credit',
					method : 'GET'
				}).success(function(res){
					// console.log(res)
					// console.log(res)
					self.dropdowndatas(res.data)
					logger.logSuccess('Dropdown View Success')
				}).error(function(res){
					logger.logError('Dropdown View Failed')
				})
			},
			dropdowndatas : function(x){
				// console.log(x)
				this.dropdownDataExpat = x.expat.dropdown
				this.dropdownDataLocal = x.local.dropdown
				// console.log(this.dropdownDataExpat,this.dropdownDataLocal)


			},
			searchingData: function(){
				if(self.form == null || self.form == undefined || self.form.local_it == null || self.form.local_it == undefined || self.form.type == null || self.form.type == undefined){
					return logger.logError('Please Select Report / Type Slips First!')
				}
				// console.log(self.form)
				if(typeof(self.form.local_it) == 'string'){
					self.form.local_it = Number(self.form.local_it)
					// console.log(self.form.local_it)
				}

				datas = self.form
				console.log(self.form)
				$http({
					url : ApiURL.url + "/atm_adsive_credit/search", 
					method : 'POST',
					data : datas,
					headers: {
						"Content-Type": "application/json"
					}
				}).success(function(res){
					console.log(res)
					self.setdata(res.data)
					logger.logSuccess("Searching Data Success!")

				}).error(function(res){
					logger.logError("Searching Data Failed!")
				})
			},
			cutoffData : function(){
				x = this.maindata
				console.log(self.form, "0000000000000000000000")
				self.form.grand_total = 0
				for(i = 0; i < x.length; i++){
					self.form.grand_total = self.form.grand_total + x[i].net_pay
					delete x[i].employee_id
					delete x[i].department_id
					delete x[i].department_name
					if (x[i].hold == undefined || x[i].hold == null) {
						x[i].hold = false
						// statement
					}else{
						x[i].hold =true
					}


				}
				console.log(x)
				self.form.grand_total = Number(self.form.grand_total.toFixed(2))
				console.log(new Date())
				newObj = {
					type : self.form.type,
					local: self.form.local_it,
					created_by : $cookieStore.get('employee_id'),
					date : self.form.date,
					json_data : {
						bank_name : self.form.bank_name,
						bank_address : self.form.bank_address,
						re : self.form.re,
						centavos: self.form.centavos,
						account_employee : x,
						grand_total : self.form.grand_total,
						message : self.form.message,
						submit_by : {
							file : self.form.submit_by.files[0].name,
							name : self.form.submit_by.name,
							position : self.form.submit_by.position,
						},
						approve_by : {
							file : self.form.approve_by.files[0].name,
							name : self.form.approve_by.name,
							position : self.form.approve_by.position,
						}


					}


				}
				console.log(newObj)
				$http({
					method: 'POST',
					url : ApiURL.url + '/atm_adsive_credit/edit',
					data : newObj,
					headers: {
						"Content-Type": "application/json"
					}
				}).success(function(res) {
					console.log(res)
				}).error(function(res){
					console.log(res)
				})
			},

		}
	})

.controller('atmAdiviseController',function(atmAdviseFactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = atmAdviseFactory;
	self.handler.dropdownAtmAdvise()
	// self.handler.getdataAllowance()
})
.controller('addModalIncentiveAllowance',function($modalInstance,data){	
	var self = this;
	self.save = function(){
		$modalInstance.close(self.form);
	}

	self.test = function(){
		$modalInstance.dismiss('close')
		// console.log/("LOL")
	}
})


.controller('modaleditemployee',function(atmAdviseFactory,$modalInstance,data){	
	console.log("test")
	var self = this;
	self.handler = atmAdviseFactory;
	self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(atmAdviseFactory){
	atmAdviseFactory.setheaders();
})

}())