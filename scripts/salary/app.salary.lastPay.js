(function(){

	angular.module('app.salary.lastPay',['checklist-model'])

	.factory('lastPayFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Department','Department'],
					['Employee Name','employee_name'],
					['Working Days','Days Off'],
					['VL','vl'],
					['EL','el'],
					['SL','sl'],
					['BL','bl'],
					['BeL','bel'],
					['MaL','mal'],
					['PL','pl'],
					['ML','ml'],
					
					['BL','bl'],
					['MaL','mal'],
					['PL','pl'],
					['BeL','bel'],


					['RH','rh'],
					['SH','sh'],

					['G.OT','got'],
					['R.OT','rot'],
					['RH.OT','rhot'],
					['SH.OT','shot'],
					['RD.OT-1st','rdot1'],
					['RD.OT-exc','rdotexc'],
					['RHRD.OT-1st','rhrdot1'],
					['RHRD.OT-exc','rhrdotexc'],
					['SHRD.OT-1st','shrdot1'],
					['SHRD.OT-exc','shrdotexc'],


					['ND','nd'],
					['RH.ND','rhnd'],
					['SH.DN','shdn'],
					['RD.ND','rdnd'],
					['RHRD.ND','rhrdnd'],
					['SHRD.ND','shrdnd'],

					['Late','late'],
					['Undertime','undertime'],



					['Basic','basic'],
					['E-Cola','ecola'],
					['Other Allowance','other_allowance'],
					['Total','total'],


					['RH','rh'],
					['SH','sh'],

					['G.OT','got'],
					['R.OT','rot'],
					['RH.OT','rhot'],
					['SH.OT','shot'],
					['RD.OT-1st','rdot1'],
					['RD.OT-exc','rdotexc'],
					['RHRD.OT-1st','rhrdot1'],
					['RHRD.OT-exc','rhrdotexc'],
					['SHRD.OT-1st','shrdot1'],
					['SHRD.OT-exc','shrdotexc'],


					['ND','nd'],
					['RH.ND','rhnd'],
					['SH.DN','shdn'],
					['RD.ND','rdnd'],
					['RHRD.ND','rhrdnd'],
					['SHRD.ND','shrdnd'],

					['Late','late'],
					['Undertime','undertime'],






					


					);
				this.setactiveheader(this.headers[0],false)
			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},
			onSelect : function ($item,$model,$label){

				self.model = $item.name;
				self.label = $item.employee_id;
				self.getIDEmployee(self.label)

			},

			getDepartment:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/last_pay_sheet/get'
				}).then(function(res){

					if (res.status !==200 ) {
						logger.logError("Get Data Failed")
					}else{
						logger.logSuccess("Get Data Success")
						self.department = res.data.data.department;
						self.year = res.data.data.year;
						self.month = res.data.data.month;
						self.payout = res.data.data.payout;
						self.terminate = res.data.data.terminate;
						//console.log(res.data.data,"TERMNATE");
						// self.setdataDepartment(res.data.data[0].department)
						// self.cutoffDate(res.data.data[0].payout)
					}
				});
			},
			cutoffDate:function(x){

				for (var i = 0; i < x.length; i++) {
					x[i].payout_date = x[i].payout_date.split("T")
					x[i].payout_date = x[i].payout_date[0]
				}

				this.cutoff_date = x

			},


			setdataDepartment:function(x){
				this.setdataDepartment = x
			},
			getdatas:function(){
				$http({
					url : ApiURL.url + '/last_pay_sheet/search' ,
					method: 'POST',
					data : self.form,
					header : { "Content-Type" : 'application/json' }
				}).then(function(res){
					console.log(res)
					const status = res.status

					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						console.log(res.data.data)
						self.setdata(res.data.data)
						logger.logSuccess("View Data Success");
					}
				},function(res){
					res.data.header ? logger.logError(res.data.message) : logger.logError("Show Incentive Allowance Failed")
				})
			},
			getdataAllowance:function(){
				$http({
					url : ApiURL.url + '/payroll/incentive_allowance_config/get/0' ,
					method: 'GET'
				}).then(function(res){
					console.log(res)
					const status = res.status

					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						self.setdata(res.data.data)
						logger.logSuccess("View Data Success");
					}
				},function(res){
					res.data.header ? logger.logError(res.data.message) : logger.logError("Show Incentive Allowance Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					// url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
					console.log(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
				console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
				
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodalemployee',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditemployee',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				console.log(b)
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						console.log(data)
						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
							data : data,
							headers : {	'Content-Type' :'application/json' }
						}).then(function(res){
							console.log(res)
							self.adddata(res.data.data[0])
							logger.logSuccess("Adding Incentive Allowance Success");
							self.getdataAllowance()
						},function(res){
							res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						});
						break;
						case 'edit':
						$http({
							method : 'POST',
									// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.message);
									self.getdataAllowance()
								},function(res){
									res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
								});			
								break;
							}
						});
			},
			deldata: function(id){
				$http({
					// url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
					url : ApiURL.url + '/payroll/incentive_allowance_config/delete/' + id,
					method: 'GET'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Success');
					self.getdataAllowance()
				},function(res){
					console.log(res.data.header)
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Failed")
				})
			},

		}
	})

.controller('lastPayCtrl',function(lastPayFactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = lastPayFactory;
	//self.handler.getdataAllowance()
	self.handler.getDepartment();
	
	//console.log(self.handler,"--------------xxxx")
})
.controller('addmodalemployee',function(lastPayFactory,$modalInstance,data){	

	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.controller('modaleditemployee',function(lastPayFactory,$modalInstance,data){	
	console.log("test")
	var self = this;
	self.handler = lastPayFactory;
	self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(lastPayFactory){
	lastPayFactory.setheaders();
})

}())