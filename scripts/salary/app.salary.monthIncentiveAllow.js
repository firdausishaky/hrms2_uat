(function(){
	
	angular.module("app.salary.monthIncentiveAllow",['ngFileUpload'])
	
	.factory('monthIncenAllowFactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,Upload){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var NAME = '_schedule';
		var s = localStorage.getItem(NAME);
		var items = s ? JSON.parse(s) : {};
		var self;
		return self = {
			getID(){
				const onlyUser = $cookieStore.get('UserAccess')
				const notUser = $cookieStore.get('NotUser')
				if (notUser == null) {
					return(onlyUser.id)
				}else{
					return(notUser)
				}

			},
			fullmonth : ["Januari","Febuari","Maret","April","Juni","Juli","Agustus","September","Oktober","November","Desember"],
			storeMonths : {
				"Januari":{ start_:null, end_:null },
				"Febuari":{ start_:null, end_:null },
				"Maret":{ start_:null, end_:null },
				"April":{ start_:null, end_:null },
				"Juni":{ start_:null, end_:null },
				"Juli":{ start_:null, end_:null },
				"Agustus":{ start_:null, end_:null },
				"September":{ start_:null, end_:null },
				"Oktober":{ start_:null, end_:null },
				"November":{ start_:null, end_:null },
				"Desember":{ start_:null, end_:null }
			},
			setheaders: function(){
				this.headers = new createheaders(
					['Department','department'],
					['Job Title','job'],
					['Name','name'],
					['Tardiness','tardiness'],
					['Absences','absences'],
					['Disciplinary Action','disciplinary'],
					['Monthly Incentives','monthly_incentives'],
					['Deductions','deductions'],
					['Food Allowance','food_allowance'],
					['Deductions Food','deductions_food'],
					['Adjustment','adjustment'],
					['Total','total'],
					['Generate To Excel','generate_excel'],



					);
				this.setactiveheader(this.headers[0],false)	
			},adddata: function(a){
				this.maindata.push(a);
			},getaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/payroll/adjustment_payroll'
				}).then(function(res){
					console.log(res)
					if (res.status !==200 ) {
						logger.logError("Get Data Failed")
					}else{
						logger.logSuccess("Get Data Success")

						self.setdataDepartment(res.data.data[0].department)
						self.setdataJobs(res.data.data[0].jobs)
						self.setdataPeriod(res.data.data[0].period);
						//self.setdataPeriod(res.data.data[0].period)

// 						var tmp = [];
// 						for(var i in res.data.data[0].period){
// 							var split = res.data.data[0].period[i].periode.split("-");

// 							var start = new Date(split[0]);
// 							var end = new Date(split[1]);
// 							var start_m = (start.getMonth());
// 							console.log(self.storeMonths[self.fullmonth[start_m]],'=====================')
// 							if(self.storeMonths[self.fullmonth[start_m]].start_){
// 								self.storeMonths[self.fullmonth[start_m]].end_ = split[1];
// 							}else{
// 								self.storeMonths[self.fullmonth[start_m]].start_ = split[0];
// 								tmp.push(self.fullmonth[start_m]);
// 							}

// 						}
						//self.setdataPeriod(res.data.data[0].period)
						//console.log("asdkasdasd",tmp)
					}


				});
			},setdataDepartment:function(x){
				this.maindataDepartment = x
			},
			setdataJobs:function(x){
				this.maindataJobs = x
			},
			setdataPeriod:function(x){
				this.maindataPeriod = x
			},
			getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},setform:function(data){
				if(data.status == 'user'){
					this.role = data.status;
					this.iduser = data.employee_id;
					this.nameuser = data.employee_name;
					self.form={};
					self.form.name = this.nameuser;
					self.hide = true;
				}else{
					this.role = data.status;
					self.form={};
					self.form.name = '';
					self.hide = false;
				}
				self.datastatus = data.status;
			},onSelect : function ($item,$model,$label){
				self.model = $item.name;
				self.label = $item.employee_id;

			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},getYear : function(res){
				var results = res.data.data;
				var getdata = results.length;
				var arr = [];
				var monthArr = [];
				if(getdata > 0){
					var i = 0;
					for(; i < getdata; i++){
						var sub = res.data.data[i].date.substr(0,7);
						var month = res.data.data[i].date.substr(5,2);
						var day = res.data.data[i].date.substr(8,2);

						var subday = day.substr(0,1);
						if(subday == 0){
							day = day.substr(1,1);
						}
						arr[sub] = day;
					}

					var getFinally = [];
					for(key in arr){
						getFinally.push(key+'-'+arr[key]);
					}

					var countFinally = getFinally.length;

					var etc = [{"min" :  getFinally[0]},{"max" :  getFinally[countFinally-1]}];
					return etc;

				}
			},dateEnd : function(data){
				//console.log(data);
				var dates = data.toString();
				var arr = [];
				var arr = dates.split(" ");
				if(arr[1] ==  "Jan"){ arr[1] = 1;}
				if(arr[1] ==  "Feb"){ arr[1] = 2;}
				if(arr[1] ==  "Mar"){ arr[1] = 3;}
				if(arr[1] ==  "Apr"){ arr[1] = 4;}
				if(arr[1] ==  "May"){ arr[1] = 5;}
				if(arr[1] ==  "Jun"){ arr[1] = 6;}
				if(arr[1] ==  "Jul"){ arr[1] = 7;}
				if(arr[1] ==  "Aug"){ arr[1] = 8;}
				if(arr[1] ==  "Sep"){ arr[1] = 9;}
				if(arr[1] ==  "Oct"){ arr[1] = 10;}
				if(arr[1] ==  "Nov"){ arr[1] = 11;}
				if(arr[1] ==  "Dec"){ arr[1] = 12;}
				
				var newdate = arr[3]+'-'+arr[1]+'-'+arr[2];
				//console.log(newdate);
				var result = self.getdateEnd
				//console.log(result);
				var count = result.length;
				var endSearch = [];
				if(count > 0){
					for(var i = 0; i < count; i++){
						
						if(result[i].date ==  newdate){
							endSearch =  result[i];
						}
					}
				}
				if(endSearch == null){
					res.data.header ? logger.logError("data not found") : logger.logError("Data not found")
				}else{
					self.setdata([endSearch]);
				}
				
			},next_paging : function(data,f){
				console.log("f", f);
				console.log("data", data);
				console.log("self.paging", self.paging);
				
				
				if(data > 2 && f  ==  'end' ){
					self.paging[0] = '...'
					self.paging[1] = self.default_paginate[data];

					if(self.default_paginate[data+1] >  self.default_paginate[data]){
						self.paging[2] =  self.default_paginate[data+1] 
					}else{
						delete self.paging[2];
					}
					
					self.paging[3] = '...'
				}else if(data ==  1 && f  == "end"){

				}else{

				}


			},
			setToday : function(datas,type){
				var counts = 1;
				var page = 1;
				var dt = this.maindata;
				console.log("setToday",datas,dt[0].date);
				for(var i in dt){
					if(type=='date'){
						if(dt[i].date == datas){
							//console.log("PAGING",i);
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;
						}
					}else if(type=="month"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getMonth()+1;
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}else if(type=="year"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getFullYear();
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}
				}
				self.Selectdate 	= datas;
				self.Selectdate_end = dt[dt.length-1].date;
				self.Selectdate_start = dt[0].date;
				
				if(page==0){ page++; }
				console.log(page,"PAGE");
				self.selectedPage 	= page;
				self.currentPage 	= self.selectedPage;
				//self.currentPage = 25;
			},
			clickable : 'pointer',
			action_save : false,
			select_month : function(month_date){
				self.action_save = month_date;
				return self.save();
			},
			select_monthnew : function(month_date){
				self.action_save = month_date;
				console.log(month_date,'+++++++++++++++ select_monthnew +++++++++++++++++++')
				return self.saveNew();
			},
			save : function(){
					// console.log(self.form)
					if (self.form == undefined || self.form == null) {
						return logger.logError('Please Input Search First!')

					}
					if (self.form.cutoff_date == undefined || self.form.cutoff_date == null) {
						return logger.logError('Please select Cut Off Date')
					}
					if (self.form.department == undefined || self.form.department == null) {
						self.form.department = 0
					}
					if (self.form.job == undefined || self.form.job == null) {
						self.form.job = 0
					}	
					if (self.form.employee_id == undefined || self.form.employee_id == null) {
						self.form.employee_id = null
					}else{

						self.form.employee_id = self.label
					}

					console.log(self.form)
					self.form.type = "payroll"


					$http({
						method : 'POST',
						url : ApiURL.url + '/payroll/monthly_incentive_allow/get',
						headers : {
							'Content-Type' : 'application/json'
						},
						data: self.form
					}).then(function(res){
						console.log(res)

						const status = res.status

						if (status !== 200) {
							logger.logError("Access Unauthorized");
						}else{

							self.form.employee_id = self.model
							self.setdata(res.data.data)
							logger.logSuccess("View Data Success");
						}
					})

				},restore : function(){
					self.setdata(items);
				},setdata: function(a){
					this.maindata = a;
				// console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},
			
			getActive : function(y){
				if(self.maindata){	
					while (true){
						if( y == 1 || y == 2 || y == 3){
							break;
						}
						y = y - 3;
					}
					return y;
				}
			},getdateRequest : function(form){
				var a = self.maindata;
				var b = form;
				var c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeIn = d.timeIn;
						b.timeOut = d.timeOut;
						b.schedule = d.schedule;
						break;
					}else{
						var d = '-';
						form.timeIn = d;
						form.timeOut = d;
						form.schedule = d;
					}
				}
			},getlateRequest : function(form){
				var a = self.maindata;
				var b = form;
				console.log(b)
				var c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeIn = d.timeIn;
						b.Late = d.Late;
						b.schedule = d.schedule;
						break;
					}else{
						var d = '-';
						form.timeIn = d;
						form.Late = d;
						form.schedule = d;
					}
				}
			},getearlyRequest : function(form){
				var a = self.maindata;
				var b = form;
				console.log(b)
				var c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeOut = d.timeOut;
						b.EarlyOut = d.EarlyOut;
						b.schedule = d.schedule;
						break;
					}else{
						var d = '-';
						form.timeOut = d;
						form.EarlyOut = d;
						form.schedule = d;
					}
				}
			},setdisabled:function(a){
				if(a.timeIn=='-' || a.timeIn=='00:00'){
					self.formdisabled = false
				}else{
					self.formdisabled = true
				}
			},

			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodalemployee',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditmonthlyincentives',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},

			openmodal: function(a,b){
				console.log(b)
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						console.log(data)
						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
							data : data,
							headers : {	'Content-Type' :'application/json' }
						}).then(function(res){
							console.log(res)
							self.adddata(res.data.data[0])
							logger.logSuccess("Adding Incentive Allowance Success");
							self.getdataAllowance()
						},function(res){
							res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						});
						break;
						case 'edit':
						logger.logSuccess('Saving Success');
						self.tmpSavingDatas(data.b)
						// this.maindata = tmpDatasFactory.tmpDataAll(data.b)
						// console.log(this.maindata)

						// $http({
						// 	method : 'POST',
						// 			// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
						// 			url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
						// 			data : data.b
						// 		}).then(function(res){
						// 			self.update(res.data.data)
						// 			self.getdataAllowance()
						// 		},function(res){
						// 			res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
						// 		});			
						break;
					}
				});
			},
			tmpSavingDatas: function(a){
				x = this.maindata
				y = a
				for (var i = 0; i < x.length; i++) {
					if (x[i].id === y.id) {
						x[i] = y
					}
				}


				return this.maindata = x
			},

			cutOffData:function(){
				x = this.maindata

				for (var i = 0; i < x.length; i++) {
					
					newObj = {
						"employee_id": x[i].employee_id,
						"department_id": x[i].department_id,
						"job": x[i].job_id,
						"cutoff_date": x[i].per_cutoff_date,
						"month": x[i].month,
						"year": x[i].year,
						"tar": x[i].tar,
						"abs": x[i].abs,
						"disciplin": x[i].disciplin,
						"monthly_incentive": x[i].monthly_incentive,
						"deduction_monthly": x[i].deductions_monthly,
						"food_incentive": x[i].food_incentive,
						"deduction_food": x[i].deductions_food,
						"adjusment": x[i].adjustment_payroll,
						"total": x[i].total
					}

					$http({
						method :'POST',
						url : ApiURL.url+'/payroll/monthly_incentive_allow/cutoff',
						data : newObj,
						headers: {
							"Content-Type": "application/json"
						}
					}).success(function(res){
						logger.logSuccess("Cut Off Success")
					}).error(function(res){
						logger.logError("Cut Off Failed")

					})
				}

				
			},

			generateExcel(a){

				newObj = {
					"full_name" : a.Name,
					"employee_id": a.employee_id,
					"department_id": a.department_id,
					"job": a.job_id,
					"cutoff_date": a.per_cutoff_date,
					"month": a.month,
					"year": a.year,
					"tar": a.tar,
					"abs": a.abs,
					"disciplin": a.disciplin,
					"monthly_incentive": a.monthly_incentive,
					"deduction_monthly": a.deductions_monthly,
					"food_incentive": a.food_incentive,
					"deduction_food": a.deductions_food,
					"adjusment": a.adjustment_payroll,
					"total": a.total,
					"total_gross" : a.monthly_incentive + a.food_incentive 
				}

				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/monthly_incentive_allow/generate_excel',
					data : newObj
				}).success(function(res){
					logger.logSuccess('Download File success!')
				}).error(function(res){
					logger.logError('Download File Failed')
				})

			}





		}
	})


.factory('superCache', ['$cacheFactory', function($cacheFactory) {
	return $cacheFactory('super-cache');
}])

/** MAIN CONTROLLER **/  
.controller('monthIncenAllowCtrl',function(monthIncenAllowFactory,ApiURL,$cookieStore,$scope,$http,$filter,GetName,superCache){
	var self = this;
	self.handler = monthIncenAllowFactory;
	self.handler.getaccess()
	self.handler.setdata([])

})

/** MAIN CONTROLLER **/  
.controller('yearlyBonusCtrl',function(yearlyBonusFactory,ApiURL,$cookieStore,$scope,$http,$filter,GetName,superCache){
	var self = this;
	self.handler = yearlyBonusFactory;
	self.handler.monthPay13=true
	self.handler.getaccess()
	self.handler.setdata([])

})
.controller('modaleditmonthlyincentives',function(yearlyBonusFactory,$modalInstance,data){	




	var self = this;
	self.handler = yearlyBonusFactory;
	self.handler.button();
	self.form = data;

	
	self.disabledButton1 = function(){
		if (self.form.monthly_incentive < self.form.deductions_monthly) {

			return ((self.disabledbuttonsave = true) && (self.showErrorCompare1 = true))
		}
		if (self.form.monthly_incentive > self.form.deductions_monthly || self.form.monthly_incentive == self.form.deductions_monthly ) {


			self.disabledbuttonsave = false
			self.showErrorCompare1 = false
			// self.form.total = (self.form.monthly_incentive - self.form.deductions_monthly) + (self.form.food_incentive - self.form.deductions_food) +  self.form.adjustment_payroll
			console.log(self.form.total, self.form.adjustment_payroll)
			// self.form.total = (self.form.monthly_incentive - self.form.deductions_monthly) +  ( self.form.food_incentive - self.form.deductions_food ) + self.form.adjusment
		}



	}
	self.disabledButton2 = function(){
		if ( self.form.food_incentive < self.form.deductions_food ) {


			self.disabledbuttonsave = true
			self.showErrorCompare2 = true

		}
		if ( self.form.food_incentive > self.form.deductions_food || self.form.food_incentive == self.form.deductions_food ) {
			
			// self.form.total = (self.form.monthly_incentive - self.form.deductions_monthly) + (self.form.food_incentive - self.form.deductions_food) +  self.form.adjustment_payroll


			self.disabledbuttonsave = false
			self.showErrorCompare2 = false
		}
	}

	self.calculate=function(){
		self.form.total = (self.form.monthly_incentive - self.form.deductions_monthly) + (self.form.food_incentive - self.form.deductions_food) +  self.form.adjustment_payroll
		
	}


	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})







.run(function(monthIncenAllowFactory){
	monthIncenAllowFactory.setheaders();
})

}())


