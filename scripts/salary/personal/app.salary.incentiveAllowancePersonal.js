(function(){

	angular.module('app.salary.incentiveAllowancePersonal',['checklist-model'])

	.factory('personalIncentiveAndAllowanceFactory',function($routeParams,pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,personalIncentiveAndAllowanceHistoryFactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.title =arguments[i][0];
				x.amount =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Incentive Allowance Name','Amount',50]
					);
				this.setactiveheader(this.headers[0],false)
			},
			checkId :function(){
				if ($routeParams.id == undefined || $routeParams.id == null) {
					return $cookieStore.get('NotUser')
				}else{
					return $routeParams.id
				}
			},
			getdataDropdown:function(){
				// y = this.maindata
				$http({
					url : ApiURL.url + '/payroll/incentive_allowance_config/get/0' ,
					method: 'GET'
				}).success(function(res){
					console.log(res)
					self.setdataDropdown(res.data)
					logger.logSuccess("View Data Success");
					
				}).error(function(res){
					logger.logError("Show Data Failed")

				})
				

			},setdataDropdown:function(x){
				// console.log(x)
				this.dataDropdown = x
			},
			showDifferentDatas : function(){
				x = this.dataDropdown	
				y = this.maindata
				console.log(x,y,"=========================")
				// console.log(x,y)
				if (y == null || y == undefined) {
					y = [{}]
				}
				var final = x.filter(function(e){
					return (y.findIndex(xx=>xx.id_incentive == e.id )) == -1
				})
				console.log(final)
				this.dropdown = final
			},
			getdataAllowance:function(){

				// x = $cookieStore.get('employee_id')
				x = self.checkId()
				datas = {employee_id : x}
				$http({
					method: 'POST',
					url : ApiURL.url + '/payroll/personal_incentive_allowance/get'  ,
					data : datas,
					headers: {
						"Content-Type": "application/json"
					}

				}).success(function(res){


					self.setdata(res.data)
					logger.logSuccess("Show Incentive Allowance Success")

				}).error(function(res){
					logger.logError("Show Incentive Allowance Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					// url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
					console.log(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
				// console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
				
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodalIncen',
					controller: 'addModalIncentive',
					controllerAs: 'addmodalincen',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},

				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditemployee',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						
						// x = $cookieStore.get('employee_id')
						x = self.checkId()

						datas = {
							employee_id : x,
							id_incentive_allowance : data.id_incentive_allowance
						}
						console.log(datas)
						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/personal_incentive_allowance/insert',
							data : datas,
							headers: {
								"Content-Type": "application/json"
							}

						}).success(function(res){
							logger.logSuccess("Saving Incentive Allowance Success");
							self.getdataAllowance()
						}).error(function(res){
							logger.logError("Adding Incentive Allowance Failed")

						})
						// $http({
						// 	method : 'POST',
						// 	url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
						// 	data : data,
						// 	headers : {	'Content-Type' :'application/json' }
						// }).then(function(res){
						// 	console.log(res)
						// 	self.adddata(res.data.data[0])
						// 	logger.logSuccess("Adding Incentive Allowance Success");
						// 	self.getdataAllowance()
						// },function(res){
						// 	res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						// });
						break;
						case 'edit':
						$http({
							method : 'POST',
									// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.message);
									self.getdataAllowance()
								},function(res){
									res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
								});			
								break;
							}
						});
			},
			deldata: function(id){

				for (var i = 0; i < id.length; i++) {

					$http({
					// url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
					url : ApiURL.url + '/payroll/personal_incentive_allowance/delete/',
					method: 'POST',
					data : {id:id[i]},
					headers: {
						"Content-Type": "application/json"
					}

				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Success');
					self.getdataAllowance()
					personalIncentiveAndAllowanceHistoryFactory.getdataAllowance()
				},function(res){

					logger.logError("Deleting Failed")
				})
			}
			
		},

	}
})

// HISTORY PERSONAL INCENTIVE ALLOWANCE

.factory('personalIncentiveAndAllowanceHistoryFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,$routeParams){
	function createheaders(){
		var temp = [];
		for(var i=0;i<arguments.length;i++){
			var x = {};
			x.id = i;
			x.changed_date = arguments[i][0]
			x.title =arguments[i][1];
			x.amount =arguments[i][2];
			x.width =arguments[i][3] ? arguments[i][3]+"%" :'auto';
			x.order =false;
			temp.push(x);
		}
		return temp;	
	}
	var self;
	return self = {
		setheaders: function(){
			this.headers = new createheaders(
				['Changed Date', 'Incentive Allowance Name','Amount',33]
				);
			this.setactiveheader(this.headers[0],false)
		},
		checkId :function(){
			if ($routeParams.id == undefined || $routeParams.id == null) {
				return $cookieStore.get('NotUser')
			}else{
				return $routeParams.id
			}
		},
		getdataAllowance:function(){
			// x = $cookieStore.get('employee_id')
			x = self.checkId()
			
			datas = {employee_id : x}
			$http({
				method: 'POST',
				url : ApiURL.url + '/payroll/personal_incentive_allowance_history/get'  ,
				data : datas,
				headers: {
					"Content-Type": "application/json"
				}

			}).success(function(res){


				self.setdata(res.data)
				logger.logSuccess("Show Incentive Allowance Success")

			}).error(function(res){
				logger.logError("Show Incentive Allowance Failed")
			})
		},getpermission:function(active){
			if(active){
				this.mainaccess = active;
			}
		},button:function(a){
			if(this.mainaccess){
				switch(a){
					case 'create':
					if(this.mainaccess.create == 1){return true}
						break;
					case 'delete':
					if(this.mainaccess.delete == 1){return true}		
						break;
					case 'update':
					if(this.mainaccess.update == 1){return true}		
						break;
				}
			}
		},callback:function(){
			$http({
				method : 'GET',
					// url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
					console.log(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
				// console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
				
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodalIncen',
					controller: 'addModalIncentive',
					controllerAs: 'addmodalincen',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},

				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditemployee',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						// console.log("xxxxxx")
						// x = $cookieStore.get('employee_id')
						// console.log(x)

						// datas = {
						// 	employee_id : x,
						// 	// id_incentive_allowance
						// }

						// $http({
						// 	method : 'POST',
						// 	url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
						// 	data : data,
						// 	headers : {	'Content-Type' :'application/json' }
						// }).then(function(res){
						// 	console.log(res)
						// 	self.adddata(res.data.data[0])
						// 	logger.logSuccess("Adding Incentive Allowance Success");
						// 	self.getdataAllowance()
						// },function(res){
						// 	res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						// });
						break;
						case 'edit':
						$http({
							method : 'POST',
									// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.message);
									self.getdataAllowance()
								},function(res){
									res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
								});			
								break;
							}
						});
			},
			deldata: function(id){
				// $http({
				// 	// url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
				// 	url : ApiURL.url + '/payroll/personal_incentive_allowance_history/delete' + id,
				// 	method: 'GET'
				// }).then(function(res){
				// 	var i;
				// 	for (i = 0; i < id.length; ++i) {
				// 		self.maindata = filterFilter(self.maindata, function (store) {
				// 			return store.id != id[i];
				// 		})
				// 	}
				// 	self.check = [];
				// 	logger.logSuccess('Deleting Success');
				// 	self.getdataAllowance()
				// },function(res){
				// 	console.log(res.data.header)
				// 	res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Failed")
				// })
				

				for (var i = 0; i < id.length; i++) {

					$http({
					// url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
					url : ApiURL.url + '/payroll/personal_incentive_allowance_history/delete/',
					method: 'POST',
					data : {id:id[i]},
					headers: {
						"Content-Type": "application/json"
					}

				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Success');
					self.getdataAllowance()
					personalIncentiveAndAllowanceHistoryFactory.getdataAllowance()
				},function(res){

					logger.logError("Deleting Failed")
				})
			}

		},


	}
})

.controller('personalIncentiveAndAllowanceCtrl',function(personalIncentiveAndAllowanceFactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = personalIncentiveAndAllowanceFactory;
	self.handler.getdataAllowance()
	self.handler.getdataDropdown()
})


.controller('personalIncentiveAndAllowanceHistoryCtrl',function(personalIncentiveAndAllowanceHistoryFactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = personalIncentiveAndAllowanceHistoryFactory;
	self.handler.getdataAllowance()
})



.controller('addModalIncentive',function(personalIncentiveAndAllowanceFactory,$modalInstance,data){	
	var self = this;
	
	self.handler = personalIncentiveAndAllowanceFactory
	// self.handler.getdataDropdown()
	self.handler.showDifferentDatas()

	// self.datadropdownfinal = self.handler.
	self.changeAmount = function(){
		x = self.handler.dataDropdown
		y = self.form.id_incentive_allowance
		
		dataChoose = x.find(function(a){
			return a.id == y
		})
		// console.log(dataChoose)
		self.form.amount = dataChoose.amount
	}

	self.save = function(){
		$modalInstance.close(self.form);
	}

	self.test = function(){
		$modalInstance.dismiss('close')
		// console.log/("LOL")
	}
})


.controller('modaleditemployee',function(personalIncentiveAndAllowanceFactory,$modalInstance,data){	
	console.log("test")
	var self = this;
	self.handler = personalIncentiveAndAllowanceFactory;
	self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(personalIncentiveAndAllowanceFactory){
	personalIncentiveAndAllowanceFactory.setheaders();
})
.run(function(personalIncentiveAndAllowanceHistoryFactory){
	personalIncentiveAndAllowanceHistoryFactory.setheaders();
})

}())