(function(){

	angular.module('app.salary.salaryNewProfile',[])
	
	.factory('salaryNewfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,$routeParams,$window,Upload){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			reverse : function(){
				console.log("1");
				if(self.hdmfhide == false){
					self.hdmfhide = true
				}
				if(self.hmohide == false){
					self.hmohide = true
				}
				if(self.ssshide == false){
					self.ssshide = true
				}

			},
			/** table salary history **/
			// setheaders: function(){
			// 	this.headers = new createheaders(
			// 		['Information','',null],
			// 		['SSS Loan','',null],
			// 		['HDMF Loan','',null],
			// 		['HMO','',null],


			// 		);
			// 	this.setactiveheader(this.headers[0],false)
			// },
			setheaders: function(){
				this.headers = new createheaders(
					['Changed Date','changed_date',null],
					['Basic','basic',null],
					['Ecola','ecola',null],
					['CutOff Salary','cutoff_salary',null],
					['CutOff Ecola','cutoff_ecola',null],
					['Daily Salary','daily_salary',null],
					['Daily Ecola','daily_ecola',null],
					['Hourly Salary','hourly_salary',null],
					['Hourly Ecola','hourly_ecola',null],
					['Minutely Salary','minutely_salary',null],
					['Minutely Ecola','minutely_ecola',null],
					['ATM Card No','atm_card_no',null],
					['ATM Account No','atm_account_no',null],
					['Net Gross','net_gross',null],
					['Tax Status','tax_status',null],
					['Effective Date','effective_date',null],


					);
				this.setactiveheader(this.headers[0],false)
			},

			checkId :function(){
				if ($routeParams.id == undefined || $routeParams.id == null) {
					return $cookieStore.get('NotUser')
				}else{
					return $routeParams.id
				}
			},
			getdataDeduction:function(){
				newID = self.checkId()

				$http({
					method : 'GET',
					url: ApiURL.url + '/payroll/deduction_contributions/get/' + newID + '?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					const status = res.status
					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{
						// self.setdata(res.data.data)
						self.dataDefaultDeduction(res.data.data[0])
						logger.logSuccess("View Salary History Success");
					}
				})
			},getdatatable:function(){
				newID = self.checkId()

				$http({
					method : 'GET',
					url : ApiURL.url + '/payroll/payment_details_history/get/' + newID + '?key=' + $cookieStore.get('key_api') 
				}).then(function(res){
					const status = res.status
					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{
						self.setdata(res.data.data)
						// self.dataDefault(res.data.data[0])
						logger.logSuccess("View Salary History Success");
					}
				},function(res){
					logger.logError("Show Salary History Failed")
				});
			},
			getdatasalary:function(){
				newID = self.checkId()
				
				$http({
					method : 'GET',
					url : ApiURL.url + '/payroll/payment_details/get/' + newID + '?key=' + $cookieStore.get('key_api') 

					// url : 'http://hrmsapi2uat.leekie.com:8080/payroll/payment_details/get/2016015'
					
				}).success(function(res){

						// self.setdata(res.data.data)
						console.log(res.data,"----------------")
						if (res.data == null) {
							logger.logError("Data is Empty");
							return self.dataDefault([])
							
						}else{

							self.dataDefault(res.data[0])
							logger.logSuccess("View Data Success");
						}

					}).error(function(res){
						logger.logError("Show Data Salary Failed")

					})

				// then(function(res){


				// 	const status = res.status

				// 	if (status !== 200) {
				// 		logger.logError("Access Unauthorized");
				// 	}else{

				// 		// self.setdata(res.data.data)
				// 		self.dataDefault(res.data.data[0])
				// 		logger.logSuccess("View Data Success");
				// 	}


				// },function(res){
				// 	logger.logError("Show Data Salary Failed")
				// });
			},	dataDefaultDeduction:function(data){
				
				// if (data == null || ) {
				// 	// statement
				// }
				if (data) {
					self.formDeduction = data


					if(self.formDeduction==null){
						self.formDeduction={};
					}
				}
			},

			dataDefault:function(data){
				if(data.length == 0){
					
					return self.form={} 
				}
				console.log(data.length,"pppppppppppppppppp")
				if (data) {

					const dateSplit = data.effective_date
					const dateFinal = dateSplit.split("T")
					data.effective_date = dateFinal[0]

					console.log(data,"TTTTTTTTTTTTTTTTTTTTT")
					self.form = data

					if(self.form==null){
						self.form={};
					}
				}
			},
			getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},hiden:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}
							break;
					}
				}
			},save:function(datas){
				// console.log(datas.expat_status)

				x = this.finalStatusExpatLocal
				// return console.log(x,"++++++++++++++++++++=")
				datas.effective_date = new Date(datas.effective_date);
				dateX = datas.effective_date.getDate()
				monthX = datas.effective_date.getMonth()+1
				yearX = datas.effective_date.getFullYear()

				monthXX = monthX.toString()
				dateXX = dateX.toString()		
				if (monthXX.length < 2) {
					monthX= "0" + monthXX
				}
				if (dateXX.length < 2) {
					dateX= "0" + dateXX
				}
				console.log(monthX,dateX)
				datas.effective_date = yearX + "-" + monthX + "-" + dateX


				x = datas.effective_date.split("T")
				datas.effective_date = x[0]


				if (datas.employee_id==null || datas.employee_id==undefined ) {
				// 	datas.employee_id = $cookieStore.get('NotUser')
				// newID = 
				datas.employee_id = self.checkId()


			}



			delete datas.created_at
			delete datas.id

			console.log(datas)
			$http({
				method : 'POST',
				url : ApiURL.url + '/payroll/payment_details/insert?key=' + $cookieStore.get('key_api') ,
					// url: 'http://hrmsapi2uat.leekie.com:8080/payroll/payment_details/insert/', 
					data : datas,
					headers: {
						"Content-Type": "application/json"
					}

				}).success(function(res){
					self.setdata(res.data)
					logger.logSuccess("View Salary History Success");
					self.getdatatable()
				}).error(function(res){
					logger.logError("View Salary History Failed");

				})

				// then(function(res){
				// 	console.log(res)
				// 	const status = res.status
				// 	if (status !== 200) {
				// 		logger.logError("Access Unauthorized");

				// 	}else{
				// 		self.setdata(res.data.data)
				// 		logger.logSuccess("View Salary History Success");
				// 		self.getdatatable()
				// 	}

				// })

				

			},
			saveDeduction:function(datas){
				console.log(datas)
				// if (datas.id == null) {
				// }	
				delete datas.id
				delete datas.created_at
				delete datas.updated_at
				delete datas.active

				datas.employee_id = self.checkId()
				console.log(datas)
				$http({
					method: 'POST',
					// url: 'http://hrmsapi2uat.leekie.com:8080/payroll/deduction_contributions/update',
					url: ApiURL.url + '/payroll/deduction_contributions/update/?key=' + $cookieStore.get('key_api'),
					data : datas,
					headers: {
						"Content-Type": "application/json"
					}
				}).success(function(res){
					// logger.logError("Saving Failed");

					logger.logSuccess("Saving Success");
				}).error(function(res){
					logger.logError("Saving Failed");
					
				})

				// then(function(res){
				// 	const status = res.status
				// 	if (status !== 200) {
				// 		logger.logError("Saving Failed");
				// 	}else{

				// 		logger.logSuccess("Saving Success");
				// 	}
				// })
			},
			setfalsebutton:function(){
				self.button=false;
				self.hdmf=false;
				self.sss=false;
				self.hmo=false;	
			},
			adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				if(a == null || a == undefined || a == ""){
					a = []
				}
				else {
					a.forEach((x)=>{
						if (x.net_gross == 0) {
							x.net_gross = "Gross Pay"
						}
						if (x.net_gross == 1) {
							x.net_gross = "Net Pay"
						}
						if (x.tax_status == 1) {
							x.tax_status = "Single"
						}if (x.tax_status == 2) {
							x.tax_status = "Married"
						}if (x.tax_status == 3) {
							x.tax_status = "Single w/ 1 Dependent"
						}if (x.tax_status == 4) {
							x.tax_status = "Single w/ 2 Dependent"
						}if (x.tax_status == 5) {
							x.tax_status = "Single w/ 3 Dependent"
						}if (x.tax_status == 6) {
							x.tax_status = "Single w/ 4 Dependent"
						}if (x.tax_status == 7) {
							x.tax_status = "Married w/ 1 Dependent"
						}if (x.tax_status == 8) {
							x.tax_status = "Married w/ 2 Dependent"
						}if (x.tax_status == 9) {
							x.tax_status = "Married w/ 3 Dependent"
						}if (x.tax_status == 10) {
							x.tax_status = "Married w/ 4 Dependent"
						}
					// console.log(x,"hihihihi")
				})
				}
				
				// console.log(a)
				this.maindata = a;
				// console.log(a,)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},deldatahistory: function(id){

				for (var i = 0; i < id.length; i++) {
					$http({

					// url: 'http://hrmsapi2uat.leekie.com:8080/payroll/payment_details_history/delete/' + id,
					url : ApiURL.url + '/payroll/payment_details_history/delete/' + id[i] + '?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).success(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.deductionId != id[i];
						})
					}
					self.check = [];
					// self.getdatatable()
					self.getdatatable()
					logger.logSuccess('Deleting Salary History Success');
				}).error(function(res){
					logger.logError("Deleting   Salary History Failed")

				})

				// 	then(function(res){
				// 	var i;
				// 	for (i = 0; i < id.length; ++i) {
				// 		self.maindata = filterFilter(self.maindata, function (store) {
				// 			return store.deductionId != id[i];
				// 		})
				// 	}
				// 	self.check = [];
				// 	// self.getdatatable()
				// 	self.getdatatable()
				// 	logger.logSuccess('Deleting Salary History Success');
				// },function(res){
				// 	logger.logError("Deleting   Salary History Failed")
				// })
			}

		},
		buttonCtrl:function(input){
				// console.log(input)
				if(input=='edit'){
					if ((self.saveButton=true)&&(self.cancelButton=true)) {
						return ((self.editButton=false))
					}
					
				}
				if (input=='save' || input=='cancel') {
					if (self.editButton=true) {
						return ((self.saveButton=false)||(self.cancelButton=false) )
					}
					
					
				}
			},
			buttonCtrlDeduction:function(input){
				// console.log(input)
				if(input=='edit'){
					if ((self.saveButtonDeduction=true)&&(self.cancelButtonDeduction=true)) {
						return ((self.editButtonDeduction=false))
					}
					
				}
				if (input=='save' || input=='cancel') {
					if (self.editButtonDeduction=true) {
						return ((self.saveButtonDeduction=false)||(self.cancelButtonDeduction=false) )
					}
					
					
				}
			},
			sumAuto:function(){
				if (self.form.basic == null ) {
					self.form.basic = 0
				}
				if (self.form.ecola == null ) {
					self.form.ecola = 0
				}
				self.form.total = self.form.basic + self.form.ecola
				x = self.form.total
				self.form.total = x.toFixed(2)
				self.form.total = Number(self.form.total)
				
				// console.log(self.form.total)
			},
			sumAutoCutOff:function(){
				if (self.form.cutoff_salary == null ) {
					self.form.cutoff_salary = 0
				}
				if (self.form.cutoff_ecola == null ) {
					self.form.cutoff_ecola = 0
				}
				self.form.cutoff_total = self.form.cutoff_salary + self.form.cutoff_ecola
				x = self.form.cutoff_total
				self.form.cutoff_total = x.toFixed(2)
				self.form.cutoff_total = Number(self.form.cutoff_total)

				// console.log(self.form.cutoff_total)

			},
			sumAutoDaily:function(){
				if (self.form.daily_salary == null ) {
					self.form.daily_salary = 0
				}
				if (self.form.daily_ecola == null ) {
					self.form.daily_ecola = 0
				}
				self.form.daily_total = self.form.daily_salary + self.form.daily_ecola
				x = self.form.daily_total
				

				self.form.daily_total = x.toFixed(2)
				self.form.daily_total = Number(self.form.daily_total)

				// console.log(self.form.daily_total)

			},
			sumAutoHourly:function(){
				if (self.form.hourly_salary == null ) {
					self.form.hourly_salary = 0
				}
				if (self.form.hourly_salary == null ) {
					self.form.hourly_salary = 0
				}
				self.form.hourly_total = self.form.hourly_salary + self.form.hourly_ecola
				x = self.form.hourly_total
				// console.log(x)

				self.form.hourly_total = x.toFixed(2)
				self.form.hourly_total = Number(self.form.hourly_total)

				// console.log(self.form.hourly_total)

			},
			sumAutoMinutely:function(){
				if (self.form.minutely_salary == null ) {
					self.form.minutely_salary = 0
				}
				if (self.form.minutely_ecola == null ) {
					self.form.minutely_ecola = 0
				}
				self.form.minutely_total = self.form.minutely_salary + self.form.minutely_ecola
				x = self.form.minutely_total
				
				self.form.minutely_total = x.toFixed(2)
				self.form.minutely_total = Number(self.form.minutely_total)

				// console.log(self.form.minutely_total)

			},
			getStatusExpatLocal: function(){
				x = self.checkId()
				$http({
					url : ApiURL.url + '/api/employee-details/personal/' + x + '?key=' + $cookieStore.get('key_api'),
					method : 'GET'
				}).success(function(res){
					// console.log(res,"")
					// console.log(res.data.local_it,"lllllllllllllllllllllllllll")
					self.hideExpatLocal(res.data.local_it)
					logger.logSuccess('Get Status Success!')
					
				}).error(function(res){
					logger.logError('Get Status Failed!')
					// self.getStatusExpatLocal()
				})
			},
			statusExpatLocal:function(x){
				// console.log(x,"oooooooooooooooooooooooooo")
				this.finalStatusExpatLocal = x
			},
			hideExpatLocal:function(a){
				// console.log(x)
				x = a
				
				if (x == "LOCAL" || x == "LOCAL IT") {
					self.hideSalaryLocal = true
					self.hideSalaryExpat = false
				}
				if (x == "EXPAT") {
					self.hideSalaryLocal = false
					self.hideSalaryExpat = true	
				}
			},
			getTaxStatus : function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/payroll/tax_status_get'
				}).success(function(res){
					logger.logSuccess("Get Data Success")
					self.dataTaxStatus(res.data)
				}).error(function(res){
					logger.logError('Get Data Failed')
				})
			},
			dataTaxStatus:function(x){
				// console.log(x)
				this.mainDataTaxStatus = x
			}

		}
	})

.controller('salaryNewCtrl',function(salaryNewfactory,$scope,$interval,$timeout){
	var self = this;
	self.handler = salaryNewfactory;
	// self.handler.getData()


	$timeout(function(){
		self.handler.getStatusExpatLocal()
		// self.handler.hideExpatLocal()
	},10)

	$timeout(function(){
		// self.handler.hideExpatLocal()
		self.handler.getdatasalary();
	},20)
	
	$timeout(function(){
		// self.handler.hideExpatLocal()
		self.handler.getdatatable()
		
	},30)
	
	$timeout(function(){
		// self.handler.hideExpatLocal()
		self.handler.getdataDeduction()
		
	},40)
	
	$timeout(function(){
		// self.handler.hideExpatLocal()
		self.handler.getTaxStatus()
		
	},50)
	
	

	////////////////////////////
	self.handler.editButtonDeduction=true
	self.handler.editButton=true 
	$scope.effective_date_try = new Date(2013,1,1)


})

.run(function(salaryNewfactory){
	salaryNewfactory.setheaders();
})

/** directive format input currency**/
.directive('format',function($filter){
	return {
		require: '?ngModel',
		link: function (scope,elem,attrs,ctrl){
			if (!ctrl) return;
			var format = {
				prefix: '',
				centsSeparator: '.',
				thousandsSeparator: ','
			};
			ctrl.$parsers.unshift(function(value){
				elem.priceFormat(format);

				return elem[0].value;
			});
			ctrl.$formatters.unshift(function (value){
				elem[0].value = ctrl.$modelValue * 100 ;
				elem.priceFormat(format);
				return elem[0].value;
			})
		}
	};
});
(function($){$.fn.priceFormat=function(options){var defaults={prefix:'US$ ',suffix:'',centsSeparator:',',thousandsSeparator:'.',limit:false,centsLimit:2,clearPrefix:false,clearSufix:false,allowNegative:false,insertPlusSign:false};var options=$.extend(defaults,options);return this.each(function(){var obj=$(this);var is_number=/[0-9]/;var prefix=options.prefix;var suffix=options.suffix;var centsSeparator=options.centsSeparator;var thousandsSeparator=options.thousandsSeparator;var limit=options.limit;var centsLimit=options.centsLimit;var clearPrefix=options.clearPrefix;var clearSuffix=options.clearSuffix;var allowNegative=options.allowNegative;var insertPlusSign=options.insertPlusSign;if(insertPlusSign)allowNegative=true;function to_numbers(str){var formatted='';for(var i=0;i<(str.length);i++){char_=str.charAt(i);if(formatted.length==0&&char_==0)char_=false;if(char_&&char_.match(is_number)){if(limit){if(formatted.length<limit)formatted=formatted+char_}else{formatted=formatted+char_}}}return formatted}function fill_with_zeroes(str){while(str.length<(centsLimit+1))str='0'+str;return str}function price_format(str){var formatted=fill_with_zeroes(to_numbers(str));var thousandsFormatted='';var thousandsCount=0;if(centsLimit==0){centsSeparator="";centsVal=""}var centsVal=formatted.substr(formatted.length-centsLimit,centsLimit);var integerVal=formatted.substr(0,formatted.length-centsLimit);formatted=(centsLimit==0)?integerVal:integerVal+centsSeparator+centsVal;if(thousandsSeparator||$.trim(thousandsSeparator)!=""){for(var j=integerVal.length;j>0;j--){char_=integerVal.substr(j-1,1);thousandsCount++;if(thousandsCount%3==0)char_=thousandsSeparator+char_;thousandsFormatted=char_+thousandsFormatted}if(thousandsFormatted.substr(0,1)==thousandsSeparator)thousandsFormatted=thousandsFormatted.substring(1,thousandsFormatted.length);formatted=(centsLimit==0)?thousandsFormatted:thousandsFormatted+centsSeparator+centsVal}if(allowNegative&&(integerVal!=0||centsVal!=0)){if(str.indexOf('-')!=-1&&str.indexOf('+')<str.indexOf('-')){formatted='-'+formatted}else{if(!insertPlusSign)formatted=''+formatted;else formatted='+'+formatted}}if(prefix)formatted=prefix+formatted;if(suffix)formatted=formatted+suffix;return formatted}function key_check(e){var code=(e.keyCode?e.keyCode:e.which);var typed=String.fromCharCode(code);var functional=false;var str=obj.val();var newValue=price_format(str+typed);if((code>=48&&code<=57)||(code>=96&&code<=105))functional=true;if(code==8)functional=true;if(code==9)functional=true;if(code==13)functional=true;if(code==46)functional=true;if(code==37)functional=true;if(code==39)functional=true;if(allowNegative&&(code==189||code==109))functional=true;if(insertPlusSign&&(code==187||code==107))functional=true;if(!functional){e.preventDefault();e.stopPropagation();if(str!=newValue)obj.val(newValue)}}function price_it(){var str=obj.val();var price=price_format(str);if(str!=price)obj.val(price)}function add_prefix(){var val=obj.val();obj.val(prefix+val)}function add_suffix(){var val=obj.val();obj.val(val+suffix)}function clear_prefix(){if($.trim(prefix)!=''&&clearPrefix){var array=obj.val().split(prefix);obj.val(array[1])}}function clear_suffix(){if($.trim(suffix)!=''&&clearSuffix){var array=obj.val().split(suffix);obj.val(array[0])}}$(this).bind('keydown.price_format',key_check);$(this).bind('keyup.price_format',price_it);$(this).bind('focusout.price_format',price_it);if(clearPrefix){$(this).bind('focusout.price_format',function(){clear_prefix()});$(this).bind('focusin.price_format',function(){add_prefix()})}if(clearSuffix){$(this).bind('focusout.price_format',function(){clear_suffix()});$(this).bind('focusin.price_format',function(){add_suffix()})}if($(this).val().length>0){price_it();clear_prefix();clear_suffix()}})};$.fn.unpriceFormat=function(){return $(this).unbind(".price_format")};$.fn.unmask=function(){var field=$(this).val();var result="";for(var f in field){if(!isNaN(field[f])||field[f]=="-")result+=field[f]}return result}})(jQuery);

}())

