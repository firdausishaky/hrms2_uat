(function(){
	
	angular.module("app.salary.performanceBonusYear",['ngFileUpload'])
	
	.factory('perBonusYearFactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,Upload,tmpDatasFactory){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var NAME = '_schedule';
		var s = localStorage.getItem(NAME);
		var items = s ? JSON.parse(s) : {};
		var self;
		return self = {

			setheaders: function(){
				this.headers = new createheaders(
					['Employee Name','employee_name'],
					['Department','department'],
					['Monthly Salary','month_salary'],
					['Bonus Rate','bonus_rate'],
					['Deductions','deduction'],
					['Total','total'],
					['Hired Date','hired_date'],

					);
				this.setactiveheader(this.headers[0],false)	
			},adddata: function(a){
				this.maindata.push(a);
			},getaccess:function(){

				$http({
					method : 'GET',
					url : ApiURL.url + '/payroll/year_end_bonus_13th'
				}).then(function(res){
					console.log(res)
					if (res.status !==200 ) {
						logger.logError("Get Data Failed")
					}else{
						logger.logSuccess("Get Data Success")

						self.setdataDepartment(res.data.data[0].department)
						self.setdataYears(res.data.data[0].years)

					}


				});
			},setdataDepartment:function(x){
				this.maindataDepartment = x
			},
			setdataYears:function(x){
				this.years = x
			},
			
			getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},setform:function(data){
				if(data.status == 'user'){
					this.role = data.status;
					this.iduser = data.employee_id;
					this.nameuser = data.employee_name;
					self.form={};
					self.form.name = this.nameuser;
					self.hide = true;
				}else{
					this.role = data.status;
					self.form={};
					self.form.name = '';
					self.hide = false;
				}
				self.datastatus = data.status;
			},onSelect : function ($item,$model,$label){
				self.model = $item.name;
				self.label = $item.employee_id;

			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},getYear : function(res){
				var results = res.data.data;
				var getdata = results.length;
				var arr = [];
				var monthArr = [];
				if(getdata > 0){
					var i = 0;
					for(; i < getdata; i++){
						var sub = res.data.data[i].date.substr(0,7);
						var month = res.data.data[i].date.substr(5,2);
						var day = res.data.data[i].date.substr(8,2);

						var subday = day.substr(0,1);
						if(subday == 0){
							day = day.substr(1,1);
						}
						arr[sub] = day;
					}

					var getFinally = [];
					for(key in arr){
						getFinally.push(key+'-'+arr[key]);
					}

					var countFinally = getFinally.length;

					var etc = [{"min" :  getFinally[0]},{"max" :  getFinally[countFinally-1]}];
					return etc;

				}
			},dateEnd : function(data){
				//console.log(data);
				var dates = data.toString();
				var arr = [];
				var arr = dates.split(" ");
				if(arr[1] ==  "Jan"){ arr[1] = 1;}
				if(arr[1] ==  "Feb"){ arr[1] = 2;}
				if(arr[1] ==  "Mar"){ arr[1] = 3;}
				if(arr[1] ==  "Apr"){ arr[1] = 4;}
				if(arr[1] ==  "May"){ arr[1] = 5;}
				if(arr[1] ==  "Jun"){ arr[1] = 6;}
				if(arr[1] ==  "Jul"){ arr[1] = 7;}
				if(arr[1] ==  "Aug"){ arr[1] = 8;}
				if(arr[1] ==  "Sep"){ arr[1] = 9;}
				if(arr[1] ==  "Oct"){ arr[1] = 10;}
				if(arr[1] ==  "Nov"){ arr[1] = 11;}
				if(arr[1] ==  "Dec"){ arr[1] = 12;}
				
				var newdate = arr[3]+'-'+arr[1]+'-'+arr[2];
				//console.log(newdate);
				var result = self.getdateEnd
				//console.log(result);
				var count = result.length;
				var endSearch = [];
				if(count > 0){
					for(var i = 0; i < count; i++){
						
						if(result[i].date ==  newdate){
							endSearch =  result[i];
						}
					}
				}
				if(endSearch == null){
					res.data.header ? logger.logError("data not found") : logger.logError("Data not found")
				}else{
					self.setdata([endSearch]);
				}
				
			},next_paging : function(data,f){
				console.log("f", f);
				console.log("data", data);
				console.log("self.paging", self.paging);
				
				
				if(data > 2 && f  ==  'end' ){
					self.paging[0] = '...'
					self.paging[1] = self.default_paginate[data];

					if(self.default_paginate[data+1] >  self.default_paginate[data]){
						self.paging[2] =  self.default_paginate[data+1] 
					}else{
						delete self.paging[2];
					}
					
					self.paging[3] = '...'
				}else if(data ==  1 && f  == "end"){

				}else{

				}


			},
			setToday : function(datas,type){
				var counts = 1;
				var page = 1;
				var dt = this.maindata;
				console.log("setToday",datas,dt[0].date);
				for(var i in dt){
					if(type=='date'){
						if(dt[i].date == datas){
							//console.log("PAGING",i);
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;
						}
					}else if(type=="month"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getMonth()+1;
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}else if(type=="year"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getFullYear();
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}
				}
				self.Selectdate 	= datas;
				self.Selectdate_end = dt[dt.length-1].date;
				self.Selectdate_start = dt[0].date;
				
				if(page==0){ page++; }
				console.log(page,"PAGE");
				self.selectedPage 	= page;
				self.currentPage 	= self.selectedPage;
				//self.currentPage = 25;
			},
			clickable : 'pointer',
			action_save : false,
			select_month : function(month_date){
				self.action_save = month_date;
				return self.save();
			},
			select_monthnew : function(month_date){
				self.action_save = month_date;
				console.log(month_date,'+++++++++++++++ select_monthnew +++++++++++++++++++')
				return self.saveNew();
			},
			checkButtonYear:function(){
				console.log(self.form.years)

				// if (self.form.years ==undefined || self.form.years ==null || self.form.years=="") {
				// 	self.buttonDisabled = true;
				// }else{
				// 	self.buttonDisabled = false;	
				// }
			},

			checkButtonDepartment:function(){
				console.log(self.form.department)

				// if (self.form.department ==undefined || self.form.department ==null || self.form.department=="") {
				// 	self.buttonDisabled = true;
				// }else{
				// 	self.buttonDisabled = false;	
				// }
			},
			tmpSearch : function(a){
				this.dataSearching = a

			},


			findData:function(){
				console.log("performancce bonus year")

				a = tmpDatasFactory.maindata
				console.log(a)
				logger.logSuccess("View Data Success");
				self.setdata(a)

				// console.log(yearlyBonusFactory.newId)
				// a = yearlyBonusFactory.tmpDatas
				// if (a.department_id == undefined || a.department_id == null) {
				// 	a.department_id = 0
				// }
				// if (a.employee_id == undefined || a.employee_id == null || a.employee_id=="") {
				// 	a.employee_id = 0
				// }else{
				// 	a.employee_id = self.label
				// } 

				// a.years = Number(a.years)


				// const xx = yearlyBonusFactory.tmpDatas
				// self.tmpSearch(xx)


				// $http({
				// 	method: 'POST',
				// 	url : ApiURL.url + '/payroll/year_end_bonus_13th/get',
				// 	data : a
				// }).then(function(res){
				// 	console.log(res)
				// 	const status = res.status

				// 	if (status !== 200) {
				// 		logger.logError("Access Unauthorized");
				// 	}else{


				// 		self.setdata(res.data.data)
				// 		logger.logSuccess("View Data Success");
				// 	}

				// })



			},
			save : function(){
				const xx = yearlyBonusFactory.tmpDatas
				self.tmpSearch(xx)
				

				find = self.dataSearching
				console.log(find)

				datas = {
					employee_id: 0,
					department_id : 0,
					years : 2019,
					types: "year_end"
				}


				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/performance_bonus/get',
					data: datas,
					headers : {
						'Content-Type' : 'application/json'
					},
					data: self.form
				}).then(function(res){
					console.log(res)
					
					const status = res.status

					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						self.form.employee_id = self.model
						self.setdata(res.data.data)
						logger.logSuccess("View Data Success");
					}
				})
				
			},restore : function(){
				self.setdata(items);
			},setdata: function(a){
				
				for (var i = 0; i < a.length; i++) {
					a[i].sum_suspension = a[i].salary *a[i].sus_rate
					a[i].sum_maternity = a[i].salary *a[i].maternity
					a[i].sum_tardiness = a[i].salary* (a[i].tardiness/175200)

					/////////////////FIXED///////////////////
					a[i].sum_suspension = a[i].sum_suspension.toFixed(2)
					a[i].sum_maternity = a[i].sum_maternity.toFixed(2)
					a[i].sum_tardiness = a[i].sum_tardiness.toFixed(2)

					a[i].sum_suspension = Number(a[i].sum_suspension)
					a[i].sum_maternity = Number(a[i].sum_maternity)
					a[i].sum_tardiness = Number(a[i].sum_tardiness)

					// a[i].sum_total = a[i].sum_suspension +  a[i].sum_maternity + a[i].sum_tardiness
					

				}

				this.maindata = a;
				// console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},
			
			getActive : function(y){
				if(self.maindata){	
					while (true){
						if( y == 1 || y == 2 || y == 3){
							break;
						}
						y = y - 3;
					}
					return y;
				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodalemployee',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaleditPerformance',
					controller: 'modaleditPerBonus',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				},
				edit_bonus:{
					animation: true,
					templateUrl: 'modaleditPerformanceBonusRate',
					controller: 'modaleditPerBonusBonusRate',
					controllerAs: 'modaleditBonus',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},

			openmodal: function(a,b){
				console.log(b)
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						console.log(data)
						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
							data : data,
							headers : {	'Content-Type' :'application/json' }
						}).then(function(res){
							console.log(res)
							self.adddata(res.data.data[0])
							logger.logSuccess("Adding Incentive Allowance Success");
							self.getdataAllowance()
						},function(res){
							res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						});
						break;
						case 'edit':
						$http({
							method : 'POST',
									// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.message);
									self.getdataAllowance()
								},function(res){
									res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
								});			
								break;

								case 'edit_bonus' : 
								logger.logSuccess('Saving Success');
								this.maindata = tmpDatasFactory.tmpDataAll(data.b)
								break;
							}
						});
			},




		}
	})


.factory('superCache', ['$cacheFactory', function($cacheFactory) {
	return $cacheFactory('super-cache');
}])

/** MAIN CONTROLLER **/  
.controller('perBonusYearCtrl',function(perBonusYearFactory,ApiURL,$cookieStore,$scope,$http,$filter,GetName,superCache){
	console.log('xxxxxxx hehehe')
	var self = this;
	self.handler = perBonusYearFactory;
	self.handler.getaccess()
	self.handler.setdata([])

})
.controller('modaleditPerBonus',function(perBonusYearFactory,$modalInstance,data){	

	console.log("hahahah")
	var self = this;
	self.handler = perBonusYearFactory;
	self.handler.button();
	self.form = data;


	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})

.controller('modaleditPerBonusBonusRate',function(perBonusYearFactory,$modalInstance,data){	

	console.log("MODAL EDIT BONUS")
	var self = this;
	self.handler = perBonusYearFactory;
	self.handler.button();
	self.form = data;


	self.calculatePerformanceBonus = function(){

		console.log(self.form.performance, "xxx")
		console.log(data)
		if (self.form.performance==null || self.form.performance==undefined) {
			self.form.total = data.total
			self.form.performance = ""
			console.log("DATA KOSONG")
		}
		if (typeof(self.form.performance) == 'number' ) {
			if(self.form.performance < 0){
				logger.logError("Numbers Cannot Be Negative")
				console.log("angka harus positive")
			}if (self.form.performance >= 0) {

				console.log(self.form.performance,self.form.salary, "xxxxxxxxxxxxxxxxxxxxxx")
				self.form.total = (self.form.salary * self.form.performance) - self.form.deduction
			}
		}
		
	}
	

	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})






.run(function(perBonusYearFactory){
	perBonusYearFactory.setheaders();
})

}())


