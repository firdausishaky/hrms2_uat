


(function(){

	angular.module('app.salary.slips_salary',['checklist-model'])

	.factory('slips_salaryFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Employee Name','employee_name'],
					['Department','department'],
					['Slips Type','slips_type'],
		
					);
				this.setactiveheader(this.headers[0],false)
			},
			get_slips: function(){

				$http({
					method : 'GET',
					url : ApiURL.url + '/slips'
				}).success(function(res){
					self.tmpDataGetSlips(res.data)
					logger.logSuccess('Get data success')
				}).error(function(res){
					logger.logError('Get data failed')
				})


				$http({
					method : 'GET',
					url : ApiURL.url + '/cutoff_perpayroll?key=' + $cookieStore.get('key_api')
				}).success(function(res){
					logger.logSuccess("data cut off success")
					self.cutoffDate(res.data[0].cutoff_periode)
				}).error(function(res){
					logger.logError("cant't get data cut off")
				})

				// then(function(res){
				// 	const status = res.status

				// 	if (status !== 200) {
				// 		logger.logError("Access Unauthorized");
				// 	}else{
				// 		//console.log(res.data.data[0].cutoff_periode,"======================================================")
				// 		// self.setDataDateCutOff(res.data.data[0].cutoff_periode)
				// 		self.cutoffDate()
				// 		logger.logSuccess("View Data Success");
				// 	}
				// })

			},

			tmpDataGetSlips : function(x){
				self.setdataDepartment(x.department)
				self.slipsLocal(x.generate_type.local)
				self.slipsExpat(x.generate_type.expat)

			},
			slipsLocal : function(x){
				// console.log(x)

				this.slipsLocalData = x.dropdown
			},
			slipsExpat: function(x){
				// console.log(x.dropdown)
				this.slipsExpatData = x.dropdown
			},		
			getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},
			onSelect : function ($item,$model,$label){

				self.model = $item.name;
				self.label = $item.employee_id;
				self.getIDEmployee(self.label)

			},

			searchDatas: function(a,b,c){
				console.log(a,b,c)
				// console.log(self.handler.employee_id)

				if (a == null || a==undefined) {
					a = 0
				}
				if (b == null || b==undefined) {
					b = 0
				}
				if (c == null || c==undefined) {
					logger.logError('Please select Cut Off Periode')
				}

				b = b.toString()
				// console.log(self.form.employee_id)
				datas = {
					employee_id:a,
					department: b,
					cutoff_periode: c
				}
				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/monthly_expat_payroll/get',
					data : datas,
					headers: {
						"Content-Type": "application/json"
					}
				}).success(function(res){
					logger.logSuccess("Viewing Data Success");
					self.setdata(res.data)
						// console.log(res)
						
					}).error(function(res){
						logger.logError("Viewing Data Error");

					})
				},
				getDepartment:function(){
					$http({
						method : 'GET',
						url : ApiURL.url + '/payroll/year_end_bonus_13th'
					}).then(function(res){

						if (res.status !==200 ) {
							logger.logError("Get Data Failed")
						}else{
							logger.logSuccess("Get Data Success")
							// self.setdataDepartment(res.data.data[0].department)
							self.cutoffDate(res.data.data[0].payout)
						}
					});
				},
				cutoffDate:function(x){
					console.log(x)

					for (var i = 0; i < x.length; i++) {
						x[i].payout_date = x[i].payout_date.split("T")
						x[i].payout_date = x[i].payout_date[0]
					}

					this.cutoff_date = x

				},


				setdataDepartment:function(x){
					this.setdataDepartment = x
				},
				getdataAllowance:function(){
					$http({
						url : ApiURL.url + '/payroll/incentive_allowance_config/get/0' ,
						method: 'GET'
					}).then(function(res){
						console.log(res)
						const status = res.status

						if (status !== 200) {
							logger.logError("Access Unauthorized");
						}else{

							self.setdata(res.data.data)
							logger.logSuccess("View Data Success");
						}
					},function(res){
						res.data.header ? logger.logError(res.data.message) : logger.logError("Show Incentive Allowance Failed")
					})
				},getpermission:function(active){
					if(active){
						this.mainaccess = active;
					}
				},button:function(a){
					if(this.mainaccess){
						switch(a){
							case 'create':
							if(this.mainaccess.create == 1){return true}
								break;
							case 'delete':
							if(this.mainaccess.delete == 1){return true}		
								break;
							case 'update':
							if(this.mainaccess.update == 1){return true}		
								break;
						}
					}
				},callback:function(){
					$http({
						method : 'GET',
					// url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
					console.log(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
				console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
				
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodalemployee',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditemployee',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				console.log(b)
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						console.log(data)
						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
							data : data,
							headers : {	'Content-Type' :'application/json' }
						}).then(function(res){
							console.log(res)
							self.adddata(res.data.data[0])
							logger.logSuccess("Adding Incentive Allowance Success");
							self.getdataAllowance()
						},function(res){
							res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						});
						break;
						case 'edit':
						$http({
							method : 'POST',
									// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.message);
									self.getdataAllowance()
								},function(res){
									res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
								});			
								break;
							}
						});
			},
			deldata: function(id){
				$http({
					// url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
					url : ApiURL.url + '/payroll/incentive_allowance_config/delete/' + id,
					method: 'GET'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Success');
					self.getdataAllowance()
				},function(res){
					console.log(res.data.header)
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Failed")
				})
			},

			change_payroll_slips:function(){
				x = self.form.expat_local 
				// console.log(x)
				if (x == 'expat') {
					if (self.expat_dropdown = true) {}
						return self.local_dropdown = false
				}
				if (x == 'local') {
					if (self.local_dropdown = true) {
						return self.expat_dropdown = false

					}
					// self.local_drodown = true
				}
			},

			dropdownSlips : function(){
				$http({
					url: ApiURL.url + '/atm_adsive_credit',
					method : 'GET'
				}).success(function(res){
					console.log(res)
				}).error(function(res){
					logger.logError('Dropdown View Failed')
				})
			}

		}
	})

.controller('slips_salaryController',function(slips_salaryFactory,ApiURL,$cookieStore){
	console.log("xxxx")
	var self = this;
	self.handler = slips_salaryFactory;
	// self.handler.dropdownSlips()
	// self.handler.getdataAllowance()
	// self.handler.getDepartment()
	self.handler.get_slips()
})
.controller('addmodalemployee',function(slips_salaryFactory,$modalInstance,data){	

	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.controller('modaleditemployee',function(slips_salaryFactory,$modalInstance,data){	
	console.log("test")
	var self = this;
	self.handler = slips_salaryFactory;
	self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(slips_salaryFactory){
	slips_salaryFactory.setheaders();
})

}())