(function(){

	angular.module('app.dashboard.salary',[])
	
	.factory('salaryfactoryDashboard',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,$routeParams,$window,Upload){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		var self;
		return self = {
			reverse : function(){
				console.log("1");
				if(self.hdmfhide == false){
					self.hdmfhide = true
				}
				if(self.hmohide == false){
					self.hmohide = true
				}
				if(self.ssshide == false){
					self.ssshide = true
				}

			},getdatasaalary:function(){

				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/employee-details/manage-salary/emp/' +getID() + '?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data);
					// console.log(getID());
					var xdata = res.data.data;
					if(xdata.all_deduction.HDMFLoan == null){
						self.hdmfhide = true 
						console.log(self.hdmfhide);
					}else{
						self.hdmfhide = false
						console.log(self.hdmfhide);
					}
					if(xdata.all_deduction.HMO == null){
						self.hmohide = true
						console.log(self.hmohide);

					}else{
						self.hmohide = false
						console.log(self.hmohide);

					}
					if(xdata.all_deduction.SSSLoan == null){
						self.ssshide = true
						console.log(self.ssshide);
					}else{
						self.ssshide = false
						console.log(self.ssshide);
					}
					self.getpermission(res.data.header.access);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Salary Failed")
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},hiden:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}
							break;
					}
				}
			},setdata:function(data){
				if(data){
					/** set data form salary payment detail **/
					self.form = data.salary_payment;

					/** set data form salary payment detail **/
					self.formall = data.all_deduction;

					/** set data table salary history **/
					self.setdatatablehistory(data.salary_history);

				}
			},save:function(){
				console.log('test');
				if(self.hmohide == true){ self.hmohide =  false; }
				if(self.hdmfhide == true){ self.hdmfhide =  false; }
				if(self.ssshide == true){ self.ssshide =  false; }
				var postdata={};
				postdata.salary_payment=self.form;
				postdata.all_deduction=self.formall;
				var sent = postdata;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/employee-details/manage-salary/emp/store/' + getID() + '?key=' + $cookieStore.get('key_api'), 
					data :sent
				}).then(function(res){
					self.getdatasaalary()
					self.setfalsebutton()
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.setfalsebutton()
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Data Salary Failed")
				});					
			},setfalsebutton:function(){
				self.button=false;
				self.hdmf=false;
				self.sss=false;
				self.hmo=false;	
			},

			/** table salary history **/
			setheaders: function(){
				this.headers = new createheaders(
					['Change Date','dates',null],
					['Monthly Salary','salary',null],
					['Net/Gross','net_gross',null],
					['Effective Date','effective_date',null],
					['HMO','hmo',null],
					['SSS Loan','sss_loan',null],
					['HDM Loan','hdmf_loan',null]
					);
				this.setactiveheader(this.headers[0],false)
			},adddata: function(a){
				this.maindata.push(a);
			},setdatatablehistory: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},deldatahistory: function(id){
				$http({
					url : ApiURL.url + '/api/employee-details/manage-salary/emp/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.deductionId != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Salary History Success');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Deleting   Salary History Failed")
				})
			}
		}
	})

.controller('salaryCtrlDashboard',function(salaryfactoryDashboard){
	var self = this;
	self.handler = salaryfactoryDashboard;
	self.handler.getdatasaalary();
})

.run(function(salaryfactoryDashboard){
	salaryfactoryDashboard.setheaders();
})

/** directive format input currency**/
.directive('format',function($filter){
	return {
		require: '?ngModel',
		link: function (scope,elem,attrs,ctrl){
			if (!ctrl) return;
			var format = {
				prefix: '',
				centsSeparator: '.',
				thousandsSeparator: ','
			};
			ctrl.$parsers.unshift(function(value){
				elem.priceFormat(format);

				return elem[0].value;
			});
			ctrl.$formatters.unshift(function (value){
				elem[0].value = ctrl.$modelValue * 100 ;
				elem.priceFormat(format);
				return elem[0].value;
			})
		}
	};
});
(function($){$.fn.priceFormat=function(options){var defaults={prefix:'US$ ',suffix:'',centsSeparator:',',thousandsSeparator:'.',limit:false,centsLimit:2,clearPrefix:false,clearSufix:false,allowNegative:false,insertPlusSign:false};var options=$.extend(defaults,options);return this.each(function(){var obj=$(this);var is_number=/[0-9]/;var prefix=options.prefix;var suffix=options.suffix;var centsSeparator=options.centsSeparator;var thousandsSeparator=options.thousandsSeparator;var limit=options.limit;var centsLimit=options.centsLimit;var clearPrefix=options.clearPrefix;var clearSuffix=options.clearSuffix;var allowNegative=options.allowNegative;var insertPlusSign=options.insertPlusSign;if(insertPlusSign)allowNegative=true;function to_numbers(str){var formatted='';for(var i=0;i<(str.length);i++){char_=str.charAt(i);if(formatted.length==0&&char_==0)char_=false;if(char_&&char_.match(is_number)){if(limit){if(formatted.length<limit)formatted=formatted+char_}else{formatted=formatted+char_}}}return formatted}function fill_with_zeroes(str){while(str.length<(centsLimit+1))str='0'+str;return str}function price_format(str){var formatted=fill_with_zeroes(to_numbers(str));var thousandsFormatted='';var thousandsCount=0;if(centsLimit==0){centsSeparator="";centsVal=""}var centsVal=formatted.substr(formatted.length-centsLimit,centsLimit);var integerVal=formatted.substr(0,formatted.length-centsLimit);formatted=(centsLimit==0)?integerVal:integerVal+centsSeparator+centsVal;if(thousandsSeparator||$.trim(thousandsSeparator)!=""){for(var j=integerVal.length;j>0;j--){char_=integerVal.substr(j-1,1);thousandsCount++;if(thousandsCount%3==0)char_=thousandsSeparator+char_;thousandsFormatted=char_+thousandsFormatted}if(thousandsFormatted.substr(0,1)==thousandsSeparator)thousandsFormatted=thousandsFormatted.substring(1,thousandsFormatted.length);formatted=(centsLimit==0)?thousandsFormatted:thousandsFormatted+centsSeparator+centsVal}if(allowNegative&&(integerVal!=0||centsVal!=0)){if(str.indexOf('-')!=-1&&str.indexOf('+')<str.indexOf('-')){formatted='-'+formatted}else{if(!insertPlusSign)formatted=''+formatted;else formatted='+'+formatted}}if(prefix)formatted=prefix+formatted;if(suffix)formatted=formatted+suffix;return formatted}function key_check(e){var code=(e.keyCode?e.keyCode:e.which);var typed=String.fromCharCode(code);var functional=false;var str=obj.val();var newValue=price_format(str+typed);if((code>=48&&code<=57)||(code>=96&&code<=105))functional=true;if(code==8)functional=true;if(code==9)functional=true;if(code==13)functional=true;if(code==46)functional=true;if(code==37)functional=true;if(code==39)functional=true;if(allowNegative&&(code==189||code==109))functional=true;if(insertPlusSign&&(code==187||code==107))functional=true;if(!functional){e.preventDefault();e.stopPropagation();if(str!=newValue)obj.val(newValue)}}function price_it(){var str=obj.val();var price=price_format(str);if(str!=price)obj.val(price)}function add_prefix(){var val=obj.val();obj.val(prefix+val)}function add_suffix(){var val=obj.val();obj.val(val+suffix)}function clear_prefix(){if($.trim(prefix)!=''&&clearPrefix){var array=obj.val().split(prefix);obj.val(array[1])}}function clear_suffix(){if($.trim(suffix)!=''&&clearSuffix){var array=obj.val().split(suffix);obj.val(array[0])}}$(this).bind('keydown.price_format',key_check);$(this).bind('keyup.price_format',price_it);$(this).bind('focusout.price_format',price_it);if(clearPrefix){$(this).bind('focusout.price_format',function(){clear_prefix()});$(this).bind('focusin.price_format',function(){add_prefix()})}if(clearSuffix){$(this).bind('focusout.price_format',function(){clear_suffix()});$(this).bind('focusin.price_format',function(){add_suffix()})}if($(this).val().length>0){price_it();clear_prefix();clear_suffix()}})};$.fn.unpriceFormat=function(){return $(this).unbind(".price_format")};$.fn.unmask=function(){var field=$(this).val();var result="";for(var f in field){if(!isNaN(field[f])||field[f]=="-")result+=field[f]}return result}})(jQuery);

}())

