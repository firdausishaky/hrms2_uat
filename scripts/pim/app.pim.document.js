	var appTablesPim = angular.module("app.pim.document",["checklist-model"]);

	// ##  CONTROLLER DOCUMENT TEMPLATE ## // 
	appTablesPim.factory('permissiondoc', function() {
		var self;
			return self = {
				data:function(a){
					self.access = a;
					console.log(self.access)
				}
			}
	});
	
	appTablesPim.controller("docTempCtrl", function(ApiURL,$scope,filterFilter,$modal,$http,$cookieStore,logger,permissiondoc) {
		
		/** get data **/
		$http.get(ApiURL.url+'/api/module/document-template?key=' + $cookieStore.get('key_api') ).success(function(res){
			if(res.header.message == 'Unauthorized'){
				logger.logError('Access Unauthorized')
				permissiondoc.data(res.header.access);
			}else{	
				$scope.stores = res.data;	
				$scope.check = {
					stores: []
				};
				permissiondoc.data(res.header.access);
				logger.logSuccess('Show Data Document Template')
			}
		}).error(function(data) {
			data.header ? logger.logError(data.header.message) : logger.logError("Show Document Template Failed")
		});
		
		/** hide button **/
		$scope.button = function(a){
			var data = permissiondoc.access;
			if(data){
				switch(a){
					case 'create':
						if(data.create == 1){return true}
					break;
					case 'delete':
						if(data.delete == 1){return true}
					break;
				}
			}
		};
						
		$scope.open = function(size) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalDocument.html",
				controller: "modalDocCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function (newstudent) {
				if($scope.stores){	
					$scope.stores.push(newstudent);
				}else{
					$scope.stores = [];
					$scope.stores.push(newstudent);
				}
			});
		};
					
		$scope.edit = function(size,student) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditDocument.html",
				controller: "DocEditCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return student;
					}
				}
			});
		};
			
		$scope.remove = function(id){
			$http({
				method : 'DELETE',
				url : ApiURL.url + '/api/module/document-template/' + id + '?key=' + $cookieStore.get('key_api'),
			}).success(function(data, status, header) {
				var i;
					for( i = 0; i < id.length; i++) {
						$scope.stores = filterFilter($scope.stores, function (store) {
							return store.id != id[i];
						});
				};
				$scope.check.stores=[];
				logger.logSuccess(data.header.message);
			}).error(function(data, status, header) {
				logger.logError(data.header.message);
			});
		};
		
	});

		
	appTablesPim.controller("modalDocCtrl",function($scope,$modalInstance,items,logger,$cookieStore,ApiURL,$http,$rootScope) {
				
		if(items){
			$scope.formData = items;
		}	
		$http.get(ApiURL.url + '/api/module/document-template/get-data?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.sizes = res;
				$scope.update = function(a) {
				var param = $scope.sizes[a];						
					$http.get(ApiURL.url + '/api/module/document-template/get-field/'+  param +  '?key=' + $cookieStore.get('key_api') ).success(function(res){
						$scope.getselect = res;
					});
				}
				
		});							
		// FUNCTION GET SELECT BINDING
		$scope.addInput = function(){
			var a = $scope.formData.content_text;
			var b = $scope.select.binding;
			var c = a += b;
			$scope.formData.content_text = c ;
			$scope.formData.content_text = c.replace("<span>","").replace("</span>","");
			console.log($scope.formData.content_text);
		};
		// FUNCTION SAVE
		$scope.save = function(){
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/module/document-template?key=' + $cookieStore.get('key_api'),
				headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
				data : $.param($scope.formData)
			}).success(function(data, status, header, config) {
				
				var temp = {};
					temp = $scope.formData;
					temp.id = data.data.id;
					
					$scope.formData='';
				
				logger.logSuccess(data.header.message);
				$modalInstance.close(temp);	
			}).error(function(data, status, headers, config) {
				logger.logError(data.header.message);
			});
		};		
		$scope.cancel = function() { 
			$modalInstance.dismiss("cancel")
		}			
	});


	appTablesPim.controller("DocEditCtrl",function($scope,$modalInstance,items,logger,$cookieStore,ApiURL,$http,permissiondoc) {
				
		var a = String(items.description);
		var b = String(items.link_to_section);
		var c = String(items.size);
		var d = String(items.template_name);
		if(items){
			$scope.formData = items;
		}
					
		$http.get(ApiURL.url + '/api/module/document-template/get-data?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.sizes = res;
				$scope.update = function(a) {
				var param = $scope.sizes[a];						
					$http.get(ApiURL.url + '/api/module/document-template/get-field/'+  param +  '?key=' + $cookieStore.get('key_api') ).success(function(res){
						$scope.getselect = res;
					});
				}
		});										
		// FUNCTION GET SELECT BINDING
		$scope.addInput = function(){
			var a = $scope.formData.content_text;
			var b = $scope.select.binding;
			var c = a += b;
			$scope.formData.content_text = c ;
			$scope.formData.content_text = c.replace("<span>","").replace("</span>","");
			console.log($scope.formData.content_text);
		};		
		// FUNCTION UPDATE				
		$scope.save = function(){
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/module/document-template/edit/' +  items.id  +'?key=' + $cookieStore.get('key_api'),
				headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
				data : $.param($scope.formData)
			}).success(function(data, status, header, config) {
				var temp = {};
					temp = $scope.formData;
					$scope.formData='';
				
				logger.logSuccess(data.header.message);
				$modalInstance.close(temp);	
			}).error(function(data, status, headers, config) {
				logger.logError(data.header.message);
			});
		};
		$scope.cancel = function(){
			$scope.formData.description = a;
			$scope.formData.link_to_section = b;
			$scope.formData.size = c;
			$scope.formData.template_name = d;
			$modalInstance.dismiss("cancel")
		}
		$scope.hide = function(a){
			var access  = permissiondoc.access;
			if(access){
				switch(a){
					case 'update':
						if(access.update == 1){
							return true
						}
					break;
				}
			}
		};
	});

// ##  CONTROLLER TABLE TERMINATIONS ## // 
	/* appTablesPim.controller("termCtrl", ["ApiURL", "$scope", "filterFilter", "$modal", "$http", "$cookieStore", "logger", function(api, $scope, filterFilter ,$modal, $http, $cookieStore, logger) {
							
		$http.get(api.url+'/api/module/termination-reasons?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.terminations = res.data;	
			$scope.check = {
				terminations: []
			}; 
			
		}).success(function(data, status, header, config) {
				$scope.role = true;
				logger.logSuccess(data.header.message);
		}).error(function(data, status, headers, config) {
				$scope.role = false;
				logger.logError(data.header.message);
		});
					
		$scope.open = function(size) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalTerm.html",
				controller: "modalTermCtrl",
				backdrop : "static",
				size: size,        
				resolve: {
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function (newterm) {
				if($scope.terminations){
					$scope.terminations.push(newterm);
				}else{	
					$scope.terminations = [];	
					$scope.terminations.push(newterm);
				}
			
			});
		};	
		$scope.edit = function(size,term) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditTerm.html",
				controller: "TermEditCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return term;
					}
				}
			});
		};	
		$scope.remove = function(id){ 
			$http({
				method : 'DELETE',
				url : api.url + '/api/module/termination-reasons/' + id + '?key=' + $cookieStore.get('key_api'),
			}).success(function(data, status, header,config) {
				var i;
					for( i = 0; i < id.length; i++) {
						$scope.terminations = filterFilter($scope.terminations, function (term) {
							return term.id != id[i];
						});
				};
				$scope.check.terminations=[];
				logger.logSuccess(data.header.message);
			}).error(function(data, status, header) {
				logger.logError(data.header.message);
			});
		};
	}]);
		
appTablesPim.controller("modalTermCtrl",  ["$scope","$modalInstance","items","logger","$cookieStore","ApiURL","$http", function($scope,$modalInstance,items,logger,$cookieStore,api,$http) {
				
				if(items){
					$scope.formData = items;
				}
				
				$scope.save = function(){
					$http({
						method : 'POST',
						url : api.url+'/api/module/termination-reasons?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data :  $.param($scope.formData)
					}).success(function(data, status, header, config) {
						var temp = {}; 
							temp = $scope.formData;
							temp.id = data.data.id;
						logger.logSuccess(data.header.message);
						$modalInstance.close(temp);	
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				$scope.cancel = function() {
					$modalInstance.dismiss("cancel")
				}
				
}]);

appTablesPim.controller("TermEditCtrl", ["$scope","$modalInstance","items","logger","$cookieStore","ApiURL","$http", function($scope,$modalInstance,items,logger,$cookieStore,api,$http) {
				
				
				var a = String(items.title);
				if(items){
					$scope.formData = items;
				}
				
				$scope.save = function(){
					$http({
						method : 'POST',
						url : api.url+'/api/module/termination-reasons/' + items.id +'?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data :  $.param($scope.formData)
					}).success(function(data, status, header) {
						var temp = {}; 
							temp.id = items.id;
						logger.logSuccess(data.header.message);
						$modalInstance.close(temp);	
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				$scope.edit = function(){
					$scope.button=true;
				};
				
				$scope.cancel = function() {
					$scope.formData.title = a;
					$modalInstance.dismiss("cancel")
				}
}]); */
		
		// End Terminations Controller
		
		
		
		
		
		//################################################  CONTROLLER TABLE IMMIGRATIONS ############################################################################ 
		 
/* 		
appTablesPim.controller("immigCtrl", ["ApiURL","$scope","filterFilter","$modal","$http","$cookieStore","logger", function(api,$scope,filterFilter,$modal,$http,$cookieStore,logger) {
							
				$http.get(api.url+'/api/module/immigration-issuer?key=' + $cookieStore.get('key_api') ).success(function(res){
					$scope.imigs = res.data;
					
					$scope.check={imigs:[]};
					
				}).success(function(data, status, header, config) {
						$scope.role = true;
						logger.logSuccess(data.header.message);
				}).error(function(data, status, headers, config) {
						$scope.role = false;
						logger.logError(data.header.message);
				});
				
				
				
				$scope.open = function(size) {
					var modal;
					modal = $modal.open({
						templateUrl: "modalImmig.html",
						controller: "modalImmigCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return;
							}
						}
					});
					modal.result.then(function (newstudent) {
						
						if($scope.imigs){
							$scope.imigs.push(newstudent);
						}else{
							$scope.imigs = [];
							$scope.imigs.push(newstudent);
						}
					});
				};
				
				$scope.edit = function(size,imig) {
					var modal;
					modal = $modal.open({
						templateUrl: "modalEditImmig.html",
						controller: "ImmigEditCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return imig;
							}
						}
					});
				};
				
				
				// FUNCTION DELETE
				
				$scope.remove = function(id){
					$http({
						method : 'DELETE',
						url : api.url + '/api/module/immigration-issuer/'+ id + '?key=' + $cookieStore.get('key_api'),
					}).success(function(data, status, header) {
						var i;
							for( i = 0; i < id.length; i++) {
								$scope.imigs = filterFilter($scope.imigs, function (imig) {
									return imig.id != id[i];
								});
						};
						$scope.check.imigs=[];
						logger.logSuccess(data.header.message);
					}).error(function(data, status, header) {
						logger.logError(data.header.message);
					});
				};
			
		}]);
		
appTablesPim.controller("modalImmigCtrl",  ["$scope","$modalInstance","items","logger","$cookieStore","ApiURL","$http", function($scope,$modalInstance,items,logger,$cookieStore,api,$http) {
				
				if(items){
					$scope.formData = items;
				}
				
				$scope.save = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/module/immigration-issuer?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)
					}).success(function(data, status, header, config) {
						var temp    = {};
							temp    = $scope.formData;
							temp.id = data.data[0].id;
							
						//	$scope.formData='';
						$modalInstance.close(temp);	
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				$scope.cancel = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);


appTablesPim.controller("ImmigEditCtrl",["$scope","$modalInstance","items","logger","$cookieStore","ApiURL","$http", function($scope,$modalInstance,items,logger,$cookieStore,api,$http) {
				
				var a = String(items.title);
				if(items){
					$scope.formData = items;
				}
				
				$scope.save = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/module/immigration-issuer/' + items.id +'?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;
						//	$scope.formData='';
						logger.logSuccess(data.header.message);
						$modalInstance.close(temp);	
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				$scope.cancel = function() {
					$scope.formData.title=a;
					$modalInstance.dismiss("cancel")
				}
}]);
	 */	
		
		
		
		
		




		
		