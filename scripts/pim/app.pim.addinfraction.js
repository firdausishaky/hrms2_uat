(function(){
	
	angular.module("app.pim.addinfraction",["ngFileUpload"])
	
	.factory('addinfractfactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,Upload,$route){
		var self;
		return self = {
			getaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/infraction/index?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.getpermission(res.data.header.access);
					self.hide=true;
					logger.logSuccess('Access Granted');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},save: function(file){
				console.log(self.model);
				self.form.employee_id = self.label;
				if(file){
					file.upload = Upload.upload({
						method : 'POST',
						url : ApiURL.url + '/api/infraction/insert?key=' + $cookieStore.get('key_api'),
						data :self.form,
						file: file,
						headers: {
							"Content-Type": "application/json"
						}
					}).then(function(res){
						console.log('success',res)
						self.form = ""
						self.files=false;
						self.form.employee_id= "";
						var message = res.data.header.message+' for employee '+self.model
						logger.logSuccess(message);
						$route.reload();
					},function(res){
						self.files=false;
						self.form.employee_id=self.model;
						res.data.header ? logger.logError(res.data.header.message+' for employee '+self.model) : logger.logError("Saving Request Failed")
					});
				}else{
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/infraction/insert?key=' + $cookieStore.get('key_api'),
						data : self.form,
						headers: {
							"Content-Type": "application/json"
						}     
					}).then(function(res){
						console.log('success',res)
						self.form = ""
						self.form.employee_id= "";
						//self.form.employee_id=self.model;
						var message = res.data.header.message+' for employee '+self.model
						logger.logSuccess(message);
						$route.reload();
					},function(res){
						self.form.employee_id=self.model;
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
					});
				}
			},cancel : function(file){
				self.getaccess()
			},onSelect : function ($item, $model, $label) {
				self.subordinate = $item.name;
				self.model = $item.name;
				self.label = $item.employee_id;
			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params: {
						address : val,
					}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};
						a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			}
		}
	})
	.controller('addinfractionController',function(addinfractfactory,ApiURL,$cookieStore,$scope,$http,Upload,logger,$timeout){
		var self = this;
		self.handler = addinfractfactory;
		self.handler.getaccess()

	})
	

}())
