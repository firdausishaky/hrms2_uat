(function(){
	
	angular.module('app.pim.termination',['checklist-model'])

	.factory('terminationfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Name','title',100]
					);
				this.setactiveheader(this.headers[0],false)
			},termination:function(){
				$http({
					url : ApiURL.url + '/api/module/termination-reasons?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data)
						logger.logSuccess(res.data.header.message);
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Termination Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
					//console.log(active)
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/module/termination-reasons?key='  + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalterm',
					controller: 'modaladdterm',
					controllerAs: 'modalterm',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaleditterm',
					controller: 'modaledit',
					controllerAs: 'modaleditterm',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/module/termination-reasons?key=' + $cookieStore.get('key_api'),
							data : data,
							headers: {
								"Content-Type": "application/json"
							}
						}).then(function(res){
							console.log(res.data.data)
							self.adddata(res.data.data)
									//self.callback()
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Termination Failed")
								});
						break;
						case 'edit':
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/module/termination-reasons/'  + b.id + '?key=' + $cookieStore.get('key_api'),
							data : data.b,
							headers: {
								"Content-Type": "application/json"
							}
						}).then(function(res){
							self.update(res.data.data)
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Termination Failed")
						});			
						break;
					}
					
				});
			},
			deldata: function(id){
				console.log(id)
				$http({
					url : ApiURL.url + '/api/module/termination-reasons/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Termination Success');
				},function(res){
					console.log(res.data.header.message)
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Termination Failed")
				})
			}
		}
	})

.controller('terminationsctrl',function(terminationfactory){
	var self = this;
	self.handler = terminationfactory;
	self.handler.termination()
})
.controller('modaladdterm',function(terminationfactory,$modalInstance,data){	
	var self = this;
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.controller('modaledit',function(terminationfactory,$modalInstance,data){	
	var self = this;
	self.handler = terminationfactory;
	self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close({a:data,b:self.form});
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(terminationfactory){
	terminationfactory.setheaders();
})

}())

