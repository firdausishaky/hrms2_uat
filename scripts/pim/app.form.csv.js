(function(){
	
	function employeeNew(emp){
		this.first_name  		= emp[0] || '';
		this.middle_name 		= emp[1] || '';
		this.last_name   		= emp[2] || '';
		this.ad_username 		= emp[3] || '';
		this.employee_type  	= emp[4] || '';
		this.employee_id 		= emp[5] || '';
		this.biometric   		= emp[6] || '';
		this.sss 				= emp[7] || '';
		this.philhealth 		= emp[8] || '';
		this.hdmf 				= emp[9] || '';
		this.maiden_name 		= emp[10] || '';
		this.hmo_account 		= emp[11] || '';
		this.hmo_numb 			= emp[12] || '';
		this.rtn 				= emp[13] || '';
		this.mid				= emp[14] || '';
		this.tin				= emp[15] || '';
		this.gender 			= emp[16] || '';
		this.marital_status 	= emp[17] || '';
		this.nationality		= emp[18] || '';
		this.date_of_birth		= emp[19] || '';
		this.place_of_birth		= emp[20] || '';
		this.religion 			= emp[21] || '';
		this.permanent_address	= emp[22] || '';
		this.permanent_city 	= emp[23] || '';
		this.permanent_state	= emp[24]|| '';
		this.permanent_zip		= emp[25] || '';
		this.permanent_country	= emp[26] || '';
		this.present_address 	= emp[27] || '';
		this.present_city 		= emp[28] || '';
		this.present_state		= emp[29] || '';
		this.present_zip	 	= emp[30] || '';
		this.present_country	= emp[31] || '';
		this.home_telephone 	= emp[32] || '';
		this.mobile 			= emp[33] || '';
		this.work_email 		= emp[34] || '';
		this.personal_email		= emp[35] || '';
	}
	
	angular.module('app.form.csv',['checklist-model','ui.mask'])
	
	.factory('csvexcelfactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,pagination,$timeout,$window){
		
		/* function createheaders(arr){
			var temp = [];
			var arguments = arr;
			arguments ? void 0 : arguments = []
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;
		} */
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;
		}
		var self;
		return self = {	
				/* setheaders: function(header){
					this.headers = new createheaders(header)
				},getHeader: function(){
					return this.headers;
				} */
				setheaders: function(){
					this.headers = new createheaders(
						['First Name','first_name',null],
						['Middle Name','last_name',null],
						['Last Name','middle_name',null],
						['AD Username','ad_username',null],
						['Employee Type','employee_type',null],
						['Employee ID','employee_id',null]
						);
					this.setactiveheader(this.headers[0],false);
				},adddata: function(a){
					this.maindata.push(a);
				},setdata: function(a){
					this.maindata = a;
				},sentDate : function(date){
					this.date = date;
					return self.date;
				},getaccess:function(){
					$http({
						url : ApiURL.url + '/api/module/permission?key=' + $cookieStore.get('key_api'),
						method: 'GET',
					}).then(function(res){
						self.getpermission(res.data.header.access);
						logger.logSuccess('Access Granted');
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
					});
				},getpermission:function(active){
					this.mainaccess = active;
					console.log(active)
				},hiden:function(a){
					if(this.mainaccess){
						switch(a){
							case 'create':
							if(this.mainaccess.create == 1){return true}
								break;
						}
					}
				},save:function(){
					datasent={};
					datasent.employeelist=self.maindata;
					//console.log(datasent)
					$http({
						url : ApiURL.url + '/api/module/import-csv?key=' + $cookieStore.get('key_api'),
						method: 'POST',
						data:datasent,
						headers: {
							"Content-Type": "application/json"
						}
					}).then(function(res){
						data=[];
						self.setdata(data);
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Upload Excel File Failed")
					});
				},getdata: function(){
					self.filtered = $filter('filter')(this.maindata,{$:self.search});
					self.select(self.currentPage);
					return self.paginated;
				},setactiveheader:function(a,bool){
					this.activeheader = a;
					this.activeheader.order = bool;	
				},
				numPerPageOpt	: pagination.option,
				numPerPage 		: pagination.option[3],
				currentPage		: 1,
				search			: '',
				select			: function(page){
					if(self.filtered){
						start = (page-1)*self.numPerPage;
						end = start + self.numPerPage;
						self.paginated = self.filtered.slice(start, end) || false;
						self.filteredlength = self.paginated.length || 0;
					}
				},
				readXLSX: function(file){
					var X = XLSX;
					var reader = new FileReader();
					var name = file.name;
					var res = [];
					reader.readAsBinaryString(file);
					reader.onload = function(e) {
						var data = e.target.result;
						var wb;
						wb = X.read(data, {type: 'binary'});
						res = readWB(wb)
					};
					function readWB(workbook){
						var cell = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ"]
						var result = [];
						var employeeData=[];
						for(var i=0;i<workbook.SheetNames.length;i++){
							
							var sheetName = workbook.SheetNames[i];
							var worksheet = workbook.Sheets[sheetName];
							var header = [];
							
							/** get max xlsx file **/
							var max = worksheet['!ref'];
							if(max==undefined)break;
							var maxfile = max.split(':');
							var c = maxfile[1].replace(/[^\d]/g, '');
							
							/** get header **/
							for(j=1; j<=c;j++){
								
								var employeeTemp=[]
								
								for(var h=0; h < cell.length; h++){
									
									if(worksheet[(cell[0] + j).toString()] == undefined) break;
									if(j==1){
											//var setEmp = worksheet[(cell[h] + j).toString()].w;
											//var headertemp = [setEmp,setEmp,null];
											//header.push(headertemp)
										}else{
											//console.log('to here')
											var employee = worksheet[(cell[h] + j).toString()] == undefined ? "" : worksheet[(cell[h] + j).toString()].w;
											//console.log(employee)
											//h == 0 ? empIdTemp.push(employee) : void 0
											employeeTemp.push(employee)
											//console.log(worksheet[(cell[h] + j).toString()].v)
										} 
									}
									if(worksheet[(cell[0] + j).toString()] == undefined) break;
									if(j!=1){
										var s = new employeeNew(employeeTemp)
									//console.log(s)
									employeeData.push(s);
								}
								
							}
							//self.setheaders(header)
							$timeout(function(){
								self.setdata(employeeData)
							}, 100);
							//console.log(employeeData)
						}
						
					}	
				},
				/** modal handler **/
				modals: {
					del:{
						animation: true,
						templateUrl: 'modalfiles',
						controller: 'deletefilesexcel',
						controllerAs: 'modalfiles',
						size:'',
						backdrop:'static',
						resolve:{
							data:function(){
								return self.datatoprocess;
							}
						}
					},
					edit:{
						animation: true,
						templateUrl: 'modalupdate',
						controller: 'editemployee',
						controllerAs: 'modalupdate',
						//size:'lg',
						windowClass: 'large-Modal',
						//backdrop:'static',
						resolve:{
							data:function(){
								return self.datatoprocess;
							}
						}
					}
				},
				openmodal: function(a,b){
					self.datatoprocess = b
					$modal.open(self.modals[a]).result.then(function(data){
						switch(a){
							case 'del':
							for (i = 0; i < self.maindata.length; ++i) {
								self.maindata[i]==data ? self.maindata.splice(i,1) : 0
							}
							logger.logSuccess('Deleting file Successfully');
							break;
							case 'edit':
							for (i = 0; i < self.maindata.length; ++i) {
								if(data.a==self.maindata[i]){
									self.maindata[i]=data.b;
								}
							}
							logger.logSuccess('Updating Data Employee Successfully');
							break;
						}
						
					});
				},getnationalities:function(){
					$http({
						url : ApiURL.url + '/api/nationalities/form-nationalities?key=' + $cookieStore.get('key_api'),
						method: 'GET',
					}).then(function(res){
						self.datanational=res.data.data;
					},function(res){
						res.data.message ? logger.logError(res.data.message) : logger.logError("Geting Data Nationalities Failed")
					});
				},setmaiden:function(data){
					if(data.gender == 'female' && data.marital_status == 'Single'){
						self.maiden = false;
					}else if(data.gender == 'female' && data.marital_status == 'Married'){
						self.maiden = true;
					}else if(data.gender == 'female' && data.marital_status == 'Separated'){
						self.maiden = true;
					}else if(data.gender == 'female' && data.marital_status == 'Widow'){
						self.maiden = true;
					}
					if(data.gender == 'male'){
						self.maiden = false;
					}
				},selectgender:function(data){
					if(data.gender == 'male'){
						self.maiden = false;
						data.marital_status = '';
					}
				},deldata:function(a){
					console.log(a)
					for (i = 0; i < this.maindata.length; ++i) {
						this.maindata[i]==a ? this.maindata.splice(i,1) : 0
					}
				},getexcel:function(e){
					var e  = ApiURL.url + '/api/module/import-csv-getfile?key=' + $cookieStore.get('key_api'); 
					$window.open(e);
				}
			}
			
		})

.controller('csvexcelcontroller',function(csvexcelfactory,$scope,ApiURL, $cookieStore){
	var self = this;
	self.handler = csvexcelfactory;
	self.readFiles = function(file, errFiles){
		file[0] ? this.handler.readXLSX(file[0]) : 0
	}
	self.handler.getaccess()
})

.controller('editemployee',function(csvexcelfactory,$modalInstance,data){	
	var self = this;
	self.handler = csvexcelfactory;
	self.handler.setmaiden(data);
	self.handler.getnationalities()
	self.form = data;
	console.log(data)
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close({a:data,b:self.form});
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})

.controller('deletefilesexcel',function(csvexcelfactory,$modalInstance,data){
	var self = this;
	self.handler=csvexcelfactory;
	self.form={};
	self.form=angular.copy(data)
	self.submit = function(){
		$modalInstance.close(data);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}			
})



.run(function(csvexcelfactory){
	csvexcelfactory.setheaders();
})


}())


/* var appFormCsv = angular.module("app.form.csv", []);

appFormCsv.factory('permissioncsv', function() {
	var self;
		return self = {
			data:function(a){
				self.access = a;
			}
		}
});

appFormCsv.controller("csvCtrl", function(logger,$cookieStore,ApiURL,$scope,$routeParams,$http,fileUpload,permissioncsv) {
		
		// FUNCTION UPLOAD FILE CSV
		$scope.uploadFile = function(){
			var file = $scope.myFile;
			$scope.myFile = '';
			var uploadUrl = ApiURL.url + '/api/module/import-csv?key=' + $cookieStore.get('key_api');
			fileUpload.uploadFileToUrl(file, uploadUrl);
		};
		// LINK DOWNLOAD CSV
		$scope.e = ApiURL.url +  '/api/module/import-csv-getfile?key=' + $cookieStore.get('key_api');
		
		$http({
			method : 'GET',
			url : ApiURL.url + '/api/module/permission?key=' + $cookieStore.get('key_api')
		}).then(function(res){
			if(res.data.header.message == 'Unauthorized'){
				permissioncsv.data(res.data.header.access);
				logger.logError('Access Unauthorized');
			}else{
				logger.logSuccess('Access Granted');
				permissioncsv.data(res.data.header.access);
			}
		},function(res){
			res.data.header ? logger.logError(res.data.header.message) : logger.logError("Access Unauthorized")
		});
		
		$scope.button = function(a){
			var data = permissioncsv.access;
			if(data){
				switch(a){
					case 'create':
						if(data.create == 1){return true}
					break;
					case 'delete':
						if(data.delete == 1){return true}
					break;
				}
			}
		};
		
});

appFormCsv.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

appFormCsv.service('fileUpload', ['$http','logger', function ($http,logger) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd,{
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        
		}).success(function(data){
			logger.logSuccess(data.header.message);
        }).error(function(data){
			data.header ? logger.logError(data.header.message) : logger.logError("Adding CSV Import Failed")
        });
    }
}]);
 */