var appLogin = angular.module("app.login", ["ngCookies"]);

var newUser = function(user,auth){
	this.name = user.name;
	this.pass = user.password;
	this.auth = auth;
}

appLogin.controller("signCtrl", ['$location','$scope','$cookieStore','Auth','logger','GetName','notifFactory','$http', function($location,$scope,$cookieStore,Auth,logger,GetName,notifFactory,$http) {
				if($cookieStore.get('key_api')){
					$cookieStore.remove('key_api');
					Auth.get()
					.success(function(AuthData){
						$scope.auth = AuthData.token;
						var _token = AuthData.token;
					});
				}
				else{
					Auth.get()
					.success(function(AuthData){
						$scope.auth = AuthData.token;
						var _token = AuthData.token;
					});
				}

				$scope.togglePassword = function () { $scope.typePassword = !$scope.typePassword; };

				$scope.submitForm = function() {
					var u = new newUser($scope.user,$scope.auth);
                    Auth.save(u).success(function(data){
						if(!data.access){
							//$cookieStore.put('UserAccess',data.data.access);
							$cookieStore.put('regular',data.data.access);
						}

						console.log(data)
						console.log(data.data.access,"REGULAR::app.login.js")

						$http.defaults.headers.get = { 'Authorization' : "Bearer "+data.data.key };
						$http.defaults.headers.post = { 'Authorization' : "Bearer "+data.data.key };
						
						$cookieStore.put('key_api',data.data.key);
						$cookieStore.put('NotUser',data.data.emp);
						$cookieStore.put('uname',data.data.username);
						$cookieStore.put('user_depart',data.data.department_id);
						$cookieStore.put('access',data.data.access )
						$cookieStore.put('local_it',data.data.local_it)
						window.localStorage.setItem('menu',JSON.stringify(data.data.menu));
						//$cookieStore.put('menu',data.data.menu);
						GetName.setname(data.data.username)
						$location.path('/dashboard2');

						notifFactory.getNotif()

						logger.logSuccess('Access Granted');
					}).error(function(data){
						$scope.user = '';
						$location.path('pages/signin');
						data.header ? logger.logError(data.header.message) : logger.logError("Login Failed")
					});		
				};
				
			
	
}]);
/* if (data.data.access){							
	var key = data.data.key;
	$cookieStore.put('key_api',key);
	
	var user = data.data.username;
	$cookieStore.put('uname',user);

	var employee_id = data.data.access.id;
	$cookieStore.put('employee_id',employee_id);

	var access = data.data.access.name;
	$cookieStore.put('access',access);
	
	var logo = data.data.picture;
	$cookieStore.put('imagesLogo',logo);

	
	location.reload()
	$location.path('/dashboard');

} else {

	var key = data.data.key;
	$cookieStore.put('key_api',key);
	
	var user = data.data.username;
	$cookieStore.put('uname',user);

	var emp = data.data.emp;
	$cookieStore.put('emp',emp);

	var logo = data.data.picture;
	$cookieStore.put('imagesLogo',logo);

	
	location.reload()
	$location.path('/dashboard');

} */
