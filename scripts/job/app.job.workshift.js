(function(){
	
	
	angular.module('app.job.workshift',['checklist-model'])

	.factory('workshiftfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,time,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;
		}
		
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Shift Code','shift_code',23],
					['From','_from',23],
					['To','_to',23],
					['Hour Per Day','hour_per_day',23],
					['Color','color',20]
					);
				this.setactiveheader(this.headers[0],false)
			},getdataworkshift:function(){
				$http({
					url : ApiURL.url + '/api/timekeeping/work-shift?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data)
						logger.logSuccess(res.data.header.message);
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Workshift Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},update: function(dataA,dataB){
				dataA.shift_code = dataB.shift_code;
				dataA._from = dataB._from;
				dataA._to = dataB._to;
				dataA.hour_per_day = dataB.hour_per_day;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/timekeeping/work-shift/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.shift_id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Work Shift Failed")
				})
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			/** FORM VARIABLES OVERTIME **/
			schedule : time.schedule,
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalshift',
					controller: 'modalshift',
					controllerAs: 'modalshift',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}						
					}
				},
				edit:{
					animation: true,
					templateUrl: 'editshift',
					controller: 'editshift',
					controllerAs: 'editshift',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/timekeeping/work-shift?key=' + $cookieStore.get('key_api'),
							data : data,
							headers: {
								"Content-Type": "application/json"
							}
						}).then(function(res){
							console.log(res.data.data.shift_id)
							var temp = {};
							temp = data;
							temp.shift_id = res.data.data.shift_id;
							self.adddata(temp);
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header.message ? logger.logError(res.data.header.message) : logger.logError("Adding Work Shift Failed")
						});
						break;
						case 'edit':
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/timekeeping/work-shift/' + b.shift_id + '?key=' + $cookieStore.get('key_api'),
							data : data.b,
							headers: {
								"Content-Type": "application/json"
							}
						}).then(function(res){
							self.update(data.a,data.b)
							logger.logSuccess(res.data.header.message);
							self.getdataworkshift()
						},function(res){
							res.data.header.message ? logger.logError(res.data.header.message) : logger.logError("Updating Work Shift Failed")
						});			
						break;
					}
					
				});
			},
		}
	})

	// PRIMER CONTROLLER
	.controller('workshiftCtrl',function(workshiftfactory,ApiURL,$cookieStore){
		var self = this;
		self.handler = workshiftfactory;
		workshiftfactory.getdataworkshift();
	})

	
	// CONTROLLER MODAL ADD
	.controller('modalshift',function(workshiftfactory,$modalInstance,$timeout,data,time){
		
		var self = this;
		this.schedule = workshiftfactory.schedule;
		self.form = {};
		
		/** FUNCTION SAVE **/
		self.save = function(){
			$modalInstance.close(self.form);
		}
		
		/** FUNCTION CLOSE MODAL **/
		self.close = function(){
			$modalInstance.dismiss('close')
		}
		
		/** FUNCTION GET DURATION **/
		self.findOvertime = function(){
			var a = self.form._from.substring(0,2);
			var b = self.form._from.substring(3,5);
			var c = self.form._to.substring(0,2);
			var d = self.form._to.substring(3,5);
			var date1 = new Date(2015, 0, 1, a,b);
			var date2 = new Date(2015, 0, 1, c,d); 
			if (date2 < date1) {
				date2.setDate(date2.getDate() + 1);
			}
			var diff = date2 - date1;
			var e = (diff / 1000)
			if (e % 3600 === 0){
				e = e / 3600					
			}else{
				e = ((e - (e % 3600))/3600) + (((e % 3600)/60)/100)
			}
			var f = Math.round(e);
			self.form.hour_per_day = f;
		}	
		
	})
	
	
	// CONTROLLER MODAL EDIT
	.controller('editshift',function(workshiftfactory,$modalInstance,$timeout,data,time){
		
		var self = this;
		self.handler = workshiftfactory;
		self.handler.button()
		self.schedule = workshiftfactory.schedule;
		self.form = {};
		self.form = data;
		self.form = angular.copy(data);
		
		/** FUNCTION SAVE **/
		self.save = function(){
			$modalInstance.close(
			{
				a:data,
				b:self.form
			}
			
			);
		},
		
		/** FUNCTION CLOSE MODAL **/
		self.close = function(){
			$modalInstance.dismiss('close')
		},
		
		/** FUNCTION GET DURATION **/
		self.findOvertime = function(){
			var a = self.form._from.substring(0,2);
			var b = self.form._from.substring(3,5);
			var c = self.form._to.substring(0,2);
			var d = self.form._to.substring(3,5);
			var date1 = new Date(2015, 0, 1, a,b);
			var date2 = new Date(2015, 0, 1, c,d); 
			if (date2 < date1) {
				date2.setDate(date2.getDate() + 1);
			}
			var diff = date2 - date1;
			var e = (diff / 1000)
			if (e % 3600 === 0){
				e = e / 3600					
			}else{
				e = ((e - (e % 3600))/3600) + (((e % 3600)/60)/100)
			}
			var f = Math.round(e);
			self.form.hour_per_day = f;
		}	
		
	})

	
	
	// FUNCTION SET HEADER
	.run(function(workshiftfactory){
		workshiftfactory.setheaders();
	})

}())