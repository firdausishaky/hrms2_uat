(function(){
	
	angular.module('app.tables.job',['checklist-model','ngFileUpload'])

	.factory('tablejobfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,Upload,$window){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Job Title','title',50],
					['Job Description','descript',50]
					);
				this.setactiveheader(this.headers[0],false)
			},getdatajob:function(){
				$http({
					url : ApiURL.url +'/api/jobs/jobs-title?key='+ $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.getpermission(res.data.header.access);
					var temp=res.data.data;
					for(a in temp){
						if(temp[a].filename==null || temp[a].filename==''){
							temp[a].filename='Not Defined'
						}
					}
					self.setdata(temp)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Job Title Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
					console.log(active)
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					url : ApiURL.url +'/api/jobs/jobs-title?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					var temp=res.data.data;
					for(a in temp){
						if(temp[a].filename==null || temp[a].filename==''){
							temp[a].filename='Not Defined'
						}
					}
					self.setdata(temp)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodaljob',
					controllerAs: 'addmodal',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditjob',
					controllerAs: 'modaledit',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						if(data.file){
							var file = data.file;
							delete data.file;
							file.upload = Upload.upload({
								method : 'POST',
								url : ApiURL.url + '/api/jobs/jobs-title?key='  + $cookieStore.get('key_api'),
								data :data,
								file: file,
								headers: {
									"Content-Type": "application/json"
								}
							}).then(function(res){
								console.log(res.data.data)
								self.adddata(res.data.data)
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding  Job Title Failed")
							});
						}else{
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/jobs/jobs-title?key='  + $cookieStore.get('key_api'),
								data : data,
								headers: {
									"Content-Type": "application/json"
								}
							}).then(function(res){
								var temp=res.data.data;
								temp.filename='Not Defined';
								self.adddata(temp)
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding  Job Title Failed")
							});
						}
						break;
						case 'edit':
						if(data.b.filename){
							
							if(data.b.filename[0].name=='Not Defined'){	
								console.log('to string')
								delete data.b.filename
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/jobs/jobs-title/'+ b.id + '?key=' + $cookieStore.get('key_api'),
									data : data.b,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.callback()
									self.showradio=false;
									self.showbutton=false;
									logger.logSuccess(res.data.header.message);
								},function(res){
									self.showradio=false;
									self.showbutton=false;
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating  Job Title Failed")
								});
							}else{
								console.log('to upload')
								var file = data.b.filename;
								delete data.b.filename
								/** if have an file **/
								file.upload = Upload.upload({
									method : 'POST',
									url : ApiURL.url + '/api/jobs/jobs-title/'+ b.id + '?key=' + $cookieStore.get('key_api'),
									data :data.b,
									file: file,
									headers: {
										"Content-Type": "application/json"
									}
								}).then(function(res){
									self.callback()
									self.showradio=false;
									self.showbutton=false;
									logger.logSuccess(res.data.header.message);
								},function(res){
									self.showradio=false;
									self.showbutton=false;
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating  Job Title Failed")
								});
							}									
						}else{
							console.log('to string')
							delete data.b.file
							delete data.b.filename
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/jobs/jobs-title/'+ b.id + '?key=' + $cookieStore.get('key_api'),
								data : data.b,
								headers: {
									"Content-Type": "application/json"
								}
							}).then(function(res){
								self.callback()
								self.showradio=false;
								self.showbutton=false;
								logger.logSuccess(res.data.header.message);
										//self.update(res.data.data)
									},function(res){
										self.showradio=false;
										self.showbutton=false;
										res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating  Job Title Failed")
									});
						}
						break;
					}
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/jobs/jobs-title/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting  Job Title Success');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Deleting  Job Title Failed")
				})
			},
			/** action when modal edit **/
			replace:function(){
				self.showbutton=true
			},edit:function(form){
				console.log(form)
				if(form.filename=='Not Defined'){
					self.showradio=false;
					self.showbutton=true;
				}else{
					form.radio=1;
					self.showradio=true;
				}
			},setupload:function(form){
				self.showradio=true;
				form.radio=1;	
			},getfile:function(file){
				if(file == 'Not Defined'){
					logger.logError("File Not Found");
				}else{
					var e = ApiURL.url +  '/api/jobs/download/' + file + '?key=' + $cookieStore.get('key_api');
					$window.open(e);
				}
			}
		}
	})

.controller('tableJobCtrl',function(tablejobfactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = tablejobfactory;
	self.handler.getdatajob()
})

.controller('addmodaljob',function(tablejobfactory,$modalInstance,$timeout,data){	
	var self = this;
	self.save = function(file){
		self.form.file=file;
		$modalInstance.close(self.form);
	}
	self.cancel = function(){
		$modalInstance.dismiss('close')
	}
})

.controller('modaleditjob',function(tablejobfactory,$modalInstance,$timeout,data){	
	var self = this;
	self.handler = tablejobfactory;
	self.handler.button();
	self.form=data;
	self.files=[];
	var g = new File([""], self.form.filename);
	self.files.push(g);
	console.log(self.form)
	self.form = angular.copy(data);
	self.save = function(file){
		if(file){
			self.form.filename=file;
		}else{
			self.form.filename=self.form;
		}
		$modalInstance.close({a:data,b:self.form});
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(tablejobfactory){
	tablejobfactory.setheaders();
})

}())


/* 	var appTablesJob = angular.module("app.tables.job",["checklist-model","ngFileUpload"]);
	appTablesJob.factory('permissionjob', function() {
		var self;
			return self = {
				data:function(a){
					self.access = a;
					console.log(self.access)
				}
			}
	});
	appTablesJob.controller("tableJobCtrl", function(jobTitle,$scope, filterFilter ,$modal,$http,ApiURL,logger,$cookieStore,$filter,permissionjob){
				
		jobTitle.get().success(function(res){
			if(res.header.message == "Unauthorized"){
				permissionjob.data(res.header.access);
				logger.logError("Access Unauthorized");
			}else{
				$scope.check = {currentPageStores:[]}
				permissionjob.data(res.header.access);
				var end, start;
				start = 0;
				$scope.stores = res.data;
				for(a in $scope.stores) {
					if($scope.stores[a].filename == null){
						$scope.stores[a].filename = "Not Defined";
					}
				}
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
					end = start + $scope.numPerPageOpt[2];
					$scope.currentPageStores = $scope.filteredStores.slice(start, end);
					$scope.select($scope.currentPage);
				}else{
					$scope.filteredStores = [];
				}
				logger.logSuccess(res.header.message);
			}
		}).error(function(data) {
				res.data.header ? logger.logError(data.header.message) : logger.logError("Show Data Job Title Failed");
		});
		$scope.button = function(a){
			var data = permissionjob.access;
			if(data){
				switch(a){
					case 'create':
						if(data.create == 1){return true}
					break;
					case 'delete':
						if(data.delete == 1){return true}
					break;
				}
			}
		};
		$scope.open = function(size) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalJob.html",
				controller: "jobCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function (newstudent) {
				if($scope.currentPageStores){
					$scope.currentPageStores.push(newstudent);
				}else{
					$scope.currentPageStores = [];
					$scope.currentPageStores.push(newstudent);
				}
			});
		};
		$scope.edit = function(size,job) {
			var modal;
			modal = $modal.open({
				templateUrl: "editmodalJob.html",
				controller: "jobEditCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return job;
					}
				}
			});
			modal.result.then(function (jobtitle) {
				if(jobtitle.radio == 2){
					var a = [];
					for(a in $scope.jobs){
						if($scope.jobs[a].id == jobtitle.id){
							$scope.jobs[a].filename ="Not Defined";
						}
					};
				}
			});
		};
		$scope.remove = function(id){
			jobTitle.destroy(id)
				.success(function(data) {
					var i;
						for( i = 0; i < id.length; i++) {
							$scope.currentPageStores = filterFilter($scope.currentPageStores, function (store) {
								return store.id != id[i];
							});
						};
					$scope.check.currentPageStores = [];
					logger.logSuccess(data.header.message);
			  }).error(function(data) {
					logger.logError(data.header.message);
			  });
		};	
		var init;
		$scope.numPerPageOpt = [3, 5, 10, 20]; 
		$scope.numPerPage = $scope.numPerPageOpt[2]; 
		$scope.currentPage = 1; 
		$scope.currentPageStores = []; 
		$scope.searchKeywords = ""; 
		$scope.filteredStores = []; 
		$scope.row = "" ;
		$scope.stores =[];
		$scope.select = function(page) {
			 var end, start;
			 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
		};
		$scope.onFilterChange = function() {
			return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
		};
		$scope.onNumPerPageChange = function() {
			return $scope.select(1), $scope.currentPage = 1
		};
		$scope.onOrderChange = function() {
			return $scope.select(1), $scope.currentPage = 1
		};
		$scope.search = function() {
			return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
		};
		$scope.order = function(rowName) {
			return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
		};
		init = function() {
			return $scope.search(), $scope.select($scope.currentPage)
		};
				
	});
	appTablesJob.controller("jobCtrl",function(filterFilter,jobTitle,$http,$scope,$modalInstance,items,logger,ApiURL,$cookieStore,Upload,$window) {
		if(items){
			$scope.formData = items ;
		}
		$scope.test = function(a){
			var e = api.url +  '/api/jobs/download/' + $cookieStore.get('name') + '?key=' + $cookieStore.get('key_api');
				$window.open(e);		
		};
		$scope.save = function(){ 
			$scope.formData.id = $cookieStore.get('id'); 
			$scope.formData.filename = $cookieStore.get('name');
				if ($scope.formData.id == null){
					jobTitle.save2($scope.formData,$scope.formData.id)
						.success(function(data,header,message) {
							var temp = {};						
								temp = $scope.formData;
								temp.id = data.data.id;
								if(data.data.filename == null){
									temp.filename = "Not Defined";
								}
							$modalInstance.close(temp);
							logger.logSuccess(data.header.message);
					  }).error(function(data,status,header,config) {
							logger.logError(data.header.message);
					  });
				}else{
					jobTitle.save($scope.formData)
						.success(function(data,header,message) {
							var temp = {};						
								temp = $scope.formData;
								temp.id = data.data.id;
								$cookieStore.remove('id');
								$cookieStore.remove('name');
							$modalInstance.close(temp);
							logger.logSuccess(data.header.message);
					  }).error(function(data,status,header,config) {
							logger.logError(data.header.message);
					  });
				}
			
		};
		$scope.$watch('files', function () {
			$scope.upload = function (files) {
				if (files && files.length) {
					for (var i = 0; i < files.length; i++) {
						var file = files[i];
						Upload.upload({
							url:  ApiURL.url + '/api/jobs/jobs-title?key=' + $cookieStore.get('key_api'),
							file: file
						}).progress(function (evt) {
							var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
							$scope.log = 'progress: ' + progressPercentage + '% ' +
										evt.config.file.name + '\n' + $scope.log;
						}).success(function (data, status, headers, message) {
								$cookieStore.put('id',data.data.id);
								$cookieStore.put('name',data.data.filename);
							logger.logSuccess(data.header.message);
						});
					}
				}
			};
		});
		$scope.cancelFile = function(){
		 var id = $cookieStore.get('id');
		  if (id != null){
				jobTitle.destroyFile(id)
					.success(function(data,header,message){
						var temp = {};						
							temp = id;
							$cookieStore.remove('id');
						logger.logSuccess(data.header.message);
						$modalInstance.dismiss();
					}).error(function(data,status,header,config) {
								logger.logError(data.header.message);
					});
			} else {
				$modalInstance.dismiss();
			}
		};
		
	});
	appTablesJob.controller("jobEditCtrl", function($location,jobTitle,$scope,$modalInstance,items,logger,$cookieStore,$http,ApiURL,Upload,$window,permissionjob) {
				
		$scope.files=[];
		var g = new File([""], items.filename);
		$scope.files.push(g);
		var a = String(items.title);
		var b = String(items.descript);
		var c = String(items.filename);
		if(items){
			$scope.formData = items;
		}
		var id = items.id;
		$scope.test = function(a){	
			if(items.filename == 'Not Defined'){
				logger.logError("File Not Found");
			}else{
				var e = ApiURL.url +  '/api/jobs/download/' + items.filename + '?key=' + $cookieStore.get('key_api');
				$window.open(e);
			}
		};
		$scope.save = function(){
			jobTitle.updateFile(items.id,$scope.formData,$scope.files)
				.success(function(data,header,message) {
					var temp = {}; 
						temp = $scope.formData;
						temp.id = items.id;
						temp.filename = data.data.filename;
						$scope.formData='';
					logger.logSuccess(data.header.message);
					$modalInstance.close(temp);
			  }).error(function(data,status,header,message) {
					logger.logError(data.header.message);
			  });
		};
		$scope.$watch('files', function () {
			$scope.upload = function (files) {
				if (files && files.length) {
					for (var i = 0; i < files.length; i++) {
						var file = files[i];
						Upload.upload({
							url : ApiURL.url + '/api/jobs/jobs-title/update/' + id + '?key=' + $cookieStore.get('key_api'), 
							file: file
						}).progress(function (evt) {
							var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
							$scope.log = 'progress: ' + progressPercentage + '% ' +
										evt.config.file.name + '\n' + $scope.log;
						}).success(function (data, status, headers, message) {
							$scope.idEdit = data.data.id;
							$scope.bfile=true;
							$scope.formData.radio = 1;
							logger.logSuccess(data.header.message);
						});
					}
				}
			};
		});
		$scope.replaceFile = function(){
			$scope.bfile = false;
		};
		$scope.edit = function(){
			if($scope.formData.filename ==""){
				$scope.open = false;
				$scope.$watch('open',function(){
					$scope.bfile = false;
				});
			}else{
				$scope.open = true;
				$scope.formData.radio = 1;
			}
		};
		$scope.cancelFile = function(){
		 var id = $scope.idEdit;
		  if (id != null){
			jobTitle.destroyFile(id)
				.success(function(data,header,message){
					logger.logSuccess(data.header.message);
					$modalInstance.dismiss();
				}).error(function(data,status,header,config) {
							logger.logError(data.header.message);
				});
			} else {	
				$scope.formData.title = a;
				$scope.formData.descript = b;
				$scope.formData.filename = c;
				$modalInstance.dismiss();
			}
		};
		$scope.$watch('files', function() {
			if($scope.formData.filename == ""){
				$scope.bfile = false;
			}else{
				if($scope.files){
					if($scope.formData.filename != $scope.files[0].name){
						$scope.bfile = false;
					}else{
						$scope.bfile = true;
					}
				}else{
					$scope.bfile = false;
				}
			}
		});
		$scope.hide = function(a){
			var access  = permissionjob.access;				
			if(access){
				switch(a){
					case 'update':
						if(access.update == 1){
							return true
						}
					break;
				}
			}
		};
	}); */

	// CONTROLLER TABLE EMPLOYEE STATUS  
/*		
appTablesJob.controller("tableEmployeeCtrl", ["$scope","filterFilter","$modal","$http","ApiURL","logger","$cookieStore", function($scope,filterFilter,$modal,$http,api,logger,$cookieStore) {
				
				
				$http.get(api.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api') ).success(function(res){
					$scope.users = res.data;  
					$scope.check = {
						users:[]
					};
				}).success(function(data, status, header, config) {
						$scope.role = true;
						logger.logSuccess(data.header.message);
				}).error(function(data, status, headers, config) {
						$scope.role = false;
						logger.logError(data.header.message);
				});
					
				
				
				
				$scope.open = function(size) {
					var modal;
					modal = $modal.open({
						templateUrl: "modalEmployee.html",
						controller: "addEmployeeCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return;
							}
						}
					});
				
					modal.result.then(function (newuser) {
						if($scope.users){
							$scope.users.push(newuser);
						}else{
							$scope.users = [];
							$scope.users.push(newuser);
						}
					});
				};
				
				
				$scope.edit = function(size,user) {
					var modal;
					modal = $modal.open({
						templateUrl: "modalEditEmployee.html",
						controller: "editEmployeeCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return user;
								
							}
						}
					});
				
					
				};
				
			
				
				$scope.remove = function(id){
					$http({
						method : 'DELETE',
						url : api.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api')
					}).success(function(data, status, header){
						var i;
							for( i = 0; i < id.length; i++) {
								$scope.users = filterFilter($scope.users, function (user) {
									console.log(user)
									return user.id != id[i];
								});
							};
						console.log($scope.check.users)	
						$scope.check.users = [];
						logger.logSuccess(data.header.message);
					}).error(function(data, status, header) {
						logger.logError(data.header.message);
					});
				};
				
			
			
}]);



		
appTablesJob.controller("addEmployeeCtrl", ["empList","$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function(empList,$modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
				if(items){
					$scope.formData = items;
				}
				
				$scope.save = function(){
					empList.save($scope.formData)
						.success(function(data,header,message) {
							var temp = {}; 
								temp = $scope.formData;
								temp.id = data.data.id;
							logger.logSuccess(data.header.message);
							$modalInstance.close(temp);
							
					  }).error(function(data,status,header,config) {
							logger.logError(data.header.message);
					  });
				};
				
				$scope.cancel = function() {
					$modalInstance.dismiss("cancel")
				}
				
			
					
				
				
}]);
		
appTablesJob.controller("editEmployeeCtrl",  ["empList","ApiURL","$http","$cookieStore","items","$scope", "$modalInstance","items","logger", function(empList,api,$http,$cookieStore,items,$scope,$modalInstance,items,logger) {
				
				var a = String(items.title);
				if(items){
			
					$scope.formData = items;
				}
		
				$scope.save = function(){
					empList.update(items.id,$scope.formData)
						.success(function(data) {
							var temp = {}; 
								temp = $scope.formData;
								temp.id = items.id
								
							logger.logSuccess(data.header.message);
							$modalInstance.close(temp);
					  }).error(function(data,status,header,config) {
							logger.logError(data.header.message);
					  });
				};
				
				$scope.cancel = function() {
						$scope.formData.title = a;
					$modalInstance.dismiss()
				}
			
			
		}]);
		*/	
		

