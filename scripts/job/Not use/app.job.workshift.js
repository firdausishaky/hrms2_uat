var appJobWorkshift = angular.module("app.job.workshift",["dnTimePicker"]);


appJobWorkshift.controller("workshiftCtrl", ["$scope","$filter","$http","$modal","$log","$cookieStore","ApiURL","$routeParams","filterFilter","logger", function($scope,$filter,$http,$modal,$log,$cookieStore,api,$routeParams,filterFilter,logger) {		
		
			
			$http.get(api.url + '/api/timekeeping/work-shift?key='+ $cookieStore.get('key_api') ).success(function(res){
				
				// FUNCTION GT ID FOR DELETE
				$scope.check = {
					currentPageStores: []
				}
				
				var end, start;
				start = 0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
			
			}).success(function(data, status, header, config) {
					$scope.role = true;
					logger.logSuccess(data.header.message);
			}).error(function(data, status, headers, config) {
					$scope.role = false;
					logger.logError(data.header.message);
			});
			
			
			
			// FUNCTION TABLE BIOMETRIC 
			
			var init;
            $scope.numPerPageOpt = [3, 5, 10, 20]; 
            $scope.numPerPage = $scope.numPerPageOpt[2]; 
            $scope.currentPage = 1; 
            $scope.currentPageStores = []; 
            $scope.searchKeywords = ""; 
            $scope.filteredStores = []; 
            $scope.row = "" ;
            $scope.stores =[];
            $scope.select = function(page) {
                 var end, start;
                 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
            };
            $scope.onFilterChange = function() {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
            };
            $scope.onNumPerPageChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.onOrderChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.search = function() {
                return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
            };
            $scope.order = function(rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
            };
            init = function() {
                return $scope.search(), $scope.select($scope.currentPage)
            };
			
			
			
			
			//################ FUNCTION MODAL ################################################################
			
			$scope.open = function(size) {
					var modal;
					modal = $modal.open({
						templateUrl: "workShift.html",
						controller: "workShiftModalCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return;
							}
						}
					});
					modal.result.then(function (newstudent) {
						if($scope.currentPageStores){
							$scope.currentPageStores.push(newstudent);
						}else{
							$scope.currentPageStores = [];
							$scope.currentPageStores.push(newstudent);
						}
					});
			};
				
				
			$scope.edit = function(size,store) {
					var modal;
					modal = $modal.open({
						templateUrl: "EditworkShift.html",
						controller: "EditShiftCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return store;
							}
						}
					});
			};
				
				
			
			// FUNCTION DELETE
			
			$scope.remove = function(id){
					$http({
					method : 'DELETE',
					url : api.url + '/api/timekeeping/work-shift/' + id + '?key=' + $cookieStore.get('key_api')
					}).success(function(data, status, header){
						var i;
							for( i = 0; i < id.length; i++) {
								$scope.currentPageStores = filterFilter($scope.currentPageStores, function (store) {
									return store.id != id[i];
								});
							};
						$scope.check.currentPageStores = [];
						logger.logSuccess(data.header.message);
					}).error(function(data, status, header) {
						logger.logError(data.header.message);
					});
			};
			
		
				
}]);

appJobWorkshift.controller("workShiftModalCtrl", ["$interval","$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL","$filter","$window", function($interval,$modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api,$filter,$window) {
		
				
				// FUNCTION GET DURATION HOURS
				$scope.$watch('from', function() {
					var a = $filter('date')($scope.from,'HH');
					var b = $filter('date')($scope.from,'mm');
						$scope.$watch('to', function() {
							var c = $filter('date')($scope.to,'HH')
							var d = $filter('date')($scope.to,'mm')
							var date1 = new Date(2015, 0, 1, a,b); 
							var date2 = new Date(2015, 0, 1, c,d); 
							if (date2 < date1) {
								date2.setDate(date2.getDate() + 1);
							}
							var diff = date2 - date1;
							var e = (diff /1000/60/60)
							var f = Math.round(e);
							$scope.hour_per_day = f;	
						});
				});
				
		
				if(items){
					$scope.formData = items;
				}
				
				
				// FUNCTION SAVE
				$scope.save = function(){
					
					$scope.formData._from = $scope.from;
					var a = $filter('date')($scope.from,'HH:mm');
					$scope.formData._from = a;
					$scope.formData._to = $scope.to;
					var b = $filter('date')($scope.to,'HH:mm');
					$scope.formData._to = b;
					$scope.formData.hour_per_day = $scope.hour_per_day;
				
					$http({
						method : 'POST',
						url : api.url + '/api/timekeeping/work-shift?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData,$scope.formData._from,$scope.formData._to,$scope.formData.hour_per_day)  
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				// FUNCTION CANCEL
				$scope.cancel = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);


appJobWorkshift.controller("EditShiftCtrl",function($interval,$modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$filter,$window) {
			
				// FUNCTION GET DURATION
				$scope.$watch('from', function() {
					var a = $filter('date')($scope.from,'HH');
					var b = $filter('date')($scope.from,'mm');
						$scope.$watch('to', function() {
							var c = $filter('date')($scope.to,'HH')
							var d = $filter('date')($scope.to,'mm')
							var date1 = new Date(2015, 0, 1, a,b); 
							var date2 = new Date(2015, 0, 1, c,d); 
							if (date2 < date1) {
								date2.setDate(date2.getDate() + 1);
							}
							var diff = date2 - date1;
							var e = (diff /1000/60/60)
							var f = Math.round(e);
							$scope.hour_per_day = f;	
						});
				});
	
				
				var a = String(items.shift_code);
				var b = String(items.from);
				var c = String(items.to);
				var d = String(items.hour_per_day);
				if(items){
					$scope.form = items;
				}				
				
				
							
				
				// FUNCTION SAVE
				$scope.save = function(){
					
					$scope.form._from = $scope.from;
					var a = $filter('date')($scope.from,'HH:mm');
					$scope.form._from = a;
					$scope.form._to = $scope.to;
					var b = $filter('date')($scope.to,'HH:mm');
					$scope.form._to = b;
					$scope.form.hour_per_day = $scope.hour_per_day;
				
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/timekeeping/work-shift/' + items.shift_code + '?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.form,$scope.form._from,$scope.form._to,$scope.form.hour_per_day)   
					}).success(function(data, status, header, config) {
							var temp = {};
							temp = $scope.formData;		
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				$scope.cancel = function() {
					$scope.form.shift_code = a;
					$scope.form.from = b;
					$scope.form.to = c;
					$scope.form.hour_per_day = d;
					$modalInstance.dismiss("cancel")
	
				};
				
				
});

/*
	var date1 = new Date(2000, 0, 1,  9, 15); // 9:00 AM
	var date2 = new Date(2000, 0, 1, 20, 20); // 5:00 PM
	
	if (date2 < date1) {
		date2.setDate(date2.getDate() + 1);
	}

	var diff = date2 - date1;
	console.log(diff);
	console.log(diff /1000/60/60);
	console.log(date2);
	console.log(new Date);
*/
		
/*	
	$scope.$watch('from', function() {
		var a = $filter('date')($scope.from,'HH:mm')
		$scope.from = a;
	});
	
	
	$scope.$watch('to', function() {
	
			var b = $filter('date')($scope.to,'HH:mm')
			$scope.to = b;						
			var now  = $scope.from;
			var then = $scope.to;
			var ms = moment(now,"HH:mm").diff(moment(then,"HH:mm"));
			var d = moment.duration(ms);
			var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
			var t = s.replace("-","").replace(":","").replace("00","").replace(":","").replace("00","");
			$scope.hour_per_day = t;
			
	});
*/				
/*	
var a = '09';
var b = '00';
var c = '12';
var d = '15';
var date1 = new Date(2015, 0, 1, a,b); 
var date2 = new Date(2015, 0, 1, c,d); 
if (date2 < date1) {
date2.setDate(date2.getDate() + 1);
}
var diff = date2 - date1;
console.log(diff);
console.log(diff /1000/60/60);
console.log(date2);
console.log(new Date);
*/	
/*
$scope.$watch('items', function() {
	$scope.from = items._from;
	$scope.to = items._to;
});
$scope.$watch('from', function() {
	var a = $filter('date')($scope.from,'HH:mm')
	$scope.from = a;
});
$scope.$watch('to', function() {

		var b = $filter('date')($scope.to,'HH:mm')
		$scope.to = b;
		
		// GET DURATION
		var now  = $scope.from;
		var then = $scope.to;
		var ms = moment(now,"HH:mm:ss").diff(moment(then,"HH:mm:ss"));
		var d = moment.duration(ms);
		var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
		var t = s.replace("-","").replace(":","").replace("00","").replace(":","").replace("00","");
		$scope.hour_per_day = t;
		
});
*/


/*
$scope.$watch('items', function(){
	$scope.from = items._from;
	$scope.to = items._to;
	$scope.hour_per_day = items.hour_per_day;
});

$scope.$watch('from', function() {
	
	var a = $filter('date')($scope.from,'HH');
	var b = $filter('date')($scope.from,'mm');
		$scope.$watch('to', function() {
			var c = $filter('date')($scope.to,'HH')
			var d = $filter('date')($scope.to,'mm')
			var date1 = new Date(2015, 0, 1, a,b); 
			var date2 = new Date(2015, 0, 1, c,d); 
			if (date2 < date1) {
				date2.setDate(date2.getDate() + 1);
			}
			var diff = date2 - date1;
			var e = (diff /1000/60/60)
			var f = Math.round(e);
			$scope.hour_per_day = f;	
		});
});
*/	
							
