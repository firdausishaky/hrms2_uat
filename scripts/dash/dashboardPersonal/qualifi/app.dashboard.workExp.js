var appWorkExp = angular.module("app.dashboard.workExp", ["checklist-model"]);


/** factory permission **/
appWorkExp.factory('permissionqualification',function(){
	var self;
	return self = {
		data:function(a){
			self.access = a;
		}
	}
});

appWorkExp.controller("workExpCtrlDashboard",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissionqualification) {

	/** employee id **/					
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()

		/** get all data **/
		$http({
			method : 'GET',
			url : ApiURL.url + '/api/employee-details/qualification/work-exp/'  + $scope.id + '?key=' + $cookieStore.get('key_api')
		}).then(function(res){
			permissionqualification.data(res.data.header.access);
			$scope.workers = res.data.data;
			$scope.delwork = {workers:[]}
			logger.logSuccess(res.data.header.message)
		},function(res){
			res.data ? logger.logError(res.data.header.message) : logger.logError("Show Data works Experience Failed")
		});
		
		/** function hidden button **/
		$scope.hide = function(a){
			var data = permissionqualification.access;
			if(data){
				switch(a){
					case 'create':
					if(data.create == 1){return true}
						break;
					case 'delete':
					if(data.delete == 1){return true}
						break;
					case 'update':
					if(data.update == 1){return true}
						break;
				}
			}
		};
		
		/** modal handler **/
		$scope.open = function(size){
			var modal;
			modal = $modal.open({
				templateUrl: "modalWork.html",
				controller: "modalWorkCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve:{
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function (newstudent) {
				if($scope.workers){
					$scope.workers.push(newstudent);
				}else{
					$scope.workers =[];
					$scope.workers.push(newstudent);
				}
			});
		};
		$scope.edit = function(size,work) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditWork.html",
				controller: "WorkEditCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return work;
					}
				}
			});
			modal.result.then(function(newstore){
				
				for(a in $scope.workers){
					if($scope.workers[a].id==newstore[0].id){
						$scope.workers[a]=newstore[0];
					}
				}
			});
		};
		
		/** function remove **/
		$scope.remove = function(id){
			$http({
				method : 'DELETE',
				url : ApiURL.url + '/api/employee-details/qualification/work-exp/' + id + '?key=' + $cookieStore.get('key_api')
			}).success(function(data){
				var i;
				for( i = 0; i < id.length; i++) {
					$scope.workers = filterFilter($scope.workers,function(store){
						return store.id != id[i];
					});
				};
				$scope.delwork.workers = [];
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Delete Works Experience Failed")
			});
		};
	});

appWorkExp.controller("modalWorkCtrlDashboard", function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$modalInstance,items,$routeParams) {

	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** copy data **/
		if(items){
			$scope.formData=items;
		}
		
		/** get data master skill**/
		/*$http.get(ApiURL.url + '/api/employee-details/qualification/work-exp?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.skills = res;
		});		
		*/


		$scope.checkDate=function(){
			console.log(formData.from_date, formData.to_date)

		}
		/** function save **/	
		$scope.save = function(id){
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/qualification/work-exp/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
				data :$scope.formData ,
				headers: {
					"Content-Type": "application/json"
				}  
			}).success(function(data){
				var temp = {}
				temp = $scope.formData
				temp.id = data.data.id
				temp.length_of_service = data.data.length_of_service
				$modalInstance.close(temp);
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Adding Works Experience Failed")
			});
		};
		
		/** close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}	

	});



appWorkExp.controller("WorkEditCtrlDashboard",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$modalInstance,items,$routeParams,$filter,permissionqualification) {

	/** copy data **/
	if(items){
		$scope.formData = items;
		$scope.formData=angular.copy(items);
	}

	/** get data master skill **/
		/*$http.get(ApiURL.url + '/api/employee-details/qualification/work-exp?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.skills = res;	
		});*/

		/** function save **/
		$scope.save = function(id){
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/qualification/work-exp/' + items.id + '?key=' + $cookieStore.get('key_api'),
				data :$scope.formData ,
				headers: {
					"Content-Type": "application/json"
				}  
			}).success(function(data){
				$modalInstance.close(data.data);
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Update Works Experience Failed")
			});
		};
		
		/** function hide button **/
		$scope.hide = function(a){
			var access  = permissionqualification.access;	
			if(access){
				switch(a){
					case 'update':
					if(access.update == 1){
						return true
					}
					break;
				}
			}
		};

		/** close modal **/
		$scope.edit = function(){
			$scope.button=true;
		};
		$scope.cancel = function(){ 
			$modalInstance.dismiss('cancel');
		}

	});
/*
var a = String(items.company);
var b = String(items.location);
var c = String(items.from_date);
var d = String(items.to_date);
var e = String(items.job);
var f = String(items.length_of_service);
$scope.formData.company = a;
$scope.formData.location = b;
$scope.formData.from_date = c;
$scope.formData.to_date = d;
$scope.formData.job = e;
$scope.formData.length_of_service = f;		
*/



