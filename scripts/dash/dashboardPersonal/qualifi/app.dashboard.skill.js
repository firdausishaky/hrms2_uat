var appFormSkill = angular.module("app.dashboard.skill", ["checklist-model"]);

appFormSkill.controller("skillQualCtrlDashboard", function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissionqualification) {
	
	
	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser');
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** get all data **/
		$http({
			method : 'GET',
			url : ApiURL.url + '/api/employee-details/qualification/skill/'   + $scope.id + '?key=' + $cookieStore.get('key_api')
		}).then(function(res){
			$scope.skills = res.data.data;
			$scope.check = {skills:[]};
			logger.logSuccess(res.data.header.message)
		},function(res){
			res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Skills Failed")
		});
		
		
		/** function hidden button **/
		$scope.hide = function(a){
			var data = permissionqualification.access;
			if(data){
				switch(a){
					case 'create':
					if(data.create == 1){return true}
						break;
					case 'delete':
					if(data.delete == 1){return true}
						break;
				}
			}
		};
		
		/** modal handler **/
		$scope.open = function(size){
			var modal;
			modal = $modal.open({
				templateUrl: "modalSkills.html",
				controller: "SkillCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function(newskills) {
				if($scope.skills){
					$scope.skills.push(newskills);
				}else{
					$scope.skills = [];
					$scope.skills.push(newskills);
				}
			});
		};
		$scope.edit = function(size,skill) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditSkills.html",
				controller: "SkillEditCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return skill;
					}
				}
			});
			modal.result.then(function(newskill){
				for(a in $scope.skills){
					if($scope.skills[a].id==newskill.id){
						$scope.skills[a]=newskill;
					}
				}
			});
		};
		
		/** function remove **/
		$scope.remove = function(id){
			$http({
				method : 'DELETE',
				url : ApiURL.url + '/api/employee-details/qualification/skill/' + id + '?key=' + $cookieStore.get('key_api')
			}).success(function(data){
				var i;
				for( i = 0; i < id.length; i++) {
					$scope.skills = filterFilter($scope.skills, function (skill) {
						return skill.id != id[i];
					});
				};
				$scope.check.skills = [];
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Delete Skills Failed")
			});
		};
		
		
	});

appFormSkill.controller("SkillCtrlDashboard", function($routeParams,$scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,items,$modalInstance) {
	
	
	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser');
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		
		/** get data master skill**/
		$http.get(ApiURL.url + '/api/employee-details/qualification/skill?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.stores = res;
		});			
		
		/** copy data **/
		if(items){
			$scope.formData=items;
		}

		console.log(1,$scope.formData);
		
		
		/** function save **/	
		$scope.save = function(id){
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/qualification/skill/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
				data :$scope.formData ,
				headers: {
					"Content-Type": "application/json"
				}  
			}).success(function(data){
				var temp = {};
				temp = $scope.formData;
				var a, b, c = temp.level;
				for(var i = 0; i < $scope.stores.length; i++) {
					a = $scope.stores[i];
					if(a.id == c) {
						b = a.title;
					};
				};
				temp.level = b;
				temp.id=data.data;
				$modalInstance.close(temp);	 						
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Adding Data Skills Failed")
			});
		};
		
		/** modal close **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}
		
		
	});


appFormSkill.controller("SkillEditCtrlDashboard",function($routeParams,$scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,items,$modalInstance,permissionqualification) {
	
	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser');
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** copy data **/
		if(items){
			$scope.formData = items;
			$scope.formData = angular.copy(items);	
		}
		
		/** get data master skill**/
		$http.get(ApiURL.url + '/api/employee-details/qualification/skill?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.stores = res;
			console.log(2,$scope.stores);
		});		
		
		/** function save**/
		$scope.save = function(id){
			for(a in $scope.stores) {
				if($scope.stores[a].title == $scope.formData.level) {
					$scope.formData.level = $scope.stores[a].id;
				};
			};
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/qualification/skill/' + items.id + '?key=' + $cookieStore.get('key_api'),
				data :$scope.formData ,
				headers: {
					"Content-Type": "application/json"
				}  
			}).success(function(data){
				var temp = {};
				temp = $scope.formData;
				var a, b, c = temp.level;
				for(var i = 0; i < $scope.stores.length; i++) {
					a = $scope.stores[i];
					if(a.id == c) {
						b = a.title;
					};
				};
				temp.level = b;						
				$modalInstance.close(temp);
				logger.logSuccess(data.header.message);
			}).error(function(data) {
				data.header ? logger.logError(data.header.message) : logger.logError("Update Data Skills Failed")
			});
		};
		
		/** function hide button **/
		$scope.hide = function(a){
			var access  = permissionqualification.access;	
			if(access){
				switch(a){
					case 'update':
					if(access.update == 1){
						return true
					}
					break;
				}
			}
		};
		
		/** close modal **/
		$scope.edit = function(){
			$scope.button=true;
		};
		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		
		
		
	});
/*
var a = String(items.skill);
var b = String(items.year_of_exp);
var c = String(items.comment);	
$scope.formData.skill = a;
$scope.formData.year_of_exp = b;
$scope.formData.comment = c;
*/		


