(function(){
	
	angular.module("app.dashboard.overtimeReq",["ngFileUpload"])

	.factory('overtimefactoryDashboard',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,Upload){
		
		var self;
		return self = {
			getID(){
				const onlyUser = $cookieStore.get('UserAccess')
				const notUser = $cookieStore.get('NotUser')
				if (notUser == null) {
					return(onlyUser.id)
				}else{
					return(notUser)
				}

			},
			getaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/overtime/api_check?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data);
						logger.logSuccess('Access Granted');
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
				});
			},callbackaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/overtime/api_check?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setdata(res.data.data);
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},setdata:function(data){
				
				if(data.status == 'user'){
					self.idemp = data.employee_id;
					self.form={};
					self.form.name = this.mainname || data.employee_name;
					self.hide = true;
					////console.log(this.mainname)
				}else{
					self.form={};
					self.form.name = '';
					self.hide = false;
				}
				self.datastatus = data.status;
			},save:function(file){
				// if(self.datastatus=='user'){
				// 	// self.form.name =  self.idemp;
				// 	self.form.name = self.getID()

				// }else{
				// 	// self.form.name = self.label;
				// 	self.form.name = self.getID()
				// }
				x = $cookieStore.get('NotUser')
				if (x == 2014888) {
					if(this.role=='user'){
						self.form.name = this.iduser;
					// self.form.name = self.getID()
					
				}else{
					self.form.name = self.label;
					// self.form.name = self.getID()
				}
			}else{
				if(this.role=='user'){
					// self.form.name = this.iduser;
					self.form.name = self.getID()
					
				}else{
					// self.form.name = self.label;
					self.form.name = self.getID()
				}


			}
			if(file){
				file.upload = Upload.upload({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/overtime/request?key=' + $cookieStore.get('key_api'),
					data :{data:self.form},
					file: file
				}).then(function(res){
					self.callbackaccess();
					self.files=false;
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.callbackaccess()
					self.files=false;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
				});
			}else{				
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/overtime/request?key=' + $cookieStore.get('key_api'),
					data : self.form     
				}).then(function(res){
					self.callbackaccess();
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.callbackaccess();
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
				});
			}
		},
		
		/** FUNCTION GET SCHEDULE WORK HOURS OVERTIME **/
		update : function(){
			self.form.name = self.label;
			self.form.dateRequest =  self.form.dateRequest;
			self.form.test = self.model;
			$http({
				method  :  'POST',
				url  :  ApiURL.url + '/api/attendance/overtime/work-hour?key=' + $cookieStore.get('key_api'),
				data  :  self.form     
			}).success(function(data) {
				self.form.overtime = 0
				self.shiftOver =  data.data.shift_code
				self.works =  data.data.work_hour
				self.origin_time_out = data.data.work_hour.split(" to ")[1];
				self.form.name = data.data.name;
				self.form.work_hour = data.data.work_hour;

				if(data.data.total_overtime && data.data.total_overtime != '-'){
					self.form.total_overtime_bio = data.data.total_overtime;

					var split_wh = data.data.work_hour.split(" to ");
					var split_in = split_wh[0].split(":");
					var split_out = split_wh[1].split(":");

					var time1 = (parseInt(split_in[0])*60)+parseInt(split_in[1]);
					var time2 = (parseInt(split_out[0])*60)+parseInt(split_out[1]);
					var time3 = time2 - time1;

					self.form.timeIn_bio  = data.data.timeIn;
					self.form.timeOut_bio = data.data.timeOut;
					self.form.nextday = data.data.nextday;
					self.form.curdate = self.form.dateRequest;
					self.form.dateNextday = self.form.dateRequest;
					if(data.data.nextday){
						self.form.nextday_status = true;
					}else{
						self.form.nextday_status = false;
					}
				}else{
					self.form.total_overtime_bio = null;
					self.form.timeIn_bio = data.data.timeIn || null;
					self.form.timeOut_bio = data.data.timeOut || null;
					self.form.nextday = data.data.nextday || null;
					self.form.curdate = self.form.dateRequest;
					if(data.data.nextday){
						self.form.nextday_status = true;
					}else{
						var today = new Date(self.form.dateRequest);
						var tomorrow = new Date(self.form.dateRequest);
						tomorrow.setDate(today.getDate()+1);
						self.form.nextday = tomorrow;
								//self.form.nextday = self.form.nextday;
								self.form.nextday_status = false;
							}
						}
						console.log(self.form,"CEK");
						logger.logSuccess(data.header.message);
					}).error(function(data,status,header){
						self.form.name = data.data.name;
						logger.logError(data.header.message);
					});
				},

				/** FUNCTION GET SCHEDULE WORK HOURS UNDERTIME **/
				updateUnder : function(){	
					self.form.name = self.label;
					self.form.dateRequest =  self.form.dateRequest;
					self.form.test = self.model;
					$http({
						method  :  'POST',
						url  :  ApiURL.url + '/api/attendance/overtime/work-hour?key=' + $cookieStore.get('key_api'),
						headers  :  { 'Content-Type'  :  'application/x-www-form-urlencoded',},
						data  :  $.param(self.form)     
					}).success(function(data) {
						self.shiftUnder =  data.data.shift_code
						self.form.from = data.data.from;
						self.form.to = data.data.to;
						self.form.duration = data.data.duration;
						self.form.name = data.data.name;

						logger.logSuccess(data.header.message);
						//self.findUndertime(self.form)
					}).error(function(data){
						self.form.from = '';
						self.form.to = '';
						self.form.duration = ''
						self.form.name = data.data.name;
						logger.logError(data.header.message);
					});
				},


				/** FUNCTION GET SCHEDULE WORK HOURS UNDERTIME **/
			// findUndertime  :  function(a){
			// 	var a = self.form.from;
			// 	var b = self.form.shortUnder;
			// 	function parseTime(s){
			// 	   var c = s.split(':');
			// 	   return parseInt(c[0]) * 60 + parseInt(c[1]);
			// 	}
			// 	var minutes = (parseTime(a) - parseTime(b)) * 60;
			// 	var a = minutes.toString();
			// 	String.prototype.toHHMMSS = function () {
			// 		var sec_num = parseInt(this, 10);
			// 		var hours   = Math.floor(sec_num / 3600);
			// 		var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
			// 		var seconds = sec_num - (hours * 3600) - (minutes * 60);

			// 		if (hours   < 10) {hours   = "0"+hours;}
			// 		if (minutes < 10) {minutes = "0"+minutes;}
			// 		if (seconds < 10) {seconds = "0"+seconds;}
			// 		var time    = hours+':'+minutes;
			// 		return time;
			// 	}
			// 	var b = (a.toHHMMSS());
			// 	var c = b.substring(0,5);
			// 	self.form.shortTime = c;

			// 	/*////console.log(c)
			// 	var d = c.split(':');
			// 	var t = d[1].length;
			// 	////console.log(t)
			// 	if(t == 1){
			// 		var k = 12 - d[1]
			// 	}else{
			// 		var k = d[0]
			// 	}

			// 	var j =  d[0]+':'+k;
			// 	////console.log(j)*/
			// },
			findUndertimes : function(a){

				console.log(a,"xxxxxxxxxxxxxxxxxxxxxxxxxxyyyyyyyyyyyyyyyy")

				var i_modify =  a.shortUnder.split(':');
				var m_modify =  a.from.split(':');

				var  i_hour =   a.from.split(' to ');
				var  i_m_hour  = i_hour[1].split(':');


				var final_undertime  = 0;
				if(parseInt(i_modify[0])    > parseInt(i_m_hour[0])){
					logger.logError("Error you can't request above time on  schedule time");
				}else{

					//console.log(m_modify[0],i_m_hour[0])
					var  i_time2  =   (parseInt(i_modify[0])  * 60) +  parseInt(i_modify[1]);
					var  i_time1  =   (parseInt(i_m_hour[0]) * 60) +  parseInt(i_m_hour[1]);

				    ////console.log(i_time1,i_time2)

				    var total_time  =  parseInt(i_time1) - parseInt(i_time2);

				    if(parseInt(total_time)  < 60){
						//console.log(1)
						self.total   = '0.'+total_time
					}else if(total_time % 60  ==  0){
						//console.log(2,total_time)

						self.total  =  total_time / 60;
					}else{	
						//console.log(3)
						var m_final_undertime = Math.floor(total_time / 60);
						var m_minute_final_undertime   = total_time - (m_final_undertime * 60);

						self.total  = m_final_undertime+'.'+m_minute_final_undertime;
					}
				} 	




				// if(modify[0] != undefined){
				// 	var hour = parseInt(modify[0]);
				// } 
				// if(modify[1] != undefined){
				// 	var minute = ((parseInt(modify[1]) / 15) * 25) /100;
				// } 

				// self.total =  parseFloat(a.duration) - (parseFloat(minute) + parseFloat(hour)) ;
				


				//self.form.shortTime = self.total;


				////console.log('date1',self.schedule);
				
				// var count = self.schedule.length;
				// var total = self.schedule[count - 1];

				// var  arg1 = a.split(':');
				// ////console.log(hour)
				// var arg2 = a.shortUnder.split(':');
				// ////console.log(hours);

				// if(parseInt(arg2[0]) == "00"){
				// 	arg2[0] =  1
				// }

				// if(parseInt(arg1[0]) == "00"){
				// 	arg1[0] =  1
				// }


				// if(parseInt(arg2[1]) == "00"){
				// 	arg2[1] =  60
				// }

				// if(parseInt(arg1[1]) == "00"){
				// 	arg1[1] = 60
				// }



				// //console.log(arg1,'->arg1')
				// //console.log(arg2,'->arg2')
				// var argFin =  (Math.abs(parseInt(arg1[0]) - parseInt(arg2[0]))).toString().length == 1 ? '0'+(Math.abs(parseInt(arg1[0]) - parseInt(arg2[0]))) : (Math.abs(parseInt(arg1[0]) - parseInt(arg2[0]))) ;
				// var argFin1 = (Math.abs(parseInt(arg1[1]) - parseInt(arg2[1]))).toString().length == 1 ? '0'+(Math.abs(parseInt(arg1[1]) - parseInt(arg2[1]))) : (Math.abs(parseInt(arg1[1]) - parseInt(arg2[1]))) ;
				// self.form.shortTime =  argFin+':'+argFin1;


			},
			findUndertime  :  function(a){
				//console.log(a)
				////console.log('test')
				////console.log('time : ',a);

				var hour                = a.from.split(":");
				var to                  = a.to.split(":");
				var manipulateTo_minute = to[0];

				if(parseInt(to[0]) > parseInt(hour[0])){

						////console.log('test',1);
						if(hour[0] == "00"){
							var base_hour = parseInt(to[0])-1;
						}else{
							if(parseInt(to[0]) >= 13 && parseInt(to[0]) <= 23 && parseInt(hour[0]) >= 1 && parseInt(hour[0]) <= 23){
								var base_hour = parseInt(to[0]) - parseInt(hour[0]);
							}else if(parseInt(hour[0]) >= 13 && parseInt(hour[0]) <= 23 && parseInt(to[0]) >= 1 && parseInt(to[0]) <= 23){
								var base_hour = (24 - parseInt(to[0])) - parseInt(hour[0]);
							}else if(parseInt(to[0]) >= 13 && parseInt(to[0]) <= 23 && parseInt(hour[0]) >= 13 && parseInt(hour[0]) <= 23){
								var base_hour = parseInt(to[0]) - parseInt(hour[0]);
							}else{
								var base_hour = ( 12 - parseInt(hour[0])) + parseInt(to[0]);
							}
						}
					}else{
						if(hour[0] == "00"){
							var base_hour = parseInt(to[0]);
						}else{
							var base_hour = parseInt(to[0]) - parseInt(hour[0]);
						}
					}

					////console.log('from_minute',to[1]);
					////console.log('to_minute',hour[1]);
					if(parseInt(hour[1]) ==  0){
						var base_minute0 = 0
					}else if(parseInt(hour[1]) > 29){
						var base_minute0 = 60 - parseInt(hour[1]); 
					}else{
						var base_minute0 =  parseInt(hour[1]);
					} 
					////console.log('base_minute0',base_minute0);
					var compare =  parseInt(to[0]) - parseInt(hour[0]);
					if(compare == 0 && to[0] == "00"){
						var base_minute1 = 0;	
					}else{ 
						if(to[1] == "00" && hour[1] == "00"){
							var base_minute1 = 0;
						}else if(to[1] == "00"){
							var base_minute1 = 0;
						}else{
							if(parseInt(to[1]) > 29){
								var base_minute1 = parseInt(to[1]); 
							}else{
								var base_minute1 = 60 - parseInt(to[1]);	
							}
						}
						
						////console.log('base_minute1',base_minute1);
					}
				////console.log('parseMin0',parseInt(base_minute0))
				////console.log('parseMin1',parseInt(base_minute1))
				var base_minute =  (parseInt(base_minute0) + parseInt(base_minute1))/15;
				////console.log('base_hour',base_hour)	

				var manipulate_base = base_hour * 4;
				var hour_base       = "00";
				var based_time      = 0;
				var total           = parseInt(manipulate_base) + parseInt(base_minute)
				////console.log('hour',(manipulate_base))
				////console.log('minutes',(base_minute))
				////console.log(total)
				var arr = [];
				var i   = 0;
				for(i;  i < total; i++){
					////console.log(i);
					if(based_time < 31 ){
						based_time = parseInt(based_time) + 15;
						if(hour_base.toString().length ==  1){
							if(based_time.toString().length ==  1){
								arr[i]     = '0'+hour_base+':'+ based_time+'0';
							}else{
								arr[i]     = '0'+hour_base+':'+ based_time;
							}
						}else{
							arr[i]     = hour_base+':'+ based_time;
						}
					}else{	
						hour_base = (parseInt(hour_base) + 1);
						////console.log(hour_base.length);

						if(hour_base.toString().length ==  1){
							arr[i]    = '0'+hour_base+':'+'00';			
						}else{
							arr[i]    = hour_base+':'+'00';
						}
						based_time = "00";
					}

					if(total != 0){
						if(i == (total-1)){
							self.schedule =  arr
							self.form.arc = arr[total-1];

						}
					}else{
						self.schedule =  [];
					}
					
				}

				//////console.log(arr);
			},
			/** SEARCHING NAME FUNCTION **/
			onSelect  :  function ($item, $model, $label) {
				self.subordinate = $item.name;
				self.model = $item.name;
				self.label = $item.employee_id;
			},
			getLocation  :  function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name +'?&key=' + $cookieStore.get('key_api'),{
					params :  {
						address  :  val,
					}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},
			
			
			/** FUNCTION RESET FORM **/

			test : function (){
				//console.log('test');
			},
			cancel  :  function(){
				self.callbackaccess()
				self.files = false;
			},
			schedule : function(a){
				////console.log(a)
			},
			/** FORM VARIABLES OVERTIME **/
			schedule : time.schedule
			
			/** FUNCTION GET SCHEDULE WORK HOURS OVERTIME **/
			,schedule_from : function(){
				if(self.works != undefined && self.shiftOver != "DO" ){
					var test =  self.works.split(" ");
					var ti =  time.schedule;
					var i = 0;
					console.info(test[2]);
					for(var prop in ti){
						if(ti[prop] == test[2]){
							var ind  =  parseInt(prop) != 0 ? parseInt(prop) - 1 : parseInt(prop); 
						}
					}
					var arr = [];
					if(ind){
						for(var get in ti){
							if(get > ind){
								arr[i] = ti[get];
								i = i + 1;
							}
						}					
					}

					if(arr.length == 0){
						for(var prop in ti){
							console.log(ti[prop],"TEST",test[0])
							if(ti[prop] == test[0]){
								var ind  =  parseInt(prop) != 0 ? parseInt(prop) - 1 : parseInt(prop); 
							}
						}
						var arr = [];
						if(ind){
							for(var get in ti){
								if(get > ind){
									arr[i] = ti[get];
									i = i + 1;
								}
							}					
						}	
					}
					return arr;
					//console.log('data',arr)
				}

				if(self.works != undefined && self.shiftOver == "DO" ){
					return time.schedule;
				}
			},short : function(){
				//console.log(self.form);
			},findOvertime  :  function(a){
				self.form.overtime = 0;
				self.form.overtime_modified = 0;
				self.form.task = 0;
				var overto = a.overtimeTo;
				//console.log("overto", overto);
				console.log(a,"CHECKONG")
				var overtFrom = a.overtimeFrom;
				if(overto){
					var chk_1  = overto.split(':');
					var to_hour  =   parseInt(chk_1[0]) * 60;
					var to_minute =  parseInt(chk_1[1])
					var  total_1 =  to_hour + to_minute;
					
				}

				if(overtFrom){
					var  chk_2 = overtFrom.split(':');
					var from_hour  =   parseInt(chk_2[0]) * 60;
					var from_minute =  parseInt(chk_2[1]);
					var  total_2 =  from_hour + from_minute;
				}

				var dateReq = new Date(a.dateRequest+" 01:00");
				var dateNextday = new Date(a.dateNextday+" 01:00");

				function diffTime(dt1,dt2){
					var diff2 =(dt2.getTime() - dt1.getTime());
					console.log("DIFF",diff,dt2.getTime(), dt1.getTime());
				  	var diffHrs = Math.floor((diff2 % 86400000) / 3600000); // hours
					var diffMins = Math.round(((diff2 % 86400000) % 3600000) / 60000); // minutes
					return {"hours":diffHrs,"minutes":diffMins};
				}

			    //console.log(total_1, total_2,(total_1 > total_2),dateReq,dateNextday,g,"435345345");
			    if(a.dateRequest != dateNextday){
			    	console.log(a.curdate,a.overtimeFrom)
			    	var t1 = new Date(a.curdate+" "+a.overtimeFrom);
			    	var t2 = new Date(a.dateNextday+" "+a.overtimeTo);
			    	var diff =(t2.getTime() - t1.getTime());

			    	console.log("DIFF 1",diff,t2.getTime(), t1.getTime(),a.dateNextday+" "+a.overtimeTo,a.curdate+" "+a.overtimeFrom);
					  	var diffHrs = Math.floor(diff / 3600000); // hours
						var diffMins = Math.round(((diff % 86400000) % 3600000) / 60000); // minutes
						
						if(diffHrs < 10){ diffHrs = "0"+diffHrs; }
						if(diffMins < 10){ diffMins = "0"+diffMins; }
						console.log(diff,"A",diffHrs+":"+diffMins);

						resul = diffHrs+":"+diffMins;
					}else if(a.dateRequest == dateNextday){
						if(total_1 > total_2){
							var t1 = new Date(a.curdate+" "+a.overtimeFrom);
							var t2 = new Date(a.dateNextday+" "+a.overtimeTo);
							var diff =(t2.getTime() - t1.getTime());

							console.log("DIFF 2",diff,t2.getTime(), t1.getTime());
					  	var diffHrs = Math.floor((diff % 86400000) / 3600000); // hours
						var diffMins = Math.round(((diff % 86400000) % 3600000) / 60000); // minutes
						
						if(diffHrs < 10){ diffHrs = "0"+diffHrs; }
						if(diffMins < 10){ diffMins = "0"+diffMins; }
						console.log(diff,"B",diffHrs+":"+diffMins);

						resul = diffHrs+":"+diffMins;
					}else{
						return logger.logError("Time out overtime must be more than "+self.origin_time_out);
					}
				}else{
					if(a.overtimeTo && a.overtimeFrom){
						return logger.logError("Time out overtime must be more than "+self.origin_time_out);
					}else if(dateReq > dateNextday){
						return logger.logError("date overtime must be more than "+a.curdate);
					}
				}


				// if(total_1 > total_2){
				// 	var t1 = new Date(a.curdate+" "+a.overtimeFrom);
				// 	var t2 = new Date(a.nextday+" "+a.overtimeTo);
				// 	var diff =(t2.getTime() - t1.getTime());
				//   	var diffHrs = Math.floor((diff % 86400000) / 3600000); // hours
				// 	var diffMins = Math.round(((diff % 86400000) % 3600000) / 60000); // minutes
				// 	if(diffHrs < 10){
				// 		diffHrs = "0"+diffHrs;
				// 	}
				// 	if(diffMins < 10){
				// 		diffMins = "0"+diffMins;
				// 	}
				// 	resul = diffHrs+":"+diffMins;
				// 	// var get_final_data  =  total_1 -  total_2;
				// 	// //console.log("get_final_data", get_final_data);
				// 	// if(get_final_data % 60  != null){
				// 	// 	var get_divider  = get_final_data  % 60;
				// 	// 	var get_total_divider = get_final_data  - get_divider;
				// 	// 	get_final_data = get_total_divider  / 60;

				// 	// 	var count  =  String(get_divider);
				// 	// 	if(count.length < 2){
				// 	// 		var resul  =  get_final_data+":00";	
				// 	// 	}else{
				// 	// 		var resul  =  get_final_data+' : '+get_divider;
				// 	// 	} 

				// 	// }	
				// }else{
			 //    	console.log(total_1 , total_2,a.nextday_status, a.dateNextday ,a.nextday,(a.nextday_status && a.dateNextday == a.nextday && a.curdate == a.nextday));
			 //    	//if(!a.total_overtime_bio && a.total_overtime_bio != '-'){
				// 		if(a.dateNextday == a.curdate){
				// 			if(total_1 && total_2){
				// 				return logger.logError("Time out overtime must be more than "+self.origin_time_out);
				// 			}
				// 		}
				// 		total_1 += 1440;
			 //    	//}

				// 	var t1 = new Date(a.curdate+" "+a.overtimeFrom);
				// 	var t2 = new Date(a.nextday+" "+a.overtimeTo);
				// 	var diff =(t2.getTime() - t1.getTime());
				//   	var diffHrs = Math.floor((diff % 86400000) / 3600000); // hours
				// 	var diffMins = Math.round(((diff % 86400000) % 3600000) / 60000); // minutes
				// 	if(diffHrs < 10){
				// 		diffHrs = "0"+diffHrs;
				// 	}
				// 	if(diffMins < 10){
				// 		diffMins = "0"+diffMins;
				// 	}
				// 	resul = diffHrs+":"+diffMins;
				// 	/*var  get_final_data  =  total_1 - total_2;
				// 	if(get_final_data % 60  != null){
				// 		var get_divider  = get_final_data  % 60;
				// 	 	var get_total_divider = get_final_data  - get_divider;
				// 		get_final_data = get_total_divider  / 60;

				// 		var count  =  String(get_divider);
				// 		if(count.length < 2){
				// 			var resul  =  get_final_data+":00";	
				// 		}else{
				// 			var resul  =  get_final_data+' : '+get_divider;
				// 		}   

				// 	}*/
				// }
				////console.log(overto, overtFrom)
				// String.prototype.toHHMMSS = function () {
				// 	var sec_num = parseInt(this, 10);
				// 	var hours   = Math.floor(sec_num / 3600);
				// 	var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
				// 	var seconds = sec_num - (hours * 3600) - (minutes * 60);

				// 	if (hours   < 10) {hours   = "0"+hours;}
				// 	if (minutes < 10) {minutes = "0"+minutes;}
				// 	if (seconds < 10) {seconds = "0"+seconds;}
				// 	var time    = hours+':'+minutes;
				// 	return time;
				// }
				
				// var overto = a.overtimeTo;
			 //    var overtFrom = a.overtimeFrom;
			 //    var to  = overto.split(':');
			 //    var from = overtFrom.split(':');

			 //    if(parseInt(from[0]) >= 20 && parseInt(to[0]) >= 0  ){
			 //    	var to1 = parseInt(to[0])  * 60;
			 //    	var from1 =  ( 24 - parseInt(from[0])) * 60;
			 //    }else if(parseInt(from[0]) <  parseInt(to[0])){
			 //    	var to1 = parseInt(to[0])  * 60;
			 //    	var from1 = parseInt(from[0]) * 60;
			 //    }else if(parseInt(from[0]) >  parseInt(to[0]) && parseInt(to[0]) < parseInt(from[0]) ){
			 //    	var to1 = (24 - parseInt(to[0]))  * 60;
			 //    	var from1 = parseInt(from[0]) * 60;
			 //    }else{
			 //    	var to1 =  parseInt(to[0])  * 60;
			 //    	var from1 =  parseInt(from[0]) * 60;
			 //    }

			 //    if(parseInt(from[1]) == 0 ){
			 //    	if(from[0] == to[0]){
			 //    		var total_hour =  (parseInt(from1) - parseInt(from[1])) + parseInt(to[1]); 
			 //    	}else{
			 //    		var total_hour = ((parseInt(to1) +  parseInt(from1)) - parseInt(from[1])) + parseInt(to[1]); 
			 //    	}
			 //    }

			 //    if(parseInt(from[1]) != 0){
			 //    	if(from[0] == to[0]){
			 //    		to[1] = parseInt(to[1]) == 0 ? 60 : parseInt(to[1])  
			 //    		var total_hour = parseInt(to[1]) -  parseInt(from[1])
			 //    	}else{
			 //    		var total_hour = ((parseInt(to1) -  parseInt(from1)) - parseInt(from[1])) + parseInt(to[1]);
			 //    	}
			 //    }

			 //    var total_overtime =  total_hour /60;

			/*self.form.nextday = data.data.nextday || null;
			self.form.curdate = self.form.dateRequest;
			self.nextday_status = false;*/
				// if(a.total_overtime_bio){

				// 	var split_H_bio = a.timeOut_bio.split(":"),
		 	// 			H_bio 		= parseInt(split_H_bio[0]),
		 	// 			M_bio		= parseInt(split_H_bio[1]),
		 	// 			total_bio 	=  (H_bio*60)+M_bio,

		 	// 			split_H_req = a.overtimeTo.split(":"),
		 	// 			H_req 		= parseInt(split_H_req[0]),
		 	// 			M_req		= parseInt(split_H_req[1]);
		 	// 			total_req 	=  (H_req*60)+M_req,

		 	// 			split_H_origin 	= self.origin_time_out.split(":"),
		 	// 			H_origin 		= parseInt(split_H_origin[0]),
		 	// 			M_origin		= parseInt(split_H_origin[1]);
		 	// 			total_origin 	=  (H_origin*60)+M_origin;

		 	// 		var split_ot		= a.total_overtime_bio.split(":");
			 // 		var total_ot 		= (parseInt(split_ot[0])*60)+parseInt(split_ot[1]);
			 // 		var dateFromChange 	= false;

			 // 		if(total_2 > total_origin){
			 // 			dateFromChange = true;
			 // 			total_ot = total_ot - (total_2 - total_origin);
			 // 		}

			 // 		var t1 = new Date(a.curdate+" "+a.overtimeFrom);
				// 	var t2 = new Date(a.nextday+" "+a.overtimeTo);
				// 	var diff =(t2.getTime() - t1.getTime());
				//   	var diffHrs = Math.floor((diff % 86400000) / 3600000); // hours
				// 	var diffMins = Math.round(((diff % 86400000) % 3600000) / 60000); // minutes
				// 	if(diffHrs < 10){
				// 		diffHrs = "0"+diffHrs;
				// 	}
				// 	if(diffMins < 10){
				// 		diffMins = "0"+diffMins;
				// 	}
				// 	resul = diffHrs+":"+diffMins;
				// 	console.log(total_2,total_origin,a,"-===========-")
				// 	if(a.dateNextday == a.nextday && a.nextday_status){

				// 		//console.log(a.timeIn_bio , a.overtimeFrom ,a.timeOut_bio, a.overtimeTo,"check lagi");


				// 	  	console.log(diffHrs,diffMins,"dasdasd");
				// 		if(total_bio > total_req){
				// 			var tmp1 	= total_bio - total_req;
				// 			var tmp_h	= Math.floor((total_ot - tmp1)/60);
				// 			var tmp_m 	= ((total_ot - tmp1)%60);
				// 			console.log(tmp_m,tmp_h,"check1");
				// 			if(tmp_h < 10){
				// 				tmp_h = "0"+tmp_h;
				// 			}
				// 			if(tmp_m < 10){
				// 				tmp_m = "0"+tmp_m;
				// 			}
				// 			resul = tmp_h+":"+tmp_m; 
				// 			//console.log(resul,"resul1");
				// 		}
				// 		else if(total_bio < total_req){
				// 			var tmp1 	= total_req;
				// 			var tmp_h	= Math.floor((total_ot + (tmp1-60))/60);
				// 			var tmp_m 	= ((total_ot + tmp1)%60);
				// 			console.log(tmp_m,tmp_h,"check2");
				// 			if(tmp_h < 10){
				// 				tmp_h = "0"+tmp_h;
				// 			}
				// 			if(tmp_m < 10){
				// 				tmp_m = "0"+tmp_m;
				// 			}
				// 			resul = tmp_h+":"+tmp_m; 
				// 			//console.log(resul,"resul2");
				// 		}
				// 		else{
				// 			//var split_H = a.total_overtime_bio.split(":");
				//  			//a.total_overtime_bio = parseInt(split_H[0])
				//  			if(dateFromChange){
				//  				var tmp1 	= total_req;
				// 				var tmp_h	= Math.floor((total_ot + (tmp1-60))/60);
				// 				var tmp_m 	= ((total_ot + tmp1)%60);
				// 				console.log(tmp_m,tmp_h,"check2");
				// 				if(tmp_h < 10){
				// 					tmp_h = "0"+tmp_h;
				// 				}
				// 				if(tmp_m < 10){
				// 					tmp_m = "0"+tmp_m;
				// 				}
				// 				resul = tmp_h+":"+tmp_m; 
				//  			}else{
				//  				resul = a.total_overtime_bio; 
				//  			}
				// 		}

				// 	}

				// }
				if(a.overtimeTo && a.overtimeFrom){
					if(resul.search("-") != -1){
						self.form.overtime = 0;
						return logger.logError("Time out overtime must be more than "+self.origin_time_out);
					}
					self.form.overtime =  resul;
					
				}else{
					self.form.overtime = 0;
				}
				try{

					var broken_to_piece  = resul.split(':');
					var broken_to_piece  =  (parseInt(broken_to_piece[0]) *  60) + parseInt(broken_to_piece[1]);   
				    //console.log(broken_to_piece)

				}catch(e){
					var broken_to_piece  =  resul;   
				    //console.log(broken_to_piece)
				}

				self.form.overtime_modified =  broken_to_piece;
				self.form.task = self.shiftOver
				return;
				// //console.log(self.form.overtime)
			},
			hideShow:function(){
				x = $cookieStore.get('NotUser')
				if (x) {
					if (x == 2014888) {
						if (this.SuperUserSearch = true){
							this.NotSuperUserSearch = false
						}
					}else{
						if (this.NotSuperUserSearch = true){
							this.SuperUserSearch = false
						}
					}					
				}
			}

		}
	})


/** CONTROLLER **/  
.controller('overUnderControllerDashboard',function(overtimefactoryDashboard,ApiURL,$cookieStore,$scope,$http,GetName){
	var self = this;
	self.handler = overtimefactoryDashboard;
	self.handler.getaccess()
	self.handler.short()
	self.handler.hideShow()
	$scope.name = GetName.getname()
})


}())

/*
 FUNCTION SHOW COMPENSATION WITH
 if(self.form.total >= 4){
	self.form.hidecom = true
}else{
	self.form.hidecom = false

				
				

//alert(a.toHHMMSS());
		
		//if(	minutes >= 60 ){
		//	var e = minutes/60;
		//}else{
		//	var e = minutes;
		//}
		
	/*	
		var a = self.maindata.data.to.substring(0,2)
		var b = self.maindata.data.to.substring(3,5)
		var c = self.form.shortUnder.substring(0,2)
		var d = self.form.shortUnder.substring(3,5)
		if(b < d ){
			var e = 60 - d;
		}else{
			var e = b - d;
		}
		var f = a - c ;
		if(e != 0){
			var f = f -1;
		}else{
			var f = f;
		}
		self.form.shortTime = f + ' . ' + e;


var f = Math.round(e);
		$scope.hour_per_day = f;
		
//e % 3600 == 0 ? e : e = ((e - (e % 3600))/3600) + (((e % 3600)/3600)/100)
						
		
var d = [];
var j = 0;
while(j<60){
var e = j+=15;
d.push(e);
}

var a = [];
for(var i =0; i<24; i++){
for(var j = 0; j < d.length; j++) {
	var b = i +":"+ d[j];
	if(b == i +":"+ 60) {
		b = i+1 +":"+ "00";
		if(b == "24:00") {
			b = "00:00";
		}
	}
	a.push(b);
}
}
//////console.log(a)

var hours, minutes, ampm;
var time = [];
for(var i = 0; i <= 1440; i += 15){
	hours = Math.floor(i / 60);
	minutes = i % 60;
	if (minutes < 10){
		minutes = '0' + minutes; // adding leading zero
	}
	ampm = hours % 24 < 12 ? 'AM' : 'PM';
	hours = hours % 12;
	if (hours === 0){
		hours = 12;
	}
	time.push(hours + ':' + minutes + ' ' + ampm); 

}
////console.log(time)	
*/
