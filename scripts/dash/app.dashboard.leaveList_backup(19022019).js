(function(){

	angular.module('app.dashboard.LeaveList',[])
	
	.factory('leavelistfactoryDashboard',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,Upload,$window,GetName){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;
		}
		var NAME = '_employee_id';
		var FIRST = '_data';
		var x = localStorage.getItem(FIRST);
		var s = localStorage.getItem(NAME);
		var itemx = x ? JSON.parse(x) : {};
		var items = s ? JSON.parse(s) : {};
		var self;
		return self = {
			getID:function(){
				const onlyUser = $cookieStore.get('UserAccess')
				const notUser = $cookieStore.get('NotUser')
				if (notUser == null) {
					return(onlyUser.id)
				}else{
					return(notUser)
				}

			},
			setheaders: function(){

				//['Leave Balance (days)','LeaveBalance',15],
				this.headers = new createheaders(
					['Date Request','Date',25],
					['Availment Date','Date',25],
					['Employee Name','EmployeeName',15],
					['Leave Type','leave_type',15],
					['Number of Days','NumberOfDay',15],
					['Status','status',15]
					);
				this.setactiveheader(this.headers[0],false)
			},adddata: function(a){
				this.maindata.push(a);
				self.zigot.push(a)
			},setdata: function(a){
				for(var i=0; i < a.length; i++){
					if(a[i].leave_type == 'Accumulation Day Off'){ 
						a[i].taken_day = a[i].dayoff_between; 
					}
				}
				this.maindata = a;
				self.zigot =  a
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				detail:{
					animation: true,
					templateUrl: 'modalrequest',
					controller: 'modallistdetail',
					controllerAs: 'modalrequest',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Edit"
						}	
					}
				},
				app:{
					animation: true,
					templateUrl: 'modalapprej',
					controller: 'aproveleavelist',
					controllerAs: 'modalapprej',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Approve"
						}
					}
				},
				rej:{
					animation: true,
					templateUrl: 'modalapprej',
					controller: 'rejectleavelist',
					controllerAs: 'modalapprej',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Reject"
						}
						
					}
				},
				can:{
					animation: true,
					templateUrl: 'modalapprej',
					controller: 'cancelleavelist',
					controllerAs: 'modalapprej',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Cancel"
						}
						
					}
				}
			},
			openmodal: function(a,b){
				
				var emp_id = b.employee_id;
				
				self.form.employees =  $cookieStore.get('employee_id')
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'app':
						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
							data : { 0 : data}
						}).then(function(res){
							console.log(res,"Approve")
								//console.log("res", res);
								// var result =  res.data.data;
								// //console.log("result", result);
								// var approval  =  result.approval;
								// var approver  =  result.approver;
								// var  idx   =  result.id
								// //console.log("idx", idx);

								// //console.log(self.zigot)

								// //loop data 
								// //
								// for(var prop in self.zigot){
								// 	var index  =   self.zigot[prop];
								// 	//console.log("index", index);

								// 	if(index.id  ==  idx){
								// 		if(approver ==  true){ 
								// 			index.status = "Approval"	
								// 		}	
								// 		console.log(self.zigot)
								// 	}
								// }

								//self.setdata(res.data.data});
								self.searchData(emp_id);
								logger.logSuccess("Success Approving data");
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Approve Leave List Failed")
							});
						break;
						case 'rej':
						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule/reject_notif?key=' + $cookieStore.get('key_api'),
							data : { 0 : data}
						}).then(function(res){
							console.log(res,"REJECTED")
								// //console.log("res", res);
								// var result =  res.data.data;
								// //console.log("result", result);
								
								// var reject  =  result.reject;
								// var  idx   =  result.id
								// //loop data 
								// //
								// for(var prop in self.zigot){
								// 	var index  =   self.zigot[prop];
								// 	//console.log("index", index);

								// 	if(index.id  ==  idx){
								// 		if(reject ==  true){ 
								// 			index.status = "Rejected"	
								// 		}	
								// 		console.log(self.zigot)
								// 	}
								// }
								// self.searchlist();
								self.searchData(emp_id);
								logger.logSuccess('Success to reject request :)');
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Reject Leave List Failed")
							});
						break;
						case 'can':
						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule/cancle_notif?key=' + $cookieStore.get('key_api'),
							data : data
						}).then(function(res){
							console.log(res,"CANCEL")
								// //console.log("res", res);
								// var result =  res.data.data;
								// //console.log("result", result);
								
								// var Cancel  =  result.Cancel;
								// var  idx   =  result.id
								// //loop data 
								// //
								// for(var prop in self.zigot){
								// 	var index  =   self.zigot[prop];
								// 	//console.log("index", index);

								// 	if(index.id  ==  idx){
								// 		if(Cancel ==  true){ 
								// 			index.status = "Cancel"	
								// 		}	
								// 		console.log(self.zigot)
								// 	}
								// }
								// self.searchlist();
								self.searchData(emp_id);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Cancel Leave List Failed")
							});
						break;
					}
					
				});
			},onSelect:function($item,$model,$label){
				self.model = $item.name;
				self.label = $item.employee_id;
				self.form.employees = $item.employee_id;
				console.log(self.label)
			},getLocation:function(val){
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_name +'?&key=' + $cookieStore.get('key_api'),{
					params:{address:val,}
				}).then(function(response){
					//console.log('response',response)
					//items = response.data[0].employee_id;
					//localStorage.setItem(NAME,JSON.stringify(items));
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},getaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/leave/getList?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					if(res.data.header.message == 'Show record data'){
						$cookieStore.put('employee_id',res.data.data.employee_id)
						self.setdata([]);
						if(res.data.data.employee_role == 'User'){
							self.accessOtherEmp = false;
							self.accessAction = true
						}else{
							self.accessAction =  false
							self.accessOtherEmp = true;
						}
						self.setselect(res.data.data);
						self.mainaccess = res.data.header.access;
						logger.logSuccess('Access Granted');
					}else{
						self.mainaccess = res.data.header.access;
						logger.logError('Access Unauthorized');
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave List Failed")
				});
			},setselect:function(select){
				self.leaves = select.LeaveType;
				self.data = select.data_department;		
				self.form = {};
				self.form.from = select.from;
				self.form.to = select.to;
				self.form.pending = select.pending;
				this.name = select.employee_name;
				this.idemployee = select.employee_id;
				this.emprole = select.employee_role;
				if(this.emprole=="Supervisor" || this.emprole=="HRD" || this.emprole=="SUPERUSER" ){
					self.form.employee_name = '';
					self.form.employee_role = 'HRD';
				}else{
					self.form.employee_name = this.name;
					self.form.employee_role = 'User';
				}
				self.form.employee_name = self.getID()
				self.form.employees = self.getID()

			},button:function(a){
				if(self.mainaccess){
					switch(a){
						case 'read':if(self.mainaccess.read == 1){return true}break;
					}
				}
			},searchlist: function(){
				// if(this.emprole=="Supervisor" || this.emprole=="HRD"){
				// 	self.form.employee_name = self.label;
				// }else{
				// 	self.form.employee_name = this.idemployee;
				// }
				//console.log('items employee_id' ,items)
				//self.form.employee_name =  items

				//console.log(self.label,$cookieStore.get('employee_id'))
				// self.form.employee_name =   $cookieStore.get('employee_id');
				console.log("ACCESS",self.accessOtherEmp,self.form.employees);
				if(self.accessOtherEmp){
					if(!self.form.employees){
						self.form.employee_name = null;
					}else{
						self.form.employee_name = self.form.employees;
					}
				}else{
					self.form.employee_name =   $cookieStore.get('employee_id')
				}
				
				$cookieStore.put('employee_id',$cookieStore.get('employee_id'))
				//console.log('test',self.form.employee_name)	
				//console.log('test',self.form)
				console.log(self.form)
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/leave/searchList?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.setdata(res.data.data);
					console.log(res.data.data,"SEARCH LIS #########################")
					//itemx = res.data.data;
					//localStorage.setItem(FIRST,JSON.stringify(itemx))
					if(!self.form.employees){
						self.form.employee_name = null;
					}else{
						self.form.employee_name = self.model || self.name;
					}
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.employee_name = self.model || self.name;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave List Failed")
				});
			},searchData: function(id){
				// if(this.emprole=="Supervisor" || this.emprole=="HRD"){
				// 	self.form.employee_name = self.label;
				// }else{
				// 	self.form.employee_name = this.idemployee;
				// }
				//console.log('items employee_id' ,items)
				//self.form.employee_name =  items

				//console.log(self.label,$cookieStore.get('employee_id'))
				
				self.form.employee_name =   $cookieStore.get('employee_id')
				console.log("ACCESS",self.accessOtherEmp);
				self.form.employees = id;
				if(self.accessOtherEmp){
					if(!self.form.employees){
						self.form.employee_name = null;
					}else{
						self.form.employee_name = self.form.employees;
					}
				}else{
					self.form.employee_name =   $cookieStore.get('employee_id')
				}
				$cookieStore.put('employee_id',$cookieStore.get('employee_id'))
				//console.log('test',self.form.employee_name)	
				//console.log('test',self.form)
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/leave/searchList?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.setdata(res.data.data);
					console.log(res.data.data,"SEARCH LIS #########################")
					//itemx = res.data.data;
					//localStorage.setItem(FIRST,JSON.stringify(itemx))
					if(!self.form.employees){
						self.form.employee_name = null;
					}else{
						self.form.employee_name = self.model || self.name;
					}
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.employee_name = self.model || self.name;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave List Failed")
				});
			},
			reset: function(){
				self.getaccessDefault()
			},export: function(data){
				if(!data){void 0; logger.logError('Please Searching Data Before Export') }
				for(a in self.maindata){
					console.log(self.maindata)
					delete self.maindata[a].id;
					delete self.maindata[a].Employee_id;
					delete self.maindata[a].permission;
					alasql('SELECT * INTO XLSX("Leave List.xlsx",{headers:true}) FROM ?',[self.maindata]);
					break;
				}
			}
			,setbuttonapprove:function(a,b){
				//return console.log(b);
				var stat = b.status.toLowerCase();
				//console.log(b,90000000000000000);
				var emp_login = $cookieStore.get('NotUser');
				if(b.status_id==1){
					switch(a){
						case 'approval':
						return b.action_button.approve;
				 			/*if(b.status_id==1 && stat=='pending approval'){
				 				if(emp_login == b.employee_id && emp_login != '2014888'){
				 					if(b.requestor == 'not_emp'){
				 						return true;
				 					}else if(b.requestor == 'emp'){
				 						return false;
				 					}
				 				}else{
									return true;
				 				}
							}else{
								return false;
							}*/
							break;
							case 'cancel':
							return b.action_button.cancel;
				 			/*if(b.status_id==1 && stat=='pending approval'){
				 				if(emp_login != b.employee_id && emp_login != '2014888'){
				 					if(b.requestor == 'not_emp'){
				 						return true;
				 					}else if(b.requestor == 'emp'){
				 						return false;
				 					}
				 				}else{
									return true;
				 				}
							}else{
								return false;
							}*/	
							break;
							case 'reject':
							return b.action_button.reject;
				 			/*if(b.status_id == 1 && stat=='pending approval'){
				 				//console.log("reject",emp_login == b.employee_id,emp_login, b.employee_id);
								if(emp_login == b.employee_id && emp_login != '2014888'){
				 					if(b.requestor == 'not_emp'){
				 						return true;
				 					}else if(b.requestor == 'emp'){
				 						return false;
				 					}
				 				}else{
									return true;
				 				}
							}else{
								return false;
							}*/
							break;
						}
					}
				/*this.role = b;
				switch(a){
					case 'approve':
						if(this.role == 'HRD' || this.role == 'Supervisor' || this.role == 'SUPERUSER'){return true}
					break;
					case 'reject':
						if(this.role == 'HRD' || this.role == 'Supervisor' || this.role == 'SUPERUSER'){return true}
					break;
					case 'cancel':
						if(this.role == 'User'){return true}
					break;
			}*/
		}
		,getdetail:function(form){ 
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/leave/searchDetail/' +  form.id  +'?key=' + $cookieStore.get('key_api'),
				data :form     
			}).then(function(res){
					//console.log(1,res)
					// var splits = res.data.data.Time.split(" ");
					// res.data.data.Time = splits[1];
					// res.data.data.Dates = splits[0];
					self.setdetailrequest(form,res.data.data);
					self.comment(res.data.data.comment)
					logger.logSuccess('Show Details Request');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Details Request Failed")
				});
		},hideByStatus:function(status){ 
			return status;
		},callgetdetail:function(form){
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/leave/searchDetail/' +  form.id  +'?key=' + $cookieStore.get('key_api'),
				data :form     
			}).then(function(res){
				console.log(2,res)
				self.setdetailrequest(form,res.data.data);
				self.comment(res.data.data.comment)
			});
		},comment:function(data){
			self.datacomment = data;
		},timeConvertor : function(time) {
			var PM = time.match('PM') ? true : false

			time = time.split(':')
			var min = time[1]

			if (PM) {
				var hour = 12 + parseInt(time[0],10)
				var sec = time[2].replace('PM', '')
			} else {
				var hour = time[0]
				var sec = time[2].replace('AM', '')       
			}

			var t= hour + ':' + min + ':' + sec;
			return t;

		},setdetailrequest:function(dataA,dataB){
				// dataA.request_time = dataB.request_time;
				if(dataB.leave_type != 'Accumulation Day Off'){
					var date1_new = new Date(dataB.from_);
					var date2_new = new Date(dataB.to_);
					var timeDiff_new = Math.abs(date2_new.getTime() - date1_new.getTime());
					var diffDays_new = Math.ceil(timeDiff_new / (1000 * 3600 * 24)); 
					var dob  = diffDays_new+1 ;
					if(dataB.taken_day != dob && dob){
						dataB.taken_day = dob;
						dataB.dayoff_between = dob;
					}else{
						dataB.taken_day = dataB.dayoff_between;
					}
				}
				if(dataB.date_ado_Next_Offday || dataB.leave_type == 'Accumulation Day Off'){

					for(var i=0; i < dataB.data_ado.length; i++){
						var tmp_newSch 		= "NewSchedule_"+dataB.data_ado[i].name_ado;
						var dt_ado 			= "date_ado_"+dataB.data_ado[i].name_ado;
						var avail_ado 			= "avail_"+dataB.data_ado[i].name_ado;

						dataA[tmp_newSch] 	= " ( "+dataB.data_ado[i].Schedule+" )";
						dataA[dt_ado] 		= dataB.data_ado[i].date_ado;
						dataA[avail_ado] 	= dataB.data_ado[i].change_date+" ( ADO )"
					}
					if(dataB.date_ado_Next_Offday){
						dataA.date_ado_Next_Offday=dataB.date_ado_Next_Offday;
					}
					if(dataB.leave_type == 'Accumulation Day Off'){
						dataB.taken_day = dataB.dayoff_between;
					}
					dataA.availment = true;
					dataA.data_ado = dataB.data_ado;
/*					dataA.NewSchedule_1st=dataB.NewSchedule_1st;
					dataA.NewSchedule_2nd=dataB.NewSchedule_2nd;
					dataA.NewSchedule_3rd=dataB.NewSchedule_3rd;
					dataA.NewSchedule_4th=dataB.NewSchedule_4th;
					dataA.date_ado_1st=dataB.date_ado_1st;
					dataA.date_ado_2nd=dataB.date_ado_2nd;
					dataA.date_ado_3rd=dataB.date_ado_3rd;
					dataA.date_ado_4th=dataB.date_ado_4th;
					dataA.date_ado_4th=dataB.date_ado_4th;*/
				}

				dataA.LeaveBalance = dataB.LeaveBalance;
				dataA.taken_day = dataB.taken_day;
				dataA.numberDay = dataB.number_of_day;
				dataA.approval = dataB.approval;
				dataA.approver = dataB.approver;
				dataA.Date   =  dataB.create_date; 
				dataA.Dates 	= dataB.Time[0];
				dataA.Time = self.timeConvertor(dataB.Time[1]);
				dataA.request_time  =  dataB.request_time
				dataA.approval = dataB.approval;
				dataA.approver = dataB.approver;
				dataA.status = dataB.status;
				dataA.Date_table = dataB.Date_table;
				dataA.dayoff_between=dataB.dayoff_between;
				dataA.flow_approval = dataB.flow_approval;
				dataA.RequestBy = dataB.RequestBy;
				//console.log(dataB,"ADO INC")
				/** with ado **/
			},actiondetail:function(a,datas){
				
				if(datas.leave_type != "Accumulation Day Off"){
					var data = datas.form;
				}else{
					var data = datas;
				}
				if(!data){
					data = datas;	
				}

				var emp_id = data.employee_id;
				switch(a){
					case 'app':
						//console.log('test',data)
						data.type = 2;

						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
							data :{ 0 :data}
						}).then(function(res){
							// self.setdetailrequest(data,res.data.data);
							// console.log(self.zigot)
							// var result =  itemx;
							// var value = res.data.data;
							// for(var prop in result){
							// 	if(result[prop].id == value.id){
							// 		delete result[prop]
							// 		self.setdata(result)									
							// 	}
							// }
							//self.form.status = 'Approval';
							//$modalInstance.close();
							self.modalInstance.dismiss("Close");
							self.searchData(emp_id);
							logger.logSuccess(res.data.header.message);

						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Approve Leave List Failed")
						});
						break;
						case 'rej':
						
						data.type = 3;
						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule/reject_notif?key=' + $cookieStore.get('key_api'),
							data : { 0 : data}
						}).then(function(res){
							// self.setdetailrequest(data,res.data.data);
							// var result =  itemx;
							// var value = res.data.data;
							// for(var prop in result){
							// 	if(result[prop].id == value.id){
							// 		delete result[prop]
							// 		self.setdata(result)									
							// 	}
							// }
							//self.form.status = 'Rejected';
							//$modalInstance.close();
							self.modalInstance.dismiss("Close");
							self.searchData(emp_id);
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Reject Leave List Failed")
						});
						break;
						case 'can':
						data.type = 4;
						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule/cancle_notif?key=' + $cookieStore.get('key_api'),
							data : { 0 : data},
						}).then(function(res){
							// self.setdetailrequest(data,res.data.data);
							// var result =  itemx;
							// var value = res.data.data;
							// for(var prop in result){
							// 	if(result[prop].id == value.id){
							// 		delete result[prop]
							// 		self.setdata(result)									
							// 	}
							// }
							// console.log('hole',self.setdata());
							//self.form.status = 'cancel';
							//$modalInstance.close();
							self.modalInstance.dismiss("Close");
							self.searchData(emp_id);
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Cancel Leave List Failed")
						});
						break;
					}
				},getfile:function(file){
					if(file == 'Not Defined' || file == 'null' ){
						logger.logError("File Not Found");
					}else{
						var e = ApiURL.url +  '/api/leave/getFile/' + file + '?key=' + $cookieStore.get('key_api');
						$window.open(e);
					}
				},send:function(file,form){
					console.log('test');
					if(file){
						file.upload = Upload.upload({
							method : 'POST',
							url : ApiURL.url + '/api/leave/save_request/' + form.id + '?key='+ $cookieStore.get('key_api'),
							data :form,
							file: file
						}).then(function(res){
							self.callgetdetail(form)
							form.newcomment='';
							self.files = false; 
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
						});
					}else{				
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/save_request/' + form.id + '?key='+ $cookieStore.get('key_api'),
							data :form    
						}).then(function(res){
							self.callgetdetail(form)
							form.newcomment='';
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
						});
					}
				},
				hideShow : function(){
					console.log($cookieStore.get('access'))
					checkRole = $cookieStore.get('access')
					if (checkRole == 200) {
						if (this.canAccess = true){
							this.cantAccess = false
						}
					}else{
						if (this.cantAccess = true){
							this.canAccess = false

						}
					} 
				},
				getaccessDefault:function(){
					$http({
						method : 'GET',
						url : ApiURL.url + '/api/leave/getList?key=' + $cookieStore.get('key_api')
					}).then(function(res){
						if(res.data.header.message == 'Show record data'){
							$cookieStore.put('employee_id',res.data.data.employee_id)
							if(res.data.data.employee_role == 'User'){
								self.accessOtherEmp = false;
								self.accessAction = true
							}else{
								self.accessAction =  false
								self.accessOtherEmp = true;
							}
							self.setdata([])
							self.setselectDefault(res.data.data);
							self.mainaccess = res.data.header.access;
							logger.logSuccess('Access Granted');
						}else{
							self.setdata([])
							self.mainaccess = res.data.header.access;
							logger.logError('Access Unauthorized');
						}
					},function(res){
						self.setdata([])
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave List Failed")
					});
				},setselectDefault:function(select){
					self.leaves = select.LeaveType;
					self.data = select.data_department;		
					self.form = {};
					self.form.from = select.from;
					self.form.to = select.to;
					self.form.pending = select.pending;
					this.name = select.employee_name;
					this.idemployee = select.employee_id;
					this.emprole = select.employee_role;
					if(this.emprole=="Supervisor" || this.emprole=="HRD" || this.emprole=="SUPERUSER" ){
						self.form.employee_name = '';
						self.form.employee_role = 'HRD';
					}else{
						self.form.employee_name = this.name;
						self.form.employee_role = 'User';
					}
				}
			}
		})

.controller('leaveListCtrlDashboard',function(leavelistfactoryDashboard,GetName,$scope){
	var self = this;
	self.handler = leavelistfactoryDashboard;
	self.handler.getaccess()
	self.handler.hideShow()
	// self.handler.searchlist()
	// console.log(GetName.getname())
	$scope.employee_name = GetName.getname()
	
})

.controller('modallistdetail',function(leavelistfactoryDashboard,$modalInstance,data,title){
	var self = this;
	self.handler = leavelistfactoryDashboard;
	self.form = data;
	if(data.status_id == 1){
		self.handler.hideByStatus(false)

		console.log(self.handler.hideByStatus())
	}else{


		self.handler.hideByStatus(true);
		console.log(self.handler.hideByStatus(true))
	}
	self.form=angular.copy(data);
	console.log(self.form,"TEST##############")
	self.handler.modalInstance = $modalInstance
	self.handler.getdetail(self.form);
	self.approve = function(){
		$modalInstance.close(self.form);
	}
	self.reject = function(){
		$modalInstance.close(self.form);
	}
	self.back = function(){
		$modalInstance.dismiss("Close")
	}
			//self.handler.setbuttonapprove();
		})

.controller('aproveleavelist',function(leavelistfactoryDashboard,$modalInstance,title,data){
	var self = this;
	self.title = title;
	self.form=data
	self.submit = function(){
				//$modalInstance.dismiss('Close');
				$modalInstance.close(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss("close")
			}
			self.back = function(){
				$modalInstance.dismiss("Close")
			}			
		})
.controller('rejectleavelist',function(leavelistfactoryDashboard,$modalInstance,data,title){

	var self = this;
	self.title = title;
	self.form=data
	self.submit = function(){
				//$modalInstance.dismiss('Close');
				$modalInstance.close(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss("Close")
			}
			self.back = function(){
				$modalInstance.dismiss("Close")
			}
		})
.controller('cancelleavelist',function(leavelistfactoryDashboard,$modalInstance,title,data){
	var self = this;
	self.title = title;
	self.form=data
	self.submit = function(){
				//$modalInstance.dismiss('Close');
				$modalInstance.close(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
			self.back = function(){
				$modalInstance.dismiss("Close")
			}			
		})


.run(function(leavelistfactoryDashboard){
	leavelistfactoryDashboard.setheaders();
})

}())