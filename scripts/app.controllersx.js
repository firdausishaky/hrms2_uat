var appCtrl = angular.module("app.controllers", ["ngCookies"]);
var myIntervalFunction = function() {
    cancelRefresh = $timeout(function myFunction() {
        // do something
        cancelRefresh = $timeout(myIntervalFunction, 3000);
    },3000);
};
appCtrl.controller("NavContainerCtrl", ["ApiURL","$location","$cookies","$cookieStore","$http","$rootScope","$scope","logger","$routeParams","$window", function(api,$location,$cookies,$cookieStore,$http,$rootScope,$scope,logger,$routeParams,$window){	
}]);

appCtrl.factory('GI',function($cookieStore){
	var self;
	return self = {
		logo: $cookieStore.get('images'),
		setData: function(a){
			this.logo = a;
			$cookieStore.put('images',a);
		},
		getData: function(){
			return this.logo
		}
	}
});

appCtrl.factory('GetName',function(ApiURL,$http,$cookieStore){
	
	var self;
	return self = {
		name:$cookieStore.get('uname'),
		setname: function(a){
			$cookieStore.put('uname',a)
			this.name = a;
		},
		getname: function(){
			return this.name
		}
	}
	
});

appCtrl.factory('notifFactory',function(ApiURL,$http,$cookieStore){
	
	var self;
	return self = {
		getNotif:function(){
			$http({
				method : 'GET',
				url : ApiURL.url +'/api/attendance/schedule/count_notif?key=' + $cookieStore.get('key_api'),
				ignoreLoadingBar : true
			}).success(function(data){
				self.form = data
			}).error(function(data){
				//logger.logError("Error count request");
			});


		},dataNotif:function(data){
			return data;
		},
		// get_chat: function(){
		// 	$http({
		// 		method : 'GET',
		// 		url : ApiURL.url +'/api/attendance/schedule/gotcha?key=' + $cookieStore.get('key_api'),
		// 		ignoreLoadingBar : true
		// 	}).success(function(data){
		// 		self.test =  data
		// 	}).error(function(data){
		// 		//logger.logError("Error get chat");
		// 	});
		// },
		sort_approval  : function(data,cb){
			var arr   =  [];
			var i  = 0;
			for(var prop  in data){
			   if(data[prop].approval  != null &&  data[prop].approver != null){
	  		 	delete  data[prop];
			   }else{
			   	arr[i] =  data[prop]
				i++;
			   }
			}
			
			var count  =  arr.length;
			return cb(arr,count);		
		},
		find_approval  :  function(data,b){
		    console.log(1,data)
		    var arr1   =  [];
		    var i  = b;
		    //console.log(i)
			for (var prop  in data){
			   if(data[prop].approval  == null &&  data[prop].approver == null){
				console.log(1,'find_approval')
	  		 	delete  data[prop];
			   }else{
				//console.log(0)
			   	arr1[i] =  data[prop]
				i++;
			   }
			}
		    return arr1;
		    //console.log(arr) 

		},
		read_notif: function(){
			$http({
				method : 'GET',
				url : ApiURL.url +'/api/attendance/schedule/read_notif?key=' + $cookieStore.get('key_api'),
				ignoreLoadingBar : true
			}).success(function(data){
				//var result  =  data
				//console.log(data)
				//var data1 =  self.sort_approval(data,function(a,b){
				//	return a;
				//});

				//var count =  self.sort_approval(data,function(a,b){
				//	return b;
				//});
				
				//console.log('before',result,data1,count);
				//var data2  =  self.find_approval(result,count);
					
				self.readX = data

				if(self.readX == []){
					self.uknown =   true
				}
				//console.log('chitchat',self.readX);
			}).error(function(data){
				//logger.logError("Error get notification request")
			});


		},clickRequest :  function(id){

			if(self.readX[id] != undefined){
				var id  = self.readX[id].id;	
				self.getZ =  id;
				$cookieStore.put('clickable',self.getZ)
			}
			// else{
			// 	var id = id
			// 	self.getZ =  id;
			// 	$cookieStore.put('clickable',self.getZ)
			// }
			 
			$http({
				method : 'GET',
				url : ApiURL.url +'/api/attendance/schedule/gotcha?key=' + $cookieStore.get('key_api') + "&id="+ id,
				ignoreLoadingBar : true
			}).success(function(data){
				self.test =  data
				self.indexShow  =  id
			}).error(function(data){
				console.log('test send_stat',self.readX);
				//logger.logError("Error get chat");
			});

			$http({
				method : 'GET',
				url : ApiURL.url +'/api/attendance/schedule/send_stat?key=' + $cookieStore.get('key_api')+ "&id="+ id,
				ignoreLoadingBar : true
			}).success(function(data){
				console.log('test send_stat',self.readX);
				// console.log('test send_stat',data.data);
				// if(data.data.length == 0){
				// 	//console.log('i will made it');
					
				// 	$cookieStore.remove('clickable');
				// }
			}).error(function(data){
				//logger.logError("Error get notification request")
			});
		}, onMessage : function(comment){
			var id = self.getZ;
			var  exists   =    $cookieStore.get('clickable');

			/**
			 * disable  text in message if input status  id  2 or  3
			 */




			if(exists  != null  || exists != undefined){
				$http({
					method : 'POST',
					url : ApiURL.url +'/api/attendance/schedule/onMessage?key=' + $cookieStore.get('key_api') + "&id="+ $cookieStore.get('clickable'),
					data : {'comment' :   comment, 'id' : id},
					ignoreLoadingBar : true
				}).success(function(data){
					//if(self.readX.[self.indexShow].id )
					self.clickRequest(id)

					self.test =  data
					console.log(self.test)
				}).error(function(data){
					//logger.logError("Error get chat");
				});	
			}else{
				log.Error('Error  post message');
			}			
		},refreshMessage  : function(){
			//console.log($cookieStore.get('clickable'));
			var exists   =  $cookieStore.get('clickable');
			//console.log(exists)
			if(exists != null | exists != undefined){
				var id = $cookieStore.get('clickable')
				$http({
					method : 'GET',
					url : ApiURL.url +'/api/attendance/schedule/gotcha?key=' + $cookieStore.get('key_api') + "&id="+ id,
					ignoreLoadingBar : true
				}).success(function(data){
					self.test =  data
					//console.log(self.test)
				}).error(function(data){
					//logger.logError("Error get chat");
				});
			}
		},deleteCookie : function(){
			if(self.readX  ==  null){
				$cookieStore.remove('clickable');
			}
		}		
	}
	
});


appCtrl.controller("HeaderCtrl",function(notifFactory,GI,GetName,$timeout,ApiURL,$location,$cookies,$cookieStore,$http,$rootScope,$scope,logger,$interval,$routeParams,$rootScope,$window){
	
		var self = this;
		    //  $timeout.cancel(notifFactory.getNotif());
			
			self.handler = notifFactory;
			self.handling  =   GetName;
		   	

		   	self.handler.read_request  = self.readX

    		// $scope$interval(function(){
    		// 	notifFactory.getNotif();
    		
    		// 	notifFactory.read_notif();
    			
    		// },3000);
    		

			//console.log(notifFactory)
			

		// if($cookieStore.get('key_api')){
		// 	notifFactory.getNotif()
		// }
			

		/** get user name login **/
		$scope.name = GetName;
		//$scope.getPing();
		/*$http({
			method : 'GET',
			url : ApiURL.url +'/api/attendance/schedule/count_notif?key=' + $cookieStore.get('key_api')
		}).success(function(data){
			$scope.pinger =  data.ping;
			$scope.request = data.request;
		}).error(function(data) {
			console.log(data)
			logger.logError(data.header.message);
		});*/

		/** get logo **/
		$scope.logo = GI;
		
		/** function logout **/	
		$scope.checkDisabled  =  function(){
			
			if($cookieStore.get('clickable') != undefined){
				var id =  $cookieStore.get('clickable');
				var res = self.handler.readX;

				for(var prop  in  res){
					if(res[prop].id ==  id ){
						if(res[prop].status_id != 1){
							//console.log('checkdisabled' , false);
							return  true;
						}else{
							//console.log('chakcdisabled', true);
							return  false;
						}
					}
				}	
			}

			//console.log('checkDisabled');

			
		};
		$scope.approve = function(data,t){
			console.log("data,t", data,t[0]);
			var res  = []
			var res = t;
		
			$http({
			method : 'POST',
			url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
			data : { 0 : t}
			}).success(function(data){
				//logger.logError('failed to, please try again, or refresh the browser');
				 return data ;
				 logger.logSuccess(data);
				// $scope.pinger =  data.ping;
				// $scope.request = data.request;
				// notifFactory.getNotif()
			}).error(function(data) {
				logger.logError('failed to, please try again, or refresh the browser');
			});
		},
		$scope.reject = function(data){
			console.log(self.handler.readX[data])
			$http({
			method : 'POST',
			url : ApiURL.url +'/api/attendance/schedule/reject_notif?key=' + $cookieStore.get('key_api'),
			data : self.handler.readX[data]
			}).success(function(data){
				//console.log(data,'asdasd')
				// notifFactory.getNotif();
				// $scope.pinger =  data.ping;
				// $scope.request = data.request;
				 
				 logger.logSuccess(data);
			}).error(function(data) {
				console.log(data)
				//logger.logError(data.header.message);
			});
		};
		
		$scope.cancle = function(data){
			console.log(self.readX)
			$http({
			method : 'POST',
			url : ApiURL.url +'/api/attendance/schedule/cancle_notif?key=' + $cookieStore.get('key_api'),
			data : self.handler.readX[data]
			}).success(function(data){
				// notifFactory.getNotif();
				// $scope.pinger =  data.ping;
				// $scope.request = data.request;
				slogger.logSuccess(data);
			}).error(function(data) {
				console.log(data)
				//logger.logError(data.header.message);
			});
		};


		$scope.logOut = function(){
			$http.get(ApiURL.url + '/api/login/del-auth?key=' + $cookieStore.get('key_api')).success(function(data){	
				$cookieStore.remove('key_api');
				$cookieStore.remove('uname');
				$cookieStore.remove('iduserlogin');
				$cookieStore.remove('images');
				$cookieStore.remove('UserAccess');
				$location.path('pages/signin');
				logger.logSuccess('Logout Successfully');
			}).error(function(data) {
				$cookieStore.remove('key_api');
				$location.path('views/pages/404.html');	
				logger.logError(data.header.message);
			});	
		};

		$scope.onClick =  function(a){
			
		}

		$scope.unread =  function(data,b){
			if(data == 'read'){
				var data_exploit = {data : self.handler.form.request[b], whoiam : $cookieStore.get('NotUser') }
				console.log(data_exploit);
				$http({
					method : 'POST',
					url : ApiURL.url +'/api/attendance/schedule/read_notif?key=' + $cookieStore.get('key_api'),
					data : data_exploit
				}).success(function(data){
					console.log(data)
					//notifFactory.getNotif()
					// notifFactory.getNotif();
					// $scope.pinger =  data.ping;
					// $scope.request = data.request;
				}).error(function(data) {
					console.log(data)
					//logger.logError(data.header.message);
				});
			
			}
		}

		$scope.toggled = function(open) {
    if (open) {
        console.log('is open');
    } else console.log('close');
}

		// $scope.test  =  function(data){
		// 	console.log(test);
		// }

		// $scope.approve =  function(result){
		// 	console.log(result)
		// 	$http({
		// 	method : 'POST',
		// 	url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
		// 	data : result
		// 	}).success(function(data){
		// 		//logger.logError('failed to, please try again, or refresh the browser');
		// 		 return data ;
		// 		 logger.logSuccess(data);
		// 		// $scope.pinger =  data.ping;
		// 		// $scope.request = data.request;
		// 		// notifFactory.getNotif()
		// 	}).error(function(data) {
		// 		logger.logError('failed to, please try again, or refresh the browser');
		// 	});
		// }

		// $scope.reject =  function(result){
		// 	$http({
		// 	method : 'POST',
		// 	url : ApiURL.url +'/api/attendance/schedule/reject_notif?key=' + $cookieStore.get('key_api'),
		// 	data : result
		// 	}).success(function(data){
		// 		//console.log(data,'asdasd')
		// 		// notifFactory.getNotif();
		// 		// $scope.pinger =  data.ping;
		// 		// $scope.request = data.request;
		// 		 return data ;
		// 		 logger.logSuccess(data);
		// 	}).error(function(data) {
		// 		console.log(data)
		// 		//logger.logError(data.header.message);
		// 	});
		// }
		// $scope.cancle =  function(result){
		// 	$http({
		// 	method : 'POST',
		// 	url : ApiURL.url +'/api/attendance/schedule/cancle_notif?key=' + $cookieStore.get('key_api'),
		// 	data : result
		// 	}).success(function(data){
		// 		//console.log(data,'asdasd')
		// 		// notifFactory.getNotif();
		// 		// $scope.pinger =  data.ping;
		// 		// $scope.request = data.request;
		// 		 return data ;
		// 		 logger.logSuccess(data);
		// 	}).error(function(data) {
		// 		console.log(data)
		// 		//logger.logError(data.header.message);
		// 	});
		// }

		
		
});




appCtrl.controller("AppCtrl", ["$scope","$rootScope","$routeParams","ApiURL","$cookieStore","$http","logger","$location", function($scope,$rootScope,$routeParams,api,$cookieStore,$http,logger,$location) {

	
				
				$scope.store = {
					first_name : String($routeParams.id).split(';')[0],
				};
				
				if(String($routeParams.id).split(';')[0]){
					$scope.id =$routeParams.id;
				}else{
					$scope.id = String($routeParams.id).split(';')[0];
				}
				
				
				var $window;
				$window = $(window);
				$scope.main = {
						brand: "HRMS",
						//logo:"images/leekie_logo.jpg",
						user:"Sign in"
						//name:$cookieStore.get('uname')
				};
				
		
	

				$scope.color = {
					primary: "#248AAF",
					success: "#3CBC8D",
					info: "#29B7D3",
					infoAlt: "#666699",
					warning: "#FAC552",
					danger: "#E9422E"
				};
				
	//console.log($cookieStore.get('uname'));
	
	/*$scope.$watch('$routeParams.id', function() {
		console.log($routeParams.id);
	});*/
	//var n = String($routeParams.id).split(';');
	//console.log(n);
	
	// split 0 for get id
	 //console.log(String($routeParams.id).split(';')[0]);
				
	//$scope.id = String($routeParams.id).split(';')[0] ? $routeParams.id : String($routeParams.id).split(';')[0];
	//console.log(n);
					
				
}]);


appCtrl.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


appCtrl.service('fileUpload', ['$http','logger', function ($http,logger) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data,status,headers,config){
			logger.logSuccess(data.header.message);
        })
        .error(function(data,status,headers,config){
			logger.logError(data.header.message);
        });
    }
}]);

appCtrl.controller("NavCtrl", ["$scope", "taskStorage", "filterFilter","$cookieStore", function($scope, taskStorage, filterFilter,$cookieStore) {
		
		var access = $cookieStore.get('access');
		
		$scope.$watch('access', function() {

			if(access === 'user'){
				$scope.access = false;
			}else{
				$scope.access = true;
			}
			
		});
	
	var tasks;
    return tasks = $scope.tasks = taskStorage.get(), 
		$scope.taskRemainingCount = filterFilter(tasks, { completed: !1 }).length, 
		$scope.$on("taskRemaining:changed", function(event, count) { return $scope.taskRemainingCount = count })
		
				
		
		



}]);

appCtrl.controller("CalendarCtrl",function($scope,$compile,uiCalendarConfig) {
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();
			
			$scope.changeTo = 'Hungarian';
			/* event source that pulls from google.com */
			$scope.eventSource = {
					url: "",
					className: 'gcal-event',           // an option!
					currentTimezone: 'America/Chicago' // an option!
			};
			/* event source that contains custom events on the scope */
			$scope.events = [
			  {title: 'Event 1',start: new Date(y, m, 1)},
			  {title: 'Event 2',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
			  {id: 999,title: 'Event 3',start: new Date(y, m, d - 3, 16, 0),allDay: false},
			  {id: 999,title: 'Event 4',start: new Date(y, m, d + 4, 16, 0),allDay: false},
			  {title: 'Event 5',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
			  {title: 'Event 6',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
			];
			/* event source that calls a function on every view switch */
			$scope.eventsF = function (start, end, timezone, callback) {
			  var s = new Date(start).getTime() / 1000;
			  var e = new Date(end).getTime() / 1000;
			  var m = new Date(start).getMonth();
			  var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
			  callback(events);
			};

			$scope.calEventsExt = {
			   color: '#f00',
			   textColor: 'yellow',
			   events: [ 
				  {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
				  {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
				  {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
				]
			};
			/* alert on eventClick */
			$scope.alertOnEventClick = function( date, jsEvent, view){
				$scope.alertMessage = (date.title + ' was clicked ');
			};
			/* alert on Drop */
			 $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
			   $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
			};
			/* alert on Resize */
			$scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
			   $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
			};
			/* add and removes an event source of choice */
			$scope.addRemoveEventSource = function(sources,source) {
			  var canAdd = 0;
			  angular.forEach(sources,function(value, key){
				if(sources[key] === source){
				  sources.splice(key,1);
				  canAdd = 1;
				}
			  });
			  if(canAdd === 0){
				sources.push(source);
			  }
			};
			/* add custom event*/
			$scope.addEvent = function() {
			  $scope.events.push({
				title: 'Open Sesame',
				start: new Date(y, m, 28),
				end: new Date(y, m, 29),
				className: ['openSesame']
			  });
			};
			/* remove event */
			$scope.remove = function(index) {
			  $scope.events.splice(index,1);
			};
			/* Change View */
			$scope.changeView = function(view,calendar) {
			  uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
			};
			/* Change View */
			$scope.renderCalender = function(calendar) {
			  if(uiCalendarConfig.calendars[calendar]){
				uiCalendarConfig.calendars[calendar].fullCalendar('render');
			  }
			};
			 /* Render Tooltip */
			$scope.eventRender = function( event, element, view ) { 
				element.attr({'tooltip': event.title,
							 'tooltip-append-to-body': true});
				$compile(element)($scope);
			};
			/* config object */
			$scope.uiConfig = {
			  calendar:{
				height: 450,
				editable: true,
				header:{
				  left: 'title',
				  center: '',
				  right: 'today prev,next'
				},
				eventClick: $scope.alertOnEventClick,
				eventDrop: $scope.alertOnDrop,
				eventResize: $scope.alertOnResize,
				eventRender: $scope.eventRender
			  }
			};

			$scope.changeLang = function() {
			  if($scope.changeTo === 'Hungarian'){
				$scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
				$scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
				$scope.changeTo= 'English';
			  } else {
				$scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
				$scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
				$scope.changeTo = 'Hungarian';
			  }
			};
			/* event sources array*/
			$scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
			$scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];
		})
		
		
appCtrl.controller("DatepickerDemoCtrl", ["$scope","$filter", function($scope,$filter) {
    
	$scope.today = function() {
        $scope.dt = new Date
		var t = $filter('date')($scope.dt, 'yyyy/MM/dd')
		console.log(t);
		
    }
	
	$scope.today()
	$scope.showWeeks = !0
	$scope.toggleWeeks = function() {
        $scope.showWeeks = !$scope.showWeeks
    }
	
	$scope.clear = function() {
        $scope.dt = null
    }
	
	$scope.disabled = function(date, mode) {
        return "day" === mode && (0 === date.getDay() || 6 === date.getDay())
    }
	
	$scope.toggleMin = function() {
        var _ref;
        return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
            "null": new Date
        }
    }
	
	$scope.toggleMin()
	$scope.open = function($event) {
        $event.preventDefault()
		$event.stopPropagation()
		$scope.opened = !0
    }
	
	$scope.dateOptions = {
        "year-format": "'yy'",
        "starting-day": 1
    }
	
	$scope.formats = ["dd-MMMM-yyyy", "yyyy/MM/dd", "shortDate"]
	$scope.format = $scope.formats[1]
}]);		

//appCtrl.controller("DatepickerDemoCtrl", ["$scope","$filter", function($scope,$filter) {



appCtrl.run(function(notifFactory){
  		setInterval(function(){
    			notifFactory.getNotif();
    			notifFactory.deleteCookie();
    			notifFactory.read_notif();
    			notifFactory.refreshMessage();
    		},3000);
  });