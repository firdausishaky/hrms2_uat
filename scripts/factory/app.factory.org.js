var appFactoryOrg = angular.module("app.factory.org", []);

appFactoryOrg.factory('genORG',["$http","$cookieStore","ApiURL", function($http, $cookieStore,api){
	
	var KeyApi = $cookieStore.get('key_api');
    
	return {
	
        // get all data organization
        
		get : function(){
            return $http.get(api.url +'/api/organization/general-information?key=' +  KeyApi)
        },

		
        // save organization (pass in data)
        
		save : function(data){
            return $http({
				method : 'POST',
                url : api.url + '/api/organization/general-information?key=' + KeyApi, 
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
			return store;
        },

		
        // destroy  organization
        
		destroy : function(id){
            return $http.delete(api.url + '/api/organization/general-information/' + id + '?key=' +  KeyApi);
        },
		
		
		// update  organization

		update : function(id,data){
            return $http({
                method : 'POST',
                url : api.url + '/api//organization/general-information/' + id + '?key=' + KeyApi,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
			return store;
        }
    }

}]);


appFactoryOrg.factory('strucORG',["$http","$cookieStore","ApiURL", function($http, $cookieStore,api){
	
	var KeyApi = $cookieStore.get('key_api');
    
	return {
	
        // get all the employee
        
		get : function(){
            return $http.get(api.url +'/api/jobs/employment-stucture?key=' +  KeyApi)
        },

		
        // save employee (pass in data)
        
		save : function(data){
            var store =  $http({
				method : 'POST',
                url : api.url + '/api/jobs/employment-stucture?key=' + KeyApi, 
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
            return store;
        },

		
        // destroy  employee
        
		destroy : function(id){
            return $http.delete(api.url + '/api/jobs/employment-stucture/' + id + '?key=' +  KeyApi);
        },
		
		
		// update  employee

		update : function(id,data){
            var store =  $http({
                method : 'POST',
                url : api.url + '/api/jobs/employment-stucture/' + id + '?key=' + KeyApi,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
            return store;
        }
    }

}]);



