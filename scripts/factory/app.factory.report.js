var appFactoryReport = angular.module("app.factory.report", []);

appFactoryReport.factory("assSpv",["$http","$cookieStore","ApiURL","$routeParams","$route", function($http,$cookieStore,api,$routeParams,$route){
	
	$route.id = String($routeParams.id).split(';')[1];
	var KeyApi = $cookieStore.get('key_api');
    
	return {
	
        // get all data report
		
		get : function(){
            return $http.get(api.url + '/api/employee-details/assigned-supervisor/'  +  $route.id  + '?key=' +  KeyApi);
        },


		
        // save report (pass in data)
       
		save : function(data){
            return $http({
				method  : 'POST',
                url     : api.url + '/api/employee-details/qualification/work-exp/store/'  +  $route.id  + '?key=' +  KeyApi, 
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data    : $.param(data)
            });
			return store;
        },
	
	
	
	
        // destroy  report
        
		destroy : function(id){
            return $http.delete( api.url + '/api/employee-details/qualification/work-exp/'  +  id  + '?key=' +  KeyApi )
        },
		
		
		// update  report

		update : function(id,data){
            return $http({
                method : 'POST',
                url     : api.url + '/api/employee-details/qualification/work-exp/'   +  id  + '?key=' +  KeyApi, 
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
			return store;
        }
    }

}]);




appFactoryReport.factory("assSub",["$http","$cookieStore","ApiURL","$route","$routeParams", function($http, $cookieStore,api,$route,$routeParams){
	
	
	$route.id = String($routeParams.id).split(';')[1];
	var KeyApi = $cookieStore.get('key_api');
    
	return {
	
        // get all teh subordinates
        
		get : function(){
             return $http.get(api.url + '/api/employee-details/qualification/education/'  +  $route.id  + '?key=' +  KeyApi);
        },

		
        // save subordinates (pass in data)
        
		save : function(data){
            var store =  $http({
				method : 'POST',
                url :api.url + '/api/employee-details/qualification/education/store/'  +  $route.id  + '?key=' +  KeyApi,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
            return store;
        },

		
        // destroy  subordinates
        
		destroy : function(id){
            return $http.delete('/api/employee-details/qualification/education/'  +  id  + '?key=' +  KeyApi);
        },
		
		
		// update  subordinates

		update : function(id,data){
            var store =  $http({
                method : 'POST',
                url : '/api/employee-details/qualification/education/'  +  id  + '?key=' +  KeyApi,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
            return store;
        }
    }

}]);

