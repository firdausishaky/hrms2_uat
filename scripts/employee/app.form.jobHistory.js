(function(){
	
	angular.module('app.form.jobHistory',['checklist-model','ngFileUpload'])
	
	.factory('jobhistoryfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,$routeParams,$window,Upload,$location){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			getdatajob:function(){
				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/employee-details/job-history/emp/' +$routeParams.id + '?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.sub_department_selected = null;
					if(res.data.data.sub_department == []){
						self.subDataDepart = [];
						self.openDepartment = true;
						self.form.subDepartment = res.data.data.subDepartment[0].name;
					}else{
						if(res.data.data.sub_department.length > 0){
							self.sub_department_selected = res.data.data.sub_department[0].name;
						}
						self.subDataDepart =  res.data.data.sub_department;
							//console.log('test',self.subDataDepart);
							self.openDepartment =  false;
						}

						self.setlocal_it(res.data.data);
						self.hidelocal_it(res.data.data);
						self.setterminate(res.data.data);
						self.getpermission(res.data.header.access);
						self.setdataform(res.data.data);
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Job Employee Failed")
					});
			},getname:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/employee-details/personal/' +$routeParams.id+ '?key=' + $cookieStore.get('key_api'),
				}).then(function(res){	
					for(a in self.datajobs){
						if(self.datajobs[a].title==self.form.job){

							self.form.job=self.datajobs[a].id;
								//console.log(self.form.job)
							}
						}
						self.name = res.data.data.first_name +' ' + res.data.data.last_name;
						self.localIT =  res.data.data.local_it;
						if (res.data.pictures == null ){
							self.pic = "images/user2.jpg";
						}else{
							self.pic = ApiURL.url + "/images/" + $routeParams.id + '/' + res.data.pictures;
						}
					});
			},checkSubRoot : function(idx){
				var arr = [];
				if(typeof idx == "string"){
					for(var prop in self.datadepart){
						if(self.datadepart[prop].name == idx){
							idx = self.datadepart[prop].id;
						}
					}
				}
				for(var prop in self.datadepart){
					if(self.datadepart[prop].parent == idx){
						arr.push(self.datadepart[prop]);
					}
				}
				if(arr.length != 0){
					self.subDataDepart = arr
					self.openDepartment = false
				}else{
					self.subDataDepart = [];
					self.openDepartment = true;
				}
			},useracess:function(a){
				var access=$cookieStore.get('UserAccess');
				if(access){
					switch(a){
						case 'user':
						if(access.name == 'user'){return true}
							break;
					}
				}
			},onSelect : function ($item,$model,$label){
				$location.path('/employee/personalDetail/' + $item.employee_id)
			},getLocation : function(val){
				return $http.get(ApiURL.url + '/api/module/employee-info-search?search=' + self.employee + '&key=' + $cookieStore.get('key_api'),{
					params: {
						address : val,
					}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};
						a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},hiden:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
					}
				}
			},setdataform:function(data){
				if(data){
					
						//console.log(self.datadepart)
						// for(a in self.datadepart){
						// 	if(self.datadepart[a].name==self.form.sub_unit){
						// 		self.form.sub_unit=self.datadepart[a].id;
						// 	}
						// }
						/** set data form **/
						self.form=data.job_history.current_data;
						
						/** change name files **/
						if(self.form==null){
							self.form={};
						}else{
							self.form.files=[];
							var g = new File([""],self.form.file_jobhistory);
							self.form.files.push(g);
						}
						
						/** set data table history **/
						self.setdatatablehistory(data.job_history.log_data);
					}
				},hidelocal_it:function(data){
					//console.log(1,data);
					if(data.key == "su"){
						if(data.local_it =='both'){
							//console.log(data.local_it);
							self.radioLocalSU = true;
							self.showlocal= true;
							self.showExpat= true;
							self.local_it = data.local_it; 
						}else if(data.local_it == "yes"){
							//console.log(data.local_it);
							self.radioLocalSU = true;
							self.showlocal = true;
							self.showExpat = true;
							self.local_it = data.local_it
						}else{
							//console.log(data.local_it);
							self.radioLocalSU = true;
							self.local_it = data.local_it;
							self.showlocal= true;
							self.showExpat = true;
						}
					}else{
						if(data.local_it =='both'){
							self.radioLocalSU = false;
							self.showlocal= false;
							self.showExpat= true;
							self.expat = data.local_it
						}else if(data.local_it == "yes"){
							self.radioLocalSU = false;
							self.showlocal = true;
							self.showExpat = false;
							self.local_it = data.local_it
						}else{
							self.radioLocalSU = false;
							self.local_it = data.local_it;
							self.showlocal= true;
							self.showExpat = false;
						}
					}

				},setlocal_it:function(data){
					if(data.local_it){
						self.form={};
						self.form.local_it=data.local_it;
					}
				},setterminate:function(data){
					var a = data.action_terminate;
					var b = data.terminate;
					self.dataterminate = b
					if(a){
						self.terminateaction=true;
					}else{
						self.terminateaction=false;
					}
					if(b){
						self.terminateon=true;
						self.terminateaction=false;
						self.termination_date=b.termination_date;
					}else{
						self.terminateon=false;
					}
				},getdataselect:function(){
					$http({
						method : 'GET',
						url : ApiURL.url +  '/api/employee-details/job-history/emp?key=' + $cookieStore.get('key_api'),
					}).then(function(res){
						//console.log(res.data)
						self.setdataselect(res.data);
					},function(res){
						//res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Select Job History Failed")
					});
				},setdataselect:function(select){
					self.datadepart=select.department;
					self.datajobs=select.job;
					self.datastatus=select.status;
				},getidjob:function(){
					for(a in self.datajobs){
						if(self.datajobs[a].title==self.form.job){
							self.form.job=self.datajobs[a].id;
						}
					}
					return self.form.job
				},getidstatus:function(){
					for(a in self.datastatus){
						if(self.datastatus[a].title==self.form.status){
							self.form.status=self.datastatus[a].id;
						}
					}
					return self.form.status
				},getiddepart:function(){
					for(a in self.datadepart){
						if(self.datadepart[a].name==self.form.sub_unit){
							self.form.sub_unit=self.datadepart[a].id;
						}
					}
					return self.form.sub_unit
				},checkRoot : function(id){
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/employee-details/job-historyemp/subroot' + '?key=' + $cookieStore.get('key_api'), 
						data : id,
						headers: {
							"Content-Type": "application/json"
						}
					}).then(function(res){
						//console.log(res)							
						// self.calldatajob();
							// self.button=false;
							// logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Data Job History Failed")
						});
				},save:function(file){

					if(self.subDataDepart[0] == undefined){
						self.form.subDepartment = null;
					}
					/*else{
						self.form.subDepartment = self.subDataDepart[0].id;						
					}*/

					if(self.sub_department_selected){
						var idx = self.subDataDepart.findIndex((str)=>{
							return self.sub_department_selected == str.name;
						});

						if(idx!=-1){
							self.form.subDepartment = self.subDataDepart[idx].id;
						}
					}

					self.form.local_it=self.local_it;
					self.form.job=self.getidjob();
					self.form.status=self.getidstatus();
					self.form.sub_unit=self.getiddepart();
					
					//return console.log(2,self.form);
					if(file){
						file.upload = Upload.upload({
							method : 'POST',
							url : ApiURL.url + '/api/employee-details/job-history/emp/store1/' + $routeParams.id + '?key=' + $cookieStore.get('key_api'), 
							data :self.form,
							file: file,
							headers: {
								"Content-Type": "application/json"
							}
						}).then(function(res){
							self.calldatajob();
							self.button=false;
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Data Job History Failed")
						});
					}else{

						$http({
							method : 'POST',
							url : ApiURL.url + '/api/employee-details/job-history/emp/store1/' + $routeParams.id+ '?key=' + $cookieStore.get('key_api'), 
							data :self.form,
							headers: {
								"Content-Type": "application/json"
							}     
						}).then(function(res){
							self.calldatajob();
							self.button=false;
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Data Job History Failed")
						});
					}
				},calldatajob:function(){
					$http({
						method : 'GET',
						url : ApiURL.url +  '/api/employee-details/job-history/emp/' +$routeParams.id + '?key=' + $cookieStore.get('key_api'),
					}).then(function(res){
						
						self.setdataform(res.data.data);
						self.getdataselect()
					},function(res){
					});
				},getfilemasterjob:function(file){
					var e = ApiURL.url +  '/api/jobs/download/'  + file + '?key=' + $cookieStore.get('key_api');
					$window.open(e);
				},getfilejobhistory:function(file){
					var e = ApiURL.url +  '/api/employee-details/job-history/'  + $routeParams.id +  '/' + file + '?key=' + $cookieStore.get('key_api');
					$window.open(e);
				},
				/** table job history **/
				setheaders: function(){
					this.headers = new createheaders(
						['Change Date','title',null],
						['Training Date','title',null],
						['Probationary Date','title',null],
						['Contract Date','title',null],
						['Annual Contract date','title',null],
						['Job Title','title',null],
						['Employement Status','title',null],
						['Department','title',null],
						['Contract Start Date','title',null],
						['Contract End Date','title',null]
						);
					this.setactiveheader(this.headers[0],false)
				},adddata: function(a){
					this.maindata.push(a);
				},setdatatablehistory: function(a){
					this.maindata = a;
				},getdata: function(){
					self.filtered = $filter('filter')(this.maindata,{$:self.search});
					self.select(self.currentPage);
					return self.paginated;
				},setactiveheader:function(a,bool){
					this.activeheader = a;
					this.activeheader.order = bool;
				},
				numPerPageOpt	: pagination.option,
				numPerPage 		: pagination.option[0],
				currentPage		: 1,
				search			: '',
				select			: function(page){
					if(self.filtered){
						start = (page-1)*self.numPerPage;
						end = start + self.numPerPage;
						self.paginated = self.filtered.slice(start, end) || false;
						self.filteredlength = self.paginated.length || 0;

					}
				},
				modals: {
					add:{
						animation: true,
						templateUrl: 'modaladd',
						controller: 'modaladdtermination',
						controllerAs: 'modaladd',
						size:'',
						backdrop:'static',
						resolve:{
							data:function(){
								return self.datatoprocess;
							},title: function(){
								return "Add"
							}
						}
					},
					edit:{
						animation: true,
						templateUrl: 'modaladd',
						controller: 'modalshowtermination',
						controllerAs: 'modaladd',
						size:'',
						backdrop:'static',
						resolve:{
							data:function(){
								return self.datatoprocess;
							},
							title: function(){
								return "Show"
							}
						}
					}
				},
				openmodal: function(a,b){
					self.datatoprocess = b
					$modal.open(self.modals[a]).result.then(function(data){
						switch(a){
							case 'add':
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/employee-details/job-history/termination/confirm/' + $routeParams.id +  '?key=' + $cookieStore.get('key_api'), 
								data : data,
								headers: {
									"Content-Type": "application/json"
								}
							}).then(function(res){
								self.getname();
								self.calldatajob();
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Terminate Employment Failed")
							});
							break;
							case 'edit':
							
							break;
						}
					});
				},getselecttermination:function(){ 
					$http({
						method : 'GET',
						url : ApiURL.url + '/api/employee-details/job-history/termination/' + $routeParams.id +  '?key=' + $cookieStore.get('key_api')
					}).then(function(res){
						self.dataterminations = res.data;
						logger.logSuccess('Show Data Select Terminations');
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Data Termination Reasons Failed")
					});
				},deldatahistory: function(id){
					$http({
						url : ApiURL.url + '/api/employee-details/job-history/emp/' + id + '?key=' + $cookieStore.get('key_api'),
						method: 'DELETE'
					}).then(function(res){
						var i;
						for (i = 0; i < id.length; ++i) {
							self.maindata = filterFilter(self.maindata, function (store) {
								return store.id != id[i];
							})
						}
						self.check = [];
						logger.logSuccess('Deleting  Job History Success');
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Deleting  Job History Failed")
					})
				}
			}
		})

.controller('jobHistorycontroller',function(jobhistoryfactory,$timeout,$scope,$routeParams,$cookieStore){
	var self = this;
	self.handler = jobhistoryfactory;
	self.handler.getdatajob();
	
	self.handler.getname();
	$timeout(function() {
		self.handler.getdataselect();
	}, 100);
	$scope.id = $routeParams.id;
	x = $cookieStore.get('NotUser')
	if (x == "2014888") {
		$scope.hideButton = true
	} else{
		$scope.hideButton = false
	}

})

.controller('modaladdtermination',function(jobhistoryfactory,$modalInstance,title){
	var self = this;
	self.title = title
	self.handler = jobhistoryfactory;
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.cancel = function(){
		$modalInstance.dismiss('close');
	}
	self.handler.getselecttermination();
})

.controller('modalshowtermination',function(jobhistoryfactory,$modalInstance,title,data){
		//console.log(2,data);
		var self = this;
		self.title = title;
		self.form = data;
			//console.log(data)
			self.handler = jobhistoryfactory;
			self.cancel = function(){
				$modalInstance.dismiss('close');
			}
		})

.run(function(jobhistoryfactory){
	jobhistoryfactory.setheaders();
})

}())

