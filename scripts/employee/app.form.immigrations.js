var appFormImmig = angular.module("app.form.immigrations", ["checklist-model","ngFileUpload"]);



/** factory permission **/
appFormEmmer.factory('permissionimmig',function(){
	var self;
	return self = {
		data:function(a){
			self.access = a;
		}
	}
});

appFormImmig.controller("ImmigrationsCtrl",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,$window,permissionimmig) {
	
	/** employee id **/
	$scope.id = $routeParams.id;
	x = $cookieStore.get('NotUser')
	if (x == "2014888") {
		$scope.hideButton = true
	} else{
		$scope.hideButton = false
	}

	
	/** get all data **/
	$http({
		method : 'GET',
		url : ApiURL.url + '/api/employee-details/immigration/' + $scope.id + '?key=' + $cookieStore.get('key_api')
	}).then(function(res){
		console.log(res.data.header.access,'12')
		var temp=res.data.data;
		for(a in temp){
			if(temp[a].filename=='null'){
				temp[a].filename='Not Defined';
			}
		}
		$scope.dataimmigrations=temp;
		$scope.check = {dataimmigrations:[]};
		permissionimmig.data(res.data.header.access);
		logger.logSuccess(res.data.header.message)
	},function(res){
		res.data.header ? permissionimmig.data(res.data.header.access) : permissionimmig.data = [] ; 
		res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Immigrations failed")
	});
	
	
	/** function download file **/ 
	$scope.test = function(a){
		if(a.target.innerText == 'Not Defined'){
			logger.logError("File Not Found");
		}else{	
			var e = ApiURL.url +  '/api/employee-details/immigration/'  + $scope.id +  '/' + a.target.innerText + '?key=' + $cookieStore.get('key_api');
			$window.open(e);
		}
	};
	
	/** function hidden button **/
	$scope.hide = function(a){
		var data = permissionimmig.access;
		if(data){
			switch(a){
				case 'create':
				if(data.create == 1){return true}
					break;
				case 'delete':
				if(data.delete == 1){return true}
					break;
				case 'update':
				if(data.update == 1){return true}
					break;
			}
		}
	};
	
	/** modal handler **/
	$scope.open = function(size) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalImmigrations.html",
			controller: "immigrationCtrl",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return;
				}
			}
		});
		modal.result.then(function(newstudent){
					//console.log(newstudent)
					if($scope.dataimmigrations){
						$scope.dataimmigrations.push(newstudent);
					}else{
						$scope.dataimmigrations =[];
						$scope.dataimmigrations.push(newstudent);
					}
				});
	};
	$scope.edit = function(size,student){
		var modal;
		modal = $modal.open({
			templateUrl: "modalEditImmigration.html",
			controller: "EditImmigrations",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return student;
				}
			}
		});
		modal.result.then(function(newdata){
			for(a in $scope.dataimmigrations){	
				if($scope.dataimmigrations[a].id == newdata.id){
					$scope.dataimmigrations[a]=newdata;
				}
			};
		});	
	};
	
	
	/** function remove **/
	$scope.remove = function(id){
				//console.log(id)
				$http({
					method : 'DELETE',
					url : ApiURL.url + '/api/employee-details/immigration/' + id + '?key=' + $cookieStore.get('key_api')
				}).success(function(data){
					var i;
					for( i = 0; i < id.length; i++){
						$scope.dataimmigrations = filterFilter($scope.dataimmigrations,function(student){
							//console.log(student.id)
							return student.id != id[i];
						});
					};
					$scope.check.dataimmigrations = [];
					logger.logSuccess(data.header.message);
				}).error(function(data) {
					data.header ? logger.logError(data.header.message) : logger.logError("Delete Immigration Failed")
				});
			};
			
			
		});

appFormImmig.controller("immigrationCtrl", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams,Upload) {
	
	/** employee id **/
	$scope.id = $routeParams.id;
	
	/** copy data **/
	if(items){
		$scope.immig = items;
	}
	
	/** get immigration issuer **/
	$http.get(ApiURL.url+'/api/module/immigration-issuer?key=' + $cookieStore.get('key_api') ).success(function(res){
		$scope.jobs = res.data;
	});
	
	/** function save **/
	$scope.save = function(file){
		if(file){
			/** function upload **/
			file.upload = Upload.upload({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/immigration/store1/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
				data :$scope.immig,
				file: file,
				headers: {
					"Content-Type": "application/json"
				}
			}).then(function(res){
				$modalInstance.close(res.data.data[0]); 
				logger.logSuccess(res.data.header.message);
			},function(res){
				res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
			});
		}else{				
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/immigration/store1/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
				data :$scope.immig,
				headers: {
					"Content-Type": "application/json"
				}     
			}).then(function(res){
				var temp = res.data.data[0];
				temp.filename='Not Defined';
				$modalInstance.close(temp);
				logger.logSuccess(res.data.header.message);
			},function(res){
				res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
			});
		}
	};
	
	/** close modal **/
	$scope.cancel = function(){
		$modalInstance.dismiss("cancel")
	}
	
});

appFormImmig.controller("EditImmigrations", function(permissionimmig,$routeParams,$modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,Upload) {
	
	/** empoloyee id **/
	$scope.id = $routeParams.id;			
	
	/** copy data **/
	if(items){
		$scope.immig = items;
		$scope.immig=angular.copy(items);
	}
	
	/** function change filename **/
	$scope.immig.files=[];
	var g = new File([""], items.filename);
	$scope.immig.files.push(g);
	
	
	/** function get immigration issuer **/
	$http.get(ApiURL.url+'/api/module/immigration-issuer?key=' + $cookieStore.get('key_api') ).success(function(res){
		$scope.checklist = res.data;
	});
	
	/** function save **/
	$scope.save = function(file){
		for(a in $scope.checklist){
			if($scope.checklist[a].title == $scope.immig.issued_by){
				$scope.immig.issued_by = $scope.checklist[a].id;
			};
		};
		if(file){
			if(file[0].name=='Not Defined'){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/employee-details/immigration/update/'+ items.id +'/'+ $scope.id +'?key='+$cookieStore.get('key_api'),
					data :$scope.immig,
					headers: {
						"Content-Type": "application/json"
					}     
				}).then(function(res){
					var temp=res.data.data[0];
					temp.filename='Not Defined'
					$modalInstance.close(temp);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
				});
			}else{
				/** function upload **/
				file.upload = Upload.upload({
					method : 'POST',
					url : ApiURL.url + '/api/employee-details/immigration/update/'+ items.id +'/'+ $scope.id +'?key='+$cookieStore.get('key_api'),
					data :$scope.immig,
					file: file,
					headers: {
						"Content-Type": "application/json"
					}
				}).then(function(res){
					$modalInstance.close(res.data.data[0]); 
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
				});
			}
		}else{				
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/immigration/update/'+ items.id +'/'+ $scope.id +'?key='+$cookieStore.get('key_api'),
				data :$scope.immig ,
				headers: {
					"Content-Type": "application/json"
				}    
			}).then(function(res){
					//console.log(res.data.data)
					var temp=res.data.data[0];
					temp.filename='Not Defined'
						//console.log(temp)
						$modalInstance.close(temp);
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
					});
		}
	};
	
	
	/** function hide delete button upload **/
	$scope.$watch('items', function(){
		if(items.filename=='Not Defined'){
			$scope.delbutton=false;
		}else{
			$scope.delbutton=true;
		}
	});
	
	/** close modal **/
	$scope.cancel = function() {
		$modalInstance.dismiss("cancel")
	}
	
	/** function hide button **/
	$scope.hide = function(a){
		var access  = permissionimmig.access;				
		console.log(access)
		if(access){
			switch(a){
				case 'update':
				if(access.update == 1){
					return true
				}
				break;
			}
		}
	};
	
});

/** New Url From Leekie Before Update for edit  
url : api.url + '/api/employee-details/immigration/update/'+items.id+'/'+ $scope.id+ '?key=' + $cookieStore.get('key_api'),
**/

/**
var a = String(items.expiry_date);
var b = String(items.immigration_numb);
var c = String(items.filename);
var d = String(items.issued_by);
var e = String(items.expiry_date);
var f = String(items.type_doc);
var g = String(items.comment);
$scope.immig.expiry_date = a;
$scope.immig.immigration_numb = b;
$scope.immig.filename = c;
$scope.immig.issued_by = d;
$scope.immig.type_doc = f;
$scope.immig.expiry_date = e;
$scope.immig.comment = g;
**/

/**
old concept in upload file from modal add
$scope.save = function(id){
$scope.immig.id = $cookieStore.get('idImmig');
$scope.immig.filename = $cookieStore.get('nameImmig');	
$http({
method : 'POST',
url : ApiURL.url + '/api/employee-details/immigration/store2/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
data : $.param($scope.immig,$scope.immig.id,$scope.immig.filename)   
}).success(function(data) {
var temp = {};
temp = $scope.immig;
var a, b, c = temp.issued_by;
for(var i = 0; i < $scope.jobs.length; i++) {
a = $scope.jobs[i];
if(a.id == c) {
b = a.title;
};
};
temp.issued_by = b;
temp.id = data.data.id;
if(data.data.filename == null){
temp.filename = "Not Defined";
}else{
temp.filename = data.data.filename;
}
$modalInstance.close(temp);
$cookieStore.remove('idImmig');
$cookieStore.remove('nameImmig');							
logger.logSuccess(data.header.message);
}).error(function(data) {
data.header ? logger.logError(data.header.message) : logger.logError("Adding Immigration Failed")
});
};
$scope.$watch('files', function(){				
$scope.upload = function (files) {
if (files && files.length) {
for (var i = 0; i < files.length; i++) {
var file = files[i];
Upload.upload({
url : ApiURL.url + '/api/employee-details/immigration/store1/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
file: file
}).progress(function (evt){
var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
$scope.log = 'progress: ' + progressPercentage + '% ' +
evt.config.file.name + '\n' + $scope.log;
}).success(function (data) {
$cookieStore.put('idImmig',data.data.id);
$cookieStore.put('nameImmig',data.data.filename);
logger.logSuccess(data.header.message);
});
}
}
};
});
**/

/**
old concept in upload file from modal edit
$scope.save = function(id){
var a = [];
for(a in $scope.checklist){
if($scope.checklist[a].title == $scope.immig.issued_by) {
$scope.immig.issued_by = $scope.checklist[a].id;
};
};		
$http({
method : 'POST',
url : ApiURL.url + '/api/employee-details/immigration/update/'+items.id+'/'+ $scope.id+ '?key=' + $cookieStore.get('key_api'),
headers :{'Content-Type' : 'application/x-www-form-urlencoded',},
data : $.param($scope.immig,$scope.immig.issued_by)   
}).success(function(data){
var temp = {};
temp.filename = $scope.immig.filename = $scope.files[0].name ;
temp = $scope.immig;
var a, b, c = temp.issued_by;
for(var i = 0; i < $scope.checklist.length; i++) {
a = $scope.checklist[i];
if(a.id == c) {
b = a.title;
};
};
temp.filename = data.data.filename;
temp.issued_by = b;
$modalInstance.close(temp);
logger.logSuccess(data.header.message);
}).error(function(data) {
data.header ? logger.logError(data.header.message) : logger.logError("Update Immigration Failed")
});
};
$scope.$watch('files', function(){			
$scope.upload = function (files) {
if (files && files.length) {
for (var i = 0; i < files.length; i++) {
var file = files[i];
Upload.upload({
url : ApiURL.url + '/api/employee-details/immigration/update/' + items.id + '?key=' + $cookieStore.get('key_api'),
file: file
}).progress(function(evt){
var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
$scope.log = 'progress: ' + progressPercentage + '% ' +
evt.config.file.name + '\n' + $scope.log;
}).success(function (data, status, headers, message) {
$scope.immig.radio = 1;
$scope.bfile = true;
logger.logSuccess(data.header.message);
});
}
}
};
});

**/

