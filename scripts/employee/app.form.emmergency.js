var appFormEmmer = angular.module("app.form.emmergency", ["checklist-model"]);


/** factory permission **/
appFormEmmer.factory('permissionemmer',function(){
	var self;
	return self = {
		data:function(a){
			self.access = a;
		}
	}
});

appFormEmmer.controller("emmergencyCtrl", function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissionemmer) {
	
	/** employee id **/		
	$scope.id = $routeParams.id;
	x = $cookieStore.get('NotUser')
	if (x == "2014888") {
		$scope.hideButton = true
	} else{
		$scope.hideButton = false
	}

	
	/** get data emmergency**/		
	$http({
		method : 'GET',
		url : ApiURL.url +  '/api/employee-details/emergency/' + $scope.id + '?key=' + $cookieStore.get('key_api')
	}).then(function(res){
		permissionemmer.data(res.data.header.access);
		$scope.storage = res.data.data;
		$scope.check = {storage:[]};
		logger.logSuccess(res.data.header.message)
	},function(res){
		res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Emergency Failed")
	});
	
	/** function hidden button **/
	$scope.hide = function(a){
		var data = permissionemmer.access;
		if(data){
			switch(a){
				case 'create':
				if(data.create == 1){return true}
					break;
				case 'delete':
				if(data.delete == 1){return true}
					break;
				case 'update':
				if(data.update == 1){return true}
					break;
			}
		}
	};
	
	/** modal handler **/
	$scope.open = function(size){
		var modal;
		modal = $modal.open({
			templateUrl: "modalEmmer.html",
			controller: "modalEmmerCtrl",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return;
				}
			}
		});
		modal.result.then(function (newemmer) {
			if($scope.storage){
				$scope.storage.push(newemmer);
			}else{
				$scope.storage = [];
				$scope.storage.push(newemmer);
			}
		});
		
	};
	$scope.edit = function(size,store) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalEditEmmer.html",
			controller: "EmmerEditCtrl",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return store;
				}
			}
		});
		modal.result.then(function(newstore){
			for(a in $scope.storage){
				if($scope.storage[a].id==newstore.id){
					$scope.storage[a]=newstore;
				}
			}
		});
	};
	
	/** delete function**/
	$scope.remove = function(id){
		$http({
			method : 'DELETE',
			url : ApiURL.url + '/api/employee-details/emergency/' + id + '?key=' + $cookieStore.get('key_api')
		}).success(function(data){
			var i;
			for( i = 0; i < id.length; i++) {
				$scope.storage = filterFilter($scope.storage, function (student) {
					return student.id != id[i];
				});
			};
			$scope.check.storage = [];
			logger.logSuccess(data.header.message);
		}).error(function(data) {
			data.header ? logger.logError(data.header.message) : logger.logError("Deleting Emergency contact Failed")
		});
	};
});


appFormEmmer.controller("modalEmmerCtrl", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams) {
	
	/** employee id **/		
	$scope.id = $routeParams.id;
	
	/** copy data **/
	if(items){
		$scope.coEn = items;
	}
	
	/** function save **/			
	$scope.save = function(){
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/emergency/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
			data :$scope.coEn,
			headers: {
				"Content-Type": "application/json"
			}   
		}).success(function(data){
			var temp = {};
			temp = $scope.coEn;
			temp.id = data.data.id;
			$modalInstance.close(temp);
			logger.logSuccess(data.header.message);
		}).error(function(data){
			data.header ? logger.logError(data.header.message) : logger.logError("Adding Emergency contact Failed")
		});
	};
	
	/** close modal **/
	$scope.cancel = function() {
		$modalInstance.dismiss("cancel")
	}	
});

appFormEmmer.controller("EmmerEditCtrl",function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams,permissionemmer) {
	
	
	/** copy data **/		
	if(items){
		$scope.coEn = items;
		$scope.coEn = angular.copy(items);
	}	
	
	/** function save **/
	$scope.save = function(id){
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/emergency/' + items.id + '?key=' + $cookieStore.get('key_api'),
			data : $scope.coEn,
			headers: {
				"Content-Type": "application/json"
			}   
		}).success(function(data){
			$modalInstance.close(data.data);
			logger.logSuccess(data.header.message);
		}).error(function(data){
			data.header ? logger.logError(data.header.message) : logger.logError("Update Emergency contact Failed")
		});
	};
	
	/** close modal **/			
	$scope.cancel = function(){				
		$modalInstance.dismiss("cancel")
	}
	
	/** function hide button **/
	$scope.hide = function(a){
		var access  = permissionemmer.access;				
		if(access){
			switch(a){
				case 'update':
				if(access.update == 1){
					return true
				}
				break;
			}
		}
	};
	
});
/*
var a = String(items.name);
var b = String(items.address);
var c = String(items.relationship);
var d = String(items.home_telephone);
var e = String(items.mobile_telephone);
var f = String(items.work_telephone);
var g = String(items.country);				
var h = String(items.zip);				
var i = String(items.state);
var j = String(items.city);				
$scope.coEn.name = a;
$scope.coEn.address = b;
$scope.coEn.relationship = c;
$scope.coEn.home_telephone = d;
$scope.coEn.mobile_telephone = e;
$scope.coEn.work_telephone = f;	
$scope.coEn.country = g;
$scope.coEn.zip = h;
$scope.coEn.state = i;
$scope.coEn.city = j;
*/		