var appFormContact = angular.module("app.form.contact", []);

/** factory permission **/
appFormContact.factory('permissioncontact', function() {
	var self;
	return self = {
		data:function(a){
			self.access = a;
		}
	}
});

appFormContact.controller("contactPerCtrl",function($scope,$http,ApiURL,logger,$cookieStore,$routeParams,permissioncontact) {
	
	/** employee id **/
	$scope.id = $routeParams.id;
	
	/** get data contact **/
	$http({
		method : 'GET',
		url : ApiURL.url + '/api/employee-details/contact/'  + $scope.id + '?key=' + $cookieStore.get('key_api')
	}).then(function(res){
		if(res.data.header.message == "Unauthorized"){
			permissioncontact.data(res.data.header.access);
			logger.logError("Access Unauthorized");
		}else{
			$scope.perInfo = res.data;
			$scope.name = $scope.perInfo.first_name +" "+ $scope.perInfo.last_name;
			permissioncontact.data(res.data.header.access);
			$scope.Contact = res.data.data[0];
			logger.logSuccess(res.data.header.message)
		}
	},function(res){
		res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Contact Failed")
	});
	
	/** function hidden button **/
	$scope.hide = function(a){
		var data = permissioncontact.access;
		if(data){
			switch(a){
				case 'create':
				if(data.create == 1){return true}
					break;
				case 'read':
				if(data.read == 1){return true}
					break;
			}
		}
	};
	
	/** function save **/
	$scope.save = function(){
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/contactv2/' + $scope.id + '?key=' + $cookieStore.get('key_api'), 
			data :$scope.Contact,
			headers: {
				"Content-Type": "application/json"
			} 
		}).success(function(data){
			var temp = {};
			temp = $scope.Contact;
			logger.logSuccess(data.header.message);
			$scope.button=false;
		}).error(function(data){
			$scope.button=false;
			data.header ? logger.logError(data.header.message) : logger.logError("Saving Contact Details Failed");
		});
	};

	x = $cookieStore.get('NotUser')
	if (x == "2014888") {
		$scope.hideButton = true
	} else{
		$scope.hideButton = false
	}

	
});

