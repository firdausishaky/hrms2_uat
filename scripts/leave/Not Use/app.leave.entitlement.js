var appLeaveTables = angular.module("app.leave.entitlement",[]);


	// ##################################################  CONTROLLER LEAVE ENTITLEMENTS #######################################################################
	
appLeaveTables.controller("leaveEntitCtrl", ["$scope","$filter","$http","$modal","$log","$cookieStore","ApiURL","$routeParams","filterFilter","logger", function($scope,$filter,$http,$modal,$log,$cookieStore,api,$routeParams,filterFilter,logger) {		
		
		
		/*
			$http.get(api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api') ).success(function(res){
				
				// FUNCTION GT ID FOR DELETE
				$scope.check = {
					currentPageStores: []
				}
				
				var end, start;
				start =0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
				
				
			}).success(function(data, status, header, config) {
					$scope.role = true;
					logger.logSuccess(data.header.message);
			}).error(function(data, status, headers, config) {
					$scope.role = false;
					logger.logError(data.header.message);
			});
			
		*/	

		
			$http.get("json/json/entitlement.json").success(function(res){
				
				// FUNCTION GT ID FOR DELETE
				$scope.check = {
					currentPageStores: []
				}
				
				var end, start;
				start =0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
				
				
			});
			
		
			// FUNCTION TABLE VACATION 
			
			var init;
            $scope.numPerPageOpt = [3, 5, 10, 20]; 
            $scope.numPerPage = $scope.numPerPageOpt[2]; 
            $scope.currentPage = 1; 
            $scope.currentPageStores = []; 
            $scope.searchKeywords = ""; 
            $scope.filteredStores = []; 
            $scope.row = "" ;
            $scope.stores =[];
            $scope.select = function(page) {
                 var end, start;
                 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
            };
            $scope.onFilterChange = function() {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
            };
            $scope.onNumPerPageChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.onOrderChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.search = function() {
                return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
            };
            $scope.order = function(rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
            };
            init = function() {
                return $scope.search(), $scope.select($scope.currentPage)
            };
			
			
			
			//################ FUNCTION MODAL ADJUSTMENT ################################################################
			
			$scope.open = function(size) {
					var modal;
					modal = $modal.open({
						templateUrl: "modalAdjustment.html",
						controller: "adjustModalCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return;
							}
						}
					});
					modal.result.then(function (newstudent) {
						if($scope.currentPageStores){
							$scope.currentPageStores.push(newstudent);
						}else{
							$scope.currentPageStores = [];
							$scope.currentPageStores.push(newstudent);
						}
					});
			};
			
			
			
				
}]);

appLeaveTables.controller("adjustModalCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
		
				if(items){
					$scope.formData = items;
				}
								
						
				$scope.save = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;				
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.cancel = function() {
					$modalInstance.dismiss("cancel")
				}
				
				
}]);

	
appLeaveTables.controller("vacEditCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL","Upload", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api,Upload) {
				
			
				
				var a = String(items.device_name);
				var b = String(items.device_number);
				var c = String(items.device_location);
				if(items){
					$scope.formData = items;
				}
			
				
				$scope.save = function(){
					$http({
						method : 'PUT',
						url : api.url + '/api/user-management/system-user/' + items.id + '?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
							var temp = {};
							temp = $scope.formData;		
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				
				$scope.cancel = function() {
				
					$scope.formData.device_name = a;
					$scope.formData.device_number = b;
					$scope.formData.device_location = c;
					$modalInstance.dismiss("cancel")
	
				};
				
				
}]);


