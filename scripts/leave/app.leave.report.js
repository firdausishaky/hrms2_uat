(function(){
	angular.module('app.leave.report',[])
	
	.factory('leavereportfactory',function(pagination,$filter,$modal,$http,logger,$cookieStore,ApiURL,reportleavetypefactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {
			setheaders: function(a){
				if(a == 1){
					this.headers = new createheaders(
						['Leave Type','leave_type',15],
						['Date Taken','DateTaken',15],
						['Taken (days) ','taken_day',15],
						['Entitlement (days)','EntitlementDay',15],
						['Remaining (days)','RemainingDay',15],
						['Approver','approver',15]
						);
				}else{
					this.headers = new createheaders(
						['Employee','Employee',15],
						['Date Taken','DateTaken',15],
						['Taken (days) ','taken_day',15],
						['Entitlement (days)','EntitlementDay',15],
						['Remaining (days)','RemainingDay',15],
						['Approver','approver',15]
						);
				}
				this.setactiveheader(this.headers[0],false)
			},setgenerate:function(a){
				self.setheaders(a)
				self.setdata([])
			},setyear:function(a){
				this.datayear = a;
			},getyear:function(){
				return this.datayear;
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
				console.log(a,"xxxxxxxxxxxxxxxxxx")
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},	
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[3],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},getnextprev:function(a){
				switch(a){
					case 'view':
					self.form.name = self.label;
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/leave/searchReport?key=' + $cookieStore.get('key_api'),
						data :self.form,
						headers: {
							"Content-Type": "application/json"
						}
					}).then(function(res){
						self.setdata(res.data.data.value);
						self.setyear(res.data.data.date);
						self.dataexport(res.data.data.value);
						self.form.name = self.model;
						logger.logSuccess(res.data.header.message);
					},function(res){
						self.form.name = self.model;
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave Report Failed")
					});
					break;
					case 'prev':
					self.form.name = self.label;
					var a = Number(self.datayear);
							//console.log(a)
							self.form.date = a - 1;
							//console.log(self.form.date);
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/leave/searchReport?key=' + $cookieStore.get('key_api'),
								data :self.form,
								headers: {
									"Content-Type": "application/json"
								}
							}).then(function(res){
								if(res.data.data.value==undefined){
									self.setdata([]);
									self.setyear(res.data.data.date);
								}else{
									self.setdata(res.data.data.value);
									self.setyear(res.data.data.date);
									self.dataexport(res.data.data.value);
								}
								self.form.name = self.model;
								logger.logSuccess(res.data.header.message);
							},function(res){
								self.form.name = self.model;
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave Report Failed")
							});
							break;
							case 'next':
							self.form.name = self.label;
							var a = Number(self.datayear);
							//console.log(a)
							self.form.date = a + 1;
							//console.log(self.form.date);
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/leave/searchReport?key=' + $cookieStore.get('key_api'),
								data :self.form,
								headers: {
									"Content-Type": "application/json"
								}
							}).then(function(res){
								//console.log(res.data.data.value==undefined)
								if(res.data.data.value==undefined){
									self.setdata([]);
									self.setyear(res.data.data.date);
								}else{
									self.setdata(res.data.data.value);
									self.setyear(res.data.data.date);
									self.dataexport(res.data.data.value);
								}
								self.form.name = self.model;
								logger.logSuccess(res.data.header.message);
							},function(res){
								self.form.name = self.model;
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave Report Failed")
							});
							break;
						}
					},dataexport: function(a){
						this.dataex = a;
					},exportTo : function(data){
						if(!data){void 0; logger.logError('Please Searching Data Before Export') }
						for(a in self.maindata){
							delete self.maindata[a].id;
							delete self.maindata[a].type_id;
							delete self.maindata[a].status_id;
							alasql('SELECT * INTO XLSX("Leave Report.xlsx",{headers:true}) FROM ?',[self.maindata]);
							break;
						}
					},getselect : function(URL){
						$http.get(URL).success(function(res){
							if(res.header.message == 'success'){
								self.leaves = res.data.leave;
								self.depart = res.data.department;
								self.jobs = res.data.job;
								self.form = {};
								self.form.for = 2;
								self.mainaccess = res.header.access;
								logger.logSuccess('Access Granted');
							}else{
								self.mainaccess = res.header.access;
								logger.logError('Access Unauthorized');
							}
						});	
					},button:function(a){
						if(self.mainaccess){
							switch(a) {
								case 'search':
								if(self.mainaccess.read == 1){return true}break;
							}
						}
					},onSelect:function($item,$model,$label){self.model = $item.name;self.label = $item.employee_id;
					},getLocation:function(val){
						return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name +'?&key=' + $cookieStore.get('key_api'),{
							params:{address:val,}
						}).then(function(response){
							if(response.data.data === null){
								logger.logError(response.data.header.message);
								var a = {};a.result = [];
								return a.result.map(function(item){
									return item;
								});  
							}else if(response.data != null){
								var a = {};
								a.result = response.data;
								return a.result.map(function(item){
									return item;
								});
							}  
						});
					}
				}
			})
.controller('leaveReportCtlr',function(leavereportfactory,$http,$cookieStore,ApiURL){	
	var self = this;
	self.handler = leavereportfactory;
	self.handler.getselect(ApiURL.url + '/api/leave/view?key=' + $cookieStore.get('key_api'));
})
.run(function(leavereportfactory){
		//leavereportfactory.setheaders();
	})	


/** factory table leave report by leave type **/
.factory('reportleavetypefactory',function(pagination,$filter,$modal,$http,logger){
	function createheaders(){
		var temp = [];
		for(var i=0;i<arguments.length;i++){
			var x = {};
			x.id = i;
			x.name =arguments[i][0];
			x.sort =arguments[i][1];
			x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
			x.order =false;
			temp.push(x);
		}return temp;
	}

	var self;
	return self = {
		setheaders: function(){
			this.headers = new createheaders(
				['Employee','leave',15],
				['Date Taken','days',15],
				['Taken (days) ','days_rem',15],
				['Entitlement (days)','availed',15],
				['Remaining (days)','last_date',15],
				['Approver','number',15]
				);
			this.setactiveheader(this.headers[0],false)
		},setyear:function(a){
			this.datayear = a;
		},getyear:function(){
			return this.datayear;
		},dataexport: function(a){
			this.dataex = a;
		},exportTo:function(){
			alasql('SELECT * INTO XLSX("Leave Report.xlsx",{headers:true}) FROM ?',[this.dataex]);
		},prevnext:function(a){
			console.log(a)

				/* switch(a){
					case 'prev':
						self.form.name = self.label;
						var a = Number(self.datayear);
						self.form.date = a - 1;
						console.log(self.form.date);
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/searchReport?key=' + $cookieStore.get('key_api'),
							data :self.form
						}).then(function(res){
							self.setdata(res.data.data.value);
							self.setyear(res.data.data.date);
							self.dataexport(res.data.data.value);
							self.form.name = self.model;
							logger.logSuccess(res.data.header.message);
						},function(res){
							self.form.name = self.model;
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave Report Failed")
						});
					break;
					case 'next':
						self.form.name = self.label;
						var a = Number(self.datayear);
						self.form.date = a + 1;
						console.log(self.form.date);
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/searchReport?key=' + $cookieStore.get('key_api'),
							data :self.form
						}).then(function(res){
							self.setdata(res.data.data.value);
							self.setyear(res.data.data.date);
							self.dataexport(res.data.data.value);
							self.form.name = self.model;
							logger.logSuccess(res.data.header.message);
						},function(res){
							self.form.name = self.model;
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave Report Failed")
						});
					break;
				} */
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[3],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			}
		}
	}).controller('reportLeaveCtrl',function(reportleavetypefactory,$http){
		var self = this;
		self.handler = reportleavetypefactory;
		self.handler.setdata([])
	})
	
	

	
	
	.run(function(reportleavetypefactory){
		reportleavetypefactory.setheaders();
	})

}())

