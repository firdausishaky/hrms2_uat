(function(){
	
	/** MODULE LEAVE TABLES **/
	angular.module('app.leave.tables',['checklist-model'])
	
	.factory('permissionfactory',function(){
		var self;
			return self = {
				setpermission:function(data){
					self.access = data;
					//console.log(self.access)
				}	
			}
	})
	
	/** FACTORY SEARCH **/
	.factory('leavetablefactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,$filter,tablevacationfactory,tablesickfactory,tablebereavementfactory,tableenhancefactory,permissionfactory){
		var self;

		return self = {
			vacLev: '',
			search: function(){
				//console.log(self.form)
				if(self.form.for == '1'){ $cookieStore.put('vacLev','expat')}
				if(self.form.for == '2'){ $cookieStore.put('vacLev','local')}
				if(self.form.for == '3'){ $cookieStore.put('vacLev','both') }
				//console.log(self.vacLev)
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					data : self.form 
				}).then(function(res){
					if(self.form.leave_type == 'Vacation Leave'){
						tablevacationfactory.setdata(res.data.data);
						tablevacationfactory.setacvtivebutton('active');
					}else if(self.form.leave_type == 'Sick Leave'){
						tablesickfactory.setdata(res.data.data);
						tablesickfactory.setacvtivebutton('active');
					}else if(self.form.leave_type == 'Bereavement Leave'){
						tablebereavementfactory.setdata(res.data.data);
						tablebereavementfactory.setacvtivebutton('active');
					}else if(self.form.leave_type == 'Enhance Vacation Leave'){
						tableenhancefactory.setdata(res.data.data);
						tableenhancefactory.setacvtivebutton('active');
					}
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Record Data Failed")
				});
			},setDefaultLeaveType: function(URL){
				$http.get(URL).success(function(res){
					if(res.header.message == 'success'){
						self.checking = res.data;
						self.mainaccess = res.header.access;
						permissionfactory.setpermission(res.header.access);
						self.radio = res.status;
						////console.log(self.radio)
						logger.logSuccess('Access Granted');
					}else{
						self.mainaccess = res.header.access;
						logger.logError('Access Unauthorized');
					}
				});	
			},button:function(a){
				if(self.mainaccess){
					switch(a){
						case 'read':
							if(self.mainaccess.read == 1){return true}
						break;
					}
				}
			},role:function(a){
				if(self.radio){
					switch(a){
						case 'local':
							if(self.radio=='local'){return false}
						break;
						case 'expat':
							if(self.radio=='expat'){return false}
						break;
						//case 'hr':
							
						//break;
					}

				}
			},rolex:function(a){
				if(self.radio){
					switch(a){
						case 'local':
							if(self.radio=='local'){return true}
						break;
						case 'expat':
							if(self.radio=='expat'){return false}
						break;
						//case 'hr':
							
						//break;
					}		
				}
			},sethideradio:function(form){
				if(form=='Enhance Vacation Leave'){
					//console.log('to here')
					self.hideradio= true
				}else{
					self.hideradio= true
				}			     	
//hideradio
			},
		}
	})
	
	/** SEARCH LEAVE TABLES CONTROLLER **/
	.controller('leaveTableController',function(leavetablefactory,$http,ApiURL,$cookieStore){
		var self = this;
			self.handler = leavetablefactory;
			self.handler.setDefaultLeaveType(ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'));
	})
	/** FACTORY VACATION TABLE **/
	.factory('tablevacationfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Leave Rules','leave_rules',25],
					['Years In Service','years',25],
					['Entitled Days Leave','entitled',25],
					['Day Off / On Call','day',25]
				);
				this.setactiveheader(this.headers[0],false)
			},setacvtivebutton:function(active){
				if(active){
					this.mainaccess = permissionfactory.access;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update : function(newdata){
				for(a in this.maindata){
					if(this.maindata[a].id == newdata.id){
						this.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalVacation',
					controller: 'modaladdVacation',
					controllerAs: 'modalVacation',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalVacation',
					controller: 'modalEditVacation',
					controllerAs: 'modalVacation',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch(a){
						case 'add':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/addVacation?key=' + $cookieStore.get('key_api'),
									data : data,
								}).then(function(res){
									self.adddata(res.data.data[0]);
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Vacation Leave Failed")
								});
							break;
						case 'edit':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/updateVacation/' + b.id + '?key=' + $cookieStore.get('key_api'),
									data : data.b
								}).then(function(res){
									//self.form.vacLev = $cookieStore.get('vacLev');
									self.update(res.data.data[0])
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Vacation Leave Failed")
								});			
							break;
					}
					
				});
			},
			/**	FUNCTION DELETE**/
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/leave/deleteVacation/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Vacation Leave Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Vacation Leave Failed")
				})
			},
			
			/** ADD NEW SUB VACATION **/
			addsubvacation: function(form){
				var a = {};
				a.year ='';
				a.days ='';
				a.offday_oncall ='';
				form.subvacation.push(a);
			},
			
			/** REMOVE SUB VACATION **/
			removesubvacation: function(form,a){
				form.subvacation.splice(a,1);
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
							if(self.radio=='local'){return true}
						break;
						case 'expat':
							if(self.radio=='expat'){return true}
						break;
					}
				}
			},getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.jobs = res.data;
					////console.log(self.jobs)
					//a.form = self.cleanData(data);
					//a.form.department = self.jobs[0];
				});	
			}
			/*,
			cleanData: function(data){
				data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
				data.Department = data.Department.replace("Department : ","");
				data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
				data.StartFrom = data.StartFrom.replace("StartFrom : ","");
				return data
			}*/
		}
	})
	/** VACATION TABLES CONTROLLER **/
	.controller('vacationTableCtrl',function(tablevacationfactory,$http,ApiURL,$cookieStore){
		var self = this;
			self.handler = tablevacationfactory;
			//self.handler.setDefaultLeaveType(ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'));
	})
	/** CONTROLLER ADD MODAL VACATION **/
	.controller('modaladdVacation',function(tablevacationfactory,$modalInstance,$timeout,data,time,title,icon,ApiURL,$cookieStore){
		var self = this;
			self.handler = tablevacationfactory;
			////console.log($cookieStore.get('vacLev'));
			self.title = title;
			self.icon = icon;
			self.form = {};
			self.form.subvacation = [];
			self.form.vacLev = $cookieStore.get('vacLev');
			self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
			self.handler.getlocalexpat()
	})
	/** CONTROLLER EDIT MODAL VACATION **/
	.controller('modalEditVacation',function(tablevacationfactory,$cookieStore,ApiURL,$modalInstance,$timeout,data,time,title,icon){
		var self = this;
			self.handler = tablevacationfactory;
			self.title = title;
			self.icon = icon;
			//console.log($cookieStore.get('vacLev'));
			data = angular.copy(data);
			//console.log(data)
			data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
			data.Department = data.Department.replace("Department : ","");
			//console.log('test',data.Entitlementdays.replace("Entitlementdays : ",""),'test');			
			data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : " ,""); 
			data.StartFrom = data.StartFrom.replace("StartFrom : ","");
			
			self.form = data;
			self.form.vacLev = $cookieStore.get('vacLev');
			//console.log('sub',angular.copy(data));
			self.form = angular.copy(data);
			//console.log(self.form)
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
			
	})
	
	
	
	
	/** FACTORY SICK LEAVE TABLE **/
	.factory('tablesickfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Leave Rules','leave_rules',40],
					['Years In Service','years',30],
					['Entitled Days Leave','entitled_days',30]
				);
				this.setactiveheader(this.headers[0],false)
			},setacvtivebutton:function(active){
				if(active){
					this.mainaccess = permissionfactory.access;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				//console.log('test',newdata)
				//self.form.vacLev = $cookieStore.get('vacLev');
				for(a in this.maindata){
					if(this.maindata[a].id == newdata.id){
						this.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalSick',
					controller: 'modalSick',
					controllerAs: 'modalSick',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalSick',
					controller: 'modalEditSick',
					controllerAs: 'modalSick',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/sickLeave?key=' + $cookieStore.get('key_api'),
									data : data,
								}).then(function(res){
									self.adddata(res.data.data[0]);
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Sick Leave Failed")
								});
							break;
						case 'edit':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/sickUpdate/' + b.id + '?key=' + $cookieStore.get('key_api'),
									data : data.b
								}).then(function(res){
									self.update(res.data.data[0])
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Sick Leave Failed")
								});			
							break;
					}
					
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/leave/sickDelete/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					//logger.logSuccess(res.data.header.message);
					logger.logSuccess('Deleting Sick Leave Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Vacation Leave Failed")
				})
			},
			/** add new sub sick **/
			addsubsick: function(form){
				var a = {};
				a.year ='';
				a.days ='';
				form.subsick.push(a);
			},removesubsick: function(form,a){
				form.subsick.splice(a,1);
			},getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.jobs = res.data;
				});	
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
							if(self.radio=='local'){return true}
						break;
						case 'expat':
							if(self.radio=='expat'){return true}
						break;
					}
				}
			}
			
		}
	})
	/** SICK TABLES CONTROLLER **/
	.controller('sickTableCtrl',function(tablesickfactory,ApiURL,$cookieStore){
		var self = this;
			self.handler = tablesickfactory;	
	})
	/** CONTROLLER MODAL SICK **/
	.controller('modalSick',function(tablesickfactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,time,title,icon){
		var self = this;
			self.handler = tablesickfactory;
			self.title = title;
			self.icon = icon;
			self.form = {};
			self.form.subsick = [];
			self.form.vacLev = $cookieStore.get('vacLev');
			self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
			self.handler.getlocalexpat()
	})
	/** CONTROLLER MODAL EDIT SICK **/
	.controller('modalEditSick',function(tablesickfactory,$cookieStore,ApiURL,$modalInstance,$timeout,data,time,title,icon){
		var self = this;
			self.handler = tablesickfactory;
			self.title = title;
			self.icon = icon;
			
			data = angular.copy(data);
			data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
			data.Department = data.Department.replace("Department : ","");
			data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
			data.StartFrom = data.StartFrom.replace("StartFrom : ","");
			
			self.form = data;
			self.form = angular.copy(data);
			self.form.vacLev = $cookieStore.get('vacLev');
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close');
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
	})
	
	
	
	
	/** FACTORY BEREAVEMENT TABLE **/
	.factory('tablebereavementfactory',function(filterFilter ,pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,permissionfactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {		
			setheaders: function(){
				this.headers = new createheaders(
					['Leave Rules','leave_rules',25],
					['Relation to Deceased','relation',25],
					['Entitled Days Leave','entitled',25],
					['Days With Pay','day',25]
				);
				this.setactiveheader(this.headers[0],false)
			},setacvtivebutton:function(active){
				if(active){
					this.mainaccess = permissionfactory.access;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				console.log('main_data',newdata)
				for(a in this.maindata){
					if(this.maindata[a].id == newdata[0].id){
						this.maindata[a] = newdata[0];
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalBereavement',
					controller: 'modalBereavement',
					controllerAs: 'modalBereavement',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalBereavement',
					controller: 'modalEditBereavement',
					controllerAs: 'modalBereavement',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/bereavementLeave?key=' + $cookieStore.get('key_api'),
									data : data,
								}).then(function(res){
									self.adddata(res.data.data[0]);
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Bereavement Failed")
								});
							break;
						case 'edit':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/bereavementUpdate/' + b.id + '?key=' + $cookieStore.get('key_api'),
									data : data.b
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Bereavement Failed")
								});			
							break;
					}
					
				});
			},
		
			/**	FUNCTION DELETE**/
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/leave/bereavementDelete/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					//logger.logSuccess(res.data.header.message);
					logger.logSuccess('Deleting Bereavement Leave Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Vacation Leave Failed")
				})
			},
			
			addsubbereavement: function(form){
				var a = {};
				a.relation_to_deceased ='';
				a.days ='';
				a.days_with_pay='';
				form.subbereavement.push(a);
			},removesubbereavement: function(form,a){
				form.subbereavement.splice(a,1);
			},
			getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.data = res.data;
				});	
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
							if(self.radio=='local'){return true}
						break;
						case 'expat':
							if(self.radio=='expat'){return true}
						break;
					}
				}
			}
			
		}
	})
	/** BEREAVEMENT TABLES CONTROLLER **/
	.controller('bereavementTableCtrl',function(tablebereavementfactory,$http){
		var self = this;
		self.handler = tablebereavementfactory;
	})
	/** CONTROLLER MODAL BEREAVEMENT **/
	.controller('modalBereavement',function(tablebereavementfactory,$modalInstance,$timeout,data,time,title,icon,ApiURL,$cookieStore){
		var self = this;
			self.handler = tablebereavementfactory;
			self.title = title;
			self.icon = icon;
			self.form = {};
			self.form.subbereavement = [];
			self.form.vacLev = $cookieStore.get('vacLev');
			self.save = function(){
				$modalInstance.close(self.form);
				//console.log(self.form)
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
			self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			self.handler.getlocalexpat()
			
	})
	/** CONTROLLER MODAL EDIT BEREAVEMENT **/
	.controller('modalEditBereavement',function(tablebereavementfactory,ApiURL,$cookieStore ,$modalInstance,$timeout,data,time,title,icon){
		var self = this;
			self.handler = tablebereavementfactory;
			self.title = title;
			self.icon = icon;
			
			data = angular.copy(data);
			data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
			data.Department = data.Department.replace("Department : ","");
			data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
			data.StartFrom = data.StartFrom.replace("StartFrom : ","");
			
			self.form = data;
			self.form = angular.copy(data);
			self.form.vacLev = $cookieStore.get('vacLev');
			//console.log(self.form)
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.cancel = function(){
				$modalInstance.dismiss('close');
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
	})
	
	
	
	
	
	/** FACTORY EHANCED TABLE **/
	.factory('tableenhancefactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Job Title','title',50],
					['Entitled Enhance Days','relation',50]
				);
				this.setactiveheader(this.headers[0],false)
			},setacvtivebutton:function(active){
				if(active){
					this.mainaccess = permissionfactory.access;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				//console.log(newdata)
				for(a in self.maindata){
					if(self.maindata[a].id == newdata[0].id){
						self.maindata[a] = newdata[0];
					}

				}
			},
			setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalenhanced',
					controller: 'AddmodalEnchanced',
					controllerAs: 'modalenhanced',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalenhanced',
					controller: 'modalEditEnchanced',
					controllerAs: 'modalenhanced',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/enchancedLeave?key=' + $cookieStore.get('key_api'),
									data : data,
								}).then(function(res){
									//self.adddata(res.data.data);
									self.setdata(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Enhanced Failed")
								});
							break;
						case 'edit':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/updateEnchanced/' + b.id + '?key=' + $cookieStore.get('key_api'),
									data : data.b
								}).then(function(res){
									self.setdata(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Enhanced Failed")
								});			
							break;
					}
					
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/leave/deleteEnchanced/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Enhanced Leave Failed")
				})
			},
			addsubenhanced: function(form){
				//console.log(form)
				var a = {};
				a.title ='';
				a.entitled_enhanced_days_leave='';
				form.subenhanced.push(a);
			},
			removesubenhanced: function(form,a){
				form.subenhanced.splice(a,1);
			},
			getjobtitle : function(URL){
				$http.get(URL).success(function(res){
					self.data = res.data;
				});	
			},getedit : function(URL,data,form){
				$http.get(URL).success(function(res){
					self.data = res.data;
					data = angular.copy(data)
					form.subenhanced[0] = data;
				});	
			},
			
		}
	})
	/** ENCHANCED TABLES CONTROLLER **/
	.controller('enchanceTableCtrl',function(tableenhancefactory,$http){
		var self = this;

		self.handler = tableenhancefactory;
	})
	/** CONTROLLER ENCHANCED LEAVE **/
	.controller('AddmodalEnchanced',function(tableenhancefactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,time,title,icon){
		var self = this;
			self.handler = tableenhancefactory;
			self.title = title;
			self.icon = icon;
			self.form = {};
			self.form.subenhanced = [];
			self.handler.getjobtitle(ApiURL.url + '/api/leave/viewJobTitle?key=' + $cookieStore.get('key_api'));
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
	})
	/** CONTROLLER MODAL EDIT ENCHANCED **/
	.controller('modalEditEnchanced',function(tableenhancefactory,$modalInstance,$timeout,data,time,title,icon,ApiURL,$cookieStore ){
		var self = this;
			self.handler = tableenhancefactory;
			self.title = title;
			self.icon = icon;
			self.form = {};
			self.form.subenhanced = [];
			self.handler.getedit(ApiURL.url + '/api/leave/viewJobTitle?key=' + $cookieStore.get('key_api'),data,self.form);
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.cancel = function(){
				$modalInstance.dismiss('close');
			}
	})
	
	
	/** FUNCTION SET HEADER TABLES VACATION **/
	.run(function(tablevacationfactory){
		tablevacationfactory.setheaders();
	})
	
	/** FUNCTION SET HEADER TABLES SICK **/
	.run(function(tablesickfactory){
		tablesickfactory.setheaders();
	})
	
	/** FUNCTION SET HEADER TABLES SICK **/
	.run(function(tablebereavementfactory){
		tablebereavementfactory.setheaders();
	})
	
	/** FUNCTION SET HEADER TABLES ENCHANCED **/
	.run(function(tableenhancefactory){
		tableenhancefactory.setheaders();
	})

}())

