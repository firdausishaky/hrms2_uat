(function(){

	angular.module('app.leave.entitlement',['rzModule'])

	.factory('entitlementsFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Leave Type','LeaveType',null],
					['Days Entitled','DaysEntitled',null],
					['Days Remaining ','DaysRemaining',null],
					['Availed Days','AvailedDays',null],
					['Last date of availment','lastDate',null],
					['Number of Availments','NumberOfAvailment',null],
					['Valid Until','ValidUntil',null]
				);
				this.setactiveheader(this.headers[0],false)
			},find:function(){
				self.form.employee = self.label;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/leave/searchEmp?key=' + $cookieStore.get('key_api'),
					data : self.form
				}).then(function(res){
					self.setdata(res.data.data)
					self.form.employee = self.model;
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.employee = self.model;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave Entitlement Failed")
				});	
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				edit:{
					animation: true,
					templateUrl: 'modalAdjust',
					controller: 'editAdjust',
					controllerAs: 'modalAdjust',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}	
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch (a) {
						case 'edit':
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/attendance/schedule/updateFix/' + b.id + '?key=' + $cookieStore.get('key_api'),
								data : data.b
							}).then(function(res){
								self.update(res.data.data[0])
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Leave Failed")
							});	
						break;
					}
				});
			},
			onSelect:function($item,$model,$label){self.model = $item.name;self.label = $item.employee_id;},
			getLocation:function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee +'?&key=' + $cookieStore.get('key_api'),{
				  params: {
					address : val,
				  }
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						  return a.result.map(function(item){
							return item;
						  });
					}  
				});
			},getselectleave:function(URL){
				$http.get(URL).success(function(res){
					if(res.header.message == 'success'){
						self.selectleave = res.data;
						self.mainaccess = res.header.access;
						logger.logSuccess('Access Granted');
					}else{
						self.mainaccess = res.header.access;
						logger.logError('Access Unauthorized');
					}
				});	
			},button:function(a){
				if(self.mainaccess){
					switch(a) {
						case 'search':
							if(self.mainaccess.read == 1){return true}break;
						case 'adjust':
							if(self.mainaccess.read == 1){return true}break;
					}
				}
			},getadjust:function(form,data){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/leave/adjust?key=' + $cookieStore.get('key_api'),
					data : data
				}).then(function(res){
					self.setadjust(form,res.data.data)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Showing Data Adjusment Failed")
				});	
			},setadjust:function(dataA,dataB){
				dataA.vacLeave = dataB.vacLeave;
				dataA.AvailedVacation = dataB.AvailedVacation;
				dataA.offDay = dataB.offDay;
				dataA.total = dataB.total;
				dataA.AvailedOffDay = dataB.AvailedOffDay;
				dataA.Max = dataB.Max;
				dataA.Min = dataB.Min;
			}
		}
	})
	.controller('leaveEntitCtrl',function(entitlementsFactory,ApiURL,$cookieStore){
		var self = this;
			self.handler = entitlementsFactory;
			self.handler.getselectleave(ApiURL.url + '/api/leave/Emp?key=' + $cookieStore.get('key_api'));
	})
	.controller('editAdjust',function(entitlementsFactory,$modalInstance,$timeout,data,$scope){		
		var self = this;
			self.handler = entitlementsFactory;
			self.form = {}
			self.handler.getadjust(self.form,data)
			self.confirm = function(){
				$modalInstance.dismiss(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss("Close")
			}
		self.priceSlider =66;
	})
	.run(function(entitlementsFactory){
		entitlementsFactory.setheaders();
	})
	

}())
