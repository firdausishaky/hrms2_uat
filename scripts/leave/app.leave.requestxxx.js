(function(){
	angular.module('app.leave.request',['ngFileUpload','rzModule'])
	.factory('leaverequestfactory',function(pagination,$filter,$modal,$http,logger,$cookieStore,$rootScope,$timeout,ApiURL,Upload,$route){
		var self;
			//self.from_check_button =  true;
			return self = {
				getselectleavetype : function(URL){
					
					self.input_number   =  true;
					self.statHide =  false;
					$http.get(URL).success(function(res){
						console.log(res)
						console.log(res.data.status.employee_name)
						if(res.header.message == 'success'){	
							self.label_x = res.data.status.employee_id
							self.label_x_name  = res.data.status.employee_name
							self.leaves = res.data.data;
							self.setacces(res.data.status)
							self.mainaccess = res.header.access;
							logger.logSuccess('Access Granted');
							self.suspen = true;
						}else{
							self.mainaccess = res.header.access;
							logger.logError('Access Unauthorized');
						}
					});	
				},gettype:function(){
					if(self.label == undefined){
						//console.log(self.label_x)	
						//self.form.name = self.label_x;
						def = true
						self.getNameEmployee =  self.label_x;
					}else{
						//self.form.name = self.label;
						def = true
						self.getNameEmployee = self.label
					}
					//console.log(self.form.name);
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/leave/RequestBrea?key=' + $cookieStore.get('key_api'),
						data : {LeaveType  : self.form.LeaveType, employee_id : self.form.LeaveType, name :  self.getNameEmployee  }
					}).then(function(res){
						self.bereaves = res.data.data	
						console.log(res.data.data)
						$cookieStore.put('bereavement',res.data.data);
						logger.logSuccess('Success load LEAVE BEREAVEMENT');
					},function(res){
						self.breaHide = true;
						logger.logError('BEREAVEMENT LEAVE is not yet entitled') 
					})
				},getToday : function(){
					//2200-01-01
					var date =  new Date();
					var year =   date.getFullYear().toString();
					var month  =  date.getMonth().toString();
					var day    = date.getDate().toString();

					// day +1 from current year one days
					day   = parseInt(day) + 1;
					day  =  day.toString();

					month =   parseInt(month) + 1;
					month  =  month.toString();
					if(month < 10){
						month =  '0'+month
					}

					if(day <  10){
						day =  '0'+day
					}


					var fullDay =   year+'-'+month+'-'+day;
					self.getStartDay =   fullDay;

					console.log(self.getStartDay)
					
					
				},addDays : function(theDate, days) {
					var test =  new Date(theDate)
					days  -= 1;

					var a =  new Date(test.getTime() + days*24*60*60*1000);
					console.log(a)
					var year  = parseInt(a.getMonth())+1;
							
					var day   = a.getFullYear();  
					var month = a.getDate();
					var st_R  =  day+'-'+year+'-'+month
					console.log(st_R)
					return st_R;
				},getLastYear  : function(){
					var date =  new Date();
					var year =   date.getFullYear().toString();
					var month  =  date.getMonth().toString();
					var day    = date.getDay().toString();

					var fullYear  =   year+'-12-31';
					self.getLastDay  =  fullYear;
				},getEnabledTo  : function(id){

					if(id == undefined){		
						self.actived  =  true;
					}else{
						if(id != undefined){
							self.actived =  false;
							//console.log(self.form.From);
							self.injectTo  =  self.form.From;

							var  get_data_form  =  self.form.remaining_day;
				 			var  get_date_Froms =  self.form.From;
				 			console.log(get_date_Froms)
				 			var new_time = self.addDays(self.form.From,get_data_form);
				 			console.log(new_time)
							self.getLastDay  =  new_time ;
						    
						}
					}
				},getDisabledActive :  function(a){
					if(self.form  == undefined ){
						return true
					//for from
					}else{
						if(self.form.Name != undefined  && a == 'Name'){
							return true; 
						}else if(self.form.LeaveType == undefined  && a == 'From'){
							return true 
						//for leave type 
						}else if(self.form.Name  == undefined &&  a  ==  'leavetype'){
							return  true
						}else{
							return false;
						}
					}
				},getsuspension:function(){
					self.form.name = self.label;
					////console.log(self.form.name)
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/leave/RequestSus?key=' + $cookieStore.get('key_api'),
						data :self.form
					}).then(function(res){

						self.susPen = res.data.data	
					},function(res){
						res.data.header ? logger.logError('Not list suspension') : logger.logError("Not set up bereavement")
					})
				},testSuspen:function(){
					////console.log(self.susPen)
					var result  = self.susPen
					for(var prop in result){
						if(result[prop].infraction_name == self.form.suspension){
							var days = result[prop].days
							self.form.remaining_day =  days
							self.sanction  = result[prop].sanction_date  
						}
					}
				
				},getdate_bereav:function(){
					self.form.name = self.label;
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/leave/RequestRemainBrea?key=' + $cookieStore.get('key_api'),
						data :self.form
					}).then(function(res){
						if(res.data.data.remaining_day == null){
							self.form.remaining_day = 'Not Defined';
							self.form.date={};
							self.form.date.last_date = res.data.data.date.last_date || '';
							self.form.date.max_date_req = res.data.data.date.max_date_req || '';
							self.form.date.for_request = res.data.data.date.for_request || '';
							logger.logSuccess('Can Not found Remaining Days');
						}else{
							self.form.remaining_day =res.data.data.remaining_day;
							self.form.date={};
							self.form.date.last_date = res.data.data.date.last_date || '';
							self.form.date.max_date_req = res.data.data.date.max_date_req || '';
							self.form.date.for_request = res.data.data.date.for_request || '';
							logger.logSuccess(res.data.header.message);
						}
						
					},function(res){
						
						res.data.header ? logger.logError('Not set up bereavement') : logger.logError("Not set up bereavement")
					})
				},setacces:function(data){
					self.inputhide = data
					if(data.status == 'user'){
						self.form = {}
						self.form.name = data.employee_name
						this.iduser = self.inputhide.employee_id;
						$cookieStore.put('user_emp',this.iduser)
					}else{
						void 0
					}
				},				
				onSelect:function($item,$model,$label){self.model = $item.name;self.label = $item.employee_id; $cookieStore.put('user_emp',$item.employee_id)},
				getLocation:function(val){
					return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name +'?&key=' + $cookieStore.get('key_api'),{
					  params:{address:val,}
					}).then(function(response){
						////console.log(response);
						self.form.employee_id =  response.data[0].employee_id;
						if(response.data.data === null){
							logger.logError(response.data.header.message); 
							self.form.employee_id =  response.data[0].employee_id
							var a = {};a.result = [];
							return a.result.map(function(item){
								return item;
							});  
						}else if(response.data != null){
							var a = {};
								a.result = response.data;
							  return a.result.map(function(item){
								return item;
							  });
						}  
					});
				},new_number : function(a){
					self.form.DayOffBetween   =   0
					self.input_number = false
					self.getLastDay =  self.form.From;
					self.injectTo =  self.form.From;

				}, getdayone: function(rd, from,dob){
					self.remaining_day  =  rd

					//create manipulation

					var dat = new Date(from);
					console.log(dat,dob)
  					dat.setDate(dat.getDate() + (dob-1) + parseInt(self.form.remaining_day));
  					console.log(dat.getDate())
  					console.log(dat)
  					var  i_year   =  dat.getFullYear();
  					var  i_month  =  dat.getMonth() + 1;
  					var  i_date   =  dat.getDate();

  					var  concat =  i_year+'-'+i_month+'-'+i_date ;
  					console.log(concat)
  					
					//console.log('test');
					// if(self.label == undefined){
					// 	self.get_employee_id   =  self.label_x;
					// }else{
					// 	self.get_employee_id   =  self.label;
					// }



					///var data  = { remaining_day : rd, from_date  :  from, name :  self.get_employee_id  }
					// if(rd  !=  'No Limit' &&  rd  != '-' ){
					// 	$http({
					// 		method : 'POST',
					// 		url : ApiURL.url + '/api/leave/get_date_range?key=' + $cookieStore.get('key_api'),
					// 		data :data
					// }).then(function(res){
					// 		self.date_from =   res.data.data.date 
					//		console.log(res.data.data.date)
							self.actived =  false;
							//console.log(self.form.From);
							self.injectTo  =  self.form.From;
							//self.getday(self);
							// var  get_data_form  =  self.form.remaining_day;
				 		// 	var  get_date_Froms =  self.form.From;
				 		// 	console.log(get_date_Froms)
				 		// 	var new_time = self.addDays(self.form.From,get_data_form);
				 		// 	console.log(new_time)
	
				 		// 	self.form.DOB = res.data.data.day_between

							self.getLastDay  =  concat ;
							//console.log(res)
							//self.form.DOB = res.data[0].result;
							self.form.DOB = dob ;
					//},function(res){
							//res.data.header ? logger.logError('Error : get data, try refresh') : logger.logError("Error : get data, try refresh")
							//res.data.header ? logger.logError('Error : get data, try refresh') : logger.logError("Error : get data, try refresh")
					// })	
					// }else{
					// 	self.actived =  false;
					// 	self.form.DOB =   0;
					// }

					
				},getday:function(form){
					console.log('yang in masuk');
					//console.log('test');
					//console.log(self.form);
					var oneDay = 24*60*60*1000;

					var firstDate = self.form.From;
					var secondDate = self.remaining_day;
					//var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

					//console.log()
					//self.form.DayOffBetween = parseInt(diffDays);
					if(self.label == undefined){
						self.get_employee_id   =  self.label_x;
					}else{
						self.get_employee_id   =  self.label;
					}
					// if(self.form.employee_id == undefined && self.form.employee_name == undefined){
					// 	logger.logError('please submit name first before change start and end date request');
					// 	var getday_default  =  false
					// }else{
					// 	var getday_default =  true
					// }
					if(self.actived == false){
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/getDOB?key=' + $cookieStore.get('key_api'),
							data :  {employee_id : self.get_employee_id, from : firstDate, remain : secondDate  }
						}).then(function(res){
							//console.log(res)
							self.form.DOB = res.data[0].result;
						},function(res){
							res.data.header ? logger.logError('Error : get data, try refresh') : logger.logError("Error : get data, try refresh")
						})
					}
				},getBL:function(form){
					//
					//console.log(self.form)
					//console.log(self.form);
					// var oneDay = 24*60*60*1000;
					// var firstDate = new Date(form.From);
					// var secondDate = new Date(form.To);
					// var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
					// self.form.DayOffBetween = parseInt(diffDays) + 1;
					if(self.form.employee_id == undefined && self.form.name == undefined){
						logger.logError('please submit name first before change start and end date request');
						var getday_default  =  false
					}else{
						var getday_default =  true
					}
					if(getday_default == true){
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/gettg?key=' + $cookieStore.get('key_api'),
							data :self.form
						}).then(function(res){
							//console.log(res.data[0].tg)
							if(res[0] == 1){
								res.data.header ? logger.logError('Error : please fill your Birthday first before request ') : logger.logError("Error : get data, try refresh")
							}else{
								self.statHide =  true;
								self.form.From = res.data[0].tg
								self.form.To = res.data[0].tg
							 
							}
						},function(res){
							res.data.header ? logger.logError('Error : get data, try refresh') : logger.logError("Error : get data, try refresh")
						})
					}
				},getado:function(){

					////console.log(self.form)
					
					self.breaHide = true;
					self.suspen = true;
					//self.getremaining()
					if(self.form.LeaveType == 'Accumulation Day Off'){	
						self.getremaining()
						self.showformado = true;
						self.hideADO = true;
						self.form.IncludeAdo ='';
					}else if(self.form.LeaveType == "Bereavement Leave"){
						self.gettype()

						self.breaHide = false;
					}else if(self.form.LeaveType == "Suspension"){
						////console.log(1)
						self.getremaining()
						self.getsuspension()
						self.suspen = false;
					}else if(self.form.LeaveType == "Birthday Leave"){
						self.getBL()
						self.getremaining() 
						self.showado = false
					}else{
						self.getremaining()
						self.statHide =  false;
						self.showformado = false;
						self.hideADO = false;
						if(self.form.LeaveType == 'Vacation Leave' || self.form.LeaveType == 'Enhance Leave'){
							self.showado = true;
						}else{
							self.showado = false;
						}
					}	
					},getbreav:function(i,data){
					//console.log(i,data)
					if(self.form.LeaveBrea != undefined && self.form.LeaveBrea != ""){
					  var test  =  $cookieStore.get('bereavement')

					  //console.log(i)
					}
				},setvalue:function(data,value){
					//console.log(data,value)
					if(value  == ""){
						self.form.remaining_day =  'Not Allowed';
					}else{
						for(a in data){
							if(data[a].relation_to_deceased == value){
								self.form.remaining_day = data[a].days;
							}
						}
					}
				},getinclude:function(){
					
					if(self.form.IncludeAdo == 'Yes'){
						self.showformado = true;
					}else{
						self.showformado = false;
					}
				},
				/** get schedule for in select ADO **/
				getschedule : function(URL){
					self.button_submit  =  true;
					self.from_check_button  =  true;
					$http.get(URL).success(function(res){
						if(res.header.status == 200){
							self.schedule = res.data;
						}
					});	
				},open_quest : function(data){
					console.log('test >>>>>>>>>>')
					self.button_submit  =  false;
					self.from_check_button =false;
				},getremaining:function(){
					console.log('tgeyvdnasas')
					var def =  false
					if(self.label == undefined){
						console.log(self.label_x)	
						//self.form.name = self.label_x;
						self.get_employee_id   =  self.label_x;
						def = true
					}else if(self.form.LeaveType == undefined){
						def = false
					}else{
						//self.form.name = self.label;
						def = true
						self.get_employee_id   =  self.label;
					}


					//console.log(self.form)
					if(def == true && self.form.LeaveType != undefined && self.form.LeaveType != ""  ){
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/remaining?key=' + $cookieStore.get('key_api'),
							data : {'name' : self.get_employee_id , 'LeaveType' : self.form.LeaveType }
						}).then(function(res){
							console.log(res.data);

							console.log('test',1)
							self.button_submit =  false;
							//console.log(res.data.data)
							if(res.data.data == null){
								 self.hideButton =  true;
								 logger.logError(res.data.message);
							}

							if(res.data.data != null){
								self.getLastDayFirst =  res.data.date_max_from_request;
								self.hideButton =  false;
								if(res.data.data.length ==  0){
									self.form.remaining_day = 'Not Allowed';
									self.form.date={};
									logger.logError(res.data.message);
								}else if(res.data.last != undefined){
									self.form.date={};
									self.form.remaining_day = res.data.data;
									self.form.date.last_date = res.data.last;
									self.form.date.max_date_req = res.data.max;
									self.form.date.for_request = res.data.next;
								}else{
									self.form.remaining_day =res.data.data;
									
									// self.form.date.last_date = res.data.data.date.last_date || '';
									// self.form.date.max_date_req = res.data.data.date.max_date_req || '';
									// self.form.date.for_request = res.data.data.date.for_request || '';
									logger.logSuccess(res.data.message);
								}
							}
							
							if(self.model == undefined){
								//console.log(self.label_x)	
								self.form.name = self.label_x_name;
								 def = true
							}else{
								self.form.name = self.model;
								def = true
							}

							self.from_check_button  =  false
							//console.log(1,self.from_check_button(false))
						},function(res){
							console.log('test',2)
							self.button_submit =  true ;
							//console.log('test');
							self.from_check_button = true
							if(self.label == undefined){
								//console.log(self.label_x)	
								self.form.name = self.label_x_name;
								 def = true
							}else{
								self.form.name = self.model;
								def = true
							}
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Search Remaining days Failed")
						});
					}		
				},save:function(file){

					var defauult = true;
					if(self.inputhide.status=='user'){
						self.form.name = this.iduser
					}else{
						self.form.name = self.label;
					}
					if(self.form.LeaveType=='Accumulation Day Off'){
						self.form.availmentdate = self.form.date1+ ',' + self.form.date2 + ',' + self.form.date3 + ',' + self.form.date4;
					}

					if(self.form.LeaveType=='Suspension'){
						var a = new Date(self.sanction)
						var b = new Date(self.form.To)
						////console.log('to',self.form.To)
						if(a > b){
							 defauult = false;
							logger.logError('effective date start from '+self.sanction+" can't  lower than date effective") 
						}
					}

					if(self.form.remaining_day != "Not Limited"){
						if(parseInt(self.form.remaining_day) <= parseInt(self.form.DayOffBetween)){
							defauult = false;
							var check  = parseInt(self.form.remaining_day) - parseInt(self.form.DayOffBetween);   
							if(check < 0){
								logger.logError('More than remaining day');
							}else{
								logger.logError('More than remaining day');
							}
							//console.log(self.form.remaining_day,self.form.DayOffBetween)
						
						}
					}else{
						if(parseInt(self.form.DayOffBetween) > 1){
							defauult = false;
							logger.logError('More than 1 day');	
						}
					}
					//console.log(self.form.remaining_day,self.form.DayOffBetween)
					if(defauult == true){
						if(file){
							console.log(self.bereaves)
							//url : ApiURL.url + '/api/leave/requestLeave?key=' + $cookieStore.get('key_api'),
						    if(self.form.LeaveType == "Bereavement Leave" ){
						    	self.form.id_bereavement  = self.bereaves[0].id
						    }

						    var date1_new = new Date(self.form.From);
							var date2_new = new Date(self.form.To);
							var timeDiff_new = Math.abs(date2_new.getTime() - date1_new.getTime());
							var diffDays_new = Math.ceil(timeDiff_new / (1000 * 3600 * 24)); 

							self.form.DayOffBetween  = diffDays_new 
							console.log('self.form.DayOffBetween',self.form.DayOffBetween);
						    console.log(self.form)
							file.upload = Upload.upload({
								method : 'POST',
								url : ApiURL.url + '/api/leave/saving_request?key=' + $cookieStore.get('key_api'),
								data :{data:self.form},
								file: file
							}).then(function(res){
								self.form = {}
								logger.logSuccess(res.data.header.message);
								$route.reload();
							},function(res){
								$route.reload()
								self.form = {}
								self.form.name = self.model;
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Leave Request Failed")
							});
						}else{				
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/leave/saving_request?key=' + $cookieStore.get('key_api'),
								data :self.form
							}).then(function(res){
								$route.reload()
								console.log(self.model)
								self.form.name = self.model;
								logger.logSuccess(res.data.header.message);
							},function(res){
								$route.reload();
								self.form.name = self.model;
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Leave Request Failed")
							});
						}
					}
				},cancel:function(){
					self.form={};
				},
				
				/**=======================
					ADI SLIDERS (NOT USED) 
				========================**/
				slider1: function(){
					self.resultElement = document.getElementById('red')
					noUiSlider.create(self.resultElement,{
						start:0,
						step:1,
						range: {
							'min':0,
							'max':3
						}
					});
					self.form={};
					self.form.date1, self.form.date2, self.form.date3, self.form.date4 = "";
					self.resultElement.noUiSlider.on('slide',function(values, handle){	
						////console.log([parseInt(values)])
						if(values=='0.00'){
							self.form.nextday = self.ranges[0] 
							////console.log(self.form.nextday)
						}
					});
				},/*slider:function(a){
					var a = [];
					self.ranges = [self.form.date1, self.form.date2, self.form.date3, self.form.date4];	
				},*/
				
				/**==============
					RZS SLIDERS 
				==============**/
				slider:function(a){
					if(a==0){
						self.form.nextday = self.form.date1;
						self.setnext(self.form)
					}else if(a==1){
						self.form.nextday = self.form.date2;
						self.setnext(self.form)						
					}else if(a==2){
						self.form.nextday = self.form.date3;
						self.setnext(self.form)						
					}else if(a==3){
						self.form.nextday = self.form.date4;
						self.setnext(self.form)
					}
				},setnext:function(form){
					if(form.nextday == form.date1){
						self.form.date1=self.form.date2;
						self.form.date2=self.form.date3;
						self.form.date3=self.form.date4;
						self.form.date4=self.getnextday(self.form.date3)
					}else if(form.nextday == form.date2){
						self.form.date2 = self.form.date3;
						self.form.date3 = self.form.date4;
						self.form.date4 = self.getnextday(self.form.date3)
					}else if(form.nextday == form.date3){
						self.form.date3 = self.form.date4;
						self.form.date4 = self.getnextday(self.form.date3)
					}else if(form.nextday == form.date4){
						self.form.date4 = self.getnextday(self.form.date4)
					}
				},getnextday:function(date){
					var a = date.substring(0,4)
					var b = date.substring(5,7) - 1;
					var c = date.substring(8,10)
					var lastDayOf2015 = new Date(a,b,c);
					var nextDay = new Date(lastDayOf2015);
					var dateValue = nextDay.getDate() + 1;
					nextDay.setDate(dateValue);
					var a = $filter('date')(nextDay,'yyyy-MM-dd')
					return a;
				},setslider:function(data){
					self.refreshSlider();
					////console.log('asdf', data)
					var dates = [];
					data.datev1 != '- ' ? dates.push(new Date(data.datev1).getTime()) : null;
					data.datev2 != '- ' ? dates.push(new Date(data.datev2).getTime()) : null;
					data.datev3 != '- ' ? dates.push(new Date(data.datev3).getTime()) : null;
					data.datev4 != '- ' ? dates.push(new Date(data.datev4).getTime()) : null;
					data.nextday != '- ' ? dates.push(new Date(data.nextday).getTime()) : null;

					/*for (var i = 1; i <= 31; i++) {
						dates.push(new Date(2016, 7, i));
					}*/
					dates.sort(function (a, b) {
					    var key1 = a;
					    var key2 = b;

					    if (key1 < key2) {
					        return -1;
					    } else if (key1 == key2) {
					        return 0;
					    } else {
					        return 1;
					    }
					});
					var newArr = [];
					for(var a in dates) {
						var arr = {value: a + 1, legend: $filter('date')(new Date(dates[a]), 'yyyy-MM-dd')}
						newArr.push(arr);
					}
					////console.log(dates, newArr)
					self.priceSlider = {
						value: dates.indexOf(new Date(data.nextday).getTime()),
						options: {
						    showTicksValues: true,
						    stepsArray: newArr
						  }
					};
				},
				refreshSlider: function () {
				    $timeout(function () {
				        $rootScope.$broadcast('rzSliderForceRender');
				    });
				},check_ado : function(){
					console.log(self.get_employee_id)
					var user =  self.form.name;

					self.form.employee_id   =  self.get_employee_id

					////console.log(self.form.employee_id);
					if(self.form.name == undefined || self.form.date1 == undefined){
						logger.logError('please insert name before choose date')
					}else{
						////console.log(self.form.date1);
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/check_do?key=' + $cookieStore.get('key_api'),
							data :self.form
						}).then(function(res){
							////console.log(self.form.employee_id)
							logger.logSuccess(res.data.header.message);
						},function(res){
							
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Do not found in schedule")
						});
					}
				},nearest_day : function(){
					////console.log(self.form)
					var arr = []
					if(self.form.date1 != undefined){
						arr[1] = self.form.date1
					}
					if(self.form.date2 != undefined){
						arr[2] = self.form.date2
					}
					if(self.form.date3 != undefined){
						arr[3] = self.form.date3
					}
					if(self.form.date4 != undefined){
						arr[4] = self.form.date4
					}

					arr.sort();
				    var current = null;
				    var cnt = 0;
				    for (var i = 0; i < arr.length; i++) {
				        if (arr[i] != current) {
				            if (cnt > 1) {
				               logger.logError('ADO days you input same each other')
				            }else{
				            	$http({
									method : 'POST',
									url : ApiURL.url + '/api/leave/nearest_day?key=' + $cookieStore.get('key_api'),
									data :self.form
								}).then(function(res){
									self.form.datev1 = (res.data.data[0].date1 == "" ? "- " : res.data.data[0].date1)  
									self.form.datev2 = (res.data.data[0].date2 == "" ? "- " : res.data.data[0].date2)
									self.form.datev3 = (res.data.data[0].date3 == "" ? "- " : res.data.data[0].date3)
									self.form.datev4 = (res.data.data[0].date4 == "" ? "- " : res.data.data[0].date4)
									self.setslider(self.form);
									logger.logSuccess('Nearest days found')
								},function(res){
									logger.logError('Nearest DO not found')
								});
				            }
				            current = arr[i];
				            cnt = 1;
				        } else {
				            cnt++;
				        }
				    }
				    if (cnt > 1) {
				        logger.logError('ADO days you input same each other')
				    }
				}
				
			}
	})
	.controller('leaveRequestctrl',function(leaverequestfactory,ApiURL,$http,$cookieStore,$scope){
		var self = this;
			self.handler = leaverequestfactory;			
			self.handler.getselectleavetype(ApiURL.url + '/api/leave/Request?key=' + $cookieStore.get('key_api'));
			self.handler.getschedule(ApiURL.url + '/api/attendance/schedule/indexFixShift?key=' + $cookieStore.get('key_api'));
			self.handler.breaHide =  true;
			self.handler.getToday();

			self.handler.getLastYear();
			self.handler.getEnabledTo();
			self.handler.getDisabledActive();
			//self.handler.getselectBereavement(ApiURL.url +'/api/leave/RequestBrea?key=' + $cookieStore.get('key_api'));
			//leaverequestfactory.slider1();
			// self.handler.setslider();
			//$scope.priceSlider = 150;
			//var lastDayOf2015 = new Date(2016,04,01);
			//var nextDay = new Date(lastDayOf2015);
			//var lastday = new Date(lastDayOf2015);
			//var dateValue = nextDay.getDate() + 1;
			//var resultValue = nextDay.getDate() - 1;
			//nextDay.setDate(dateValue);
			//lastday.setDate(resultValue)
			//////console.log(nextDay)
			//////console.log(lastday)
			
	})
	
}())

