var appUICtrl = angular.module("app.ui.ctrls", []);
appUICtrl.controller("LoaderCtrl", ["$scope", "cfpLoadingBar", function($scope, cfpLoadingBar) {
    $scope.start = function() {
        cfpLoadingBar.start()
    }
	$scope.inc = function() {
        cfpLoadingBar.inc()
    }
	$scope.set = function() {
        cfpLoadingBar.set(.3)
    }
	$scope.complete = function() {
        cfpLoadingBar.complete()
    }
}]);
appUICtrl.controller("NotifyCtrl", ["$scope", "logger", function($scope, logger) {
    $scope.notify = function(type) {
        switch (type) {
            case "info":
                return logger.log("Heads up! This alert needs your attention, but it's not super important.");
            case "success":
                return logger.logSuccess("Well done! You successfully read this important alert message.");
            case "warning":
                return logger.logWarning("Warning! Best check yo self, you're not looking too good.");
            case "error":
                return logger.logError("Oh snap! Change a few things up and try submitting again.")
        }
    }
}]);
appUICtrl.controller("AlertDemoCtrl", ["$scope", function($scope) {
    $scope.alerts = [
		{
			type: "success",
			msg: "Well done! You successfully read this important alert message."
		}, {
            type: "info",
            msg: "Heads up! This alert needs your attention, but it is not super important."
        }, {
            type: "warning",
            msg: "Warning! Best check yo self, you're not looking too good."
        }, {
            type: "danger",
            msg: "Oh snap! Change a few things up and try submitting again."
        }
	]
	$scope.addAlert = function() {
        var num = Math.ceil(4 * Math.random());
		var type = undefined ;
		
        switch (num) {
            case 0:
                type = "info";
                break;
            case 1:
                type = "success";
                break;
            case 2:
                type = "info";
                break;
            case 3:
                type = "warning";
                break;
            case 4:
                type = "danger"
        }
        $scope.alerts.push({
            type: type,
            msg: "Another alert!"+num
        })
    }
	$scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1)
    }
}])
appUICtrl.controller("ProgressDemoCtrl", ["$scope", function($scope) {
    $scope.max = 200
	$scope.random = function() {
        var type, value;
        value = Math.floor(100 * Math.random() + 10);
		type = undefined;
		type = 25 > value ? "success" : 50 > value ? "info" : 75 > value ? "warning" : "danger";
		$scope.showWarning = "danger" === type || "warning" === type;
		$scope.dynamic = value;
		$scope.type = type;
    }
	$scope.random()
}]);
appUICtrl.controller("AccordionDemoCtrl", ["$scope", function($scope) {
    $scope.oneAtATime = true
	$scope.groups = [
		{
			title: "Dynamic Group Header - 1",
            content: "Dynamic Group Body - 1"
        }, {
            title: "Dynamic Group Header - 2",
            content: "Dynamic Group Body - 2"
        }, {
            title: "Dynamic Group Header - 3",
            content: "Dynamic Group Body - 3"
        }
	]
	$scope.items = ["Item 1", "Item 2", "Item 3"]
	$scope.status = {
        isFirstOpen: !0,
        isFirstOpen1: !0,
        isFirstOpen2: !0,
        isFirstOpen3: !0,
        isFirstOpen4: !0,
        isFirstOpen5: !0,
        isFirstOpen6: !0
    }
	$scope.addItem = function() {
        var newItemNo;
        newItemNo = $scope.items.length + 1
		$scope.items.push("Item " + newItemNo)
    }
}]);
appUICtrl.controller("CollapseDemoCtrl", ["$scope", function($scope) {
    $scope.isCollapsed = !1
}]);
	// Modal Demo Ctrl
appUICtrl.controller("ModalDemoCtrl", ["$scope", "$modal", "$log", function($scope, $modal, $log) {
    $scope.items = ["item1", "item2", "item3"]
	$scope.open = function(size) {
        var modalInstance;
        modalInstance = $modal.open({
            templateUrl: "myModalContent.html",
            controller: "ModalInstanceCtrl",
			size: size,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
		modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
			$log.info("Modal dismissed at: " + new Date)
        })
    }
}]);

appUICtrl.controller("ModalInstanceCtrl", ["$scope", "$modalInstance", "items", function($scope, $modalInstance, items) {
    $scope.items = items
	$scope.selected = {
        item: $scope.items[0]
    }
	$scope.ok = function() {
        $modalInstance.close($scope.selected.item)
    }
	$scope.cancel = function() {
        $modalInstance.dismiss("cancel")
    }
}]);
	// Modal Role Ctrl
appUICtrl.controller("ModalRoleCtrl", ["$scope", "$modal", "$log", function($scope, $modal, $log) {
    $scope.items = ["item1", "item2", "item3"]
	$scope.open = function(size) {
        var modalInstance;
        modalInstance = $modal.open({
            templateUrl: "modalRole.html",
            controller: "roleCtrl",
			size: size,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
		modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
			$log.info("Modal dismissed at: " + new Date)
        })
    }
}]);
appUICtrl.controller("roleCtrl", ["$scope", "$modalInstance", "items", function($scope, $modalInstance, items) {
    $scope.items = items
	$scope.selected = {
        item: $scope.items[0]
    }
	$scope.ok = function() {
        $modalInstance.close($scope.selected.item)
    }
	$scope.cancel = function() {
        $modalInstance.dismiss("cancel")
    }
}]);

// Modal Job Ctrl


appUICtrl.controller("PaginationDemoCtrl", ["$scope", function($scope) {
    $scope.totalItems = 64
	$scope.currentPage = 4
	$scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo
    }
	$scope.maxSize = 5
	$scope.bigTotalItems = 175
	$scope.bigCurrentPage = 1
}]);
appUICtrl.controller("TabsDemoCtrl", ["$scope", function($scope) {
    $scope.tabs = [
		{
			title: "Dynamic Title 1",
			content: "Dynamic content 1.  Consectetur adipisicing elit. Nihil, quidem, officiis, et ex laudantium sed cupiditate voluptatum libero nobis sit illum voluptates beatae ab. Ad, repellendus non sequi et at."
        }, {
            title: "Disabled",
            content: "Dynamic content 2.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, quidem, officiis, et ex laudantium sed cupiditate voluptatum libero nobis sit illum voluptates beatae ab. Ad, repellendus non sequi et at.",
            disabled: !0
        }
	]
	$scope.navType = "pills"
}]);



appUICtrl.controller("MapDemoCtrl", ["$scope", "$http", "$interval", function($scope, $http, $interval) {
    var i, markers;
    for (markers = [], i = 0; 8 > i;) markers[i] = new google.maps.Marker({
                title: "Marker: " + i
            }), i++;
            $scope.GenerateMapMarkers = function() {
                var d, lat, lng, loc, numMarkers;
                for (d = new Date, $scope.date = d.toLocaleString(), numMarkers = Math.floor(4 * Math.random()) + 4, i = 0; numMarkers > i;) lat = 43.66 + Math.random() / 100, lng = -79.4103 + Math.random() / 100, loc = new google.maps.LatLng(lat, lng), markers[i].setPosition(loc), markers[i].setMap($scope.map), i++
            }, $interval($scope.GenerateMapMarkers, 2e3)
        }])
   