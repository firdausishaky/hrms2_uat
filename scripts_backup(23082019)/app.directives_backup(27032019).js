var appDir = angular.module("app.directives", []);

appDir.directive("clockpicker", function () {
		return {
			restrict: "EA",
			replace: true,
			templateUrl: "template/clockpicker.html",
			scope: {
				datetime: "=ngModel"
			},
			controller: function ($scope) {
				$scope.hourOptions = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
				$scope.minuteOptions = ['00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'];
				//$scope.minuteOptions1 = ['00','02', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'];
				$scope.periodOptions = ['am', 'pm'];
				$scope.selectionMode = true;

				var timeMatches = /(\d{2}):(\d{2}):(\d{2})/.exec($scope.datetime.toString());

				$scope.hour = timeMatches[1];
				$scope.minute = timeMatches[2];
				$scope.period = "am";

				if ($scope.hour > 12) {
					$scope.hour = parseInt($scope.hour - 12);
					$scope.period = "pm";
				}

				var toggleOnSelection = false;
				var currentIndex = function () {
					if ($scope.selectionMode) {
						for (var i = 0; i < $scope.hourOptions.length; i++) {
							if ($scope.hourOptions[i] == $scope.hour) return i;
						}
					}
					else {
						for (var j = 0; j < $scope.hourOptions.length; j++) {
							if ($scope.minuteOptions[j] == $scope.minute) return j;
						}
					}
				};
				$scope.selectValue = function (value) {
					$scope.selectionMode ? $scope.hour = value : $scope.minute = value;
					if (toggleOnSelection) {
						$scope.selectionMode = !$scope.selectionMode;
					}
				};
				$scope.selectPeriod = function (value) {
					$scope.period = value;
				};
				$scope.togglePeriod = function () {
					$scope.selectPeriod($scope.period == "am" ? "pm" : "am");
				};
				$scope.lineStyle = function () {
					var angle = "rotate(" + (currentIndex() * 30 - 180) + "deg)";
					return "transform: " + angle + "; -webkit-transform: " + angle;
				};
				$scope.$watch("selectionMode", function (value) {
					$scope.options = value ? $scope.hourOptions : $scope.minuteOptions;
				});
				$scope.$watch("hour + period", function () {
					$scope.datetime.setHours($scope.period == "pm" ? $scope.hour + 12 : $scope.hour);
				});
				$scope.$watch("minute", function (value) {
					$scope.datetime.setMinutes(value);
					
				});
			}
		};
	})
	.run(["$templateCache", function ($templateCache) {
		$templateCache.put("template/clockpicker.html",
			"\n" +
			"<div class='ui-clockpicker'>\n" +
			"  <div class='ui-clockpicker-selection'>\n" +
			"    <a ng-click='selectionMode = true' ng-class='{selected: selectionMode}'>{{hour}}</a>:" +
			"<a ng-click='selectionMode = false' ng-class='{selected: !selectionMode}'>{{minute}}</a> " +
			"<a ng-click='togglePeriod()'>{{period}}</a>\n" +
			"  </div>\n" +
			"  <div class='ui-clockpicker-selector' ng-class='{minute: !selectionMode}'>\n" +
			"     <div class='ui-clockpicker-line' style='{{lineStyle()}}'></div>" +
			"     <ol class='ui-clockpicker-time'>\n" +
			"       <li ng-repeat='option in options' " +
			"         ng-class='{selected: selectionMode ? hour == option : minute == option }' " +
			"         ng-click='selectValue(option)'>{{option}}</li>\n" +
			"     </ol>\n" +
			"     <ol class='ui-clockpicker-period'>\n" +
			"       <li ng-repeat='periodOption in periodOptions' " +
			"         ng-class='{selected: period == periodOption }' " +
			"         ng-click='selectPeriod(periodOption)'>{{periodOption}}</li>\n" +
			"     </ol>\n" +
			"  </div>\n" +
			"</div>\n" +
			"");
}]);

appDir.directive("numberTest", function(){
	return{
		require: "ngModel",
		link: function(scope, element, attrs, ngModelCtrl){

			var maxlength = Number(attrs.numberTest);
			console.log(maxlength);
			function form(text) {
				if(text.length >= 10 ){
					var input = text.substring(0, 12);
					console.log(input);
          var numbers = input && input.replace(/-/g, '');
          var matches = numbers && numbers.match(/^(\d{3})(\d{3})(\d{4})$/);
            if(matches){
              input = matches[1] + "-" + matches[2] + "-" + matches[3];
            }
					ngModelCtrl.$setViewValue(input);
					ngModelCtrl.$render();
					return input;
				}
				return text;
			}
				ngModelCtrl.$parsers.push(form);
      
		}
	};
});

appDir.directive('numbersOnly', function(){
   return {
    require: '?ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      if(!ngModelCtrl) {
        return; 
      }
      
      ngModelCtrl.$parsers.push(function(val) {
        var clean = val.replace( /[^0-9]+/g, '');
        if (val !== clean) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        return clean;
      });
      
      element.bind('keypress', function(event) {
        if(event.keyCode === 32) {
          event.preventDefault();
        }
      });
     }
   };
});

appDir.directive('validEn', function() {
  return {
    require: '?ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      if(!ngModelCtrl) {
        return; 
      }

      ngModelCtrl.$parsers.push(function(val) {
      	//var clean = val.replace( /[^0-9+.+a-z+@]+/g, '');
        var clean = val.replace( /[^a-zA-Z]+/g, '');
        if (val !== clean) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        return clean;
      });

      element.bind('keypress', function(event) {
        if(event.keyCode === 32) {
          event.preventDefault();
        }
      });
    }
  };
});



appDir.directive("imgHolder", [function() {
    return {
        restrict: "A",
        link: function(scope, ele) {
            return Holder.run({
                images: ele[0]
            })
        }
    }
}]);

		
appDir.directive("customPage", function() {
    return {
        restrict: "A",
        controller: ["$scope", "$element", "$location", function($scope, $element, $location) {
            var addBg, path;
            return path = function() {
                return $location.path()
            }, addBg = function(path) {
                switch ($element.removeClass("body-wide body-lock"), path) {
                    case "/404":
                    case "/pages/404":
                    case "/pages/500":
                    case "/pages/signin":
                    case "/pages/signup":
                    case "/pages/forgot-password":
                        return $element.addClass("body-wide");
                    case "/pages/lock-screen":
                        return $element.addClass("body-wide body-lock")
                }
            }, addBg($location.path()), 
			$scope.$watch(path, function(newVal, oldVal) {
                return newVal !== oldVal ? addBg($location.path()) : void 0
            })
        }]
    }
});

appDir.directive("uiColorSwitch", [function() {
    return {
        restrict: "A",
        link: function(scope, ele) {
            return ele.find(".color-option").on("click", function(event) {
                var $this, hrefUrl, style;
                if ($this = $(this), hrefUrl = void 0, style = $this.data("style"), "loulou" === style) hrefUrl = "styles/main.css", $('link[href^="styles/main"]').attr("href", hrefUrl);
                else {
                    if (!style) return !1;
                    style = "-" + style, hrefUrl = "styles/main" + style + ".css", $('link[href^="styles/main"]').attr("href", hrefUrl)
                }
                return event.preventDefault()
            })
        }
    }
}]);
appDir.directive("goBack", [function() {
    return {
        restrict: "A",
        controller: ["$scope", "$element", "$window", function($scope, $element, $window) {
            return $element.on("click", function() {
                return $window.history.back()
            })
        }]
    }
}]);