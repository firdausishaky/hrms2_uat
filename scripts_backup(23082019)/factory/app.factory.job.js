var appFactory = angular.module("app.factory.job", []);

appFactory.factory('jobTitle',["$http","$cookieStore","ApiURL", function($http, $cookieStore,api){
	
	var KeyApi = $cookieStore.get('key_api');
    
	return {
	
        // get all data job
       
		get : function(){
            return $http.get(api.url +'/api/jobs/jobs-title?key=' +  KeyApi)
        },

		save2 : function(data){
            return $http({
				method : 'POST',
                url : api.url + '/api/jobs/jobs-title?key=' + KeyApi, 
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
				// paramater dari title dan descript 
            });
        },
		
		
        // save job (pass in data)
   
		save : function(data,id){
            return $http({
				method : 'POST',
                url : api.url + '/api/jobs/jobs-title/store?key=' + KeyApi, 
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
				// paramater dari title dan descript 
            });
        },
		
		
		// fungsi save dengan file
		update : function(id,data){
            return $http({
                method : 'POST',
                url : api.url + '/api/jobs/jobs-title/store' + id + '?key=' + KeyApi,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
        },
			
			
        // destroy  job
		destroy : function(id){
            return $http.delete(api.url + '/api/jobs/jobs-title/' + id + '?key=' +  KeyApi);
        },
		
		
		// fungsi radio button
		destroyFile : function(id){
            return $http({
				method : 'POST',
                url : api.url + '/api/jobs/jobs-title/cancel?key=' +  KeyApi + '&id=' + id,
				//  '&id='  sebagai id untuk menghapus file yang sebelumnya tersimpan 
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', }
            });
        },
		
			// fungsi edit / update  job
		updateFile : function(id,data){
            return $http({
                method : 'POST',
                url : api.url + '/api/jobs/jobs-title/update/' + id + '?key=' + KeyApi,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
        }
	
    }

}]);




appFactory.factory('empList',["$http","$cookieStore","ApiURL", function($http, $cookieStore,api){
	
	var KeyApi = $cookieStore.get('key_api');
    
	return {
	
        // get all the employee
        
		get : function(){
            return $http.get(api.url +'/api/jobs/employment-status?key=' +  KeyApi)
        },

		
        // save employee (pass in data)
        
		save : function(data){
            var store =  $http({
				method : 'POST',
                url : api.url + '/api/jobs/employment-status?key=' + KeyApi, 
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
            return store;
        },

		
        // destroy  employee
       
		destroy : function(id){
            return $http.delete(api.url + '/api/jobs/employment-status/' + id + '?key=' +  KeyApi);
        },
	
		
		// update  employee

		update : function(id,data){
            var store =  $http({
                method : 'POST',
                url : api.url + '/api/jobs/employment-status/' + id + '?key=' + KeyApi,
                headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
                data : $.param(data)
            });
            return store;
        }
    }

}]);

