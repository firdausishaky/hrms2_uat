var appUIFormDir = angular.module("app.ui.form.directives", []);

appUIFormDir.directive("uiRangeSlider", [function() {
    return {
        restrict: "A",
        link: function(scope, ele) {
            return ele.slider()
        }
    }
}]);

appUIFormDir.directive("uiFileUpload", [function() {
    return {
        restrict: "A",
        link: function(scope, ele) {
			return ele.bootstrapFileInput()
		}
    }
}]);

appUIFormDir.directive("uiSpinner", [function() {
    return {
        restrict: "A",
        compile: function(ele) {
            return ele.addClass("ui-spinner"), {
                post: function() {
                    return ele.spinner()
                }
            }
        }
    }
}]);

appUIFormDir.directive("uiWizardForm", [function() {
    return {
        link: function(scope, ele) {
			return ele.steps()
        }
    }
}]);