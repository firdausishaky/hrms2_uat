var appDashboard = angular.module("app.dashboard", ['ngCookies']);


appDashboard.factory('dashboardFactory',function (pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,$routeParams,$window,Upload) {
	
	var self
	return self ={
		hideShowCoba: function(){
			// if (input == 'a') {
			// 	(self.aaa == true)
			// }
			// if (input == 'b') {
			// 	console.log("test b")
			// }
			// if (input == 'c') {
			// 	console.log("test C")
			// }
			// console.log("test")
			// const onlyUser = $cookieStore.get('UserAccess')
			// const notUser = $cookieStore.get('NotUser')
			// function getID(){
			// 	if (notUser == null) {
			// 		return(onlyUser.id)	
			// 	}else{
			// 		return(notUser)
			// 	}
			// }

			
		}
	}
})
appDashboard.controller("dashboardCtrl", function($scope,$cookieStore,$location,GI,ApiURL,GetName, dashboardFactory) {

	var init;
	var self = this;
	self.handler = dashboardFactory
	
	/** set data logo **/
	GI.setData(ApiURL.url + '/get-images/company/' + $scope.test);

	/** set name **/
	$scope.onlyUser = $cookieStore.get('UserAccess');
	$scope.notUser = $cookieStore.get('NotUser');

	$scope.open = function(){
		if($scope.notUser==null){
			$location.path('/employee/personalDetail/' + $scope.onlyUser.id)	
		}else{
			$location.path('/employee/personalDetail/' + $scope.notUser)	
		}
	};

	$scope.mySchedule = true

	$scope.hideShow = function(input){
		if (input == 'myDetails') {
			if ($scope.myDetails=true) {
				return (($scope.mySchedule=false)||($scope.leaveRequest=false)|| ($scope.scheduleRequest=false) || ($scope.requestStatus = false)|| ($scope.mySchedule2=false)|| ($scope.photoGraph == false))
			}
		}

		if (input == 'myschedule') {
			if ($scope.mySchedule=true) {
				return  (($scope.myDetails=false) || ($scope.leaveRequest=false) || ($scope.scheduleRequest=false) || ($scope.requestStatus = false) || ($scope.mySchedule2=false)|| ($scope.photoGraph == false))
			}
		}
		if (input == 'myschedule2') {
			if ($scope.mySchedule2=true) {
				return  (($scope.myDetails=false) || ($scope.leaveRequest=false) || ($scope.scheduleRequest=false) || ($scope.requestStatus = false) || ($scope.mySchedule=false)|| ($scope.photoGraph == false))
			}
		}
		if (input == 'leaverequest') {
			if ($scope.leaveRequest=true) {
				return(($scope.myDetails=false) || ($scope.mySchedule = false) || ($scope.scheduleRequest = false) || ($scope.requestStatus = false) || ($scope.mySchedule2=false)|| ($scope.photoGraph == false)) 		
			}
		}
		if (input == 'schedulerequest') {
			if ($scope.scheduleRequest = true) {
				return (($scope.myDetails=false) || ($scope.leaveRequest=false) || ($scope.mySchedule=false) || ($scope.requestStatus = false) || ($scope.mySchedule2=false)|| ($scope.photoGraph == false))
			}
		}
		if (input == 'requeststatus') {
			if ($scope.requestStatus = true) {
				return (($scope.myDetails=false) || ($scope.leaveRequest=false) || ($scope.mySchedule=false) || ($scope.scheduleRequest=false) || ($scope.mySchedule2=false) || ($scope.photoGraph == false))  
			}
		}

		if (input == 'photograph') {
			if ($scope.photoGraph == true) {
				return (($scope.myDetails=false) || ($scope.leaveRequest=false) || ($scope.mySchedule=false) || ($scope.scheduleRequest=false) || ($scope.mySchedule2=false) || ($scope.requestStatus = false))  
			}	
		}

	}

});
