var appSideMenu = angular.module("app.dashboard.menu", ["ngFileUpload"]);

appSideMenu.controller("MenuCtrlDashboard", ["$route","$scope","$rootScope","$routeParams","ApiURL","$cookieStore","$http","logger","$location","$route","Upload","$window", function($route,$scope,$rootScope,$routeParams,api,$cookieStore,$http,logger,$location,$route,Upload,$window) {

	// $scope.id = $cookieStore.get('NotUser');
	const onlyUser = $cookieStore.get('UserAccess')
	const notUser = $cookieStore.get('NotUser')
	function getID(){
		if (notUser == null) {
			return(onlyUser.id)	
		}else{
			return(notUser)
		}
	}
	$scope.id = getID()
	$http.get(api.url + '/api/employee-details/personal/' + $scope.id + '?key=' + $cookieStore.get('key_api') ).success(function(res){
		// console.log(res.data);
		$scope.perInfo =res.data;
		$scope.files=[];
		var g = new File([""], res.pictures || 'Photograph Not Defined');
		$scope.files.push(g);
		$scope.names = res.data.first_name+' '+res.data.last_name;
		$scope.localIT =  res.data.local_it;
		if (res.pictures == null ){
			$scope.pic = "images/user2.jpg";
			$scope.showupload = true;
		}else{
			$scope.pic = api.url + "/images/" + $scope.id + '/' + res.pictures;
			$scope.hideupload = true;
		}
		/** function download file **/
		$scope.download = function(){
			console.log(res.pictures)
			if(res.pictures==null){
				logger.logError('Photograph Not Found')
			}else{
				console.log('to file')
				var file = 	$scope.file = api.url +  '/api/employee-details/picture/'  + $scope.id +  '/' + res.pictures + '?key=' + $cookieStore.get('key_api');
				$window.open(file);
			}
		};
		/** function delete file **/
		$scope.deleteFile = function(){
			$scope.pic = "images/user2.jpg";
			$scope.pic = api.url + "/images/" + $scope.id + '/' + res.pictures ;
			$http({
				method : 'DELETE',
				url : api.url + '/api/employee-details/picture/' + res.pictures + '?key=' + $cookieStore.get('key_api')
			}).success(function(data){
				$scope.files = null;
				$route.reload()
				$scope.pic = "images/user2.jpg" ;
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Deleting Data Photograph Failed");
			});
		};
	}).error(function(data){
		data.header ? logger.logError(data.header.message) : logger.logError("Showing Data Photograph Failed");
	});

	$scope.upload = function(file){
		var t = file[0];
		for(a in t){
			var temp = t['name'];
		}
		if(temp == "Photograph Not Defined"){
			logger.logError('Please select a file')
		}else{
			file.upload = Upload.upload({
				url:  api.url + '/api/employee-details/picture/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
				file: file
			}).then(function(data){
				$route.reload()
				logger.logSuccess('Uploading Photograph Successfully');
			},function(data){
				data.header ? logger.logError(data.header.message) : logger.logError(" Uploading Photograph Failed");
			});
		}	
	};

	$scope.onSelect = function ($item,$model,$label){
		$location.path('/employee/personalDetail/' + $item.employee_id)
	};		
	$scope.getLocation = function(val) {
		return $http.get(api.url + '/api/module/employee-info-search?search=' +$scope.employee + '&key=' + $cookieStore.get('key_api'), {
			params: {
				address : val,
			}
		}).then(function(response){
			var a = {};
			a.result = response.data;
			return a.result.map(function(item){
				return item;
			});
		});
	};

	var access = $cookieStore.get('access');
	$scope.$watch('access', function(){
		if(access === 'user'){
			$scope.access = false;
		}else{
			$scope.access = true;
		}
	});

	x = $cookieStore.get('NotUser')
	if (x == "2014888") {
		$scope.hideButton = true
	} else{
		$scope.hideButton = false
	}


	$scope.showSalaryHistory=true
	$scope.showHide = function(input){
		if (input=='showpersonaldetails') {
			if ($scope.showPersonalDetails=true) {
				return (($scope.showContactDetails=false)||($scope.showEmergencyContacts=false)|| ($scope.showDependents=false)|| ($scope.showImmigrations=false)|| ($scope.showReport=false)|| ($scope.showQualification=false)|| ($scope.showJobHistory=false) || ($scope.showSalaryHistory=false)|| ($scope.showPhotograph = false))
			}
		}
		if (input=='showcontactdetails') {
			if ($scope.showContactDetails=true) {
				return (($scope.showPersonalDetails=false)||($scope.showEmergencyContacts=false)|| ($scope.showDependents=false)|| ($scope.showImmigrations=false)|| ($scope.showReport=false)|| ($scope.showQualification=false)|| ($scope.showJobHistory=false) || ($scope.showSalaryHistory=false)|| ($scope.showPhotograph = false))
			}
		}
		if (input=='showemergencycontacts') {
			if ($scope.showEmergencyContacts=true ) {
				return (($scope.showPersonalDetails=false)||($scope.showContactDetails=false)|| ($scope.showDependents=false)|| ($scope.showImmigrations=false)|| ($scope.showReport=false)|| ($scope.showQualification=false)|| ($scope.showJobHistory=false) || ($scope.showSalaryHistory=false)|| ($scope.showPhotograph = false))
			}
		}
		if (input=='showdependents') {
			if ($scope.showDependents=true ) {
				return (($scope.showPersonalDetails=false)||($scope.showContactDetails=false)|| ($scope.showEmergencyContacts=false)|| ($scope.showImmigrations=false)|| ($scope.showReport=false)|| ($scope.showQualification=false)|| ($scope.showJobHistory=false) || ($scope.showSalaryHistory=false)|| ($scope.showPhotograph = false))
			}
		}
		if (input=='showimmigrations') {
			if ($scope.showImmigrations=true ) {
				return (($scope.showPersonalDetails=false)||($scope.showContactDetails=false)|| ($scope.showEmergencyContacts=false)|| ($scope.showDependents=false)|| ($scope.showReport=false)|| ($scope.showQualification=false)|| ($scope.showJobHistory=false) || ($scope.showSalaryHistory=false)|| ($scope.showPhotograph = false))
			}
		}
		if (input=='showreport') {
			if ($scope.showReport=true  ) {
				return (($scope.showPersonalDetails=false)||($scope.showContactDetails=false)|| ($scope.showEmergencyContacts=false)|| ($scope.showDependents=false)|| ($scope.showImmigrations=false)|| ($scope.showQualification=false)|| ($scope.showJobHistory=false) || ($scope.showSalaryHistory=false)|| ($scope.showPhotograph = false))
			}
		}
		if (input=='showqualification') {
			if ($scope.showQualification=true  ) {
				return (($scope.showPersonalDetails=false)||($scope.showContactDetails=false)|| ($scope.showEmergencyContacts=false)|| ($scope.showDependents=false)|| ($scope.showImmigrations=false)|| ($scope.showReport=false)|| ($scope.showJobHistory=false) || ($scope.showSalaryHistory=false)|| ($scope.showPhotograph = false))
			}
		}

		if (input=='showjobhistory') {
			if ($scope.showJobHistory=true  ) {
				return (($scope.showPersonalDetails=false)||($scope.showContactDetails=false)|| ($scope.showEmergencyContacts=false)|| ($scope.showDependents=false)|| ($scope.showImmigrations=false)|| ($scope.showReport=false)|| ($scope.showQualification=false) || ($scope.showSalaryHistory=false)|| ($scope.showPhotograph = false))
			}
		}
		if (input=='showsalaryhistory') {
			if ($scope.showSalaryHistory=true  ) {
				return (($scope.showPersonalDetails=false)||($scope.showContactDetails=false)|| ($scope.showEmergencyContacts=false)|| ($scope.showDependents=false)|| ($scope.showImmigrations=false)|| ($scope.showReport=false)|| ($scope.showQualification=false) || ($scope.showJobHistory=false) || ($scope.showPhotograph = false))
			}
		}
		if (input=='showphotograph') {
			if ($scope.showPhotograph = true) {
				return (($scope.showPersonalDetails=false)||($scope.showContactDetails=false)|| ($scope.showEmergencyContacts=false)|| ($scope.showDependents=false)|| ($scope.showImmigrations=false)|| ($scope.showReport=false)|| ($scope.showQualification=false) || ($scope.showJobHistory=false) || ($scope.showSalaryHistory=false))				
			}
			// console.log("cie bener")

		}
	}
	

}]);