var appFormLang = angular.module("app.dashboard.language", ["checklist-model"]);

appFormLang.controller("langQualCtrlDashboard",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissionqualification) {
	
	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** get all data **/
		$http({
			method : 'GET',
			url : ApiURL.url + '/api/employee-details/qualification/lang/'   + $scope.id + '?key=' + $cookieStore.get('key_api')
		}).then(function(res){
			$scope.languages = res.data.data;
			$scope.dellang = {languages:[]};
			logger.logSuccess(res.data.header.message)
		},function(res){
			res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Languages Failed")
		});

		/** function hidden button **/
		$scope.hide = function(a){
			var data = permissionqualification.access;
			if(data){
				switch(a){
					case 'create':
					if(data.create == 1){return true}
						break;
					case 'delete':
					if(data.delete == 1){return true}
						break;
				}
			}
		};

		/** modal handler **/
		$scope.open = function(size){
			var modal;
			modal = $modal.open({
				templateUrl: "modalLang.html",
				controller: "langCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function (newstorage) {
				if($scope.languages){
					$scope.languages.push(newstorage);
				}else{
					$scope.languages = [];
					$scope.languages.push(newstorage);
				}
			});
		};
		$scope.edit = function(size,lang) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditLang.html",
				controller: "LanguageEditCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return lang;
					}
				}
			});
			modal.result.then(function (newstorage) {
				for(a in $scope.languages){
					if($scope.languages[a].id==newstorage.id){
						$scope.languages[a]=newstorage;
					}
				}
			});
		};
		
		/** function remove **/
		$scope.remove = function(id){
			console.log(id)
			$http({
				method : 'DELETE',
				url : ApiURL.url + '/api/employee-details/qualification/lang/' + id + '?key=' + $cookieStore.get('key_api')
			}).success(function(data){
				var i;
				for( i = 0; i < id.length; i++) {
					$scope.languages = filterFilter($scope.languages, function (lang) {
						return lang.id != id[i];
					});
				};
				$scope.dellang.languages = [];
				logger.logSuccess(data.header.message);
			}).error(function(data) {
				data.header ? logger.logError(data.header.message) : logger.logError("Delete Data Languages Failed")
			});
		};
		
		
	});

appFormLang.controller("langCtrlDashboard", function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$modalInstance,items,$routeParams) {
	
	
	/** emloyee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** copy data **/
		if(items){
			$scope.formData=items;
		}
		
		/** get data master languages **/
		$http.get(ApiURL.url + '/api/employee-details/qualification/lang?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.datalanguage = res;
		});		
		
		/** function save **/
		$scope.save = function(){
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/qualification/lang/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
				data : $scope.formData   
			}).success(function(data){
				var temp = {};
				temp = $scope.formData;
				var a, b, c = temp.language;
				for(var i = 0; i < $scope.datalanguage.length; i++) {
					a = $scope.datalanguage[i];
					if(a.id == c) {
						b = a.title;
					};
				};
				temp.language = b;
				temp.id=data.data;
				$modalInstance.close(temp);					
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Adding Data Languages Failed")
			});
		};
		
		/** modal close **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}		
		
	});

appFormLang.controller("LanguageEditCtrlDashboard", function($routeParams,$scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,items,$modalInstance,permissionqualification) {
	
	/**copy data **/		
	if(items){
		$scope.formData = items;
		$scope.formData =angular.copy(items);
	}
	
	/** get data master languages **/
	$http.get(ApiURL.url + '/api/employee-details/qualification/lang?key=' + $cookieStore.get('key_api') ).success(function(res){
		$scope.datalanguage = res;
	});		
	
	/** function save **/
	$scope.save = function(id){
		for(a in $scope.datalanguage){
			if($scope.datalanguage[a].title == $scope.formData.language) {
				$scope.formData.language = $scope.datalanguage[a].id;
			};
		};
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/qualification/lang/' + items.id + '?key=' + $cookieStore.get('key_api'),
			data :$scope.formData  
		}).success(function(data){
			var temp = {};
			temp = $scope.formData;
			var a, b, c = temp.language;
			for(var i = 0; i < $scope.datalanguage.length; i++) {
				a = $scope.datalanguage[i];
				if(a.id == c) {
					b = a.title;
				};
			};
			temp.language = b;
			$modalInstance.close(temp); 						
			logger.logSuccess(data.header.message);
		}).error(function(data) {
			data.header ? logger.logError(data.header.message) : logger.logError("Update Data Languages Failed")
		});
	};
	
	/** function hide button **/
	$scope.hide = function(a){
		var access  = permissionqualification.access;	
		if(access){
			switch(a){
				case 'update':
				if(access.update == 1){
					return true
				}
				break;
			}
		}
	};
	
	/** close modal **/
	$scope.edit = function(){
		$scope.button=true;
	};
	$scope.cancel = function() {	
		$modalInstance.dismiss();
	}
	
	
});
/**
var a = String(items.language);
var b = String(items.fluent);
$scope.formData.language = a;
$scope.formData.fluent = b;
**/	



