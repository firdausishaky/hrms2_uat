var appFormDepen = angular.module("app.dashboard.dependents", ["checklist-model"]);


appFormEmmer.factory('permissiondepen',function(){
	var self;
	return self = {
		data:function(a){
			self.access = a;
		}
	}
});

appFormDepen.controller("dependentsCtrlDashboard",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissiondepen) {
	
	/** employee id **/	
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		/** get all data **/
		$http({
			method : 'GET',
			url : ApiURL.url + '/api/employee-details/dependent/' + $scope.id + '?key=' + $cookieStore.get('key_api')
		}).then(function(res){
			if(res.data.header.message == "Unauthorized"){
				permissiondepen.data(res.data.header.access);
				logger.logError("Access Unauthorized");
			}else{
				permissiondepen.data(res.data.header.access);
				$scope.students = res.data.data;
				$scope.check = {students:[]}
				logger.logSuccess(res.data.header.message)
			}
		},function(res){
			permissiondepen.data(res.data.header.access);
			res.data ? logger.logError(res.data.header.message) : logger.logError("Show Data Dependents Failed")
		});



		/** function hidden button **/
		$scope.hide = function(a){
			var data = permissiondepen.access;
			if(data){
				switch(a){
					case 'create':
					if(data.create == 1){return true}
						break;
					case 'delete':
					if(data.delete == 1){return true}
						break;
					case 'update':
					if(data.update == 1){return true}
						break;
				}
			}
		};
		
		/** modal handler **/
		$scope.open = function(size) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalDepend.html",
				controller: "modalDependCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function (newstudent) {
				if($scope.students){
					$scope.students.push(newstudent);
				}else{
					$scope.students=[];
					$scope.students.push(newstudent);
				}
			});
		};
		$scope.edit = function(size,student) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditDepend.html",
				controller: "DependEditCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return student;
					}
				}
			});
			modal.result.then(function(newstudent){
				for(a in $scope.students){
					if($scope.students[a].id==newstudent.id){
						$scope.students[a]=newstudent;
					}
				}
			});
		};


		
		/** function delete **/
		$scope.remove = function(id){
			$http({
				method : 'DELETE',
				url : ApiURL.url + '/api/employee-details/dependent/' + id + '?key=' + $cookieStore.get('key_api')
			}).success(function(data){
				var i;
				for( i = 0; i < id.length; i++) {
					$scope.students = filterFilter($scope.students, function (student) {
						return student.id != id[i];
					});
				};
				$scope.check.students = [];
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Delete Assigned Dependents Failed")
			});
		};
		
	});

appFormDepen.controller("modalDependCtrlDashboard",function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams) {
	
	/** employee id **/	
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()

		/** copy data **/
		if(items){
			$scope.formData = items;
		}


		$scope.calculateAge = function(a){

			function DaysInMonth(date2_UTC)
			{
				var monthStart = new Date(date2_UTC.getFullYear(), date2_UTC.getMonth(), 1);
				var monthEnd = new Date(date2_UTC.getFullYear(), date2_UTC.getMonth() + 1, 1);
				var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
				return monthLength;
			}


			date_1 = new Date(a)
			date_2 = new Date()


			date2_UTC = new Date(date_2.getFullYear(), date_2.getMonth(), date_2.getDate());
			date1_UTC = new Date(date_1.getFullYear(), date_1.getMonth(), date_1.getDate());


			//days
			var days = date2_UTC.getDate() - date1_UTC.getDate();
			if (days < 0)
			{

				date2_UTC.setMonth(date2_UTC.getMonth() - 1);
				days += DaysInMonth(date2_UTC);
			}

			// daysmonth

			var months = date2_UTC.getMonth() - date1_UTC.getMonth();
			if (months < 0)
			{
				date2_UTC.setFullYear(date2_UTC.getFullYear() - 1);
				months += 12;
			}

			// daysyear

			var years = date2_UTC.getFullYear() - date1_UTC.getFullYear();

			$scope.age = years

		}


		/** get birthday **/
		$scope.change = function() {
			var d = new Date();
			var a = d.getFullYear();
			$scope.age = a - $scope.formData.date_of_birth.split('-')[0];
		};

		/** function save **/	
		$scope.save = function(id){
			$scope.formData.age = $scope.age;
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/dependent/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
				data : $scope.formData   
			}).success(function(data){
				var temp = {};
				temp = $scope.formData;
				temp.id = data[0].id;
				$modalInstance.close(temp);
				data.header ? logger.logSuccess(data.header.message) : logger.logSuccess("Adding Assigned Dependents Success")
			}).error(function(data) {
				data.header ? logger.logError(data.header.message) : logger.logError("Adding Assigned Dependents Failed")
			});
		};

		/** close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}


	});

appFormDepen.controller("DependEditCtrlDashboard", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,permissiondepen) {
	
	/** copy data **/		
	if(items){
		$scope.formData = items;
		$scope.formData = angular.copy(items);
	}

	/** get birthday **/
	$scope.change = function() {
		var d = new Date();
		var a = d.getFullYear();
		var c = $scope.formData.date_of_birth.split('-')[0];
		$scope.age  =  a - c;
	};
	$scope.$watch('$scope.age', function () {
		var d = new Date();
		var a = d.getFullYear();
		var c = $scope.formData.date_of_birth.split('-')[0];
		$scope.age  =  a - c;
	});
	
	/** function save **/
	$scope.save = function(id){	
		$scope.formData.age = $scope.age;
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/dependent/' + items.id + '?key=' + $cookieStore.get('key_api'),
			data : $scope.formData   
		}).success(function(data){
			$modalInstance.close(data.data); 						
			logger.logSuccess(data.header.message);
		}).error(function(data) {
			data.header ? logger.logError(data.header.message) : logger.logError("Update Assigned Dependents Failed")
		});
	};
	
	/** modal close **/
	$scope.cancel = function() {
		$modalInstance.dismiss("cancel")
	}
	
	/** function hide button **/
	$scope.hide = function(a){
		var access  = permissiondepen.access;				
		if(access){
			switch(a){
				case 'update':
				if(access.update == 1){
					return true
				}
				break;
			}
		}
	};
	
});