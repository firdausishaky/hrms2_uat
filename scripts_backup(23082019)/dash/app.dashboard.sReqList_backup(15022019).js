(function(){
	angular.module('app.dashboard.sReqList',[])
	.factory('requestListFactoryDashboard',function(notifFactory,pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,$window,Upload,$route,$modalStack){

		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;
		}
		var self;
		return self = {
			getID:function(){
				const onlyUser = $cookieStore.get('UserAccess')
				const notUser = $cookieStore.get('NotUser')
				if (notUser == null) {
					return(onlyUser.id)
				}else{
					return(notUser)
				}

			},
			setheaders: function(){
				this.headers = new createheaders(
					['Date Request','dateRequest',20],
					['Availment Date','availment_date',20],
					['Employee Name','employee_name',20],
					['Request Type','request_type',20],
					['Status','status',20]
					);
				this.setactiveheader(this.headers[0],false)
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				all:{
					animation: true,
					templateUrl: 'all',
					controller: 'all',
					controllerAs: 'all',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}						
					}
				},
				rej:{
					animation: true,
					templateUrl: 'AppRejModal',
					controller: 'rejLeave',
					controllerAs: 'AppRejModal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Reject"
						},
						icon:function(){
							return "fa fa-times-circle-o"
						}
						
					}
				},
				app:{
					animation: true,
					templateUrl: 'AppRejModal',
					controller: 'appleave',
					controllerAs: 'AppRejModal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Approve"
						},
						icon:function(){
							return "fa fa-check-square-o"
						}
					}
				},
				can:{
					animation: true,
					templateUrl: 'AppRejModal',
					controller: 'appCancel',
					controllerAs: 'AppRejModal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Cancel"
						},
						icon:function(){
							return "fa fa-ban"
						}
					}
				}
			},
			actionButtonFunction : function(a,b,data){
				switch (a) {
					case 'app':
					$http({
						method : 'POST',
						url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
						data : { 0 : {'master_type' : 1, 'id' : data.id, 'employee_id': b.employee_id, 'type':b.type, "status" : b.status, 'employee': b.employee, sub : b.sub}}
					}).then(function(res){
						self.callbacksearch()
						notifFactory.getNotif()
						logger.logSuccess(res.data.header.message);
						$modalStack.dismissAll();
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Approve Schedule Request Failed")
					});
					break;
					case 'rej':
					$http({
						method : 'POST',
						url : ApiURL.url +'/api/attendance/schedule/reject_notif?key='+ $cookieStore.get('key_api'),
						data :{ 0 : {'master_type' : 1, 'id' : data.id, 'employee_id': b.employee_id, 'type':b.type, "status" : b.status, 'employee': b.employee, sub : b.sub}}
					}).then(function(res){
								//console.log("=================================================== PROCSSee",b)
								self.callbacksearch()
								notifFactory.getNotif()
								logger.logSuccess(res.data.header.message);
								
								$modalStack.dismissAll();
								//self.callbacksearch();
							},function(res){
								if(res.data.header){
									logger.logError(res.data.header.message)	
								}else{
									logger.logError("Reject Schedule Request Failed")
								}
								self.contex.back();
								//res.data.header ? logger.logError(res.data.header.message) : logger.logError("Reject Schedule Request Failed")
							});
					break;
					case 'can':
					$http({
						method : 'POST',
						url :ApiURL.url +'/api/attendance/schedule/cancle_notif?key='+ $cookieStore.get('key_api'),
						data :{ 0 : {'master_type' : 1, 'id' : data.id, 'employee_id': b.employee_id, 'type':b.type, "status" : b.status, 'employee': b.employee, sub : b.sub}}
					}).then(function(res){
						self.callbacksearch()
						notifFactory.getNotif()
						logger.logSuccess(res.data.header.message);
						$modalStack.dismissAll();
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Cancel  Schedule Request Failed")
					});
					break;
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b;

				$modal.open(self.modals[a]).result.then(function(data){
					self.actionButtonFunction(a,b,data);
/*					switch (a) {
						case 'app':
							console.log(data);
							$http({
								method : 'POST',
								url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
								data : { 0 : {'master_type' : 1, 'id' : data.id, 'employee_id': b.employee_id, 'type':b.type, "status" : b.status, 'employee': b.employee}}
							}).then(function(res){
								self.callbacksearch()
								notifFactory.getNotif()
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Approve Schedule Request Failed")
							});
						break;
						case 'rej':
							$http({
								method : 'POST',
								url : ApiURL.url +'/api/attendance/schedule/reject_notif?key='+ $cookieStore.get('key_api'),
								data :{ 0 : {'master_type' : 1, 'id' : data.id, 'employee_id': b.employee_id, 'type':b.type, "status" : b.status, 'employee': b.employee}}
							}).then(function(res){
								//console.log("=================================================== PROCSSee",b)
								self.callbacksearch()
								notifFactory.getNotif()
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Reject Schedule Request Failed")
							});
						break;
						case 'can':
							$http({
								method : 'POST',
								url :ApiURL.url +'/api/attendance/schedule/cancle_notif?key='+ $cookieStore.get('key_api'),
								data :{ 0 : {'master_type' : 1, 'id' : data.id, 'employee_id': b.employee_id, 'type':b.type, "status" : b.status, 'employee': b.employee}}
							}).then(function(res){
								self.callbacksearch()
								notifFactory.getNotif()
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Cancel  Schedule Request Failed")
							});
						break;
					}*/
					
				});
			},onSelect : function ($item,$model,$label){
				self.model = $item.name;
				self.label = $item.employee_id;
				// self.form.employee_name = $item.name;
				// self.form.employee_id = $item.employee_id;
				self.form.employee_name = self.model;
				self.form.employee_id = self.label;


			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_name +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};
						a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},
			// setDefault: function(URL){
			// 	$http.get(URL).success(function(res){
			// 		if(res.header.message=='Unauthorized'){
			// 			logger.logError('Access Unauthorized');
			// 			//console.log(res.header.access)
			// 		}else{

			// 			var a = res.data;
			// 			console.log(a)
			// 			a.department.unshift({ id : 0, name : '-- Select --', parent : 0});
			// 			//a.requestType 	= a.requestType.unshift({ id : 0, name : '-- Select --', parent : 0});
			// 			self.test 		=  a.department;
			// 			self.storage 	= a.requestType;
			// 			//console.log(self.test.splice(0,0,{ id : 0, name : '-- Select --', parent : 0}))
			// 			//console.log(self.storage.splice(0,0,{ type :  '-- Select --', type_id : 0 }))
			// 			self.form = {};
			// 			self.form = a;
			// 			self.jobs = a.department;
			// 			self.form.department = self.jobs[0];
			// 			self.form.requestType = self.storage[0];
			// 			self.name = res.data.employee_name;
			// 			self.idemployee =  res.data.employee_id;
			// 			self.emprole =  res.data.employee_role;
			// 			if(a.employee_role === 'Supervisor'){
			// 				self.form.employee_name = '';
			// 				self.hide = true;
			// 				//console.log(self.hide)
			// 			}else if(a.employee_role === 'HRD'){
			// 				self.form.employee_name = '';
			// 				self.hide = true;
			// 				//console.log(self.hide)
			// 			}else if(a.employee_role  === 'SUPERUSER'){
			// 				self.form.employee_name = '';
			// 				self.hide = true;
			// 				//console.log(self.hide)
			// 			}else{
			// 				self.hide = false
			// 				//console.log(self.hide)						
			// 				self.form.employee_name = a.employee_name;
			// 			}
			// 			self.getpermission(res.header.access);
			// 			self.setdata([]);
			// 			logger.logSuccess('Access Granted');
			// 		}
			// 	})
			// },
			setdefault:function(){
				$http({
					method : 'GET',
					url : 	ApiURL.url + '/api/attendance/schedule-request-detail/view?key=' + $cookieStore.get('key_api')
				}).success(function(res){
					self.defaultdata(res.data)
					self.getpermission(res.header.access);
					self.setdata([]);
					logger.logSuccess('Access Granted')
				})
			},
			defaultdata:function(a){
				console.log(a)
				a.department.unshift({ id : 0, name : '-- Select --', parent : 0});
				self.test 		=  a.department;
				self.storage 	= a.requestType;
				self.form = {};
				self.form = a;
				self.jobs = a.department;
				self.form.department = self.jobs[0];
				self.form.requestType = self.storage[0];
				self.idemployee =  a.employee_id;
				self.name = a.employee_name;
				self.emprole =  a.employee_role;
				if(a.employee_role === 'Supervisor'){
					// self.form.employee_name = self.getID();
					// self.form.employee_id = self.getID()
					self.hide = true;
							//console.log(self.hide)
						}else if(a.employee_role === 'HRD'){
							self.form.employee_name = '';
							self.hide = true;
							//console.log(self.hide)
						}else if(a.employee_role  === 'SUPERUSER'){
							self.form.employee_name = '';
							self.hide = true;
							//console.log(self.hide)
						}else{
							self.hide = false
							//console.log(self.hide)						
							self.form.employee_name = a.employee_name;
						}
						x = $cookieStore.get('uname')


						// console.log(self.form)
					},
					getpermission:function(active){
						if(active){
							this.mainaccess = active;
						}
					},button:function(a){
						if(this.mainaccess){
							switch(a){
								case 'read':
								if(this.mainaccess.read == 1){return true}
									break;
								case 'create':
								if(this.mainaccess.create == 1){return true}
									break;
								case 'update':
								if(this.mainaccess.update == 1){return true}
									break;
							}
						}
					},callbacksearch:function(){
						if(self.emprole=='User'){
							self.form.employee_name = self.idemployee;
						}else{
							self.form.employee_name = self.label;
						}
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule-request-detail/search?key=' + $cookieStore.get('key_api'),
							data : self.form 
						}).then(function(res){
							var t = res.data.data;
							var tmp=[];
							for(var i=0; i < t.length; i++){
								var idx = tmp.findIndex((str)=>{
									if(t[i].type_id == 6){
										var news = t[i].date_request; var olds = str.date_request;
										var t1 = new Date(olds);
										var t2 = new Date(news);
										var dif = t1.getTime() - t2.getTime();

										var Seconds_from_T1_to_T2 = dif / 1000;
										var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
										if(Seconds_Between_Dates <= 30){
											return true;
										}
								// var news = t[i].date_request; var olds = str.date_request;
								// var a = news.substring(0, news.length-2);
								// var b = parseInt(news.substring(news.length, news.length-2));

								// var a1 = olds.substring(0, olds.length-2);
								// var b1 = parseInt(olds.substring(olds.length, olds.length-2));

								// var c = b - b1;
								// if(a == a1 && c <= 7){
								// 	return true;
								// }
							}else if(t[i].type_id == 5){
								return str.date_request == t[i].date_request;
							}
						})
								if(idx == -1){
									tmp.push({
										"approval":t[i].approval,
										"approval_id":t[i].approval_id,
										"approval_name":t[i].approval_name,
										"approver":t[i].approver,
										"approver_id": t[i].approver_id,
										"approver_name":t[i].approver_name,
										"availment_date":t[i].availment_date,
										"can_approve":t[i].can_approve,
										"can_cancel":t[i].can_cancel,
										"can_reject":t[i].can_reject,
										"comment":t[i].comment,
										"date_request":t[i].date_request,
										"employee":t[i].employee,
										"employee_id":t[i].employee_id,
										"id":t[i].id,
										"sub":[t[i].id],
										"status":t[i].status,
										"status_id":t[i].status_id,
										"type":t[i].type,
										"type_id":t[i].type_id,
										"availment":t[i].availment_date
									});
								}else{
									tmp[idx].sub.push(t[i].id)
									tmp[idx].availment += " , "+t[i].availment_date;
								}
							}
							self.table_data  =  tmp;
							if(self.emprole=='User'){
								self.form.employee_name = self.name;
							}else{
								self.form.employee_name = self.model;
							}
							self.setdata(tmp);
						})
					},getSearch : function(){

						if(self.emprole=='User'){
							self.form.employee_name = self.form.employee_id;
							self.form.employee_id = self.form.employee_id;
						}else{
							self.form.employee_name = self.form.employee_id;
							self.form.employee_id = self.form.employee_id;
						}

						console.log(self.form)
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule-request-detail/search?key=' + $cookieStore.get('key_api'),
							data : self.form 
						}).then(function(res){
							var t = res.data.data;
							var tmp=[];
							for(var i=0; i < t.length; i++){
								var idx = tmp.findIndex((str)=>{
									if(t[i].type_id == 6 || t[i].type_id == 5){
										var news = t[i].date_request; var olds = str.date_request;
										var t1 = new Date(olds);
										var t2 = new Date(news);
										var dif = t1.getTime() - t2.getTime();

										var Seconds_from_T1_to_T2 = dif / 1000;
										var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
										if(Seconds_Between_Dates <= 30){
											return true;
										}
								// var news = t[i].date_request; var olds = str.date_request;
								// var a = news.substring(0, news.length-2);
								// var b = parseInt(news.substring(news.length, news.length-2));

								// var a1 = olds.substring(0, olds.length-2);
								// var b1 = parseInt(olds.substring(olds.length, olds.length-2));

								// var c = b - b1;
								// if(a == a1 && c <= 7){
								// 	return true;
								// }
							}
							/*else if(t[i].type_id == 5){
								return str.date_request == t[i].date_request;
							}*/

						})
								if(idx == -1){
									tmp.push({
										"approval":t[i].approval,
										"approval_id":t[i].approval_id,
										"approval_name":t[i].approval_name,
										"approver":t[i].approver,
										"approver_id": t[i].approver_id,
										"approver_name":t[i].approver_name,
										"availment_date":t[i].availment_date,
										"can_approve":t[i].can_approve,
										"can_cancel":t[i].can_cancel,
										"can_reject":t[i].can_reject,
										"comment":t[i].comment,
										"date_request":t[i].date_request,
										"employee":t[i].employee,
										"employee_id":t[i].employee_id,
										"id":t[i].id,
										"sub":[t[i].id],
										"status":t[i].status,
										"status_id":t[i].status_id,
										"type":t[i].type,
										"type_id":t[i].type_id,
										"availment":t[i].availment_date
									});
								}else{
									tmp[idx].sub.push(t[i].id)
									tmp[idx].availment += " , "+t[i].availment_date;
								}
							}
					//console.log(tmp,"CHECK2")
					self.table_data  =  tmp;
					x = $cookieStore.get('employee_id')
					y = $cookieStore.get('uname')
					console.log(self.form.employee_name ,x )

					if (self.form.employee_name == x ) {
						self.form.employee_name = y
					}else{

						if(self.emprole=='User'){

							self.form.employee_name = self.name;

						}else{
							self.form.employee_name = self.model;
						}
					}		
					self.setdata(tmp);
					logger.logSuccess(res.data.header.message);
				},function(res){
					if(self.emprole=='User'){
						self.form.employee_name = self.name;
					}else{
						self.form.employee_name = self.model;
					}
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Request List Failed")
				});
					},reset: function(){
						self.setDefault(ApiURL.url + '/api/attendance/schedule-request-detail/view?key=' + $cookieStore.get('key_api'));
					},exp : function(data){
				//console.log(data);
				
				if(!data){void 0; logger.logError('Please Searching Data Before Export') }
				for(a in self.maindata){
					delete self.maindata[a].id;
					delete self.maindata[a].type_id;
					delete self.maindata[a].status_id;
					alasql('SELECT * INTO XLSX("Schedule List.xlsx",{headers:true}) FROM ?',[self.maindata]);
					break;
				}
			}
			// ,setbuttonapprove:function(a,b){
			//  	console.log(self.table_data,"===============================++++=====================")
			// 	if(a == 'approve' && self.table_data[b].can_approve ==  'y'){
			// 		return true;
			// 	}else if(a == 'cancle' && self.table_data[b].can_cancel ==  'y'){
			// 		return true;
			// 	}else if(a == 'reject' && self.table_data[b].can_reject ==  'y'){
			// 		return true;
			// 	}else{
			// 		return false;
			// 	}

			// 	// if( a == 'approve' && self.form.employee_role == "User"){
			// 	// 	return true
			// 	// }
			// }
			,setbuttonapprove:function(a,b){
			 	//console.log(self.table_data,"===============================++++=====================")
			 	var stat = b.status.toLowerCase();
			 	var login = $cookieStore.get('NotUser');
			 	switch(a){
			 		case 'approval':
			 		if(b.can_approve=='y' && stat=='pending approval'){
			 			return true;
			 		}else{
			 			return false;
			 		}
			 		break;
			 		case 'cancel':
			 		if(b.can_cancel=='y' && stat=='pending approval'){
			 			return true;
			 		}else{
			 			if(login == "2014888" && stat=='pending approval'){
			 				return true;
			 			}else{
			 				return false;
			 			}
			 		}	

			 		break;
			 		case 'reject':
			 		if(b.can_reject=='y' && stat=='pending approval'){
			 			return true;
			 		}else{
			 			return false;
			 		}
			 		break;
			 	}
				// if(a == 'approve'){
				// 	if(b.can_approve=='y'){
				// 		return true;
				// 	}
				// }else if(a == 'cancle'){
				// 	if(b.can_cancel=='y' && b.status.toLowerCase()=='pending approval'){
				// 		return true;
				// 	}
				// }else if(a == 'reject'){
				// 	if(b.can_reject=='y' && b.status.toLowerCase()=='pending approval'){
				// 		return true;
				// 	}
				// }else{
				// 	return false;
				// }

				// if( a == 'approve' && self.form.employee_role == "User"){
				// 	return true
				// }
			}
			,setbuttondisabled:function(a,b){
				this.status = b;
				switch(a){
					case 'approve':
					if(this.status == 'Approval' || this.status == 'Rejected' || this.status == 'Cancel'){return true}
						break;
					case 'reject':
					if(this.status == 'Approval' || this.status == 'Rejected' || this.status == 'Cancel'){return true}
						break;
					case 'cancel':
					if(this.status == 'Cancel' || this.status == 'Approval' || this.status == 'Rejected'){return true}
						break;
				}
			},getDetail:function(form){

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/schedule-request-detail/edit/' +  form.id +  '?key=' + $cookieStore.get('key_api'),
					data :form
				}).then(function(res){
					console.log("res", res);
					if(res.data.length > 1){
						var subdata = [];
						self.detail2(subdata,res.data);
						self.detail(form,res.data[0])
						self.subdata = subdata[0];
						//form.subdata = self.subdata;
						//return console.log(self.subdata,"SUBDATA")
					}else{
						var subdata = [];
						self.detail2(subdata,res.data);
						self.detail(form,res.data[0])
						self.subdata = subdata[0];
					}

					console.log(self.subdata,"SUBDATAS")
					self.comment(res.data[0].comment)
					
					//return console.log(self.subdata,"SUBDATAS")
					self.dataHistory=res.data.history
					self.dataButton=res.data.data.behaviour
					res.data.header ? logger.logError(res.data.header.message) : logger.logSuccess("Show Detail Schedule List")
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Schedule List Failed")
				})
			},callbackgetdetail:function(form){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/schedule-request-detail/edit/' +  form.id +  '?key=' + $cookieStore.get('key_api'),
					data :form
				}).then(function(res){
					self.detail(form,res.data[0])
					self.comment(res.data[0].comment)
					//self.dataHistory=res.data[]history
					//self.dataButton=res.data.data.behaviour
				});
			},
			detail2: function(dataA,dataB){
				var t = dataB;
				for(var i=0; i < t.length; i++){
					var idx = dataA.findIndex((str)=>{
						if(t[i].swapWith){
							var news = t[i].date_request; var olds = str.date_request;
							var t1 = new Date(olds);
							var t2 = new Date(news);
							var dif = t1.getTime() - t2.getTime();

							var Seconds_from_T1_to_T2 = dif / 1000;
							var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
							if(Seconds_Between_Dates <= 50){
								return true;
							}
								// var a = news.substring(0, news.length-2);
								// var b = parseInt(news.substring(news.length, news.length-2));

								// var a1 = olds.substring(0, olds.length-2);
								// var b1 = parseInt(olds.substring(olds.length, olds.length-2));

								// var c = b - b1;
								// console.log(a,a1,"==============")
								// if(a == a1 && c <= 7){
								// 	return true;
								// }
							}else if(t[i].original){
								var news = t[i].date_request; var olds = str.date_request;
								var t1 = new Date(olds);
								var t2 = new Date(news);
								var dif = t1.getTime() - t2.getTime();

								var Seconds_from_T1_to_T2 = dif / 1000;
								var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
								if(Seconds_Between_Dates <= 50){
									return true;
								}
								//return str.date_request == t[i].date_request;
							}
						})
					if(idx == -1){
						console.log("test",t[i].flow_approval);
						dataA.push({
							date_request : t[i].date_request,
							requestBy : t[i].requestBy,
							flow_approval : t[i].flow_approval,
							schedule : t[i].schedule,
							approval_stat  :  t[i].approval_stat,
							approver_stat  :  t[i].approver_stat,
							approver_date  :  t[i].approver_date,
							approval_date  : t[i].approval_date,
							approver_time  :  t[i].approver_time,
							approval_time  : t[i].approval_time,
							RequestWorkendTime : t[i].RequestWorkendTime,
							duration : t[i].duration,
							approver : t[i].approver,
							comment : t[i].comment,
							date : [t[i].date],
							availment_swap : [t[i].availment_swap],
							time : t[i].time,
							role_name : t[i].role_name,
							compensation:t[i].compensation,
							approval:t[i].approval,
							overtime_need:t[i].overtime_need,
							date_:t[i].date_,
							approval:t[i].approval,
							status:t[i].status,
							time_:t[i].time_,
							NumberOfDay:t[i].NumberOfDay,
							TrainingRequest : t[i].TrainingRequest,
							date_ : t[i].date_,
							time_ : t[i].time_,
							new : [t[i].new],
							original : [t[i].original],
							swapWith : t[i].swapWith,
							new_shift_code  :  [t[i].new_shift_code],
							old_shift_code  :  [t[i].old_shift_code],
							original_schedule:[t[i].original_schedule],
							schedule_swap:[t[i].schedule_swap],
							total_swap : t[i].total_swap,
							total_request : t[i].total_request,
							req_in:t[i].req_in,
							req_out : t[i].req_out,
							original_in : t[i].original_in,
							original_out : t[i].original_out,
							time_out : t[i].time_out,
							time_in : t[i].time_in,
							early_out:t[i].earlyOut,
							LateToT : t[i].LateToT,
							sub:[t[i].id],
							availment : [t[i].date]
						});
					}else{
						dataA[idx].sub.push(t[i].id);
						dataA[idx].new.push(t[i].new);
						dataA[idx].original.push(t[i].original);
						dataA[idx].new_shift_code.push(t[i].new_shift_code);
						dataA[idx].old_shift_code.push(t[i].old_shift_code);
						dataA[idx].original_schedule.push(t[i].original_schedule);
						dataA[idx].schedule_swap.push(t[i].schedule_swap);
						dataA[idx].date.push(t[i].date);
						dataA[idx].availment += " , "+t[i].date;
						dataA[idx].availment_swap.push(t[i].availment_swap);
					}
				}
			},
			detail: function(dataA,dataB){
				dataA.requestBy = dataB.requestBy;
				dataA.schedule = dataB.schedule;
				dataA.approval_stat  =  dataB.approval_stat;
				dataA.approver_stat  =  dataB.approver_stat;
				dataA.approver_date  =  dataB.approver_date;
				dataA.approval_date  = dataB.approval_date;
				dataA.approver_time  =  dataB.approver_time;
				dataA.approval_time  = dataB.approval_time;
				dataA.RequestWorkendTime = dataB.RequestWorkendTime;
				dataA.duration = dataB.duration;
				dataA.approver = dataB.approver;
				dataA.comment = dataB.comment;
				dataA.date = dataB.date;
				dataA.time = dataB.time;
				dataA.role_name = dataB.role_name;
				dataA.compensation=dataB.compensation;
				dataA.approval=dataB.approval;
				dataA.overtime_need=dataB.overtime_need;
				dataA.date_=dataB.date_;
				dataA.approval=dataB.approval;
				dataA.status=dataB.status;
				dataA.time_=dataB.time_;
				dataA.NumberOfDay=dataB.NumberOfDay;
				dataA.TrainingRequest = dataB.TrainingRequest;
				dataA.date_ = dataB.date_;
				dataA.time_ = dataB.time_;
				dataA.new = dataB.new;
				dataA.original = dataB.original;
				dataA.swapWith = dataB.swapWith;
				dataA.new_shift_code  =  dataB.new_shift_code;
				dataA.old_shift_code  =  dataB.old_shift_code;
				dataA.original_schedule=dataB.original_schedule;
				dataA.schedule_swap=dataB.schedule_swap;
				dataA.total_swap = dataB.total_swap;
				dataA.total_request = dataB.total_request;
				dataA.req_in=dataB.req_in;
				dataA.req_out = dataB.req_out;
				dataA.original_in = dataB.original_in;
				dataA.original_out = dataB.original_out;
				dataA.time_out = dataB.time_out;
				dataA.time_in = dataB.time_in;
				dataA.early_out=dataB.earlyOut;
				dataA.LateToT = dataB.LateToT;
				dataA.flow_approval = dataB.flow_approval
			},comment:function(data){
				for(a in data){
					if(data[a].filename=='null' || data[a].filename=='NULL' || data[a].filename==''){
						data[a].filename='Not Defined';
					}
				}
				self.datacomment = data;
			},getfile:function(file){
				if(file == 'Not Defined'){
					logger.logError("File Not Found");
				}else{
					var e = ApiURL.url +  '/api/attendance/schedule-request-detail/getfile/' + file + '?key=' + $cookieStore.get('key_api');
					$window.open(e);
				}
			},actiondetail:function(a,data){
				self.actionButtonFunction(a,data,{ id : data.id });
				/*switch(a){
					case 'app':
						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule/approve_notif/?key='+ $cookieStore.get('key_api'),
							data : {'master_type' : 1, 'id' : data.id}
						}).then(function(res){
							self.callbackgetdetail(data)
							self.callbacksearch()
							notifFactory.getNotif()
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Approve  Schedule Request Failed")
						});
					break;
					case 'rej':
						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule-request-detail/reject/' +data.id +'?key='+ $cookieStore.get('key_api'),
							data :data
						}).then(function(res){
							self.callbackgetdetail(data)
							self.callbacksearch()
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Reject Schedule Request Failed")
						});
					break;
					case 'can':
						$http({
							method : 'POST',
							url : ApiURL.url +'/api/attendance/schedule-request-detail/cancel/' +data.id +'?key='+ $cookieStore.get('key_api'),
							data :data
						}).then(function(res){
							self.callbackgetdetail(data)
							self.callbacksearch()
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Cancel  Schedule Request Failed")
						});
					break;
				}*/
			},sentdetail:function(file,form){
				var datasent = form;
					//datasent.comment = form.newcomment;
					//datasent.id = form.id;
					//datasent.type_id = form.type_id;
					if(file){
						file.upload = Upload.upload({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule-request-detail/save/' + form.id + '?key=' + $cookieStore.get('key_api'),
							data :{data:datasent},
							file: file
						}).then(function(res){

							form.comment.push(res.data.data[0])
							self.callbackgetdetail(form)
							form.newcomment='';
							self.files = false; 
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Comment Request Failed")
						});
					}else{				
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule-request-detail/save/'+ form.id + '?key=' + $cookieStore.get('key_api'),
							data :datasent    
						}).then(function(res){

							form.comment.push(res.data.data[0])
							form.newcomment='';
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Comment Request Failed")
						});
					}
				},
				hideShow : function(){
					console.log($cookieStore.get('access'))
					checkRole = $cookieStore.get('access')
					if (checkRole == 200) {
						if (this.canAccess = true){
							this.cantAccess = false
						}
					}else{
						if (this.cantAccess = true){
							this.canAccess = false

						}
					} 
				}
			}
		})

.controller('scheduleRequestListCtrlDashboard',function(requestListFactoryDashboard,ApiURL,$cookieStore,$scope,GetName){
	var self = this;
	self.handler = requestListFactoryDashboard;
	self.handler.setdefault()
	// self.handler.setDefault(ApiURL.url + '/api/attendance/schedule-request-detail/view?key=' + $cookieStore.get('key_api'));
	// self.handler.getaccess()
	self.handler.hideShow()
	$scope.employee_name = GetName.getname()

})

.controller('all',function(requestListFactoryDashboard,$modalInstance,$timeout,data,ApiURL){	
	var self = this;

	self.handler = requestListFactoryDashboard
	self.handler.contex =  self;
	self.form = data;
	self.form=angular.copy(data)
			//console.log(data)
			self.approve = function(){
				$modalInstance.close(self.form);
			}
			self.reject = function(){
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.close(self.form);
			}
			self.back = function(){
				$modalInstance.dismiss("Close")
			}
			self.handler.getDetail(self.form);
			self.handler.setbuttondisabled(self.form)
			
		})

.controller('rejLeave',function(requestListFactoryDashboard,$modalInstance,$timeout,data,title,icon){
	var self = this;
	self.title = title;
	self.icon = icon;
	self.submit = function(){
		$modalInstance.close(data);
	}
	self.form = {};
	self.form = data;
	self.close = function(){
		$modalInstance.dismiss("Close")
	}
})

.controller('appleave',function(requestListFactoryDashboard,$modalInstance,$timeout,title,data,icon){
	var self = this;
	self.title = title;
	self.icon = icon;
	self.form=data
	self.form = angular.copy(data);
	self.submit = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('Close')
	}
})

.controller('appCancel',function(requestListFactoryDashboard,$modalInstance,$timeout,title,data,icon){
	var self = this;
	self.title = title;
	self.icon = icon;
	self.form=data
	self.form = angular.copy(data);
	self.submit = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss("shuwwaaa")
	}
})

.run(function(requestListFactoryDashboard){
	requestListFactoryDashboard.setheaders();
})

}())