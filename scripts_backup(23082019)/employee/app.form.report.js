var appFormReport = angular.module("app.form.report", ["checklist-model"]);

/** factory permission **/
appFormReport.factory('permissionreport',function(){
	var self;
	return self = {
		data:function(a){
			self.access = a;
		}
	}
});

/** Supervisor Controller **/
appFormReport.controller("supervisorCtrl",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissionreport) {
	
	/** employee id **/		
	$scope.id = $routeParams.id;
	x = $cookieStore.get('NotUser')
	if (x == "2014888") {
		$scope.hideButton = true
	} else{
		$scope.hideButton = false
	}

	
	/** get all data **/
	$http({
		method : 'GET',
		url : ApiURL.url + '/api/employee-details/report-to/supervisor/' + $scope.id + '?key=' + $cookieStore.get('key_api')
	}).then(function(res){
		permissionreport.data(res.data.header.access);
		$scope.students = res.data.data;
		$scope.check = {students:[]}
		logger.logSuccess(res.data.header.message)
	},function(res){
		permissionreport.data(res.data.header.access);
		res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Dependents Failed")
	});
	
	/** function hidden button **/
	$scope.hide = function(a){
		var data = permissionreport.access;
		if(data){
			switch(a){
				case 'create':
				if(data.create == 1){return true}
					break;
				case 'delete':
				if(data.delete == 1){return true}
					break;
				case 'update':
				if(data.update == 1){return true}
					break;
			}
		}
	};
	
	/** modal handler **/
	$scope.open = function(size) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalReport.html",
			controller: "modaladdsupervisor",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return;
				}
			}
		});
		modal.result.then(function (newstore) {
			if($scope.students){
				$scope.students.push(newstore);
			}else{
				$scope.students = [];
				$scope.students.push(newstore);
			}
		});
	};
	$scope.edit = function(size,student) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalEditReport.html",
			controller: "modaleditsupervisor",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return student;
				}
			}
		});
		modal.result.then(function(newstore) {
			for(a in $scope.students){	
				if($scope.students[a].id == newstore.id){
					$scope.students[a]=newstore;
				}
			};
		});
	};
	
	
	/** function remove **/
	$scope.remove = function(id){
		$http({
			method : 'DELETE',
			url : ApiURL.url + '/api/employee-details/report-to/supervisor/' + id + '?key=' + $cookieStore.get('key_api')
		}).success(function(data){
			var i;
			for( i = 0; i < id.length; i++) {
				$scope.students = filterFilter($scope.students, function (student) {
					return student.id != id[i];
				});
			};
			$scope.check.students = [];
			logger.logSuccess(data.header.message);
		}).error(function(data){
			data.header ? logger.logError(data.header.message) : logger.logError("Delete Assigned Supervisor Failed")
		});
	};
	
});

appFormReport.controller("modaladdsupervisor", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams) {
	
	/** employee id **/		
	$scope.id = $routeParams.id;
	
	/** copy data **/
	if(items){
		$scope.supervisor = items.supervisor;
	}
	
	/** function search employee **/
	$scope.onSelect = function ($item,$model,$label){
		$scope.nameemployee = $item.name;
		$scope.idemployee = $item.employee_id;
	};
	$scope.getLocation = function(val){
		return $http.get(ApiURL.url + '/api/employee-details/report-to/supervisor?search='+ $scope.supervisor +'&key=' + $cookieStore.get('key_api') + '&emp=' + $scope.id,{           
			params:{address:val,}
		}).then(function(response){
			console.log(response.data)
			var a = {};
			if(response.data[0].name == "employee already in use or employee not found."){
				$scope.alert =  "employee already in use.";
				console.log($scope.alert);
				$scope.alertMess = true
				a.result = [];
				return a.result.map(function(item){
					return item;
				});
			}else if(response.data[0].name == "employee already in use."){
				$scope.alert =  "employee not found.";
				$scope.alertMess = true
				console.log($scope.alert);
				a.result = [];
				return a.result.map(function(item){
					return item;
				});
			}else{
				$scope.alertMess = false;
				var a = {};
				a.result = response.data;
				console.log(a.result);
				return a.result.map(function(item){
					return item;
				});
			}
		});
	};
	
	/** function save **/
	$scope.save = function(){
		var datasent={};
		datasent.supervisor=$scope.idemployee;
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/report-to/supervisor/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
			data : datasent
		}).success(function(data){
			console.log(data)
			var temp = {};
			temp.id=data.data.id;
			console.log(temp.id)
			temp.supervisor=$scope.nameemployee;
			$modalInstance.close(temp);
			console.log(temp)
			logger.logSuccess(data.header.message);
		}).error(function(data){
			$scope.form.supervisor=$scope.model;
			data.header ? logger.logError(data.header.message) : logger.logError("Adding Assigned Supervisor Failed")
		});
	};
	
	/** close modal **/
	$scope.cancel = function() {
		$modalInstance.dismiss("cancel")
	}
	
});

appFormReport.controller("modaleditsupervisor",function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams,permissionreport) {
	
	/** employee id **/
	$scope.id = $routeParams.id;
	
	/** copy data **/
	var a = String(items.supervisor);
	if(items){
		$scope.form = items;
	}
	
	/** function search employee  **/
	$scope.onSelect = function ($item,$model,$label){
		$scope.form.supervisor = $item.name;
		$scope.model = $item.name;
		$scope.label = $item.employee_id;
	};
	$scope.getLocation = function(val) {
		return $http.get(ApiURL.url + '/api/employee-details/report-to/supervisor?search='+ $scope.form.supervisor +'&key=' + $cookieStore.get('key_api') + '&emp=' + $scope.id, {
			params: {address : val,}
		}).then(function(response){
			var a = {};
			a.result = response.data;
			return a.result.map(function(item){
				return item;
			});
		});
	};
	
	
	/** function save **/
	$scope.save = function(){
		$scope.form.supervisor = $scope.label;
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/report-to/supervisor/' + items.id + '?key=' + $cookieStore.get('key_api'),
			data : $scope.form 
		}).success(function(data){
			console.log(data.data)
			$modalInstance.close();
			logger.logSuccess(data.header.message);
		}).error(function(data){
			$scope.form.supervisor=$scope.model;
			data.header ? logger.logError(data.header.message) : logger.logError("Update Assigned Supervisor Failed")
		});
	};
	
	/** function hide button **/
	$scope.hide = function(a){
		var access  = permissionreport.access;	
		if(access){
			switch(a){
				case 'update':
				if(access.update == 1){
					return true
				}
				break;
			}
		}
	};
	
	
	/** close modal **/
	$scope.cancel = function() {
		$scope.form.supervisor = a;
		$modalInstance.dismiss()
	}
	$scope.edit = function(){
		$scope.button=true;
	};		
});



/** Subordinates Controller **/

appFormReport.controller("subordinatesCtrl",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissionreport){
	
	/** employee id **/		
	$scope.id = $routeParams.id;
	
	/** get all data **/
	$http({
		method : 'GET',
		url : ApiURL.url + '/api/employee-details/report-to/subordinate/' + $scope.id + '?key=' + $cookieStore.get('key_api')
	}).then(function(res){
		$scope.students = res.data.data;
		$scope.check = {students:[]}
		logger.logSuccess(res.data.header.message)
	},function(res){
		res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Subordinates Failed")
	});
	
	/** function hidden button **/
	$scope.hide = function(a){
		var data = permissionreport.access;
		if(data){
			switch(a){
				case 'create':
				if(data.create == 1){return true}
					break;
				case 'delete':
				if(data.delete == 1){return true}
					break;
				case 'update':
				if(data.update == 1){return true}
					break;
			}
		}
	};
	
	/** modal handler **/
	$scope.open = function(size) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalSub.html",
			controller: "modaladdsub",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return;
				}
			}
		});
		modal.result.then(function (newstudent) {
			if($scope.students){
				$scope.students.push(newstudent);
			}else{
				$scope.students = [];
				$scope.students.push(newstudent);
			}
		});
	};
	$scope.edit = function(size,student) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalEditSub.html",
			controller: "modaleditsub",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return student;
				}
			}
		});
	};
	
	
	/** function remove **/
	$scope.remove = function(id){
		console.log('delete')
		$http({
			method : 'DELETE',
			url : ApiURL.url + '/api/employee-details/report-to/subordinate/' + id + '?key=' + $cookieStore.get('key_api')
		}).success(function(data){
			var i;
			for( i = 0; i < id.length; i++) {
				$scope.students = filterFilter($scope.students, function (student) {
					return student.id != id[i];
				});
			};
			$scope.check.students = [];
			logger.logSuccess(data.header.message);
		}).error(function(data) {
			data.header ? logger.logError(data.header.message) : logger.logError("Delete Assigned Subordinate Failed")
		});
	};
});

appFormReport.controller("modaladdsub",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$modalInstance,items,$routeParams) {
	
	/** employee id **/		
	$scope.id = $routeParams.id;
	
	/** copy data **/
	if(items){
		$scope.sub_unit = items.sub_unit;
	}
	
	/** function search employee **/
	$scope.onSelect = function ($item, $model, $label) {
		$scope.subordinate = $item.name;
		$scope.model = $item.name;
		$scope.label = $item.employee_id;
	};
	$scope.getLocation = function(val) {
		return $http.get(ApiURL.url + '/api/employee-details/report-to/subordinates/searching/'+ $scope.id +'?search='+ $scope.subordinate +'&key=' + $cookieStore.get('key_api'), {
			params: {
				address : val,
			}
		}).then(function(response){
			console.log(response)
			var a = {};
			if(response.data.data[0].name == "employee already in use or employee not found."){
				$scope.alert =  "employee already in use.";
				console.log($scope.alert);
				$scope.alertMess = true
				a.result = [];
				return a.result.map(function(item){
					return item;
				});
			}else if(response.data.data[0].name == "employee already in use."){
				$scope.alert =  "employee not found.";
				$scope.alertMess = true
				console.log($scope.alert);
				a.result = [];
				return a.result.map(function(item){
					return item;
				});
			}else{
				$scope.alertMess = false;
				var a = {};
				a.result = response.data.data;
				return a.result.map(function(item){
					return item;
				});
			}
		});
	};
	
	/** function save **/
	$scope.save = function(){
		var datasent={};
		datasent.subordinate=$scope.label;
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/report-to/subordinate/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
			data :datasent    
		}).success(function(data){
			var temp = {}; 
			temp.subordinate = $scope.model;
			temp.id=data.data;
			$modalInstance.close(temp);
				//	$scope.model='';
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Adding Assigned Subordinate Failed")
			});
		};
		
		/** close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}
		
	});


appFormReport.controller("modaleditsub", function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,items,$modalInstance,$routeParams,permissionreport){
	
	/** employee id **/
	$scope.id = $routeParams.id;
	
	/** copy data **/
	var a = String(items.subordinate);
	if(items){
		$scope.form = items;
	}
	
	/** function search employee **/
	$scope.onSelect = function ($item,$model,$label){
		$scope.subordinate = $item.name;
		$scope.model = $item.name;
		$scope.label = $item.employee_id;
	};
	$scope.getLocation = function(val){
		return $http.get(ApiURL.url + '/api/employee-details/report-to/subordinates/searching/'+ $scope.id +'?search='+ $scope.form.subordinate +'&key=' + $cookieStore.get('key_api'), {
			params:{address : val,}
		}).then(function(response){
			var a = {};
			a.result = response.data;
			return a.result.map(function(item){
				return item;
			});
		});
	};
	
	/** function save **/
	$scope.save = function(){
		console.log('save')
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/report-to/subordinate/' + items.id + '?key=' + $cookieStore.get('key_api'),
			headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
			data : 'subordinate='+$scope.label     
		}).success(function(data){
			var temp = {}; 
			temp.subordinate = $scope.subordinate;
			$modalInstance.close();
			logger.logSuccess(data.header.message);
		}).error(function(data) {
			data.header ? logger.logError(data.header.message) : logger.logError("Update Assigned Subordinate Failed")
		});
	};
	
	/** function hide button **/
	$scope.hide = function(a){
		var access  = permissionreport.access;	
		if(access){
			switch(a){
				case 'update':
				if(access.update == 1){
					return true
				}
				break;
			}
		}
	};
	
	
	/** close modal **/
	$scope.edit = function(){
		$scope.button=true;
	};
	$scope.cancel = function() {
		$scope.form.subordinate = a;
		$modalInstance.dismiss();
	}
	
});	

