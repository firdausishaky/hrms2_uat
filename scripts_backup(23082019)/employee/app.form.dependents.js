var appFormDepen = angular.module("app.form.dependents", ["checklist-model"]);


/** factory permission **/
appFormEmmer.factory('permissiondepen',function(){
	var self;
	return self = {
		data:function(a){
			self.access = a;
		}
	}
});

appFormDepen.controller("dependentsCtrl",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissiondepen) {
	
	/** employee id **/	
	$scope.id = $routeParams.id;
	x = $cookieStore.get('NotUser')
	if (x == "2014888") {
		$scope.hideButton = true
	} else{
		$scope.hideButton = false
	}

	
	/** get all data **/
	$http({
		method : 'GET',
		url : ApiURL.url + '/api/employee-details/dependent/' + $scope.id + '?key=' + $cookieStore.get('key_api')
	}).then(function(res){
		if(res.data.header.message == "Unauthorized"){
			permissiondepen.data(res.data.header.access);
			logger.logError("Access Unauthorized");
		}else{
			permissiondepen.data(res.data.header.access);
			$scope.students = res.data.data;
			$scope.check = {students:[]}
			logger.logSuccess(res.data.header.message)
		}
	},function(res){
		permissiondepen.data(res.data.header.access);
		res.data ? logger.logError(res.data.header.message) : logger.logError("Show Data Dependents Failed")
	});
	
	/** function hidden button **/
	$scope.hide = function(a){
		var data = permissiondepen.access;
		if(data){
			switch(a){
				case 'create':
				if(data.create == 1){return true}
					break;
				case 'delete':
				if(data.delete == 1){return true}
					break;
				case 'update':
				if(data.update == 1){return true}
					break;
			}
		}
	};
	
	/** modal handler **/
	$scope.open = function(size) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalDepend.html",
			controller: "modalDependCtrl",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return;
				}
			}
		});
		modal.result.then(function (newstudent) {
			if($scope.students){
				$scope.students.push(newstudent);
			}else{
				$scope.students=[];
				$scope.students.push(newstudent);
			}
		});
	};
	$scope.edit = function(size,student) {
		var modal;
		modal = $modal.open({
			templateUrl: "modalEditDepend.html",
			controller: "DependEditCtrl",
			backdrop : "static",
			size: size,
			resolve: {
				items: function(){
					return student;
				}
			}
		});
		modal.result.then(function(newstudent){
			for(a in $scope.students){
				if($scope.students[a].id==newstudent.id){
					$scope.students[a]=newstudent;
				}
			}
		});
	};
	
	/** function delete **/
	$scope.remove = function(id){
		$http({
			method : 'DELETE',
			url : ApiURL.url + '/api/employee-details/dependent/' + id + '?key=' + $cookieStore.get('key_api')
		}).success(function(data){
			var i;
			for( i = 0; i < id.length; i++) {
				$scope.students = filterFilter($scope.students, function (student) {
					return student.id != id[i];
				});
			};
			$scope.check.students = [];
			logger.logSuccess(data.header.message);
		}).error(function(data){
			data.header ? logger.logError(data.header.message) : logger.logError("Delete Assigned Dependents Failed")
		});
	};
	
});

appFormDepen.controller("modalDependCtrl",function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams) {
	
	/** employee id **/	
	$scope.id = $routeParams.id;

	/** copy data **/
	if(items){
		$scope.formData = items;
	}
	
	/** get birthday **/
	$scope.change = function() {
		var d = new Date();
		var a = d.getFullYear();
		$scope.age = a - $scope.formData.date_of_birth.split('-')[0];
	};
	
	/** function save **/	
	$scope.save = function(id){
		$scope.formData.age = $scope.age;
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/dependent/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
			data : $scope.formData   
		}).success(function(data){
			var temp = {};
			temp = $scope.formData;
			temp.id = data[0].id;
			$modalInstance.close(temp);
			data.header ? logger.logSuccess(data.header.message) : logger.logSuccess("Adding Assigned Dependents Success")
		}).error(function(data) {
			data.header ? logger.logError(data.header.message) : logger.logError("Adding Assigned Dependents Failed")
		});
	};
	
	/** close modal **/
	$scope.cancel = function() {
		$modalInstance.dismiss("cancel")
	}
	
	
});

appFormDepen.controller("DependEditCtrl", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,permissiondepen) {
	
	/** copy data **/		
	if(items){
		$scope.formData = items;
		$scope.formData = angular.copy(items);
	}

	/** get birthday **/
	$scope.change = function() {
		var d = new Date();
		var a = d.getFullYear();
		var c = $scope.formData.date_of_birth.split('-')[0];
		$scope.age  =  a - c;
	};
	$scope.$watch('$scope.age', function () {
		var d = new Date();
		var a = d.getFullYear();
		var c = $scope.formData.date_of_birth.split('-')[0];
		$scope.age  =  a - c;
	});
	
	/** function save **/
	$scope.save = function(id){	
		$scope.formData.age = $scope.age;
		$http({
			method : 'POST',
			url : ApiURL.url + '/api/employee-details/dependent/' + items.id + '?key=' + $cookieStore.get('key_api'),
			data : $scope.formData   
		}).success(function(data){
			$modalInstance.close(data.data); 						
			logger.logSuccess(data.header.message);
		}).error(function(data) {
			data.header ? logger.logError(data.header.message) : logger.logError("Update Assigned Dependents Failed")
		});
	};
	
	/** modal close **/
	$scope.cancel = function() {
		$modalInstance.dismiss("cancel")
	}
	
	/** function hide button **/
	$scope.hide = function(a){
		var access  = permissiondepen.access;				
		if(access){
			switch(a){
				case 'update':
				if(access.update == 1){
					return true
				}
				break;
			}
		}
	};
	
});
/* var a = String(items.name);
var b = String(items.relationship);
var c = String(items.date_of_birth);
var d = String(items.gender);
var e = String(items.hmo_account);
var f = String(items.hmo_numb);
var g = String(items.age); */	
/* $scope.formData.name = a;
$scope.formData.relationship = b;
$scope.formData.date_of_birth = c;
$scope.formData.gender = d;
$scope.formData.hmo_account = e;
$scope.formData.hmo_numb = f;
$scope.formData.age = g; */
