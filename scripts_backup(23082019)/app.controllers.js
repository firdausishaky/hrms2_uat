var appCtrl = angular.module("app.controllers", ["ngCookies"]);
var myIntervalFunction = function() {
	cancelRefresh = $timeout(function myFunction() {
        // do something
        cancelRefresh = $timeout(myIntervalFunction, 3000);
    },3000);
};

var  hide_checked  = [];
appCtrl.controller("NavContainerCtrl", ["ApiURL","$location","$cookies","$cookieStore","$http","$rootScope","$scope","logger","$routeParams","$window", function(api,$location,$cookies,$cookieStore,$http,$rootScope,$scope,logger,$routeParams,$window){	
}]);

appCtrl.factory('GI',function($cookieStore){
	var self;
	return self = {
		logo: $cookieStore.get('images'),
		setData: function(a){
			this.logo = a;
			$cookieStore.put('images',a);
		},
		getData: function(){
			return this.logo
		}
	}
});

appCtrl.factory('GetName',function(ApiURL,$http,$cookieStore){
	
	var self;
	return self = {
		name:$cookieStore.get('uname'),
		setname: function(a){
			$cookieStore.put('uname',a)
			this.name = a;
		},
		getname: function(){
			return this.name
		}
	}
	
});
///////////////////////////////////////////////
appCtrl.factory('superCache', function ($cacheFactory) {
	return $cacheFactory('super-cache')

})
///////////////////////////////////////////////



appCtrl.factory('notifFactory',function(ApiURL,$http,$cookieStore,logger){
	
	var self;
	return self = {
		setCommonHeader : function(){
			try{
				var keys = $cookieStore.get('key_api');
				if(keys){
					console.log(keys,'test---------');
					$http.defaults.headers.common.Authorization = "Bearer "+$cookieStore.get('key_api');
					// $http.defaults.headers.get = { 'Authorization' : "Bearer "+$cookieStore.get('key_api') };
					// $http.defaults.headers.post = { 'Authorization' : "Bearer "+$cookieStore.get('key_api') };
					// $http.defaults.headers.get = { 'Authorization' : "Bearer "+keys };
					// $http.defaults.headers.post = { 'Authorization' : "Bearer "+keys };
					//$http.defaults.headers.option = { 'Authorization' : "Bearer "+keys };
				}
				console.log(keys,'test1---------');
			}catch(e){
				console.log(keys,e,'test2---------');
			}
		},
		getNotif:function(){
			
			$http({
				method : 'GET',
				url : ApiURL.url +'/api/attendance/schedule/count_notif?key=' + $cookieStore.get('key_api'),
				ignoreLoadingBar : true
			}).success(function(data){
				self.form = data
			}).error(function(data){
				//logger.logError("Error count request");
			});


		},dataNotif:function(data){
			return data;
		},
		// get_chat: function(){
		// 	$http({
		// 		method : 'GET',
		// 		url : ApiURL.url +'/api/attendance/schedule/gotcha?key=' + $cookieStore.get('key_api'),
		// 		ignoreLoadingBar : true
		// 	}).success(function(data){
		// 		self.test =  data
		// 	}).error(function(data){
		// 		//logger.logError("Error get chat");
		// 	});
		// },
		sort_approval  : function(data,cb){
			var arr   =  [];
			var i  = 0;
			for(var prop  in data){
				if(data[prop].approval  != null &&  data[prop].approver != null){
					delete  data[prop];
				}else{
					arr[i] =  data[prop]
					i++;
				}
			}
			
			var count  =  arr.length;
			return cb(arr,count);		
		},
		find_approval  :  function(data,b){
		    //console.log(1,data)
		    var arr1   =  [];
		    var i  = b;
		    //console.log(i)
		    for (var prop  in data){
		    	if(data[prop].approval  == null &&  data[prop].approver == null){
				//console.log(1,'find_approval')
				delete  data[prop];
			}else{
				//console.log(0)
				arr1[i] =  data[prop]
				i++;
			}
		}
		return arr1;
		    //console.log(arr) 

		},
		read_notif: function(){
			$http({
				method : 'GET',
				url : ApiURL.url +'/api/attendance/schedule/read_notif?key=' + $cookieStore.get('key_api'),
				ignoreLoadingBar : true
			}).success(function(data){
				

				//var result  =  data
				//console.log(data)
				//var data1 =  self.sort_approval(data,function(a,b){
				//	return a;
				//});

				//var count =  self.sort_approval(data,function(a,b){
				//	return b;
				//});
				
				//console.log('before',result,data1,count);
				//var data2  =  self.find_approval(result,count);
				var sortable  = [];
				for(var i in data){
					sortable.push(data[i]);
				}
				if(sortable.length > 0){
					sortable.sort(function(a, b) {
					  // convert date object into number to resolve issue in typescript
					  return  (new Date(b.date_request) - new Date(a.date_request));
					})
				}
				self.counting_pending = 0;
				var tmp_action = {pending:[], success : []};
				for(var i in sortable){
					// if(sortable[i].status_id == 1){
					// 	tmp_action.pending.push(sortable[i]);
					// 	//self.counting_pending++;
					// }else{

					// }
					if( (sortable[i].action.approve || sortable[i].action.reject || sortable[i].action.cancel) && sortable[i].status_id == 1){
						tmp_action.pending.push(sortable[i]);
					}else{
						tmp_action.success.push(sortable[i]);
					}
				}
				sortable = tmp_action.pending.concat(tmp_action.success);
				self.readX = sortable
				//var h= [self.readX[0],self.readX[1],self.readX[2]];
				//console.log(data,"TEST")
				if(self.readX == []){
					self.uknown =   true
				}

				var tmp = [];
				var datas = self.readX;
				for(var i=0; i < datas.length; i++){
					var idx = tmp.findIndex((str)=>{
						/*if(datas[i].type_id == 6){
							var news = datas[i].date_request; var olds = str.date_request;
							var t1 = new Date(olds);
							var t2 = new Date(news);
							var dif = t1.getTime() - t2.getTime();

							var Seconds_from_T1_to_T2 = dif / 1000;
							var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
							if(Seconds_Between_Dates <= 30){
								return true;
							}
						}else if(datas[i].type_id == 5){
							return str.date_request == datas[i].date_request;
						}*/
						if(datas[i].type_id == str.type_id && datas[i].master_type == str.master_type && datas[i].employee_id == str.employee_id){							
							var news = datas[i].date_request; var olds = str.date_request;
							var t1 = new Date(olds);
							var t2 = new Date(news);
							var dif = t1.getTime() - t2.getTime();

							var Seconds_from_T1_to_T2 = dif / 1000;
							var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
							if(Seconds_Between_Dates <= 30){
								return true;
							}
						}else if(datas[i].type_id == 6){
							var news = datas[i].date_request; var olds = str.date_request;
							var t1 = new Date(olds);
							var t2 = new Date(news);
							var dif = t1.getTime() - t2.getTime();

							var Seconds_from_T1_to_T2 = dif / 1000;
							var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
							if(Seconds_Between_Dates <= 30){
								return true;
							}
						}else if(datas[i].type_id == 5){
							var news = datas[i].date_request; var olds = str.date_request;
							var t1 = new Date(olds);
							var t2 = new Date(news);
							var dif = t1.getTime() - t2.getTime();

							var Seconds_from_T1_to_T2 = dif / 1000;
							var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
							if(Seconds_Between_Dates <= 30){
								return true;
							}
							//return str.date_request == datas[i].date_request;
						}
					});
					if(idx == -1){
						if( (datas[i].type_id  == 5 || datas[i].type_id  == 6) && datas[i].master_type == 1){
							tmp.push(datas[i]);
						}else{						
							datas[i].sub = [datas[i].id];
							tmp.push(datas[i]);
						}
					}else{
						if( (datas[i].type_id  == 5 || datas[i].type_id  == 6) && datas[i].master_type == 1){

						}else{
							
							tmp[idx].sub.push(datas[i].id)
							tmp[idx].availment_date += " , "+datas[i].availment_date;
						}
					}
				}
				self.counting_pending = 0;
				for (var j =0; j < tmp.length ; j++) {
					if(tmp[j].status_id == 1){
						// console.log(tmp[j]);
						self.counting_pending++;
					}
				};

				self.counting_pendingLeaveList = 0;
				for (var i = 0; i < tmp.length ; i++) {
					if (tmp[i].master_type == 2 ) {
						// console.log("test"+[i])
						self.counting_pendingLeaveList++;
					}
				}

				self.counting_pendingScheduleList = 0;
				for (var i = 0; i < tmp.length ; i++) {
					if (tmp[i].master_type == 1 ) {
						// console.log("test"+[i])
						self.counting_pendingScheduleList++;
					}
				}
				self.readX = tmp;

				console.log('chitchat',tmp);
			}).error(function(data){
				//logger.logError("Error get notification request")
			});


		},clickRequest :  function(id,req){
			// console.log(id,master_type.master_type)
			// console.log(req)
			console.log(req)
			master_type = req.master_type
			type_id = req.type_id
			//console.log(self.readX[id].employee,".asfjdlskf")
			if(self.readX[id] != undefined){
				var t = { employee : self.readX[id].employee, type : self.readX[id].type, employee_id : self.readX[id].employee_id};
				if(self.readX[id].status == "Pending Approval"){
					self.chats = false;
				}
				//console.log(t,"f'odjs;flsd;fj;")
				var id  = self.readX[id].id;	
				self.getZ =  id;
				$cookieStore.put('clickable',self.getZ);
				$cookieStore.put('clickable_data',t);
				// $cookieStore.put('master_type', req.master_type)
				// $cookieStore.put('master_type',req.master_type)
			}

			// else{
			// 	var id = id
			// 	self.getZ =  id;
			// 	$cookieStore.put('clickable',self.getZ)
			// }

			$http({
				method : 'GET',
				url : ApiURL.url +'/api/attendance/schedule/gotcha?key=' + $cookieStore.get('key_api') + "&id="+ id+"&master_type="+master_type+"&type_id="+type_id,
				ignoreLoadingBar : true
			})
			// .success(function(data){
			// 	if (true) {}
			// 		self.test =  data
			// 	self.indexShow  =  id

			// }).error(function(data){
			// 	console.log(data)
			// 	data.data.header ? logger.logError(data.dataheader.message) : logger.logError("Error get chat");
			// });

			// .then(function(res){
			// 	if (true) {}
			// 		self.test =  data
			// 	self.indexShow  =  id
			// 	console.log(res,"++++++++++++++++++++++++++")
			
			// },function(res){
			// 	console.log(res,"++++++++++++++++++++++++++")

			// })

			// $http({
			// 	method : 'GET',
			// 	url : ApiURL.url +'/api/attendance/schedule/send_stat?key=' + $cookieStore.get('key_api')+ "&id="+ id,
			// 	ignoreLoadingBar : true
			// }).success(function(data){
			// 	//console.log('test send_stat',self.readX);
			// 	// console.log('test send_stat',data.data);
			// 	if(data.data.length == 0){
			// 		//console.log('i will made it');
			
			// 		$cookieStore.remove('clickable');
			// 	}
			// }).error(function(data){
			// 	//logger.logError("Error get notification request")
			// });
		}, onMessage : function(comment){
			var id = self.getZ;
			var  exists   =    $cookieStore.get('clickable');
			var  exists_data   =    $cookieStore.get('clickable_data');

			/**
			 * disable  text in message if input status  id  2 or  3
			 */

			 if(exists  != null  || exists != undefined){
			 	var t = {'comment' :   comment, 'id' : id};
			 	if(exists_data){
			 		t.employee = exists_data.employee;
			 		t.employee_id = exists_data.employee_id;
			 		t.type = exists_data.type;
			 	}
			 	$http({
			 		method : 'POST',
			 		url : ApiURL.url +'/api/attendance/schedule/onMessage?key=' + $cookieStore.get('key_api') + "&id="+ $cookieStore.get('clickable'),
			 		data : t,
			 		ignoreLoadingBar : true
			 	}).success(function(data){
					//if(self.readX.[self.indexShow].id )
					self.clickRequest(id)

					self.test =  data
					console.log(self.test)
				}).error(function(data){
					//logger.logError("Error get chat");
				});	
			}else{
				log.Error('Error  post message');
			}			
		},refreshMessage  : function(){
			//console.log($cookieStore.get('clickable'));
			var exists   =  $cookieStore.get('clickable');

			//console.log(exists)
			if(exists != null | exists != undefined){
				var id = $cookieStore.get('clickable')
				$http({
					method : 'GET',
					url : ApiURL.url +'/api/attendance/schedule/gotcha?key=' + $cookieStore.get('key_api') + "&id="+ id,
					ignoreLoadingBar : true
				}).success(function(data){
					self.test =  data
					//console.log(self.test)
				}).error(function(data){
					//logger.logError("Error get chat");
				});
			}
		},deleteCookie : function(){
			if(self.readX  ==  null){
				$cookieStore.remove('clickable');
			}
		},checkDisabled  :  function(){
			
			if($cookieStore.get('clickable') != undefined){
				var id =  $cookieStore.get('clickable');
				var res = self.readX;

				for(var prop  in  res){
					if(res[prop].id ==  id ){
						return  false;
						if(res[prop].status_id != 1 && (res[prop].status_request == 'approved' || res[prop].status_request == 'rejected') ){
							//console.log('checkdisabled' , false);
							return  true;
						}else{
							//console.log('chakcdisabled', true);
							return  false;
						}
					}
				}	
			}

			//console.log('checkDisabled');

			
		},approve : function(data,t){
			//return console.log(data,t,'dasdasdasdasd');
			if(t.availment_date){
				var availment = t.availment_date;
			}
			else if(t.schedule){
				var availment = t.schedule;
			}else if(t.from_ && t.to_){
				var availment = t.from_+' - '+t.to_;
			}


			if(t.type){
				var type = t.type;
			}else if(t.leave_type){
				var type = t.leave_type;
			}
			var dt = { employee_id : t.employee_id, employee : t.employee, type : type, master_type : t.master_type, status : t.status, id : t.id, availment_date : availment };
			if(t.leave_type){
				dt.leave_type = t.leave_type;
			}

			if(t.sub){
				dt.sub = t.sub;
			}
			
			var res  = []
			//var res = t;
			
			$http({
				method : 'POST',
				url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
				data : { 0 : dt}
			})
			.then(function(res){

				res.header ? logger.logSuccess(res.header.message) : logger.logSuccess('Approval Success')  
			},function(res){
				// logger.logError(res.header.message)
				res.header ? logger.logError(res.header.message) : logger.logError('Approval Failed')  

			})
			// .success(function(data){
			// 	//logger.logError('failed to, please try again, or refresh the browser');
			// 	return data ;
			// 	// console.log(data.header.message)
			// 	logger.logSuccess(data);
			// 	// $scope.pinger =  data.ping;
			// 	// $scope.request = data.request;
			// 	// notifFactory.getNotif()
			// }).error(function(data) {
			// 	logger.logError('failed to, please try again, or refresh the browser');
			// });
			// .then(function(res){
			// 	console.log(res)
			// })
		},reject : function(data,t){
			if(t.availment_date){
				var availment = t.availment_date;
			}
			else if(t.schedule){
				var availment = t.schedule;
			}else if(t.from_ && t.to_){
				var availment = t.from_+' - '+t.to_;
			}

			if(t.type){
				var type = t.type;
			}else if(t.leave_type){
				var type = t.leave_type;
			}

			var dt = { employee_id : t.employee_id, employee : t.employee, type : type, master_type : t.master_type, status : t.status, id : t.id, availment_date : availment };
			if(t.leave_type){
				dt.leave_type = t.leave_type;
			}
			//return console.log(dt, "REEJECT BUTTON");
			$http({
				method : 'POST',
				url : ApiURL.url +'/api/attendance/schedule/reject_notif?key=' + $cookieStore.get('key_api'),
				data : { 0 : dt }
			})
			.then(function(res){
				res.header ? logger.logSuccess(res.header.message) : logger.logSuccess('Reject Success')  
			},function(res){
				// logger.logError(res.header.message)
				res.header ? logger.logError(res.header.message) : logger.logError('Reject Failed')  

			})
			// .success(function(data){
			// 	//console.log(data,'asdasd')
			// 	// notifFactory.getNotif();
			// 	// $scope.pinger =  data.ping;
			// 	// $scope.request = data.request;
			// 	console.log(data.header.message)

			// 	logger.logSuccess(data);
			// }).error(function(data) {
			// 	console.log(data)
			// 	//logger.logError(data.header.message);
			// });
			// .then(function(res){
			// 	console.log(res)
			// })
		},cancle : function(data,t){
			if(t.availment_date){
				var availment = t.availment_date;
			}
			else if(t.schedule){
				var availment = t.schedule;
			}else if(t.from_ && t.to_){
				var availment = t.from_+' - '+t.to_;
			}

			if(t.type){
				var type = t.type;
			}else if(t.leave_type){
				var type = t.leave_type;
			}

			var dt = { employee_id : t.employee_id, employee : t.employee, type : type, master_type : t.master_type, status : t.status, id : t.id, availment_date : availment };
			if(t.leave_type){
				dt.leave_type = t.leave_type;
			}
			$http({
				method : 'POST',
				url : ApiURL.url +'/api/attendance/schedule/cancle_notif?key=' + $cookieStore.get('key_api'),
				data : { 0 : dt }
			})
			.then(function(res){
				res.header ? logger.logSuccess(res.header.message) : logger.logSuccess('Cancel Success')  
			},function(res){
				// logger.logError(res.header.message)
				res.header ? logger.logError(res.header.message) : logger.logError('Cancel Failed')  

			})
			// .success(function(data){
			// 	// notifFactory.getNotif();
			// 	// $scope.pinger =  data.ping;
			// 	// $scope.request = data.request;
			// 	console.log(data.header.message)
			// 	logger.logSuccess(data);
			// }).error(function(data) {
			// 	//console.log(data)
			// 	logger.logError(data.header.message);
			// });
			// .then(function(res){
			// 	console.log(res)
			// })
		},chats : true,
		getmenus : function(){
			return self.menu;
		},
		setmenus : function(){
			//self.menu = $cookieStore.get('menu') || {};
			try{
				self.menu = window.localStorage.getItem('menu') || {};
				if(self.menu){
					self.menu = JSON.parse(self.menu);
				}
				self.menu.sub_cut_off = 200;
				self.menu.master_att = 200;
				if(self.menu){
						//sub CUT_OFF
						if(!self.menu.process_attendance_cut_off && !self.menu.process_attendance_cut_off_record){
							self.menu.sub_cut_off = false;
						}else{
							self.menu.sub_cut_off = 200;
						}
						// sub SCHEDULE REQUEST
						if(!self.menu.schedule_request_change_swap_shift && !self.menu.schedule_request_list && !self.menu.schedule_request_overtime_undertime && !self.menu.schedule_request_training_official_business){
							self.menu.sub_schedule_req = false;	
						}else{
							self.menu.sub_schedule_req = 200;	
						}
						// SUB SCHEDULE
						if(!self.menu.schedule_import && !self.menu.unapproved_schedule && !self.menu.attendance_schedule_list && !self.menu.manual_input_att && !self.menu.attendance_schedule_list){
							self.menu.sub_schedule = false
						}else{
							self.menu.sub_schedule = 200
						}
						// master attendance
						if(!self.menu.sub_schedule && !self.menu.sub_schedule_req && !self.menu.sub_cut_off){
							self.menu.master_att = false;
						}else{
							self.menu.master_att = 200;
						}
						// master leave
						if(!self.menu.leave_entitlements && !self.menu.leave_list&& !self.menu.leave_report && !self.menu.leave_request && !self.menu.leave_request_detail){
							self.menu.master_leave = false
						}else{
							self.menu.master_leave = 200
						}
						// master infraction
						if(!self.menu.infraction_list && !self.menu.infraction_add_infraction){
							self.menu.master_infraction = false;
						}else{
							self.menu.master_infraction = 200;
						}
						// master configure
						if(!self.menu.admin_user_management && !self.menu.admin_user_roles && !self.menu.admin_biometric_setup && !self.menu.admin_email_setting && !self.menu.admin_ldap_setting){
							self.menu.sub_user_manage = false;
						}else{
							self.menu.sub_user_manage = 200;
						}
						if(!self.menu.sub_user_manage){
							self.menu.master_config = false;
						}else{
							self.menu.master_config = 200 ;
						}

						// master setting
						if(!self.menu.admin_employment_status && !self.menu.admin_job && 
							!self.menu.admin_workshift && 
							!self.menu.admin_orginaztion_structure && 
							!self.menu.admin_skills && 
							!self.menu.admin_education && 
							!self.menu.admin_language && 
							!self.menu._termination_reason && 
							!self.menu._immigration_issuer && 
							!self.menu.admin_nationalities && 
							!self.menu.admin_cut_off_period && 
							!self.menu.admin_holiday_setup 
							){
							self.menu.master_setting = false;
					}else{
						self.menu.master_setting = 200;
					}

					if(!self.menu.emp_list && !self.menu.admin_general_information && !self.menu.admin_orginaztion_structure && !self.menu._csv_data_import ){
						self.menu.master_company = false;
					}else{
						self.menu.master_company = 200;
					}
				}else{
						//console.log(self.menu)
					}
			}catch(e){}

			},
			setbutton:function(a,b){
				var stat = b.status.toLowerCase();
				var emp_login = $cookieStore.get('NotUser');
				switch(a){
					case 'approve':
					try{
						if(b.action.approve){ return true; }
						else{ return false; }
					}catch(e){
						console.log(e,"ERROR ACTION");
						if(b.status_id==1 && stat=='pending approval'){
							if(emp_login == b.employee_id){
								return false;
							}else{
								return true;
							}
						}else{
							return false;
						}
					}
					break;
					case 'cancel':
					try{
						if(b.action.cancel){ return true; }
						else{ return false; }
					}catch(e){
						console.log(e,"------- ERROR CANCEL");
						if(b.status_id==1 && stat=='pending approval'){
							if(emp_login != b.employee_id){
								return false;
							}else{
								return true;
							}
						}else{
							return false;
						}	
					}

					break;
					case 'reject':
					try{
						if(b.action.reject){ return true; }
						else{ return false; }
					}catch(e){
						console.log(e,"------- ERROR REJECT");
						if(b.status_id == 1 && stat=='pending approval'){
			 				//console.log("reject",emp_login == b.employee_id,emp_login, b.employee_id);
			 				if(emp_login == b.employee_id){
			 					return false;
			 				}else{
			 					return true;
			 				}
			 			}else{
			 				return false;
			 			}
			 			break;
			 		}
			 	}
			 	
			/*this.role = b;
			switch(a){
				case 'approve':
					if(this.role == 'HRD' || this.role == 'Supervisor' || this.role == 'SUPERUSER'){return true}
				break;
				case 'reject':
					if(this.role == 'HRD' || this.role == 'Supervisor' || this.role == 'SUPERUSER'){return true}
				break;
				case 'cancel':
					if(this.role == 'User'){return true}
				break;
		}*/
	},
	
}

});


appCtrl.controller("HeaderCtrl",function(notifFactory,GI,GetName,$timeout,ApiURL,$location,$cookies,$cookieStore,$http,$rootScope,$scope,logger,$interval,$routeParams,$rootScope,$window){
	
	var self = this;
		    //  $timeout.cancel(notifFactory.getNotif());

		    self.handler = notifFactory;
		    self.handling  =   GetName;
		    self.handler.counting_pending = 0;


		    self.handler.read_request  = self.readX

    		// $scope$interval(function(){
    		// 	notifFactory.getNotif();
    		
    		// 	notifFactory.read_notif();

    		// },3000);
    		

			//console.log(notifFactory)
			

		// if($cookieStore.get('key_api')){
		// 	notifFactory.getNotif()
		// }


		/** get user name login **/
		$scope.name = GetName;
		//$scope.getPing();
		/*$http({
			method : 'GET',
			url : ApiURL.url +'/api/attendance/schedule/count_notif?key=' + $cookieStore.get('key_api')
		}).success(function(data){
			$scope.pinger =  data.ping;
			$scope.request = data.request;
		}).error(function(data) {
			console.log(data)
			logger.logError(data.header.message);
		});*/

		/** get logo **/
		$scope.logo = GI;
		
		/** function logout **/	
		$scope.checkDisabled  =  function(){
			
			if($cookieStore.get('clickable') != undefined){
				var id =  $cookieStore.get('clickable');
				var res = self.handler.readX;

				for(var prop  in  res){
					if(res[prop].id ==  id ){
						return  false;
						if(res[prop].status_id != 1 && (res[prop].status_request == 'approved' || res[prop].status_request == 'rejected') ){
							//console.log('checkdisabled' , false);
							return  true;
						}else{
							//console.log('chakcdisabled', true);
							return  false;
						}
					}
				}	
			}

			//console.log('checkDisabled');

			
		};

		$scope.hide_notification =  function(index){
			hide_checked.push(self.handler.readX[index])
			//console.log(self.handler.readX[index])

			//console.log(hide_checked)

		}



/*		$scope.approve = function(data,t){
			return console.log("data,t", data,t[0]);
			var res  = []
			var res = t;
			
			$http({
			method : 'POST',
			url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
			data : { 0 : t}
			}).success(function(data){
				//logger.logError('failed to, please try again, or refresh the browser');
				 return data ;
				 logger.logSuccess(data);
				// $scope.pinger =  data.ping;
				// $scope.request = data.request;
				// notifFactory.getNotif()
			}).error(function(data) {
				logger.logError('failed to, please try again, or refresh the browser');
			});
		},
		$scope.reject = function(data){
			console.log(self.handler.readX[data])
			$http({
			method : 'POST',
			url : ApiURL.url +'/api/attendance/schedule/reject_notif?key=' + $cookieStore.get('key_api'),
			data : self.handler.readX[data]
			}).success(function(data){
				//console.log(data,'asdasd')
				// notifFactory.getNotif();
				// $scope.pinger =  data.ping;
				// $scope.request = data.request;
				 
				 logger.logSuccess(data);
			}).error(function(data) {
				console.log(data)
				//logger.logError(data.header.message);
			});
		};
		
		$scope.cancle = function(data){
			console.log(self.readX)
			$http({
			method : 'POST',
			url : ApiURL.url +'/api/attendance/schedule/cancle_notif?key=' + $cookieStore.get('key_api'),
			data : self.handler.readX[data]
			}).success(function(data){
				// notifFactory.getNotif();
				// $scope.pinger =  data.ping;
				// $scope.request = data.request;
				slogger.logSuccess(data);
			}).error(function(data) {
				console.log(data)
				//logger.logError(data.header.message);
			});
		};*/

		// BARU
		/*$scope.logOut = function(){
			$http.get(ApiURL.url + '/api/login/del-auth?key=' + $cookieStore.get('key_api')).success(function(data){	
				$cookieStore.remove('key_api');
				$cookieStore.remove('uname');
				$cookieStore.remove('iduserlogin');
				$cookieStore.remove('images');
				$cookieStore.remove('UserAccess');
				$cookieStore.remove('userChangeShift');
				// $cookies.removeAll();
				angular.forEach($cookies, function (v, k) {
					$cookieStore.remove(k);
				});
				$location.path('pages/signin');
				logger.logSuccess('Logout Successfully');
			}).error(function(data) {
				$cookieStore.remove('key_api');
				$cookieStore.remove('uname');
				$cookieStore.remove('iduserlogin');
				$cookieStore.remove('images');
				$cookieStore.remove('UserAccess');
				$cookieStore.remove('userChangeShift');
				$cookieStore.removeAll();
				$location.path('views/pages/404.html');	
				logger.logError(data.header.message);
			});	
		};*/

		// LAMA
		$scope.logOut = function(){
			$http.get(ApiURL.url + '/api/login/del-auth?key=' + $cookieStore.get('key_api')).success(function(data){	
				$cookieStore.remove('key_api');
				$cookieStore.remove('uname');
				$cookieStore.remove('iduserlogin');
				$cookieStore.remove('images');
				$cookieStore.remove('UserAccess');
				$cookieStore.remove('regular');
				$cookieStore.remove('user_depart');
				window.localStorage.removeItem('menu');
				angular.forEach($cookies, function (v, k) {
					$cookieStore.remove(k);
				});
				$location.path('pages/signin');
				logger.logSuccess('Logout Successfully');
			}).error(function(data) {
				$cookieStore.remove('key_api');
				$cookieStore.remove('uname');
				$cookieStore.remove('iduserlogin');
				$cookieStore.remove('images');
				$cookieStore.remove('UserAccess');
				$cookieStore.remove('regular');
				$cookieStore.remove('user_depart');
				$cookieStore.remove('menu');
				window.localStorage.removeItem('menu');
				$location.path('views/pages/404.html');	
				logger.logError(data.header.message);
			});	
		};

		$scope.onClick =  function(a){
			
		}

		$scope.unread =  function(data,b){
			if(data == 'read'){
				var data_exploit = {data : self.handler.form.request[b], whoiam : $cookieStore.get('NotUser') }
				//console.log(data_exploit);
				$http({
					method : 'POST',
					url : ApiURL.url +'/api/attendance/schedule/read_notif?key=' + $cookieStore.get('key_api'),
					data : data_exploit
				}).success(function(data){
				//	console.log(data)
					//notifFactory.getNotif()
					// notifFactory.getNotif();
					// $scope.pinger =  data.ping;
					// $scope.request = data.request;
				}).error(function(data) {
				//	console.log(data)
					//logger.logError(data.header.message);
				});

			}
		}

		$scope.toggled = function(open) {
			if (open) {
				console.log('is open');
			} else console.log('close');
		}

		// $scope.test  =  function(data){
		// 	console.log(test);
		// }

		// $scope.approve =  function(result){
		// 	console.log(result)
		// 	$http({
		// 	method : 'POST',
		// 	url : ApiURL.url +'/api/attendance/schedule/approve_notif?key=' + $cookieStore.get('key_api'),
		// 	data : result
		// 	}).success(function(data){
		// 		//logger.logError('failed to, please try again, or refresh the browser');
		// 		 return data ;
		// 		 logger.logSuccess(data);
		// 		// $scope.pinger =  data.ping;
		// 		// $scope.request = data.request;
		// 		// notifFactory.getNotif()
		// 	}).error(function(data) {
		// 		logger.logError('failed to, please try again, or refresh the browser');
		// 	});
		// }

		// $scope.reject =  function(result){
		// 	$http({
		// 	method : 'POST',
		// 	url : ApiURL.url +'/api/attendance/schedule/reject_notif?key=' + $cookieStore.get('key_api'),
		// 	data : result
		// 	}).success(function(data){
		// 		//console.log(data,'asdasd')
		// 		// notifFactory.getNotif();
		// 		// $scope.pinger =  data.ping;
		// 		// $scope.request = data.request;
		// 		 return data ;
		// 		 logger.logSuccess(data);
		// 	}).error(function(data) {
		// 		console.log(data)
		// 		//logger.logError(data.header.message);
		// 	});
		// }
		// $scope.cancle =  function(result){
		// 	$http({
		// 	method : 'POST',
		// 	url : ApiURL.url +'/api/attendance/schedule/cancle_notif?key=' + $cookieStore.get('key_api'),
		// 	data : result
		// 	}).success(function(data){
		// 		//console.log(data,'asdasd')
		// 		// notifFactory.getNotif();
		// 		// $scope.pinger =  data.ping;
		// 		// $scope.request = data.request;
		// 		 return data ;
		// 		 logger.logSuccess(data);
		// 	}).error(function(data) {
		// 		console.log(data)
		// 		//logger.logError(data.header.message);
		// 	});
		// }

		
		
	});




appCtrl.controller("AppCtrl", ["$scope","$rootScope","$routeParams","ApiURL","$cookieStore","$http","logger","$location", function($scope,$rootScope,$routeParams,api,$cookieStore,$http,logger,$location) {

	

	$scope.store = {
		first_name : String($routeParams.id).split(';')[0],
	};

	if(String($routeParams.id).split(';')[0]){
		$scope.id =$routeParams.id;
	}else{
		$scope.id = String($routeParams.id).split(';')[0];
	}


	var $window;
	$window = $(window);
	$scope.main = {
		brand: "HRMS",
						//logo:"images/leekie_logo.jpg",
						user:"Sign in"
						//name:$cookieStore.get('uname')
					};




					$scope.color = {
						primary: "#248AAF",
						success: "#3CBC8D",
						info: "#29B7D3",
						infoAlt: "#666699",
						warning: "#FAC552",
						danger: "#E9422E"
					};

	//console.log($cookieStore.get('uname'));
	
	/*$scope.$watch('$routeParams.id', function() {
		console.log($routeParams.id);
	});*/
	//var n = String($routeParams.id).split(';');
	//console.log(n);
	
	// split 0 for get id
	 //console.log(String($routeParams.id).split(';')[0]);

	//$scope.id = String($routeParams.id).split(';')[0] ? $routeParams.id : String($routeParams.id).split(';')[0];
	//console.log(n);


}]);


appCtrl.directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
}]);


appCtrl.service('fileUpload', ['$http','logger', function ($http,logger) {
	this.uploadFileToUrl = function(file, uploadUrl){
		var fd = new FormData();
		fd.append('file', file);
		$http.post(uploadUrl, fd, {
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		})
		.success(function(data,status,headers,config){
			logger.logSuccess(data.header.message);
		})
		.error(function(data,status,headers,config){
			logger.logError(data.header.message);
		});
	}
}]);

appCtrl.controller("NavCtrl", ["$scope", "taskStorage", "filterFilter","$cookieStore","notifFactory", function($scope, taskStorage, filterFilter,$cookieStore,notifFactory) {


	

	var access = $cookieStore.get('access');
	$scope.reguler = $cookieStore.get('regular');
	self.handler = notifFactory;
	$scope.menu = true;
	setInterval(function(){
		self.handler.setmenus();
		$scope.menu = self.handler.getmenus() || null;
		if($scope.menu){
			($scope.menu.master_att,"GETMENUS###############",self.handler.menu.master_att)
		}else{
			($scope.menu.master_att,"GETMENUS###############------",self.handler.menu.master_att)
		}
	},1000);
	
	// console.log(self.handler.menu,'====================================TEST')

	// if($scope.reguler){	
	// 	if($scope.menu){
	// 		//sub CUT_OFF
	// 		if(!$scope.menu.process_attendance_cut_off && !$scope.menu.process_attendance_cut_off_record){
	// 			$scope.menu.sub_cut_off = false;
	// 		}
	// 		// sub SCHEDULE REQUEST
	// 		if(!$scope.menu.schedule_request_change_swap_shift && !$scope.menu.schedule_request_list && !$scope.menu.schedule_request_overtime_undertime && !$scope.menu.schedule_request_training_official_business){
	// 			$scope.menu.sub_schedule_req = false;	
	// 		}
	// 		// SUB SCHEDULE
	// 		if(!$scope.menu.schedule_import && !$scope.menu.unapproved_schedule && !$scope.menu.attendance_schedule_list && !$scope.menu.manual_input_att && !$scope.menu.attendance_schedule_list){
	// 			$scope.menu.sub_schedule = false
	// 		}
	// 		// master attendance
	// 		if(!$scope.menu.sub_schedule && !$scope.menu.sub_schedule_req && !$scope.menu.sub_cut_off){
	// 			$scope.menu.master_att = false;
	// 		}
	// 		// master leave
	// 		if(!$scope.menu.leave_entitlements && !$scope.menu.leave_list&& !$scope.menu.leave_report && !$scope.menu.leave_request && !$scope.menu.leave_request_detail){
	// 			$scope.menu.master_leave = false
	// 		}
	// 		// master infraction
	// 		if(!$scope.menu.infraction_list && !$scope.menu.infraction_add_infraction){
	// 			$scope.menu.master_infraction = false;
	// 		}
	// 		// master configure
	// 		if(!$scope.menu.admin_user_management && !$scope.menu.admin_user_roles && !$scope.menu.admin_biometric_setup && !$scope.menu.admin_email_setting && !$scope.menu.admin_ldap_setting){
	// 			$scope.menu.sub_user_manage = false;
	// 		}
	// 		if(!$scope.menu.sub_user_manage){
	// 			$scope.menu.master_config = false;
	// 		}

	// 		// master setting
	// 		// if(!$scope.menu.admin_employment_status && !$scope.menu.admin_job && 
	// 		// 	!$scope.menu.admin_workshift && 
	// 		// 	!$scope.menu.admin_orginaztion_structure && 
	// 		// 	!$scope.menu.admin_skills && 
	// 		// 	!$scope.menu.admin_education && 
	// 		// 	!$scope.menu.admin_language && 
	// 		// 	!$scope.menu.201_termination_reason && 
	// 		// 	!$scope.menu.201_immigration_issuer && 
	// 		// 	!$scope.menu.admin_nationalities && 
	// 		// 	!$scope.menu.admin_cut_off_period && 
	// 		// 	!$scope.menu.admin_holiday_setup 
	// 		// 	){
	// 		// 	$scope.menu.master_setting = false;
	// 		// }

	// 		// if(!$scope.menu.emp_list && !$scope.menu.admin_general_information && !$scope.menu.admin_orginaztion_structure && !$scope.menu.201_csv_data_import ){
	// 		// 	$scope.menu.company = false;
	// 		// }

	// 	}else{
	// 		console.log($scope.menu)
	// 	}
	// }

	if($scope.reguler == 200){
		$scope.reguler = true;
	}else{
		$scope.reguler = false;
	}
	$scope.$watch('access', function() {

		if(access === 'user'){
			$scope.access = false;
		}else{
			$scope.access = true;
		}

	});
	
	var tasks;
	return tasks = $scope.tasks = taskStorage.get(), 
	$scope.taskRemainingCount = filterFilter(tasks, { completed: !1 }).length, 
	$scope.$on("taskRemaining:changed", function(event, count) { return $scope.taskRemainingCount = count })







}]);

appCtrl.controller("CalendarCtrl",function($scope,$compile,uiCalendarConfig) {
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	$scope.changeTo = 'Hungarian';
	/* event source that pulls from google.com */
	$scope.eventSource = {
		url: "",
					className: 'gcal-event',           // an option!
					currentTimezone: 'America/Chicago' // an option!
				};
				/* event source that contains custom events on the scope */
				$scope.events = [
				{title: 'Event 1',start: new Date(y, m, 1)},
				{title: 'Event 2',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
				{id: 999,title: 'Event 3',start: new Date(y, m, d - 3, 16, 0),allDay: false},
				{id: 999,title: 'Event 4',start: new Date(y, m, d + 4, 16, 0),allDay: false},
				{title: 'Event 5',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
				{title: 'Event 6',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
				];
				/* event source that calls a function on every view switch */
				$scope.eventsF = function (start, end, timezone, callback) {
					var s = new Date(start).getTime() / 1000;
					var e = new Date(end).getTime() / 1000;
					var m = new Date(start).getMonth();
					var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
					callback(events);
				};

				$scope.calEventsExt = {
					color: '#f00',
					textColor: 'yellow',
					events: [ 
					{type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
					{type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
					{type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
					]
				};
				/* alert on eventClick */
				$scope.alertOnEventClick = function( date, jsEvent, view){
					$scope.alertMessage = (date.title + ' was clicked ');
				};
				/* alert on Drop */
				$scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
					$scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
				};
				/* alert on Resize */
				$scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
					$scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
				};
				/* add and removes an event source of choice */
				$scope.addRemoveEventSource = function(sources,source) {
					var canAdd = 0;
					angular.forEach(sources,function(value, key){
						if(sources[key] === source){
							sources.splice(key,1);
							canAdd = 1;
						}
					});
					if(canAdd === 0){
						sources.push(source);
					}
				};
				/* add custom event*/
				$scope.addEvent = function() {
					$scope.events.push({
						title: 'Open Sesame',
						start: new Date(y, m, 28),
						end: new Date(y, m, 29),
						className: ['openSesame']
					});
				};
				/* remove event */
				$scope.remove = function(index) {
					$scope.events.splice(index,1);
				};
				/* Change View */
				$scope.changeView = function(view,calendar) {
					uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
				};
				/* Change View */
				$scope.renderCalender = function(calendar) {
					if(uiCalendarConfig.calendars[calendar]){
						uiCalendarConfig.calendars[calendar].fullCalendar('render');
					}
				};
				/* Render Tooltip */
				$scope.eventRender = function( event, element, view ) { 
					element.attr({'tooltip': event.title,
						'tooltip-append-to-body': true});
					$compile(element)($scope);
				};
				/* config object */
				$scope.uiConfig = {
					calendar:{
						height: 450,
						editable: true,
						header:{
							left: 'title',
							center: '',
							right: 'today prev,next'
						},
						eventClick: $scope.alertOnEventClick,
						eventDrop: $scope.alertOnDrop,
						eventResize: $scope.alertOnResize,
						eventRender: $scope.eventRender
					}
				};

				$scope.changeLang = function() {
					if($scope.changeTo === 'Hungarian'){
						$scope.uiConfig.calendar.dayNames = ["Vas�rnap", "H�tfo", "Kedd", "Szerda", "Cs�t�rt�k", "P�ntek", "Szombat"];
						$scope.uiConfig.calendar.dayNamesShort = ["Vas", "H�t", "Kedd", "Sze", "Cs�t", "P�n", "Szo"];
						$scope.changeTo= 'English';
					} else {
						$scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
						$scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
						$scope.changeTo = 'Hungarian';
					}
				};
				/* event sources array*/
				$scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
				$scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];
			})


appCtrl.controller("DatepickerDemoCtrl", ["$scope","$filter", function($scope,$filter) {

	$scope.today = function() {
		$scope.dt = new Date
		var t = $filter('date')($scope.dt, 'yyyy/MM/dd')
		console.log(t);
		
	}
	
	$scope.today()
	$scope.showWeeks = !0
	$scope.toggleWeeks = function() {
		$scope.showWeeks = !$scope.showWeeks
	}
	
	$scope.clear = function() {
		$scope.dt = null
	}
	
	$scope.disabled = function(date, mode) {
		return "day" === mode && (0 === date.getDay() || 6 === date.getDay())
	}
	
	$scope.toggleMin = function() {
		var _ref;
		return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
			"null": new Date
		}
	}
	
	$scope.toggleMin()
	$scope.open = function($event) {
		$event.preventDefault()
		$event.stopPropagation()
		$scope.opened = !0
	}
	
	$scope.dateOptions = {
		"year-format": "'yy'",
		"starting-day": 1
	}
	
	$scope.formats = ["dd-MMMM-yyyy", "yyyy/MM/dd", "shortDate"]
	$scope.format = $scope.formats[1]
}]);		

//appCtrl.controller("DatepickerDemoCtrl", ["$scope","$filter", function($scope,$filter) {



	appCtrl.run(function(notifFactory){
		notifFactory.setCommonHeader();
		setInterval(function(){
			notifFactory.getNotif();
			notifFactory.deleteCookie();
			notifFactory.read_notif();
			
			// notifFactory.refreshMessage();
		//	notifFactory.setmenus();
		//	console.log(notifFactory.getmenus())
	},90000000);
	});