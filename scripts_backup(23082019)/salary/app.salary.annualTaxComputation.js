(function(){
	
	angular.module("app.salary.annualTaxComputation",['ngFileUpload'])
	
	.factory('annualTaxCompFactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,Upload){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var NAME = '_schedule';
		var s = localStorage.getItem(NAME);
		var items = s ? JSON.parse(s) : {};
		var self;
		return self = {
			getID(){
				const onlyUser = $cookieStore.get('UserAccess')
				const notUser = $cookieStore.get('NotUser')
				if (notUser == null) {
					return(onlyUser.id)
				}else{
					return(notUser)
				}

			},
			setheaders: function(){
				this.headers = new createheaders(
					['Employee Name','emp_name'],
					['Department','department'],
					['Total Taxable Basic','total_tax'],
					['Total SSS,Philhealth,HDMF','total_deduction'],
					['Tax Exemption','tax_exem'],
					['Net Taxable Salary','net_tax'],
					['Taxable Bonus','taxable_bonus'],
					['Taxable SIL','taxable_sil'],
					['Total Taxable Salary','total_taxable_salary'],
					['Total Annual Tax','total_annual_tax'],
					['Withholding Tax','withholding_tax'],
					['Tax Still Payable','tax_still'],

					

					);
				this.setactiveheader(this.headers[0],false)	
			},adddata: function(a){
				this.maindata.push(a);
			},getaccess:function(){
				$http({
					method : 'GET',
					// url : ApiURL.url + '/payroll/adjustment_payroll'
					url : ApiURL.url + '/payroll/year_end_bonus_13th'
					
				}).then(function(res){
					console.log(res)
					if (res.status !==200 ) {
						logger.logError("Get Data Failed")
					}else{
						logger.logSuccess("Get Data Success")

						self.setdataDepartment(res.data.data[0].department)
						self.setdataJobs(res.data.data[0].jobs)
						self.setdataPeriod(res.data.data[0].period)
					}


				});
			},setdataDepartment:function(x){
				this.maindataDepartment = x
			},
			setdataJobs:function(x){
				this.maindataJobs = x
			},
			setdataPeriod:function(x){
				this.maindataPeriod = x
			},
			getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},setform:function(data){
				if(data.status == 'user'){
					this.role = data.status;
					this.iduser = data.employee_id;
					this.nameuser = data.employee_name;
					self.form={};
					self.form.name = this.nameuser;
					self.hide = true;
				}else{
					this.role = data.status;
					self.form={};
					self.form.name = '';
					self.hide = false;
				}
				self.datastatus = data.status;
			},onSelect : function ($item,$model,$label){
				self.model = $item.name;
				self.label = $item.employee_id;

			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},getYear : function(res){
				var results = res.data.data;
				var getdata = results.length;
				var arr = [];
				var monthArr = [];
				if(getdata > 0){
					var i = 0;
					for(; i < getdata; i++){
						var sub = res.data.data[i].date.substr(0,7);
						var month = res.data.data[i].date.substr(5,2);
						var day = res.data.data[i].date.substr(8,2);

						var subday = day.substr(0,1);
						if(subday == 0){
							day = day.substr(1,1);
						}
						arr[sub] = day;
					}

					var getFinally = [];
					for(key in arr){
						getFinally.push(key+'-'+arr[key]);
					}

					var countFinally = getFinally.length;

					var etc = [{"min" :  getFinally[0]},{"max" :  getFinally[countFinally-1]}];
					return etc;

				}
			},dateEnd : function(data){
				//console.log(data);
				var dates = data.toString();
				var arr = [];
				var arr = dates.split(" ");
				if(arr[1] ==  "Jan"){ arr[1] = 1;}
				if(arr[1] ==  "Feb"){ arr[1] = 2;}
				if(arr[1] ==  "Mar"){ arr[1] = 3;}
				if(arr[1] ==  "Apr"){ arr[1] = 4;}
				if(arr[1] ==  "May"){ arr[1] = 5;}
				if(arr[1] ==  "Jun"){ arr[1] = 6;}
				if(arr[1] ==  "Jul"){ arr[1] = 7;}
				if(arr[1] ==  "Aug"){ arr[1] = 8;}
				if(arr[1] ==  "Sep"){ arr[1] = 9;}
				if(arr[1] ==  "Oct"){ arr[1] = 10;}
				if(arr[1] ==  "Nov"){ arr[1] = 11;}
				if(arr[1] ==  "Dec"){ arr[1] = 12;}
				
				var newdate = arr[3]+'-'+arr[1]+'-'+arr[2];
				//console.log(newdate);
				var result = self.getdateEnd
				//console.log(result);
				var count = result.length;
				var endSearch = [];
				if(count > 0){
					for(var i = 0; i < count; i++){
						
						if(result[i].date ==  newdate){
							endSearch =  result[i];
						}
					}
				}
				if(endSearch == null){
					res.data.header ? logger.logError("data not found") : logger.logError("Data not found")
				}else{
					self.setdata([endSearch]);
				}
				
			},next_paging : function(data,f){
				console.log("f", f);
				console.log("data", data);
				console.log("self.paging", self.paging);
				
				
				if(data > 2 && f  ==  'end' ){
					self.paging[0] = '...'
					self.paging[1] = self.default_paginate[data];

					if(self.default_paginate[data+1] >  self.default_paginate[data]){
						self.paging[2] =  self.default_paginate[data+1] 
					}else{
						delete self.paging[2];
					}
					
					self.paging[3] = '...'
				}else if(data ==  1 && f  == "end"){

				}else{

				}


			},
			setToday : function(datas,type){
				var counts = 1;
				var page = 1;
				var dt = this.maindata;
				console.log("setToday",datas,dt[0].date);
				for(var i in dt){
					if(type=='date'){
						if(dt[i].date == datas){
							//console.log("PAGING",i);
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;
						}
					}else if(type=="month"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getMonth()+1;
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}else if(type=="year"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getFullYear();
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}
				}
				self.Selectdate 	= datas;
				self.Selectdate_end = dt[dt.length-1].date;
				self.Selectdate_start = dt[0].date;
				
				if(page==0){ page++; }
				console.log(page,"PAGE");
				self.selectedPage 	= page;
				self.currentPage 	= self.selectedPage;
				//self.currentPage = 25;
			},
			clickable : 'pointer',
			action_save : false,
			select_month : function(month_date){
				self.action_save = month_date;
				return self.save();
			},
			select_monthnew : function(month_date){
				self.action_save = month_date;
				console.log(month_date,'+++++++++++++++ select_monthnew +++++++++++++++++++')
				return self.saveNew();
			},

			cutoff_annual:function(){

				x = this.maindata
				for (var i = 0; i < x.length; i++) {



					if (x[i].taxable_sil==null || x[i].taxable_sil==undefined) {
						x[i].taxable_sil = 0
					}
					if (x[i].total_taxable_salary==null || x[i].total_taxable_salary==undefined) {
						x[i].total_taxable_salary = 0
					}
					if (x[i].total_annual_tax==null || x[i].total_annual_tax==undefined) {
						x[i].total_annual_tax = 0
					}
					if (x[i].withholding_tax==null || x[i].withholding_tax==undefined) {
						x[i].withholding_tax = 0
					}
					if (x[i].tax_still_playable==null || x[i].tax_still_playable==undefined) {
						x[i].tax_still_playable = 0
					}


					newSchema={
						
							employee_id: x[i].employee_id,
							department_id:x[i].department_id,
							total_taxable_basic:x[i].total_taxable_basic,
							total_sss_phic_hdmf:x[i].total_sss_phic_hdmf,
							tax_exemption:x[i].tax_exemption,
							net_taxable_salary:x[i].net_taxable_salary,
							tax_bonus:x[i].tax_bonus,
							taxable_sil:x[i].taxable_sil,
							total_taxable_salary:x[i].total_taxable_salary,
							total_annual_tax:x[i].total_annual_tax,
							withholding_tax:x[i].withholding_tax,
							tax_still_playable:x[i].tax_still_playable

						
					}
					$http({
						method: 'POST',
						url : ApiURL.url + '/payroll/annual_tax_computation/cutoff',
						data : newSchema
					}).success(function(res){
						logger.logSuccess("Saving Data Success");

					}).error(function(res){
						logger.logError("Saving Data Failed");


					})
				}
			},
			save : function(){

				
				// if (self.form.department == undefined || self.form.department == null) {
				// 	self.form.department = 0
				// }
				// if (self.form.cutoff_date == undefined || self.form.cutoff_date == null) {
				// 	self.form.cutoff_date = 0
				// }
				// if (self.form.job == undefined || self.form.job == null) {
				// 	self.form.job = 0
				// }

				// self.form.employee_id = self.label
				
				sample = {year : 2019}
				

				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/annual_tax_computation/get',
					headers : {
						'Content-Type' : 'application/json'
					},
					data: sample
				}).then(function(res){
					console.log(res)
					
					const status = res.status

					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						// self.form.employee_id = self.model
						self.setdata(res.data.data)
						logger.logSuccess("View Data Success");
					}
				})
				
			},restore : function(){
				self.setdata(items);
			},setdata: function(a){
				console.log(a)
				this.maindata = a;
				// console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},
			
			getActive : function(y){
				if(self.maindata){	
					while (true){
						if( y == 1 || y == 2 || y == 3){
							break;
						}
						y = y - 3;
					}
					return y;
				}
			},getdateRequest : function(form){
				var a = self.maindata;
				var b = form;
				var c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeIn = d.timeIn;
						b.timeOut = d.timeOut;
						b.schedule = d.schedule;
						break;
					}else{
						var d = '-';
						form.timeIn = d;
						form.timeOut = d;
						form.schedule = d;
					}
				}
			},getlateRequest : function(form){
				var a = self.maindata;
				var b = form;
				console.log(b)
				var c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeIn = d.timeIn;
						b.Late = d.Late;
						b.schedule = d.schedule;
						break;
					}else{
						var d = '-';
						form.timeIn = d;
						form.Late = d;
						form.schedule = d;
					}
				}
			},getearlyRequest : function(form){
				var a = self.maindata;
				var b = form;
				console.log(b)
				var c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeOut = d.timeOut;
						b.EarlyOut = d.EarlyOut;
						b.schedule = d.schedule;
						break;
					}else{
						var d = '-';
						form.timeOut = d;
						form.EarlyOut = d;
						form.schedule = d;
					}
				}
			},setdisabled:function(a){
				if(a.timeIn=='-' || a.timeIn=='00:00'){
					self.formdisabled = false
				}else{
					self.formdisabled = true
				}
			},




		}
	})


.factory('superCache', ['$cacheFactory', function($cacheFactory) {
	return $cacheFactory('super-cache');
}])

/** MAIN CONTROLLER **/  
.controller('annualTaxCompCtrl',function(annualTaxCompFactory,ApiURL,$cookieStore,$scope,$http,$filter,GetName,superCache){
	var self = this;
	self.handler = annualTaxCompFactory;
	self.handler.getaccess()
	self.handler.setdata([])

})






.run(function(annualTaxCompFactory){
	annualTaxCompFactory.setheaders();
})

}())


