(function(){

	angular.module('app.salary.deduction_personal',[])
	
	.factory('deductionPersonalFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,$routeParams,$window,Upload){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			reverse : function(){
				// console.log("1");
				if(self.hdmfhide == false){
					self.hdmfhide = true
				}
				if(self.hmohide == false){
					self.hmohide = true
				}
				if(self.ssshide == false){
					self.ssshide = true
				}

			},
			/** table salary history **/
			setheaders: function(){
				this.headers = new createheaders(
					['Information','',null],
					['SSS Loan','',null],
					['HDMF Loan','',null],
					['HMO','',null],
					['Cash Advance','',null],



					);
				this.setactiveheader(this.headers[0],false)
			},
			
			getdatasaalary:function(){
				self.editButton=true 
				$http({
					method : 'GET',
					url : ApiURL.url +  '/payroll/deduction_loan/get/' + $cookieStore.get('NotUser')
					
				})

				.then(function(res){

					// console.log(res)
					const status = res.status

					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{
						// self.setdataSSS(res.data.data)

						if(res.data.data == null || res.data.data == undefined){
							res.data.data = []
						}
						self.setdata(res.data.data)
						logger.logSuccess("View Data Success");
					}
					
				},function(res){
					logger.logError("Show Data Salary Failed")
				});
			},

			dataDefault:function(data){
				console.log(data)
				if (data) {

					self.form = data
					const x = self.form.effective_date.split("T")
					const xx = x[0].split("-")

					self.form.effective_date = new Date(Number(xx[0]), Number(xx[1]-1), Number(xx[2]) )

					if(self.form==null){
						self.form={};
					}
				}
			},
			getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},hiden:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}
							break;
					}
				}
			},save:function(datas){


				// $http({
				// 	method : 'POST',
				// 	url: 'http://hrmsapi2uat.leekie.com:8080/payroll/payment_details/insert/', 
				// 	data : datas

				// }).then(function(res){
					console.log(res)
				// 	if (status !== 200) {
				// 		logger.logError("Access Unauthorized");
				// 	}else{
				// 		self.setdata(res.data.data)
				// 		// self.dataDefault(res.data.data[0])
				// 		logger.logSuccess("View Salary History Success");
				// 	}
				// 	self.getdatasaalary()
				// })

			},setfalsebutton:function(){
				self.button=false;
				self.hdmf=false;
				self.sss=false;
				self.hmo=false;	
			},
			adddata: function(a){
				this.maindata.push(a);
			},

			setdataSSS: function(a){
				this.maindataSSS = a
			},
			setdata: function(a){

				console.log(a.length)  
				for (var i = 0; i < a.length; i++) {
					const startDeduc = a[i].start_deduction
					const startDeducSplit = startDeduc.split("T")
					a[i].start_deduction = startDeducSplit[0]

					//////////////////////////////////////////
					const loan_dateSplit = a[i].loan_date
					const loan_dateSplitt = loan_dateSplit.split("T")
					a[i].loan_date = loan_dateSplitt[0]

					//////////////////////////////////////////

				}

				

				// console.log(a)
				this.maindataDeduction = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},deldatahistory: function(id){
				$http({
					// url : ApiURL.url + '/api/employee-details/manage-salary/emp/' + id + '?key=' + $cookieStore.get('key_api'),
					url: 'http://hrmsapi2uat.leekie.com:8080/payroll/payment_details_history/delete/' + id,
					method: 'GET'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.deductionId != id[i];
						})
					}
					self.check = [];
					self.getdatatable()
					logger.logSuccess('Deleting Salary History Success');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Deleting   Salary History Failed")
				})
			},
			buttonCtrl:function(input){
				// console.log(input)
				if(input=='edit'){
					if ((self.saveButton=true)&&(self.cancelButton=true)) {
						return ((self.editButton=false))
					}
					
				}
				if (input=='save' || input=='cancel') {
					if (self.editButton=true) {
						return ((self.saveButton=false)||(self.cancelButton=false) )
					}
					
					
				}
			},

			modals:{
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodalDeduction',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditDeduction',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}	
				}
			},
			saveDeduction(datas){
				// datas.employee = $cookieStore.get('NotUser')
				// console.log(datas,"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
				$http({
					method: 'POST',
					url: ApiURL.url + '/payroll/deduction_loan/insert',
					data:datas
				}).then(function(res){
					// console.log(res)
					const status = res.status
					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						logger.logSuccess("Adding Deduction Success");

					}

				});
			},
			openmodal:function(a,b){
				// console.log(b)
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						data.employee = $cookieStore.get('NotUser')
						// console.log(data,"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

						$http({
							method: 'POST',
							url: ApiURL.url + '/payroll/deduction_loan/insert',
							data:data,
							headers : {
								'Content-Type' : 'application/json'
							}
						}).then(function(res){
							// console.log(res)
							const status = res.status
							if (status !== 200) {
								logger.logError("Access Unauthorized");
							}else{

								logger.logSuccess("Adding Deduction Success");
								self.getdatasaalary()
							}

						});
						break;
						case 'edit':
						// console.log(data)

						datas = {
							id 			: data.b.id,
							type_loan : data.b.type_loan,
							employee : data.b.employee,
							loan_date : data.b.loan_date,
							principal_amount : data.b.principal_amount,
							interest : data.b.interest,
							total_obligation : data.b.total_obligation,	
							less_payment : data.b.less_payment,
							net_obligation : data.b.net_obligation
						}	

						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/deduction_loan/update' ,
							data : datas,
							headers : {
								'Content-Type' : 'application/json'
							}
						}).then(function(res){
							// console.log(res)
							const status = res.status
							if (status !== 200) {
								logger.logError("Access Unauthorized");
							}else{

								logger.logSuccess("Adding Deduction Success");
								self.getdatasaalary()
							}
						});			
						break;
					}

				})
			},
			sumTotalObligation:function(){
				self.form.total_obligation = self.form.principal_amount +  self.form.interest 
				// console.log(self.form.total_obligation)
			},
			sumNetObligation:function(){

				self.form.net_obligation = self.form.total_obligation -  self.form.less_payment 
				
			},
			test:function(){
				// if (self.coba1 == null ) {
				// 	self.coba1 = 0
				// }
				// if (self.coba2 == null ) {
				// 	self.coba2 = 0
				// }
				return self.coba3 = self.coba1 + self.coba2
			}
		}
	})

.controller('deductionPersonalCtrl',function(deductionPersonalFactory,$scope){
	var self = this;
	self.handler = deductionPersonalFactory;
	// self.handler.getData()
	self.handler.getdatasaalary();

	$scope.effective_date_try = new Date(2013,1,1)


})
.controller('addmodalDeduction',function(deductionPersonalFactory,$modalInstance,data, $scope){
	var self = this;
	// self.handler = deductionPersonalFactory
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}


	self.sumTest=function(){
		if (self.form.principal_amount == null ) {
			self.form.principal_amount = 0
		}
		if (self.form.interest == null ) {
			self.form.interest = 0
		}
		self.form.total_obligation = self.form.principal_amount +  self.form.interest 
	}
	self.sumTotalObligation=function(){
		if (self.form.principal_amount == null ) {
			self.form.principal_amount = 0
		}
		if (self.form.interest == null ) {
			self.form.interest = 0
		}

		return self.form.total_obligation = self.form.principal_amount +  self.form.interest 
	}

	self.sumNetObligation=function(){
		if (self.form.total_obligation == null ) {
			self.form.total_obligation = 0
		}
		if (self.form.less_payment == null ) {
			self.form.less_payment = 0
		}
		return self.form.net_obligation = self.form.total_obligation -  self.form.less_payment 
	}

})
.controller('modaleditDeduction',function(deductionPersonalFactory,$modalInstance,data){	
	var self = this;
	self.handler = deductionPersonalFactory;
	// self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})

.run(function(deductionPersonalFactory){
	deductionPersonalFactory.setheaders();
})

/** directive format input currency**/
.directive('format',function($filter){
	return {
		require: '?ngModel',
		link: function (scope,elem,attrs,ctrl){
			if (!ctrl) return;
			var format = {
				prefix: '',
				centsSeparator: '.',
				thousandsSeparator: ','
			};
			ctrl.$parsers.unshift(function(value){
				elem.priceFormat(format);

				return elem[0].value;
			});
			ctrl.$formatters.unshift(function (value){
				elem[0].value = ctrl.$modelValue * 100 ;
				elem.priceFormat(format);
				return elem[0].value;
			})
		}
	};
});
(function($){$.fn.priceFormat=function(options){var defaults={prefix:'US$ ',suffix:'',centsSeparator:',',thousandsSeparator:'.',limit:false,centsLimit:2,clearPrefix:false,clearSufix:false,allowNegative:false,insertPlusSign:false};var options=$.extend(defaults,options);return this.each(function(){var obj=$(this);var is_number=/[0-9]/;var prefix=options.prefix;var suffix=options.suffix;var centsSeparator=options.centsSeparator;var thousandsSeparator=options.thousandsSeparator;var limit=options.limit;var centsLimit=options.centsLimit;var clearPrefix=options.clearPrefix;var clearSuffix=options.clearSuffix;var allowNegative=options.allowNegative;var insertPlusSign=options.insertPlusSign;if(insertPlusSign)allowNegative=true;function to_numbers(str){var formatted='';for(var i=0;i<(str.length);i++){char_=str.charAt(i);if(formatted.length==0&&char_==0)char_=false;if(char_&&char_.match(is_number)){if(limit){if(formatted.length<limit)formatted=formatted+char_}else{formatted=formatted+char_}}}return formatted}function fill_with_zeroes(str){while(str.length<(centsLimit+1))str='0'+str;return str}function price_format(str){var formatted=fill_with_zeroes(to_numbers(str));var thousandsFormatted='';var thousandsCount=0;if(centsLimit==0){centsSeparator="";centsVal=""}var centsVal=formatted.substr(formatted.length-centsLimit,centsLimit);var integerVal=formatted.substr(0,formatted.length-centsLimit);formatted=(centsLimit==0)?integerVal:integerVal+centsSeparator+centsVal;if(thousandsSeparator||$.trim(thousandsSeparator)!=""){for(var j=integerVal.length;j>0;j--){char_=integerVal.substr(j-1,1);thousandsCount++;if(thousandsCount%3==0)char_=thousandsSeparator+char_;thousandsFormatted=char_+thousandsFormatted}if(thousandsFormatted.substr(0,1)==thousandsSeparator)thousandsFormatted=thousandsFormatted.substring(1,thousandsFormatted.length);formatted=(centsLimit==0)?thousandsFormatted:thousandsFormatted+centsSeparator+centsVal}if(allowNegative&&(integerVal!=0||centsVal!=0)){if(str.indexOf('-')!=-1&&str.indexOf('+')<str.indexOf('-')){formatted='-'+formatted}else{if(!insertPlusSign)formatted=''+formatted;else formatted='+'+formatted}}if(prefix)formatted=prefix+formatted;if(suffix)formatted=formatted+suffix;return formatted}function key_check(e){var code=(e.keyCode?e.keyCode:e.which);var typed=String.fromCharCode(code);var functional=false;var str=obj.val();var newValue=price_format(str+typed);if((code>=48&&code<=57)||(code>=96&&code<=105))functional=true;if(code==8)functional=true;if(code==9)functional=true;if(code==13)functional=true;if(code==46)functional=true;if(code==37)functional=true;if(code==39)functional=true;if(allowNegative&&(code==189||code==109))functional=true;if(insertPlusSign&&(code==187||code==107))functional=true;if(!functional){e.preventDefault();e.stopPropagation();if(str!=newValue)obj.val(newValue)}}function price_it(){var str=obj.val();var price=price_format(str);if(str!=price)obj.val(price)}function add_prefix(){var val=obj.val();obj.val(prefix+val)}function add_suffix(){var val=obj.val();obj.val(val+suffix)}function clear_prefix(){if($.trim(prefix)!=''&&clearPrefix){var array=obj.val().split(prefix);obj.val(array[1])}}function clear_suffix(){if($.trim(suffix)!=''&&clearSuffix){var array=obj.val().split(suffix);obj.val(array[0])}}$(this).bind('keydown.price_format',key_check);$(this).bind('keyup.price_format',price_it);$(this).bind('focusout.price_format',price_it);if(clearPrefix){$(this).bind('focusout.price_format',function(){clear_prefix()});$(this).bind('focusin.price_format',function(){add_prefix()})}if(clearSuffix){$(this).bind('focusout.price_format',function(){clear_suffix()});$(this).bind('focusin.price_format',function(){add_suffix()})}if($(this).val().length>0){price_it();clear_prefix();clear_suffix()}})};$.fn.unpriceFormat=function(){return $(this).unbind(".price_format")};$.fn.unmask=function(){var field=$(this).val();var result="";for(var f in field){if(!isNaN(field[f])||field[f]=="-")result+=field[f]}return result}})(jQuery);

}())

