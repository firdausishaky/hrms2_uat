(function(){
	
	angular.module("app.salary.yearlyBonus",['ngFileUpload'])
	.factory('tmpDatasFactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,Upload){
		return self={
			tmpDatas:function(a){
				this.maindata = a
				console.log(a)
			},

			tmpDataAll:function(b){
			// console.log(b,"XXXXXXX")
			a = this.maindata
			b = b

			for (var i = 0; i < a.length; i++) {
				if (a[i].employee_id == b.employee_id ) {
					a[i] = b
				}
			}

			console.log(a)
			return this.maindata = a


		},

		saving(){


			x = this.maindata
			for (var i = 0; i < x.length; i++) {

				x[i].performance_bonus = x[i].performance 

				if (x[i].management_bonus == null || x[i].management_bonus == undefined || x[i].management_bonus == "") {
					x[i].management_bonus =0
				}


				if (x[i].withholding_tax == null || x[i].withholding_tax == undefined || x[i].withholding_tax == "") {
					x[i].withholding_tax =0
				}

				if (x[i].total_monthly_incentive == null || x[i].total_monthly_incentive == undefined || x[i].total_monthly_incentive == "") {
					x[i].total_monthly_incentive =0
				}else{
					if(x[i].total_monthly_incentive > 0){
						var t = (x[i].pay13th + x[i].performance_bonus + x[i].management_bonus);
						x[i].total_monthly_incentive += t;
					}
				}

				if (x[i].add_reimbursment == null || x[i].add_reimbursment == undefined || x[i].add_reimbursment == "") {
					if(x[i].total_monthly_incentive > 80000){
						x[i].add_reimbursment = x[i].total_monthly_incentive * 0.0015;
					}else{
						x[i].add_reimbursment =0;
					}
				}


				if (x[i].less_withholding_tax == null || x[i].less_withholding_tax == undefined || x[i].less_withholding_tax == "") {
					x[i].less_withholding_tax =0
				}


				if (x[i].net_bonus_amount == null || x[i].net_bonus_amount == undefined || x[i].net_bonus_amount == "") {
					x[i].net_bonus_amount =0
				}

				if (x[i].check_stat == null || x[i].check_stat == undefined || x[i].check_stat == "") {
					x[i].check_stat ="1"
				}

				x[i].year = x[i].years

				newObj = {
					
						employee_id 				: x[i].employee_id,
						department_id 				: x[i].department_id,
						pay13th 					: x[i].pay13th,
						performance_bonus 			:x[i].performance_bonus ,
						management_bonus 			:x[i].management_bonus ,
						total_monthly_incentive 	: x[i].total_monthly_incentive,
						add_reimbursment 			: x[i].add_reimbursment,
						less_withholding_tax 		: x[i].less_withholding_tax,
						net_bonus_amount			: x[i].net_bonus_amount,
						year					: x[i].year,
						check_stat : x[i].check_stat
					
				}
				$http({
					method:'POST',
					url: ApiURL.url + '/payroll/cut_off_years/finals',
					data : newObj
				}).success(function(res){
					logger.logSuccess("Saving Data Success")


				}).error(function(res){
					logger.logError("Saving Data Failed")

				})	
			}



		}

	}

	

})
	
	.factory('yearlyBonusFactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,Upload,tmpDatasFactory,managementBonusFactory,withHoldingTaxFactory,perBonusYearFactory){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var NAME = '_schedule';
		var s = localStorage.getItem(NAME);
		var items = s ? JSON.parse(s) : {};
		var self;
		return self = {
			// tester:function(){
			// 	console.log("lololololol")
			// },
			findData:function(input){
				console.log("xxx")
				switch(input){
					case "a":
					return this.maindata = tmpDatasFactory.maindata
					break;

					case "b":
					return perBonusYearFactory.findData()
					break;

					case "c":
					return managementBonusFactory.findData()

					break;

					case "d":
					return withHoldingTaxFactory.findData()

					break;
				}
			},
			setheaders: function(){
				this.headers = new createheaders(
					['Employee Name','employee_name'],
					['Department','department'],
					['Amount','amount'],
					['Year','year'],

					


					);
				this.setactiveheader(this.headers[0],false)	
			},adddata: function(a){
				this.maindata.push(a);
			},getaccess:function(){
				console.log("XXXX")
				$http({
					method : 'GET',
					url : ApiURL.url + '/payroll/year_end_bonus_13th'
				}).then(function(res){
					// console.log(res)
					if (res.status !==200 ) {
						logger.logError("Get Data Failed")
					}else{
						logger.logSuccess("Get Data Success")

						self.setdataDepartment(res.data.data[0].department)
						self.setdataYears(res.data.data[0].years)

					}


				});
			},setdataDepartment:function(x){
				this.maindataDepartment = x
			},
			setdataYears:function(x){
				this.years = x
			},
			
			getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},setform:function(data){
				if(data.status == 'user'){
					this.role = data.status;
					this.iduser = data.employee_id;
					this.nameuser = data.employee_name;
					self.form={};
					self.form.name = this.nameuser;
					self.hide = true;
				}else{
					this.role = data.status;
					self.form={};
					self.form.name = '';
					self.hide = false;
				}
				self.datastatus = data.status;
			},onSelect : function ($item,$model,$label){

				self.model = $item.name;
				self.label = $item.employee_id;
				self.getIDEmployee(self.label)

			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},getYear : function(res){
				var results = res.data.data;
				var getdata = results.length;
				var arr = [];
				var monthArr = [];
				if(getdata > 0){
					var i = 0;
					for(; i < getdata; i++){
						var sub = res.data.data[i].date.substr(0,7);
						var month = res.data.data[i].date.substr(5,2);
						var day = res.data.data[i].date.substr(8,2);

						var subday = day.substr(0,1);
						if(subday == 0){
							day = day.substr(1,1);
						}
						arr[sub] = day;
					}

					var getFinally = [];
					for(key in arr){
						getFinally.push(key+'-'+arr[key]);
					}

					var countFinally = getFinally.length;

					var etc = [{"min" :  getFinally[0]},{"max" :  getFinally[countFinally-1]}];
					return etc;

				}
			},dateEnd : function(data){
				//console.log(data);
				var dates = data.toString();
				var arr = [];
				var arr = dates.split(" ");
				if(arr[1] ==  "Jan"){ arr[1] = 1;}
				if(arr[1] ==  "Feb"){ arr[1] = 2;}
				if(arr[1] ==  "Mar"){ arr[1] = 3;}
				if(arr[1] ==  "Apr"){ arr[1] = 4;}
				if(arr[1] ==  "May"){ arr[1] = 5;}
				if(arr[1] ==  "Jun"){ arr[1] = 6;}
				if(arr[1] ==  "Jul"){ arr[1] = 7;}
				if(arr[1] ==  "Aug"){ arr[1] = 8;}
				if(arr[1] ==  "Sep"){ arr[1] = 9;}
				if(arr[1] ==  "Oct"){ arr[1] = 10;}
				if(arr[1] ==  "Nov"){ arr[1] = 11;}
				if(arr[1] ==  "Dec"){ arr[1] = 12;}
				
				var newdate = arr[3]+'-'+arr[1]+'-'+arr[2];
				//console.log(newdate);
				var result = self.getdateEnd
				//console.log(result);
				var count = result.length;
				var endSearch = [];
				if(count > 0){
					for(var i = 0; i < count; i++){
						
						if(result[i].date ==  newdate){
							endSearch =  result[i];
						}
					}
				}
				if(endSearch == null){
					res.data.header ? logger.logError("data not found") : logger.logError("Data not found")
				}else{
					self.setdata([endSearch]);
				}
				
			},next_paging : function(data,f){
				console.log("f", f);
				console.log("data", data);
				console.log("self.paging", self.paging);
				
				
				if(data > 2 && f  ==  'end' ){
					self.paging[0] = '...'
					self.paging[1] = self.default_paginate[data];

					if(self.default_paginate[data+1] >  self.default_paginate[data]){
						self.paging[2] =  self.default_paginate[data+1] 
					}else{
						delete self.paging[2];
					}
					
					self.paging[3] = '...'
				}else if(data ==  1 && f  == "end"){

				}else{

				}


			},
			setToday : function(datas,type){
				var counts = 1;
				var page = 1;
				var dt = this.maindata;
				console.log("setToday",datas,dt[0].date);
				for(var i in dt){
					if(type=='date'){
						if(dt[i].date == datas){
							//console.log("PAGING",i);
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;
						}
					}else if(type=="month"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getMonth()+1;
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}else if(type=="year"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getFullYear();
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}
				}
				self.Selectdate 	= datas;
				self.Selectdate_end = dt[dt.length-1].date;
				self.Selectdate_start = dt[0].date;
				
				if(page==0){ page++; }
				console.log(page,"PAGE");
				self.selectedPage 	= page;
				self.currentPage 	= self.selectedPage;
				//self.currentPage = 25;
			},
			clickable : 'pointer',
			action_save : false,
			select_month : function(month_date){
				self.action_save = month_date;
				return self.save();
			},
			select_monthnew : function(month_date){
				self.action_save = month_date;
				console.log(month_date,'+++++++++++++++ select_monthnew +++++++++++++++++++')
				return self.saveNew();
			},
			checkButtonYear:function(){
				console.log(self.form.years)

				// if (self.form.years ==undefined || self.form.years ==null || self.form.years=="") {
				// 	self.buttonDisabled = true;
				// }else{
				// 	self.buttonDisabled = false;	
				// }
			},
			getIDEmployee:function(a){
				self.newId = a
			},

			checking:function(a){
				a.employee_id = self.newId

				if (a.department_id == undefined || a.department_id == null) {
					a.department_id = 0
				}					
				



				if (a.employee_id == undefined || a.employee_id == null || a.employee_id=="") {
					a.employee_id = 0
				}else{
					a.employee_id = self.label
				}



				a.types= 'year_end',

				self.dataConvert = a
				self.tmpDatas = a


			},
			save : function(){
				self.checking(self.form)
				// console.log(self.dataConvert)
				x = self.dataConvert

				// self.tmpDataSearch(x)
				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/year_end_bonus_13th/get',
					headers : {
						'Content-Type' : 'application/json'
					},
					data: x
				}).success(function(res){
					console.log(res)
					const status = res.status
					self.form.employee_id = self.model
					self.setdata(res.data)
					logger.logSuccess("View Data Success");
					
				}).error(function(res){
					if (status !== 200) {
						logger.logError("Showing Data Failed");
					}else if(res.data == null){
						console.log("null")
						self.setdata([])
						logger.logError("Data is Empty");
					}
				})
				
			},tmpDataSearch:function(a){

				// a.employee_id = self.label
				this.tmpDatas = a
			},
			restore : function(){
				self.setdata(items);
			},setdata: function(a){
				tmpDatasFactory.tmpDatas(a)
				this.maindata = a;
				// console.log(a)
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},
			
			getActive : function(y){
				if(self.maindata){	
					while (true){
						if( y == 1 || y == 2 || y == 3){
							break;
						}
						y = y - 3;
					}
					return y;
				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodalemployee',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditYearlyBonus',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},

			openmodal: function(a,b){
				console.log(b)
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
						console.log(data)
						$http({
							method : 'POST',
							url : ApiURL.url + '/payroll/incentive_allowance_config/insert',
							data : data,
							headers : {	'Content-Type' :'application/json' }
						}).then(function(res){
							console.log(res)
							self.adddata(res.data.data[0])
							logger.logSuccess("Adding Incentive Allowance Success");
							self.getdataAllowance()
						},function(res){
							res.data.header ? logger.logError(res.data.message) : logger.logError("Adding Incentive Allowance Failed")
						});
						break;
						case 'edit':
						logger.logSuccess('Saving Success');
						this.maindata = tmpDatasFactory.tmpDataAll(data.b)
						console.log(this.maindata)

						// $http({
						// 	method : 'POST',
						// 			// url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
						// 			url : ApiURL.url + '/payroll/incentive_allowance_config/update/'  + b.id,
						// 			data : data.b
						// 		}).then(function(res){
						// 			self.update(res.data.data)
						// 			self.getdataAllowance()
						// 		},function(res){
						// 			res.data.header ? logger.logError(res.data.message) : logger.logError("Updating Allowance Failed")
						// 		});			
						break;
					}
				});
			},

			// dataeditTmp:function(b){
			// 	this.resultEdit = b
			// 	self.tmpDataAll(b)
			// },

			// tmpDataAll:function(b){
			// 	console.log(b)
			// 	a = self.maindata
			// 	b = b

			// 	for (var i = 0; i < a.length; i++) {
			// 		if (a[i].employee_id == b.employee_id ) {
			// 			a[i] = b
			// 		}
			// 	}

			// 	this.maindata = a
			// 	this.tmpdataAllin = a

			// 	console.log(a)


			// },

			tabControlMenu:function(input){
				switch(input){
					case "a": 
					if (self.monthPay13=true) {
						return ((self.PerformanceBonus=false)||(self.ManagementBonus=false)|| (self.withHoldingTax=false))
					}
					break;

					case "b": 
					if (self.PerformanceBonus=true) {
						return ((self.monthPay13=false)||(self.ManagementBonus=false)|| (self.withHoldingTax=false))
					}
					break;


					case "c": 
					if (self.ManagementBonus=true) {
						return ((self.PerformanceBonus=false)||(self.monthPay13=false)|| (self.withHoldingTax=false))
					}
					break;


					case "d": 
					if (self.withHoldingTax=true) {
						return ((self.PerformanceBonus=false)||(self.ManagementBonus=false)|| (self.monthPay13=false))
					}
					break;



				}
			},




		}
	})


.factory('superCache', ['$cacheFactory', function($cacheFactory) {
	return $cacheFactory('super-cache');
}])

/** MAIN CONTROLLER **/  
.controller('yearlyBonusCtrl',function(yearlyBonusFactory,ApiURL,$cookieStore,$scope,$http,$filter,GetName,superCache){
	var self = this;
	self.handler = yearlyBonusFactory;
	self.handler.monthPay13=true
	self.handler.getaccess()
	self.handler.setdata([])

})
.controller('modaleditYearlyBonus',function(yearlyBonusFactory,$modalInstance,data){	


	var self = this;
	self.handler = yearlyBonusFactory;
	self.handler.button();
	self.form = data;

	
	self.calculate13monthPay = function(){
		console.log(self.form.adjustment)
		console.log(data)
		if (self.form.adjustment==null || self.form.adjustment==undefined) {
			self.form.pay13th = data.pay13th
			self.form.adjustment = ""
		}
		if (typeof(self.form.adjustment) == 'number' ) {
			if(self.form.adjustment < 0){
				x = self.form.pay13th
				x = self.form.pay13th + self.form.adjustment
				self.form.pay13th = x

				
			}if (self.form.adjustment > 0) {
				x = self.form.pay13th
				x = self.form.pay13th + self.form.adjustment
				self.form.pay13th = x
			}
		}
		
	}


	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close(
			{a:data,b:self.form}
			);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})





.run(function(yearlyBonusFactory){
	yearlyBonusFactory.setheaders();
})

}())


