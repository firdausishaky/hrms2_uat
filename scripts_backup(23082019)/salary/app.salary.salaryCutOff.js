(function(){
	
	angular.module("app.salary.salaryCutOff",[])
	
	.factory('salaryCutOffFactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,$route,adjustmentHistoryFactory){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Changed Date','changed_date'],
					['Name','name'],
					['Department','deparment'],
					['Salary Basic','salary_basic'],
					['Salary Ecola','salary_ecola'],
					['Deduction','deduction'],
					['Cut Off Period','cut_off_period'],
					['Remarks','remarks'],

					);
				this.setactiveheader(this.headers[0],false)	
			},getDataDropdown:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/payroll/adjustment_payroll?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					const status = res.status

					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						self.setDataDateCutOff(res.data.data[0].period)
						self.setDataDepartment(res.data.data[0].department)
						logger.logSuccess("View Data Success");
					}
				})
			},setDataDepartment:function(a){
				this.depart = a
			},
			
			setDataDateCutOff:function(a){
				this.dataCutOff = a
			},
			getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},saveData:function(a){
				const splitDate = a.cutoff_date.split('T')
				console.log(splitDate[0])
				datas = {
					employee_id: a.employee_id,
					cutoff_date: a.cutoff_date,
					adjust_salary_basic: a.adjust_salary_basic,
					adjust_salary_ecola: a.adjust_salary_ecola,
					deduction_contribute_sss: a.adjust_contribute_sss,
					deduction_contribute_philhealth: a.adjust_contribute_philhealth,
					deduction_contribute_hdmf: a.adjust_contribute_hdmf,
					deduction_loan_sss: a.adjust_loan_sss,
					deduction_loan_hdmf: a.adjust_loan_hdmf,
					deduction_loan_hmo: a.adjust_loan_hmo,
					remarks_basic: a.remarks_basic,
					remarks_contribute_hdmf: a.remarks_contribute_hdmf,
					remarks_contribute_philhealth: a.remarks_contribute_philhealth,
					remarks_contribute_sss: a.remarks_contribute_sss,
					remarks_ecola: a.remarks_ecola,
					remarks_loan_hdmf: a.remarks_loan_hdmf,
					remarks_loan_hmo: a.remarks_loan_hmo,
					remarks_loan_sss: a.remarks_loan_sss
				}


				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/adjustment_payroll/insert',
					data : datas
				}).then(function(res){
					const status = res.status
					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						logger.logSuccess("View Data Success");
						self.searchData(self.form)
						adjustmentHistoryFactory.getdataHistory()

					}

				})
			},
			searchData:function(datas){
				datas.employee_id = self.label
				datas.type = "adjusment"

				if (datas.employee_id == null || datas.employee_id == undefined) {
					datas.employee_id = 0
				}
				$http({
					method : 'POST',
					url : ApiURL.url + '/payroll/adjustment_payroll/get_emp',
					data : datas,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function(res){
					console.log(res)
					const status = res.status
					if (status !== 200) {
						logger.logError("Access Unauthorized");
					}else{

						self.setdata(res.data.data)
						logger.logSuccess("View Data Success");
					}

				})
					// console.log(self.model, self.label)
				},
				view : function(){
					$http({
						method : 'POST',
						url : ApiURL.url + 'payroll/adjustment_payroll/get?key=' + $cookieStore.get('key_api'),
						data :self.form
					}).then(function(res){
						console.log(res)
					// if(res.data.data  ==  []){
					// 	console.log(2)
					// 	self.cutoff_disabled   = true;
					// 	self.talesnotale =    res.data.data
					// }else{	
					// 	console.log(3)
					// 	self.cutoff_disabled   =  false;
					// }

					// var tmp=[];
					// if(res.data.data.length > 0){
					// 	for(var i=0;i<res.data.data.length;i++){
					// 		var obj = Object.keys(tmp);
					// 		var t = obj.findIndex((str)=>{
						
					// 			return str==res.data.data[i].date;
					// 		});
					// 		if(t != -1){
					// 			if(res.data.data[i].employee_id){
					// 				tmp[obj[t]].push(res.data.data[i]);
					// 			}
					// 		}else{
					// 			if(res.data.data[i].employee_id){
					// 				tmp[res.data.data[i].date] = [res.data.data[i]];
					// 			}
					// 		}
					// 	}
					// }
					// console.log(tmp,"asdsadsad");
					// self.keyss = Object.keys(tmp);
					// self.group=tmp;
					// $cookieStore.put('option',self.form);

					// console.log($cookieStore.get('option'))
					
					// self.setdata(self.group[self.keyss[0]]);
					// self.setrange(self.form)
					// //self.setdata(res.data.data);
					// //$http.post('/data.json',res.data.data);

					// logger.logSuccess(res.data.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
					if(res.data.header.status == 504) {
						logger.logError("Please Input Job Title & Department")
					}
				});
				},setrange:function(form){
				//console.log(JSON.stringify(form,null,4),"+++++++++++++++++++++++++++++++")
				var base_a = form.attendance.slice(0,10);
				var base_b = form.attendance.slice(11,21);
				
				var a = new Date(base_a);
				var  b  =  new Date(base_b)
				
				var n1 = b.getDate();
				var split_a = base_a.split('/');
				var split_b = base_b.split('/');

				var lastDay = new Date(parseInt(base_a[0]),parseInt(base_a[1]), 0);
				var n = lastDay.getDate();
				
				var str = '';

				if(split_a[1] != split_b[1]){
					for(var i = parseInt(split_a[2]);  i <= parseInt(n); i++){
						var ix =  i
						str = str+'.'+ ix
						if(i == n){
							for(var j = parseInt(1);  j <= parseInt(split_b[2]); j++){
								var ij =  j
								str = str +'.'+ ij
							}
						}
					} 
				}else{
					for(var i = parseInt(split_a[2]); i <= parseInt(split_b[2]); i++){
						var ix =  i
						str = str +'.'+ ix 
					}
				}

				var getDatess = function(startDate, endDate) {
					var dates = [],
					currentDate = startDate,
					addDays = function(days) {
						var date = new Date(this.valueOf());
						date.setDate(date.getDate() + days);
						return date;
					};
					while (currentDate <= endDate) {
						dates.push(currentDate);
						currentDate = addDays.call(currentDate, 1);
					}
					return dates;
				};

				// Usage
				var tmp2=[];
				var dates = getDatess(new Date(base_a), new Date(base_b));
				dates.forEach(function(date) {
					tmp2.push(date.getDate().toString());
				});

				self.dataTabs = []
				self.datarange = /*str.split('.')*/ tmp2;
				//delete self.datarange[0];
				var arr = [];
				var len = self.datarange.length;
				console.log(len);
				var j=0;var keys=self.keyss;
				for (var i = 0; i < len; i++) {

					try{
						var tmp_tgl = keys.findIndex((idx)=>{

							var tgl  	= idx.split('-');
							tgl[2]  = parseInt(tgl[2]);
							if(tgl[2].toString() == self.datarange[i]){ return true; }
							else{ return false; }
						})

						if(tmp_tgl < 0){
							arr.push({
								tab:self.datarange[i],
								date:[],
							});
						}else{
							arr.push({
								tab:self.datarange[i],
								date:keys[tmp_tgl],
							});
						}
						/*var tgl  = keys[j].split('-');
						if(tgl[2] != self.datarange[i] && self.datarange[i] == '31'){
							
						}else{
						    arr.push({
						        tab:self.datarange[i],
						        date:keys[j],
						    });
						    j++;
						}*/
					}catch(e){

					}
				}
				self.dataRange = arr
				console.log(self.dataRange,'range')
				
			},tabIndex:0,
			setIndex:function(index){
				console.log(index)
				self.tabIndex = index;
				self.setNextData(self.dataRange[self.tabIndex])
				

			},next : function(){
				console.log(self.tabIndex)
				if(self.tabIndex !== self.dataRange.length - 1){
					self.dataRange[self.tabIndex].active = false;
					self.tabIndex++
					self.dataRange[self.tabIndex].active = true;
				}   
			},prev:function(){
				if(self.tabIndex > 0){
					self.dataRange[self.tabIndex].active = false;
					self.tabIndex--
					self.dataRange[self.tabIndex].active = true;
				}else{
					void 0
				}	
			},getdatabyIndex:function(i,j){
				self.tabIndex = j;
				self.form.tab =  i;

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.setdata(res.data.data);
					//$http.post('/data.json',res.data.data);
					self.tabIndex = j;
					console.log(res.data.data)
					self.setrange(self.form)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
					if(res.data.header.status == 504) {
						logger.logError("Please Input Job Title & Department")
					}
				});
			},testMasuk : function(data,i){
				
				self.form.tabActive = data;
				$http({		
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					console.log(i)
					self.setIndex =  i
					self.setdata(res.data.data);
					//$http.post('/data.json',res.data.data);

					self.setrange(self.form)
					console.log(res)
					logger.logSuccess(res.data.message);
				},function(res){
					res.data.header ? logger.logError(res.data.message) : logger.logError("View Attendance Cut Off Failed")
				});
			},setNextData:function(data){

				//console.log(self.form.tab)
				// $http({		
				// 	method : 'POST',
				// 	url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
				// 	data :self.form
				// }).then(function(res){

				// 	self.setdata(res.data.data);
				// 	//$http.post('/data.json',res.data.data);

				// 	self.setrange(self.form)
				// 	logger.logSuccess(res.data.header.message);
				// },function(res){
				// 	res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
				// });

				/*$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.setdata([]);
					logger.logSuccess('Success find data');
					$route.reload();
				},function(res){
					res.data.header ? logger.logError('Error cut off data') : logger.logError("Error cut off data")
				});*/
			},adddata: function(a){
				this.maindata.push(a);
			},openmodal:function(a){
				//check short exists  
				

				var thanos  =  false;
				var collect  = [];
				var i  =   0;
				for(var prop  in self.maindata){
					if(self.maindata[prop].Short != '-'){

						collect[i]  = self.maindata[prop] ;
						i += 1;
						thanos  =    true;

					}
				}

				if(thanos  ==  true){
					self.collect  =  collect
					//self.context.cancel();
					console.log(self.context)
					$modal.open(self.modals[a]).result.then(function(data){
						switch (a) {
							case 'open':
							self.collect  =  collect
							break;
							case  'open1':


							break;
						}
					});
				}else{
					var a   =   'open1'
					$modal.open(self.modals[a]).result.then(function(data){
						switch (a) {
							case  'open1':


							break;
						}
					});
				}
			},modals : {
				open:{
					animation: true,
					templateUrl: 'open',
					controller: 'cutoffmodal',
					controllerAs: 'open',
					size:'lg',
					resolve:{
						data:function(){
							return self.collect;
						}					
					}
				},
				open1:{
					animation: true,
					templateUrl: 'open1',
					controller: 'cutoffmodal1',
					controllerAs: 'open1',
					size:'lg',
					resolve:{
						data:function(){
							return self.form;
						}					
					}
				}
			},setdata: function(a){
				console.log(a)
				this.maindata = a

			},
			getdata: function(){
				if(self.dataRange){
					
					var t=self.dataRange.findIndex(function(dt){
						return dt.active==true;
					});
					if(t!=-1){
						this.maindata = self.group[self.dataRange[t].date];
					}else{
						this.maindata = [];
					}
				}
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},onSelect : function ($item,$model,$label){
				self.model = $item.name;
				self.label = $item.employee_id;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[4],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},

		}
	})
.controller('salaryCutOffController',function(salaryCutOffFactory,ApiURL,$cookieStore,$scope,$http){
	var self = this;
	self.form={}
	self.handler = salaryCutOffFactory;
	self.handler.getDataDropdown()
	
})

.controller('cutoffmodal',function(salaryCutOffFactory,ApiURL,$cookieStore,$modalInstance,$scope,$http,data,$route,logger){
	console.log(data)
	var self = this;
	self.form={}
	self.handler = salaryCutOffFactory;
	self.handler.context = this
	self.save = function(){

		var final_data =  $cookieStore.get('option');
		var k  =  0;
		var arr  = [];
		for(var prop in self.handler.collect){
			result   =    self.handler.collect[prop]
			arr[k]   = {'employee_id' : result.employee_id,  'date' : result.date, 'short' :  result.Short }
			k  += 1
		}

		console.log(self.handler.maindata)
		var obj   =  self.handler.maindata;
		for(var prop in  obj){
			if((self.maindata[prop].timeIn == '-' && self.maindata[prop].timeOut == '-') || (self.maindata[prop].timeIn == '-' || self.maindata[prop].timeOut == '-')){
				logger.logError(`Error cut off Time In or Time Out doesn't exists  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;
			}
			else if(self.maindata[prop].C_TimeIn ==  'red' || self.maindata[prop].C_TimeIn ==  'orange'){
				logger.logError(`Error cut off Time In or Time Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].OvertimeRestDay_color ==  'red' || self.maindata[prop].OvertimeRestDay_color ==  'orange'){
				logger.logError(`Error cut off Overtime Rest Day not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].colorLate ==  'red' || self.maindata[prop].colorLate ==  'orange'){
				logger.logError(`Error cut off Late not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].early_c ==  'red' || self.maindata[prop].early_c ==  'orange'){
				logger.logError(`Error cut off Early Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].new_color_overtime==  'red' || self.maindata[prop].new_color_overtime==  'orange'){
				logger.logError(`Error cut off Overtime not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else{

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data : {form  :  final_data, short_person : arr}
				}).then(function(res){
					var  base_data  = [];
					self.handler.setdata(base_data);
					self.handler.dataRange = [];
					self.handler.form =  []
					// var b = res.data.data;
					// console.log(b)
					// for(s in self.maindata){
					// 	if(b.date === self.maindata[s].date){
					// 		self.maindata[s] = res.data.data;
					// 	}
					// }

					$route.reload();
					logger.logSuccess('Success cut off data');
				},function(res){
					logger.logError('Failed cut off data')
				})
			}
		}


		$modalInstance.close(self.form);
	},
	self.cancel = function(){
		console.log('test_cancle')
		$modalInstance.close()
		self.handler.openmodal('open1')
	}
})


.controller('cutoffmodal1',function(salaryCutOffFactory,ApiURL,$cookieStore,$modalInstance,$scope,$http,data,$route,logger){
	var self = this;
	self.form={}
	self.handler = salaryCutOffFactory;

	self.save1 = function(file){
		var final_data =  $cookieStore.get('option');

		var k  =  0;
		var arr  = [];


		console.log(self.handler.maindata)
		var obj   =  self.handler.maindata;
		for(var prop in  obj){
			if((self.maindata[prop].timeIn == '-' && self.maindata[prop].timeOut == '-') || (self.maindata[prop].timeIn == '-' || self.maindata[prop].timeOut == '-')){
				logger.logError(`Error cut off Time In or Time Out doesn't exists  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;
			}
			else if(self.maindata[prop].C_TimeIn ==  'red' || self.maindata[prop].C_TimeIn ==  'orange'){
				logger.logError(`Error cut off Time In or Time Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].OvertimeRestDay_color ==  'red' || self.maindata[prop].OvertimeRestDay_color ==  'orange'){
				logger.logError(`Error cut off Overtime Rest Day not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].colorLate ==  'red' || self.maindata[prop].colorLate ==  'orange'){
				logger.logError(`Error cut off Late not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].early_c ==  'red' || self.maindata[prop].early_c ==  'orange'){
				logger.logError(`Error cut off Early Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}
			else if(self.maindata[prop].new_color_overtime==  'red' || self.maindata[prop].new_color_overtime==  'orange'){
				logger.logError(`Error cut off Overtime not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
				break;	
			}else{


				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data : {form  :  final_data, short_person : arr}
				}).then(function(res){
					$route.reload();
					var  base_data  = [];
					self.handler.setdata(base_data);
					self.handler.dataRange = [];
					self.handler.form =  []
					logger.logSuccess("Success cut off data");
				},function(res){
					logger.logError("Failed   cut off   data")
				})
			}
		}

		$modalInstance.close(self.form);
	},
	self.cancel1 = function(){
		console.log('test_cancle')
		$modalInstance.dismiss("Close")
	}
})


/** FUNCTION SET HEADER TABLES ATT CUT-OFF **/
.run(function(salaryCutOffFactory){
	salaryCutOffFactory.setheaders();
})

}())
