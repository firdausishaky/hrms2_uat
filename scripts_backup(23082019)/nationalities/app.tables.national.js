(function(){
			
	angular.module('app.tables.national',['checklist-model'])

	.factory('nationalfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Nationality','title',100]
				);
				this.setactiveheader(this.headers[0],false)
			},getdatanational:function(){
				$http({
					url : ApiURL.url + '/api/nationalities/form-nationalities?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data)
						logger.logSuccess(res.data.header.message);
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Nationalities Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/nationalities/form-nationalities?key='  + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalnational',
					controller: 'modalnational',
					controllerAs: 'modalnational',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaleditnational',
					controller: 'modaleditnational',
					controllerAs: 'modaleditnational',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/nationalities/form-nationalities?key=' + $cookieStore.get('key_api'),
									data : data,
								}).then(function(res){
									self.adddata(res.data.data)
									//console.log(res.data.data)
									//self.callback()
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Nationalities Failed")
								});
							break;
						case 'edit':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/nationalities/form-nationalities/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									data : data.b
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Nationalities Failed")
								});			
							break;
					}
					
				});
			},
			deldata: function(id){
				console.log(id)
				$http({
					url : ApiURL.url + '/api/nationalities/form-nationalities/ ' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Nationalities Success');
				},function(res){
					console.log(res.data.header.message)
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Nationalities Failed")
				})
			}
		}
	})

	.controller('nationalCtrl',function(nationalfactory){
		var self = this;
			self.handler = nationalfactory;
			self.handler.getdatanational()
	})
	.controller('modalnational',function(nationalfactory,$modalInstance,data){	
		var self = this;
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})
	.controller('modaleditnational',function(nationalfactory,$modalInstance,data){	
		var self = this;
			self.handler = nationalfactory;
			self.handler.button();
			self.form = data;
			self.form = angular.copy(data);
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})
	.run(function(nationalfactory){
		nationalfactory.setheaders();
	})

}())



/* var appTablesNational = angular.module("app.tables.national",["checklist-model"]);

		
appTablesNational.controller("nationalCtrl", ["$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL","$filter", function($scope,filterFilter,$modal,$cookieStore,$http,logger,api,$filter) {
				
				
				var init;
            $scope.numPerPageOpt = [3, 5, 10, 20]; 
            $scope.numPerPage = $scope.numPerPageOpt[2]; 
            $scope.currentPage = 1; 
            $scope.currentPageStores = []; 
            $scope.searchKeywords = ""; 
            $scope.filteredStores = []; 
            $scope.row = "" ;
            $scope.stores =[];
            $scope.select = function(page) {
                 var end, start;
                 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
            };
            $scope.onFilterChange = function() {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
            };
            $scope.onNumPerPageChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.onOrderChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.search = function() {
                return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
            };
            $scope.order = function(rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
            };
            init = function() {
                return $scope.search(), $scope.select($scope.currentPage)
            };
			
			
			$http.get(api.url + '/api/nationalities/form-nationalities?key=' + $cookieStore.get('key_api') ).success(function(res){
				
				$scope.check = {
					currentPageStores: []
				}
				
				var end, start;
				start =0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
				
				
				}).success(function(data, status, header, config) {
						$scope.role = true;
						logger.logSuccess(data.header.message);
				}).error(function(data, status, headers, config) {
						$scope.role = false;
						logger.logError(data.header.message);
				});

				
				
				$scope.open = function(size) {
			
					var modal;	
					modal = $modal.open({
						templateUrl: "modalNational.html",
						controller: "modalNationalCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return;
							}
						}
					});
					
					modal.result.then(function (newstudent) {
						if($scope.currentPageStores){
							$scope.currentPageStores.push(newstudent);
						}else{
							$scope.currentPageStores = [];
							$scope.currentPageStores.push(newstudent);
						}
					});
				};
				 
				 
				$scope.edit = function(size,national) {
			
					var modal;
					modal = $modal.open({
						templateUrl: "modalEditNational.html",
						controller: "NationalEditCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return national;
								console.log(national);
							}
						}
					});
				};
				
				
				$scope.remove = function(id){
					$http({
					method : 'DELETE',
					url : api.url + '/api/nationalities/form-nationalities/ '+ id + '?key=' + $cookieStore.get('key_api'),
					}).success(function(data, status, header){
						var i;
							for( i = 0; i < id.length; i++) {
								$scope.currentPageStores = filterFilter($scope.currentPageStores, function (store) {
									return store.id != id[i];
								});
							};
						$scope.check.currentPageStores = [];
						logger.logSuccess(data.header.message);
					}).error(function(data, status, header) {
						logger.logError(data.header.message);
					});
			};
			
			
		}]);
		
appTablesNational.controller("modalNationalCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api) {
				
			
				if(items){
					$scope.nas = items;
				}
				
				$scope.save = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/nationalities/form-nationalities?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded',},
						data : $.param($scope.nas)  
					}).success(function(data, status, header, config) {
						
						var temp = {};
							temp = $scope.nas;
							temp.id = data.data.id;
			
							$modalInstance.close(temp);
							logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				
				$scope.cancel = function() {
					$modalInstance.dismiss("cancel")
				}
				
		}]);
appTablesNational.controller("NationalEditCtrl", ["ApiURL","$http","$cookieStore","items","$scope", "$modalInstance","items","logger", function(api,$http,$cookieStore,items,$scope,$modalInstance,items,logger) {
				
				
				var a = String(items.title);
				if(items){
					$scope.nas=items;
				}
				
				$scope.save = function(){
					$http({
						method : 'POST',
						url : api.url + '/api/nationalities/form-nationalities/' + items.id + '?key=' + $cookieStore.get('key_api'), 
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.nas)
					}).success(function(data, status, header) {
						var temp = {};
							temp = $scope.nas;
							temp.id = items.id;
						logger.logSuccess(data.header.message);
						$modalInstance.close(temp);	
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				};
				
				$scope.cancel = function() {
						$scope.nas.title = a;
					$modalInstance.dismiss("cancel")
				}
				
				$scope.edit = function(){
					$scope.button=true;
				};
		}]);
		
		
		
		 */