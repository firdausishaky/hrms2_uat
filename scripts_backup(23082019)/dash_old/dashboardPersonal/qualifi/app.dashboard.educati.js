

var appFormEdu = angular.module("app.dashboard.educati", ["checklist-model","ui.bootstrap","ngAnimate"]);

appFormEdu.controller("educatiCtrlDashboard", function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,permissionqualification) {
	
	/** employee id **/					
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		
		/** get all data **/
		$http({
			method : 'GET',
			url : ApiURL.url +'/api/employee-details/qualification/education/'  + $scope.id + '?key=' + $cookieStore.get('key_api')
		}).then(function(res){
			$scope.educations = res.data.data;
			$scope.educ = {educations:[]}
			logger.logSuccess(res.data.header.message)
		},function(res){
			res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Education Failed")
		});
		
		/** function hidden button **/
		$scope.hide = function(a){
			var data = permissionqualification.access;
			if(data){
				switch(a){
					case 'create':
					if(data.create == 1){return true}
						break;
					case 'delete':
					if(data.delete == 1){return true}
						break;
				}
			}
		};

		/** modal handler **/
		$scope.open = function(size) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEdu.html",
				controller: "modalEduCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function (newstudents) {
				if($scope.educations){
					$scope.educations.push(newstudents);
				}else{
					$scope.educations=[];
					$scope.educations.push(newstudents);
				}
			});
		};
		$scope.edit = function(size,educ) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditEdu.html",
				controller: "EduEditCtrlDashboard",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return educ;
					}
				}
			});
			modal.result.then(function (newedu) {
				for(a in $scope.educations){
					if($scope.educations[a].id==newedu.id){
						$scope.educations[a]=newedu;
					}
				}
			});
		};
		
		/** function remove **/
		$scope.remove = function(id){
			$http({
				method : 'DELETE',
				url : ApiURL.url + '/api/employee-details/qualification/education/' + id + '?key=' + $cookieStore.get('key_api')
			}).success(function(data, status, header){
				var i;
				for( i = 0; i < id.length; i++) {
					$scope.educations = filterFilter($scope.educations, function (store) {
						return store.id != id[i];
					});
				};
				$scope.educ.educations = [];
				logger.logSuccess(data.header.message);
			}).error(function(data, status, header) {
				data.header ? logger.logError(data.header.message) : logger.logError("Delete Education Failed")
			});
		};
		
	});

appFormEdu.controller("modalEduCtrlDashboard", function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$modalInstance,items,$routeParams) {
	$scope.today = function() {
		$scope.dt = new Date();
	};
	$scope.today();

	$scope.clear = function() {
		$scope.dt = null;
	};

	$scope.inlineOptions = {
		customClass: getDayClass,
		minDate: new Date(),
		showWeeks: true
	};

	$scope.dateOptions = {
		dateDisabled: disabled,
		formatYear: 'yy',
		maxDate: new Date(2020, 5, 22),
		minDate: new Date(),
		startingDay: 1
	};

		  // Disable weekend selection
		  function disabled(data) {
		  	var date = data.date,
		  	mode = data.mode;
		  	return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
		  }

		  $scope.toggleMin = function() {
		  	$scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
		  	$scope.dateOptions.minDate = $scope.inlineOptions.minDate;
		  };

		  $scope.toggleMin();

		  $scope.open1 = function() {
		  	$scope.popup1.opened = true;
		  };

		  $scope.open2 = function() {
		  	console.log(true)
		  	$scope.popup2.opened = true;
		  };

		  $scope.setDate = function(year, month, day) {
		  	$scope.dt = new Date(year, month, day);
		  };

		  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		  $scope.format = $scope.formats[0];
		  $scope.altInputFormats = ['M!/d!/yyyy'];

		  $scope.popup1 = {
		  	opened: false
		  };

		  $scope.popup2 = {
		  	opened: false
		  };

		  var tomorrow = new Date();
		  tomorrow.setDate(tomorrow.getDate() + 1);
		  var afterTomorrow = new Date();
		  afterTomorrow.setDate(tomorrow.getDate() + 1);
		  $scope.events = [
		  {
		  	date: tomorrow,
		  	status: 'full'
		  },
		  {
		  	date: afterTomorrow,
		  	status: 'partially'
		  }
		  ];

		  function getDayClass(data) {
		  	var date = data.date,
		  	mode = data.mode;
		  	if (mode === 'day') {
		  		var dayToCheck = new Date(date).setHours(0,0,0,0);

		  		for (var i = 0; i < $scope.events.length; i++) {
		  			var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

		  			if (dayToCheck === currentDay) {
		  				return $scope.events[i].status;
		  			}
		  		}
		  	}

		  	return '';
		  }
		  /** employee id **/		
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** copy data **/
		if(items){
			$scope.formData=items;
		}
		
		/** get data master eduacation **/
		$http.get(ApiURL.url + '/api/employee-details/qualification/education?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.dataeducation=res;
		});	

		/** function save **/
		$scope.save = function(id){
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/qualification/education/store/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
				data : $scope.formData   
			}).success(function(data){
				var temp = {};
				temp = $scope.formData;
				console.log($scope.formData,"EDU")
				var a, b, c = temp.level;
				for(var i = 0; i < $scope.dataeducation.length; i++){
					a = $scope.dataeducation[i];
					if(a.id == c){
						b = a.level;
					};
				};
				temp.level = b;
				temp.id = data.data; 
				$modalInstance.close(temp);		 						
				logger.logSuccess(data.header.message);
			}).error(function(data){
				data.header ? logger.logError(data.header.message) : logger.logError("Adding Education Failed")
			});
		};
		
		/** close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		
		
	});


appFormEdu.controller("EduEditCtrlDashboard",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,items,$modalInstance,$routeParams,permissionqualification) {
	
	
	/** employee id **/		
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** copy data **/
		if(items){
			$scope.formData=items;
			$scope.formData=angular.copy(items);
		}
		
		/** get data master eduacation **/
		$http.get(ApiURL.url + '/api/employee-details/qualification/education?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.dataeducation=res;
		});	
		
		/** function save **/
		$scope.save = function(id){
			for(a in $scope.dataeducation){
				if($scope.dataeducation[a].level == $scope.formData.level){
					$scope.formData.level = $scope.dataeducation[a].id;
				};
			};
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/employee-details/qualification/education/' + items.id + '?key=' + $cookieStore.get('key_api'),
				data :$scope.formData   
			}).success(function(data){
				var temp = {};
				temp = $scope.formData;
				var a, b, c = temp.level;
				for(var i = 0; i < $scope.dataeducation.length; i++) {
					a = $scope.dataeducation[i];
					if(a.id == c) {
						b = a.level;
					};
				};
				temp.level = b;					
				$modalInstance.close(temp); 	
				logger.logSuccess(data.header.message);
			}).error(function(data) {
				data.header ? logger.logError(data.header.message) : logger.logError("Update Education Failed")
			});
		};
		
		
		/** function hide button **/
		$scope.hide = function(a){
			var access  = permissionqualification.access;	
			if(access){
				switch(a){
					case 'update':
					if(access.update == 1){
						return true
					}
					break;
				}
			}
		};
		
		/** close modal **/
		$scope.edit = function(){
			$scope.button=true;
		};				
		$scope.cancel = function(){
			$modalInstance.dismiss();
		}
		
	});	
/*
var a = String(items.education);
var b = String(items.institution);
var c = String(items.major);
var d = String(items.from_year);
var e = String(items.to_year);
$scope.formData.education = a;
$scope.formData.institution = b;
$scope.formData.major = c;
$scope.formData.from_year = d;
$scope.formData.to_year = e;
*/						




