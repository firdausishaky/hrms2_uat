var appFormSem = angular.module("app.dashboard.seminar", ["checklist-model","ngFileUpload"]);

appFormSem.controller("seminarCtrlDashboard",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,$window,permissionqualification) {
	
	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser');
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/** get all data **/
		$http({
			method : 'GET',
			url : ApiURL.url + '/api/employee-details/qualification/seminar/'   + $scope.id + '?key=' + $cookieStore.get('key_api')
		}).then(function(res){
			$scope.seminars = res.data.data;
			$scope.delsem = {seminars:[]};
			for(c in $scope.seminars){
				if($scope.seminars[c].filename ==''){
					$scope.seminars[c].filename = "Not Defined";
				}
			}
			logger.logSuccess(res.data.header.message)
		},function(res){
			res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Seminar & Training Failed")
		});
		
		
		/** function hidden button **/
		$scope.hide = function(a){
			var data = permissionqualification.access;
			if(data){
				switch(a){
					case 'create':
					if(data.create == 1){return true}
						break;
					case 'delete':
					if(data.delete == 1){return true}
						break;
				}
			}
		};
		
		/** function get file seminar **/
		$scope.getfile = function(a){
			if(a.target.innerText == 'Not Defined'){
				logger.logError("File Not Found");
			}else{
				var e = ApiURL.url +  '/api/employee-details/qualification/seminar/'  + $scope.id +  '/' + a.target.innerText + '?key=' + $cookieStore.get('key_api');
				$window.open(e);
			} 
		};
		
		
		/** modal handler **/
		$scope.open = function(size){
			var modal;
			modal = $modal.open({
				templateUrl: "modalSemi.html",
				controller: "modalSemiCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return;
					}
				}
			});
			modal.result.then(function (newcomer) {
				if($scope.seminars){
					$scope.seminars.push(newcomer);
				}else{
					$scope.seminars = [];
					$scope.seminars.push(newcomer);
				}
			});
		};
		
		
		
		$scope.edit = function(size,student) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalEditSemi.html",
				controller: "SemiEditCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return student;
					}
				}
			});
			modal.result.then(function (newdata){
				for(a in $scope.seminars){	
					if($scope.seminars[a].id == newdata.id){
						$scope.seminars[a]=newdata;
						console.log(newdata)
					}
				};				
			});
		};
		
		
		/** function remove **/
		$scope.remove = function(id){
			$http({
				method : 'DELETE',
				url : ApiURL.url + '/api/employee-details/qualification/seminar/' + id + '?key=' + $cookieStore.get('key_api')
			}).success(function(data){
				var i;
				for( i = 0; i < id.length; i++) {
					$scope.seminars = filterFilter($scope.seminars, function (student) {
						return student.id != id[i];
					});
				};
				$scope.delsem.seminars = [];
				logger.logSuccess(data.header.message);
			}).error(function(data) {
				data.header ? logger.logError(data.header.message) : logger.logError("Delete Data Seminar & Training Failed")
			});
		};
		
		
	});


appFormSem.controller("modalSemiCtrlDashboard",function($routeParams,$scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$modalInstance,items,Upload,$route) {
	
	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser');
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		/** copy data **/		
		if(items){
			$scope.formData=items;
		}
		
		/** function save **/
		$scope.save = function(file){
			if(file){
				/** function upload **/
				file.upload = Upload.upload({
					method : 'POST',
					url:  ApiURL.url + '/api/employee-details/qualification/seminar/store2/' + $scope.id +'?key=' + $cookieStore.get('key_api'),
					data :{data:$scope.formData},
					file: file
				}).then(function(res){
					$modalInstance.close(res.data.data); 
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Seminar & Training Failed")
				});
			}else{				
				$http({
					method : 'POST',
					url:  ApiURL.url + '/api/employee-details/qualification/seminar/store2/' + $scope.id  +'?key=' + $cookieStore.get('key_api'),
					data :$scope.formData     
				}).then(function(res){
					var temp = res.data.data;
					temp.filename='Not Defined';
					$modalInstance.close(temp);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Seminar & Training Failed")
				});
			}
		};
		
		/** close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}
		
		
	});

appFormSem.controller("SemiEditCtrlDashboard", function($routeParams,Upload,$scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,items,$modalInstance,permissionqualification){
	
	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser');
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()
		
		/**copy data **/	
		if(items){
			$scope.formData = items;
			$scope.formData = angular.copy(items);
		}
		
		
		/** function save **/
		$scope.save = function(file){
			if(file){
				if(file[0].name=='Not Defined'){
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/employee-details/qualification/seminar/update/' + items.id + '?key=' + $cookieStore.get('key_api'),
						data :$scope.formData     
					}).then(function(res){
						var temp = res.data.data;
						temp.filename='Not Defined';
						$modalInstance.close(temp);
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Update Data Seminar & Training Failed")
					});
				}else{
					/** function upload **/
					file.upload = Upload.upload({
						method : 'POST',
						url : ApiURL.url + '/api/employee-details/qualification/seminar/update/' + items.id + '?key=' + $cookieStore.get('key_api'),
						data :{data:$scope.formData},
						file: file
					}).then(function(res){
						$modalInstance.close(res.data.data);
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Update Data Seminar & Training Failed")
					});
				}
			}else{
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/employee-details/qualification/seminar/update/' + items.id + '?key=' + $cookieStore.get('key_api'),
					data :$scope.formData     
				}).then(function(res){
					var temp = res.data.data;
					temp.filename='Not Defined';
					$modalInstance.close(temp);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Update Data Seminar & Training Failed")
				});
			}
		};
		
		/** change filename **/	
		$scope.formData.files=[];
		var g = new File([""], items.filename);
		$scope.formData.files.push(g);
		
		
		/** close modal **/
		$scope.cancel = function(){
			$modalInstance.dismiss();
		}
		
		/** function hide button delete **/
		$scope.$watch('items', function(){
			if(items.filename=='Not Defined'){
				$scope.delbutton=false;
			}else{
				$scope.delbutton=true;
			}
		});
		
		/** function hide button **/
		$scope.hide = function(a){
			var access  = permissionqualification.access;	
			if(access){
				switch(a){
					case 'update':
					if(access.update == 1){
						return true
					}
					break;
				}
			}
		};
		
		
	});	
/*
file upload with old concept
in modal add

$scope.save = function(id){
	$scope.formData.id = $cookieStore.get('idSem'); 
	$scope.formData.filename = $cookieStore.get('nameSem'); 
	$http({
		method : 'POST',
		url : ApiURL.url + '/api/employee-details/qualification/seminar/store2/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
		headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
		data : $.param($scope.formData)   
	}).success(function(data, status, header, config) {
		var temp = {};
			temp = $scope.formData;
			temp.id = data.data.id;
			if(data.data.filename == null){
				temp.filename = " Not Defined";
			}
			$cookieStore.remove('idSem');
			$cookieStore.remove('nameSem');
		$modalInstance.close(temp);
		logger.logSuccess(data.header.message);
	}).error(function(data, status, headers, config) {
		logger.logError(data.header.message);
	});
};		
$scope.$watch('files', function () {
	$scope.upload = function (files) {
		if (files && files.length) {
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				Upload.upload({
					url:  ApiURL.url + '/api/employee-details/qualification/seminar/store1/' + $scope.id  +'?key=' + $cookieStore.get('key_api'),
					file: file
				}).progress(function (evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					$scope.log = 'progress: ' + progressPercentage + '% ' +
								evt.config.file.name + '\n' + $scope.log;
				}).success(function (data, status, header, message) {
					$cookieStore.put('idSem',data.data.id);
					$cookieStore.put('nameSem',data.data.filename);
					logger.logSuccess(data.header.message);
				}).error(function (data, status, header, message) {
					logger.logError(data.header.message);
					$scope.files=true;
				});
			}
		}
	};
});


$scope.files=[];
var g = new File([""], items.filename);
$scope.files.push(g);
	
var a = String(items.name);
var b = String(items.date);
var c = String(items.location);
var d = String(items.filename);
$scope.formData.name = a;
$scope.formData.date = b;
$scope.formData.location= c;
$scope.formData.filename= d;						
*/
