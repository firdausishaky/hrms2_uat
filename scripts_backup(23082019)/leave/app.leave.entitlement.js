(function(){

	angular.module('app.leave.entitlement',['rzModule'])

	.factory('entitlementsFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Employee Name','employee_id',null],
					['Leave Type','LeaveType',null],
					['Days Entitled','DaysEntitled',null],
					['Days Remaining ','DaysRemaining',null],
					['Availed Days','AvailedDays',null],
					['Last date of availment','lastDate',null],
					['Number of Availments','NumberOfAvailment',null],
					['Valid Until','ValidUntil',null]
					);
				this.setactiveheader(this.headers[0],false)
			},form:{employee: ''}
			,find:function(){
				console.log("yyyyyyyyyy"+ self.form.employee)
				x = $cookieStore.get('access')
				console.log(x)
				if (x == 200) {
					if(self.label == undefined){
						self.form.employee = null
					}
					self.form.employee = self.label;
					// self.setDefault()
				}else{
					self.form.employee = $cookieStore.get('NotUser')
				}
				
				console.log(self.form)

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/leave/searchEmp?key=' + $cookieStore.get('key_api'),
					data : self.form
				}).then(function(res){
					self.setdata(res.data.data)
					self.form.employee = self.model;
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.employee = self.model;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave Entitlement Failed")
				});	
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				edit:{
					animation: true,
					templateUrl: 'modalAdjust',
					controller: 'editAdjust',
					controllerAs: 'modalAdjust',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}	
					}
				}
			},

			getaccess: function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/leave/getList?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					console.log(res)
				})
			},openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch (a) {
						case 'edit':
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule/updateFix/' + b.id + '?key=' + $cookieStore.get('key_api'),
							data : data.b
						}).then(function(res){
							self.update(res.data.data[0])
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Leave Failed")
						});	
						break;
					}
				});
			},
			onSelect:function($item,$model,$label){self.model = $item.name;self.label = $item.employee_id;},
			getLocation:function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee +'?&key=' + $cookieStore.get('key_api'),{
					params: {
						address : val,
					}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},getselectleave:function(URL){ 
				$http.get(URL).success(function(res){ 
					if(res.header.message == 'success'){
						self.selectdepartment = res.data.department;
						self.selectleave = res.data.leave_type;
						self.mainaccess = res.header.access;
						logger.logSuccess('Access Granted');
					}else{
						self.mainaccess = res.header.access;
						logger.logError('Access Unauthorized');
					}
				});	
			},
			getYear:function(){
				tmp=[]
				today = new Date()
				yearNow = today.getFullYear()
				yearlast = yearNow - 1
				yearStringNow = yearNow.toString() 
				yearStringlast = yearlast.toString()
				tmp.push({year : yearStringlast})
				tmp.push({year : yearStringNow})
				self.datayears = tmp
				console.log(self.datayears)

			},button:function(a){
				if(self.mainaccess){
					switch(a) {
						case 'search':
						if(self.mainaccess.read == 1){return true}break;
						case 'adjust':
						if(self.mainaccess.read == 1){return true}break;
					}
				}
			},getadjust:function(form,data){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/leave/adjust?key=' + $cookieStore.get('key_api'),
					data : data
				}).then(function(res){
					self.setadjust(form,res.data.data)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Showing Data Adjusment Failed")
				});	
			},setadjust:function(dataA,dataB){
				dataA.vacLeave = dataB.vacLeave;
				dataA.AvailedVacation = dataB.AvailedVacation;
				dataA.offDay = dataB.offDay;
				dataA.total = dataB.total;
				dataA.AvailedOffDay = dataB.AvailedOffDay;
				dataA.Max = dataB.Max;
				dataA.Min = dataB.Min;
			}, hideshow:function(){
				x = $cookieStore.get('access')
				console.log(x)
				if (x == 200) {
					this.hideForm = true
				}else{
					this.hideForm = false
					this.disabledForm = true
					self.setDefault()
				}
			},setDefault:function(){
				x = $cookieStore.get('user_depart')
				y = $cookieStore.get('NotUser')
				self.form = {}
				self.form.department = x
				self.form.employee = y



			},
		}
	})
.controller('leaveEntitCtrl',function(entitlementsFactory,ApiURL,$cookieStore,$scope,GetName){
	var self = this; 
	self.handler = entitlementsFactory;
	self.handler.getselectleave(ApiURL.url + '/api/leave/Emp?key=' + $cookieStore.get('key_api'));
	self.handler.hideshow()
	self.handler.getYear()
	$scope.name = GetName.getname()

})
.controller('editAdjust',function(entitlementsFactory,$modalInstance,$timeout,data,$scope){		
	var self = this;
	self.handler = entitlementsFactory;
	self.form = {}
	self.handler.getadjust(self.form,data)
	self.confirm = function(){
		$modalInstance.dismiss(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss("Close")
	}

	self.slider_toggle = {
		value: 100,
		options: {
			floor: 0,
			ceil: 500
		}
	};
			/*$scope.slider = {
			  value: 150,
			   min: 100,
  				max: 100,
			  options: {
			    floor: 0,
			    ceil: 450
			  }
			};*/
		/*$scope.slider = {
	    value: 50,
		    options: {
		      floor: 0,
		      ceil: 100,
		      step: 1,
		      minLimit: 10,
		      maxLimit: 90
		    }
		};*/
	})
.run(function(entitlementsFactory){
	entitlementsFactory.setheaders();
})


}())
