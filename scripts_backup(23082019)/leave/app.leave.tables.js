(function(){
	
	/** MODULE LEAVE TABLES **/
	angular.module('app.leave.tables',['checklist-model'])
	
	.factory('permissionfactory',function(){
		var self;
		return self = {
			setpermission:function(data){
				self.access = data;
					//////console.log(self.access)
				}	
			}
		})
	
	/** FACTORY SEARCH **/
	.factory('leavetablefactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,$filter,tablevacationfactory,tablesickfactory,tablebereavementfactory,tableenhancefactory,tablematernityfactory,tablepaternityfactory,tablemarriagefactory,permissionfactory){
		var self;

		return self = {
			vacLev: '',
			search: function(){
				//////console.log(self.form)
				if(self.form.for == '1'){ $cookieStore.put('vacLev','expat')}
					if(self.form.for == '2'){ $cookieStore.put('vacLev','local')}
						if(self.form.for == '3'){ $cookieStore.put('vacLev','both') }
				//////console.log(self.vacLev)
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
				data : self.form 
			}).then(function(res){
				$cookieStore.put('form_leave_table',self.form)

				if(self.form.leave_type == 'Vacation Leave'){
					tablevacationfactory.setdata(res.data.data);
					tablevacationfactory.setacvtivebutton('active');
				}else if(self.form.leave_type == 'Sick Leave'){
					tablesickfactory.setdata(res.data.data);
					tablesickfactory.setacvtivebutton('active');
				}else if(self.form.leave_type == 'Bereavement Leave'){
					tablebereavementfactory.setdata(res.data.data);
					tablebereavementfactory.setacvtivebutton('active');
				}else if(self.form.leave_type == 'Enhance Leave'){
					tableenhancefactory.setdata(res.data.data)
					tableenhancefactory.setacvtivebutton('active');
				}else if(self.form.leave_type == 'Maternity Leave'){
					tablematernityfactory.setdata(res.data.data);
					tablematernityfactory.setacvtivebutton('active');
				}else if(self.form.leave_type == 'Paternity Leave'){
					tablepaternityfactory.setdata(res.data.data);
					tablepaternityfactory.setacvtivebutton('active');
				}else if(self.form.leave_type == 'Marriage Leave'){
					tablemarriagefactory.setdata(res.data.data);
					tablemarriagefactory.setacvtivebutton('active');
				}

				logger.logSuccess(res.data.header.message);
			},function(res){
				res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Record Data Failed")
			});
		},setDefaultLeaveType: function(URL){
			$http.get(URL).success(function(res){
				if(res.header.message == 'success'){
						//self.form.latestData = res.sub
						//console.log(res);
						self.checkUser = res.check_user ;
						//console.log(self.checkUser);
						self.checking = res.data;
						self.mainaccess = res.header.access;
						permissionfactory.setpermission(res.header.access);
						self.radio = res.status;
						////////console.log(self.radio)
						logger.logSuccess('Access Granted');
					}else{		
						self.mainaccess = res.header.access;
						logger.logError('Access Unauthorized');
					}
				});	
		},button:function(a){
			if(self.mainaccess){
				switch(a){
					case 'read':
					if(self.mainaccess.read == 1){return true}
						break;
				}
			}
		},role:function(a){
			if(self.radio){
				switch(a){
					case 'local':
					if(self.radio=='local'){return false}
						break;
					case 'expat':
					if(self.radio=='expat'){return false}
						break;
						//case 'hr':

						//break;
					}

				}
			},rolex:function(a){
				if(self.radio){
					switch(a){
						case 'local':
						if(self.radio=='local'){return true}
							break;
						case 'expat':
						if(self.radio=='expat'){return false}
							break;
						//case 'hr':

						//break;
					}		
				}
			},sethideradio:function(form){
				if(form=='Enhance Vacation Leave'){
					//////console.log('to here'
					self.hideradio= true
				}else{
					self.hideradio= true
				}			     	
//hideradio
},check_type : function(){
	self.form.vacLev = $cookieStore.get('vacLev')
}
}
})
	
	/** SEARCH LEAVE TABLES CONTROLLER **/
	.controller('leaveTableController',function(leavetablefactory,$http,ApiURL,$cookieStore){
		var self = this;
		self.handler = leavetablefactory;
		self.handler.setDefaultLeaveType(ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'));

	})
	/** FACTORY VACATION TABLE **/
	.factory('tablevacationfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Leave Rules','leave_rules',25],
					['Years In Service','years',25],
					['Entitled Days Leave','entitled',25],
					['Day Off / On Call','day',25]
					);
				this.setactiveheader(this.headers[0],false)
			},setacvtivebutton:function(active){
				if(active){
					this.mainaccess = permissionfactory.access;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update : function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalVacation',
					controller: 'modaladdVacation',
					controllerAs: 'modalVacation',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp = "add"
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalVacation',
					controller: 'modalEditVacation',
					controllerAs: 'modalVacation',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp = "edit"
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch(a){
						case 'add':
								////console.log('final',data);
								var count =  data.subvacation.length;
								if(count == 0){
									logger.logError('Please fill min 1 for sub data')
								}else{
									$http({
										method : 'POST',
										url : ApiURL.url + '/api/leave/addVacation?key=' + $cookieStore.get('key_api'),
										data : data,
									}).then(function(res){
										self.adddata(res.data.data[0]);
										logger.logSuccess(res.data.header.message);
									},function(res){
										res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Vacation Leave Failed")
									});
								}
								break;
								case 'edit':
								//console.log('masuk');
								var count = data.b.subvacation.length;
								var check_data = 0;
								var i = 0
								for(i; i < count; i++){
									if(data.b.subvacation[i].days == ""){
										check_data =  check_data + 1
									}
									if(data.b.subvacation[i].offday_oncall == ""){
										check_data =  check_data + 1
									}
									if(data.b.subvacation[i].offday_oncall == ""){
										check_data =  check_data + 1
									}
									//console.log('ini',i)
								}
								
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "edit"){
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/updateVacation/' + b.id + '?key=' + $cookieStore.get('key_api'),
											data : data.b
										}).then(function(res){
											///self.form.vacLev = $cookieStore.get('vacLev');
											self.update(res.data.data[0])
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Vacation Leave Failed")
										});
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}			
								break;
							}

						});
			},
			/**	FUNCTION DELETE**/
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/leave/deleteVacation/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Vacation Leave Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Vacation Leave Failed")
				})
			},
			
			/** ADD NEW SUB VACATION **/
			addsubvacation: function(form){
				var a = {};
				a.year ='';
				a.days ='';
				a.offday_oncall ='';
				form.subvacation.push(a);
			},
			
			/** REMOVE SUB VACATION **/
			removesubvacation: function(form,a,x){
				//console.log(x,x.length)
				if(x.length > 1){
					form.subvacation.splice(a,1);
				}else{
					logger.logError("can't delete must leaving 1 sub data");
				}
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					//console.log(res)
					self.checkUser = res.data.data.check_user;
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
						if(self.radio=='local'){return true}
							break;
						case 'expat':
						if(self.radio=='expat'){return true}
							break;
					}
				}
			},getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.jobs = res.data;
					////////console.log(self.jobs)
					//a.form = self.cleanData(data);
					//a.form.department = self.jobs[0];
				});	
			}
			/*,
			cleanData: function(data){
				data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
				data.Department = data.Department.replace("Department : ","");
				data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
				data.StartFrom = data.StartFrom.replace("StartFrom : ","");
				return data
			}*/
		}
	})
/** VACATION TABLES CONTROLLER **/
.controller('vacationTableCtrl',function(tablevacationfactory,$http,ApiURL,$cookieStore){
	var self = this;
	self.handler = tablevacationfactory;
			//self.handler.setDefaultLeaveType(ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'));
		})
/** CONTROLLER ADD MODAL VACATION **/
.controller('modaladdVacation',function(tablevacationfactory,$modalInstance,$timeout,data,time,title,icon,ApiURL,$cookieStore){
	var self = this;
	self.handler = tablevacationfactory;
			//////console.log($cookieStore.get('vacLev'));
			self.title = title;
			self.icon = icon;
			self.form = {};
			self.form.subvacation = [];
			self.form.vacLev = $cookieStore.get('vacLev');
			self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
			self.handler.getlocalexpat()
		})
/** CONTROLLER EDIT MODAL VACATION **/
.controller('modalEditVacation',function(tablevacationfactory,$cookieStore,ApiURL,$modalInstance,$timeout,data,time,title,icon){
	var self = this;
	self.handler = tablevacationfactory;
	self.title = title;
	self.icon = icon;
			//console.log($cookieStore.get('vacLev'),9090);
			data = angular.copy(data);
			//////console.log(data)
			data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
			data.Department = data.Department.replace("Department : ","");
			data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
			data.StartFrom = data.StartFrom.replace("StartFrom : ","");
			//console.log(data)
			self.form = data;
			self.form.vacLev = $cookieStore.get('vacLev');
			self.form = angular.copy(data);
			////console.log(self.form)
			self.save = function(){
				////console.log({a:data,b:self.form});
				$modalInstance.close({a:data,b:self.form});
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
			
		})




/** FACTORY SICK LEAVE TABLE **/
.factory('tablesickfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
	function createheaders(){
		var temp = [];
		for(var i=0;i<arguments.length;i++){
			var x = {};
			x.id = i;
			x.name =arguments[i][0];
			x.sort =arguments[i][1];
			x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
			x.order =false;
			temp.push(x);
		}
		return temp;	
	}
	var self;
	return self = {
		setheaders: function(){
			this.headers = new createheaders(
				['Leave Rules','leave_rules',40],
				['Years In Service','years',30],
				['Entitled Days Leave','entitled_days',30]
				);
			this.setactiveheader(this.headers[0],false)
		},setacvtivebutton:function(active){
			if(active){
				this.mainaccess = permissionfactory.access;
			}
		},button:function(a){
			if(this.mainaccess){
				switch (a) {
					case 'create':
					if(this.mainaccess.create == 1){return true}
						break;
					case 'delete':
					if(this.mainaccess.delete == 1){return true}		
						break;
					case 'update':
					if(this.mainaccess.update == 1){return true}		
						break;
				}
			}
		},adddata: function(a){
			this.maindata.push(a);
		},setdata: function(a){
			this.maindata = a;
		},getdata: function(){
			self.filtered = $filter('filter')(this.maindata,{$:self.search});
			self.select(self.currentPage);
			return self.paginated;
		},update: function(newdata){
				//////console.log('test',newdata)
				///self.form.vacLev = $cookieStore.get('vacLev');
				//console.log(newdata);
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalSick',
					controller: 'modalSick',
					controllerAs: 'modalSick',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp = 'add'
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalSick',
					controller: 'modalEditSick',
					controllerAs: 'modalSick',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp  = "edit"
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				//console.log(b);
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':

						var t = $cookieStore.get('form_leave_table');
						data.form_for = t.for;
						data.form_leave_type = t.leave_type;
						if(data.subsick.length == 0 || data.subsick.length == undefined){
							logger.logError('please fill at least one sub data');
						}else{
							var count = data.subsick.length;
							var check_data = 0;
							var i = 0
							for(i; i < count; i++){
								if(data.subsick[i].days == undefined){
									check_data =  check_data + 1
								}
								if(data.subsick[i].year == undefined){
									check_data =  check_data + 1
								}
									//console.log('ini',i)
								}
								//console.log(self.temp)
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "add"){
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/sickLeave?key=' + $cookieStore.get('key_api'),
											data : data,
										}).then(function(res){
											self.adddata(res.data.data[0]);
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding "+data.form_leave_type+" Failed")
										});
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}
							}


							break;
							case 'edit':
							
							var t = $cookieStore.get('form_leave_table');
							data.b.form_for = t.for;
							data.b.form_leave_type = t.leave_type;

							if(data.b.subsick == undefined){
								logger.logError('please fill at least one sub data');
							}else{
								var count = data.b.subsick.length;
								var check_data = 0;
								var i = 0
								for(i; i < count; i++){
									if(data.b.subsick[i].days == undefined){
										check_data =  check_data + 1
									}
									if(data.b.subsick[i].year == undefined){
										check_data =  check_data + 1
									}
									//console.log('ini',i)
								}
								//console.log(self.temp)
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "edit"){
										//console.log('tes masuk gak');
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/sickUpdate/' + b.id + '?key=' + $cookieStore.get('key_api'),
											data : data.b
										}).then(function(res){
											self.update(res.data.data[0])
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating "+data.form_leave_type+" Failed")
										});	
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}		
								break;
							}}});
			},
			deldata: function(id){

				var t = $cookieStore.get('form_leave_table');				

				$http({
					url : ApiURL.url + '/api/leave/sickDelete/' + id + '/'+t.leave_type+'?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					//logger.logSuccess(res.data.header.message);
					logger.logSuccess('Deleting '+t.leave_type+' Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting "+t.leave_type+" Failed");
				})
			},
			/** add new sub sick **/
			addsubsick: function(form){
				var a = {};
				a.year ='';
				a.days ='';
				form.subsick.push(a);
			},removesubsick: function(form,a,x,y){
				////console.log(form,x,a)
				if(x.length > 1){
					form.subsick.splice(a,1);
				}else{
					logger.logError("can't delete must leaving 1 sub data");
				}
			},getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.jobs = res.data;
				});	
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
						if(self.radio=='local'){return true}
							break;
						case 'expat':
						if(self.radio=='expat'){return true}
							break;
					}
				}
			}
			
		}
	})
/** SICK TABLES CONTROLLER **/
.controller('sickTableCtrl',function(tablesickfactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = tablesickfactory;
})
/** CONTROLLER MODAL SICK **/
.controller('modalSick',function(tablesickfactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,time,title,icon){
	var t = $cookieStore.get('form_leave_table');
	var self = this;
	self.handler = tablesickfactory;
	self.title = t.leave_type;
	self.icon = icon;
	self.form = {};
	self.form.subsick = [];
	self.form.vacLev = $cookieStore.get('vacLev');

	self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
	self.handler.getlocalexpat()
})
/** CONTROLLER MODAL EDIT SICK **/
.controller('modalEditSick',function(tablesickfactory,$cookieStore,ApiURL,$modalInstance,$timeout,data,time,title,icon){
	var self = this;
	self.handler = tablesickfactory;
	self.title = title;
	self.icon = icon;
			//console.log($cookieStore.get('vacLev'));
			data = angular.copy(data);
			data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
			data.Department = data.Department.replace("Department : ","");
			data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
			data.StartFrom = data.StartFrom.replace("StartFrom : ","");
			//self.form.vacLev = $cookieStore.get('vacLev');

			self.form = data;
			self.form = angular.copy(data);
			self.form.vacLev = $cookieStore.get('vacLev');
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close');
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
		})

	// maternity #############
	/** FACTORY MATERNITY LEAVE TABLE **/
	.factory('tablematernityfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Leave Rules','leave_rules',40],
					['Years In Service','years',30],
					['Entitled Days Leave','entitled_days',30]
					);
				
				this.setactiveheader(this.headers[0],false)

			},setacvtivebutton:function(active){
				if(active){
					console.log(this.setactiveheader(this.headers[0],false),"99999999999");
					this.mainaccess = permissionfactory.access;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				//////console.log('test',newdata)
				///self.form.vacLev = $cookieStore.get('vacLev');
				//console.log(newdata);
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalMaternity',
					controller: 'modalMaternity',
					controllerAs: 'modalMaternity',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp = 'add'
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalMaternity',
					controller: 'modalEditMaternity',
					controllerAs: 'modalMaternity',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp  = "edit"
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				//console.log(b);
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':

						var t = $cookieStore.get('form_leave_table');
						data.form_for = t.for;
						data.form_leave_type = t.leave_type;

						if(data.submaternity.length == 0 || data.submaternity.length == undefined){
							logger.logError('please fill at least one sub data');
						}else{
							var count = data.submaternity.length;
							var check_data = 0;
							var i = 0
							for(i; i < count; i++){
								if(data.submaternity[i].days == undefined){
									check_data =  check_data + 1
								}
								if(data.submaternity[i].year == undefined){
									check_data =  check_data + 1
								}
									//console.log('ini',i)
								}
								//console.log(self.temp)
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "add"){
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/sickLeave?key=' + $cookieStore.get('key_api'),
											data : data,
										}).then(function(res){
											self.adddata(res.data.data[0]);
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding "+data.form_leave_type+" Failed")
										});
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}
							}


							break;
							case 'edit':
							
							var t = $cookieStore.get('form_leave_table');
							data.b.form_for = t.for;
							data.b.form_leave_type = t.leave_type;

							if(data.b.submaternity == undefined){
								logger.logError('please fill at least one sub data');
							}else{
								var count = data.b.submaternity.length;
								var check_data = 0;
								var i = 0
								for(i; i < count; i++){
									if(data.b.submaternity[i].days == undefined){
										check_data =  check_data + 1
									}
									if(data.b.submaternity[i].year == undefined){
										check_data =  check_data + 1
									}
									//console.log('ini',i)
								}
								//console.log(self.temp)
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "edit"){
										//console.log('tes masuk gak');
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/sickUpdate/' + b.id + '?key=' + $cookieStore.get('key_api'),
											data : data.b
										}).then(function(res){
											self.update(res.data.data[0])
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating "+data.form_leave_type+" Failed")
										});	
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}		
								break;
							}}});
			},
			deldata: function(id){

				var t = $cookieStore.get('form_leave_table');				

				$http({
					url : ApiURL.url + '/api/leave/sickDelete/' + id + '/'+t.leave_type+'?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					//logger.logSuccess(res.data.header.message);
					logger.logSuccess('Deleting '+t.leave_type+' Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting "+t.leave_type+" Failed");
				})
			},
			/** add new sub sick **/
			addsubmaternity: function(form){
				var a = {};
				a.year ='';
				a.days ='';
				form.submaternity.push(a);
			},removesubmaternity: function(form,a){
				////console.log(form,x,a)
				if(a > 0){
					form.submaternity.splice(a,1);
				}else{
					logger.logError("can't delete must leaving 1 sub data");
				}
			},getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.jobs = res.data;
				});	
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
						if(self.radio=='local'){return true}
							break;
						case 'expat':
						if(self.radio=='expat'){return true}
							break;
					}
				}
			}
			
		}
	})
/** SICK TABLES CONTROLLER **/
.controller('maternityTableCtrl',function(tablematernityfactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = tablematernityfactory;	

})
/** CONTROLLER MODAL MATERNITY **/
.controller('modalMaternity',function(tablematernityfactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,time,title,icon){
	var t = $cookieStore.get('form_leave_table');
	var self = this;
	self.handler = tablematernityfactory;
	self.title = t.leave_type;
	self.icon = icon;
	self.form = {};
	self.form.submaternity = [];
	self.form.vacLev = $cookieStore.get('vacLev');

	self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
	self.handler.getlocalexpat()
})
/** CONTROLLER MODAL EDIT SICK **/
.controller('modalEditMaternity',function(tablematernityfactory,$cookieStore,ApiURL,$modalInstance,$timeout,data,time,title,icon){
	var self = this;
	self.handler = tablematernityfactory;
	self.title = title;
	self.icon = icon;
			//console.log($cookieStore.get('vacLev'));
			data = angular.copy(data);
			data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
			data.Department = data.Department.replace("Department : ","");
			data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
			data.StartFrom = data.StartFrom.replace("StartFrom : ","");
			//self.form.vacLev = $cookieStore.get('vacLev');

			self.form = data;
			self.form = angular.copy(data);
			self.form.vacLev = $cookieStore.get('vacLev');
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close');
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
		})

	// paternity #############
	/** FACTORY PATERNITY LEAVE TABLE **/
	.factory('tablepaternityfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Leave Rules','leave_rules',40],
					['Years In Service','years',30],
					['Entitled Days Leave','entitled_days',30]
					);
				this.setactiveheader(this.headers[0],false)
			},setacvtivebutton:function(active){
				if(active){
					this.mainaccess = permissionfactory.access;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				//////console.log('test',newdata)
				///self.form.vacLev = $cookieStore.get('vacLev');
				//console.log(newdata);
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalPaternity',
					controller: 'modalPaternity',
					controllerAs: 'modalPaternity',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp = 'add'
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalPaternity',
					controller: 'modalEditPaternity',
					controllerAs: 'modalPaternity',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp  = "edit"
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				//console.log(b);
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':

						var t = $cookieStore.get('form_leave_table');
						data.form_for = t.for;
						data.form_leave_type = t.leave_type;

						if(data.subpaternity.length == 0 || data.subpaternity.length == undefined){
							logger.logError('please fill at least one sub data');
						}else{
							var count = data.subpaternity.length;
							var check_data = 0;
							var i = 0
							for(i; i < count; i++){
								if(data.subpaternity[i].days == undefined){
									check_data =  check_data + 1
								}
								if(data.subpaternity[i].year == undefined){
									check_data =  check_data + 1
								}
									//console.log('ini',i)
								}
								//console.log(self.temp)
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "add"){
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/sickLeave?key=' + $cookieStore.get('key_api'),
											data : data,
										}).then(function(res){
											self.adddata(res.data.data[0]);
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding "+data.form_leave_type+" Failed")
										});
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}
							}


							break;
							case 'edit':
							
							var t = $cookieStore.get('form_leave_table');
							data.b.form_for = t.for;
							data.b.form_leave_type = t.leave_type;

							if(data.b.subpaternity == undefined){
								logger.logError('please fill at least one sub data');
							}else{
								var count = data.b.subpaternity.length;
								var check_data = 0;
								var i = 0
								for(i; i < count; i++){
									if(data.b.subpaternity[i].days == undefined){
										check_data =  check_data + 1
									}
									if(data.b.subpaternity[i].year == undefined){
										check_data =  check_data + 1
									}
									//console.log('ini',i)
								}
								//console.log(self.temp)
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "edit"){
										//console.log('tes masuk gak');
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/sickUpdate/' + b.id + '?key=' + $cookieStore.get('key_api'),
											data : data.b
										}).then(function(res){
											self.update(res.data.data[0])
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating "+data.form_leave_type+" Failed")
										});	
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}		
								break;
							}}});
			},
			deldata: function(id){

				var t = $cookieStore.get('form_leave_table');

				$http({
					url : ApiURL.url + '/api/leave/sickDelete/' + id + '/'+t.leave_type+'?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					//logger.logSuccess(res.data.header.message);
					logger.logSuccess('Deleting '+t.form_leave_type+' Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting "+t.form_leave_type+" Failed");
				})
			},
			/** add new sub sick **/
			addsubpaternity: function(form){
				var a = {};
				a.year ='';
				a.days ='';
				form.subpaternity.push(a);
			},removesubpaternity: function(form,a){
				if(a > 0){
					form.subpaternity.splice(a,1);
				}else{
					logger.logError("can't delete must leaving 1 sub data");
				}
			},getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.jobs = res.data;
				});	
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
						if(self.radio=='local'){return true}
							break;
						case 'expat':
						if(self.radio=='expat'){return true}
							break;
					}
				}
			}
			
		}
	})
/** SICK TABLES CONTROLLER **/
.controller('paternityTableCtrl',function(tablepaternityfactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = tablepaternityfactory;	
})
/** CONTROLLER MODAL SICK **/
.controller('modalPaternity',function(tablepaternityfactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,time,title,icon){
	var t = $cookieStore.get('form_leave_table');
	var self = this;
	self.handler = tablepaternityfactory;
	self.title = t.leave_type;
	self.icon = icon;
	self.form = {};
	self.form.subpaternity = [];
	self.form.vacLev = $cookieStore.get('vacLev');

	self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
	self.handler.getlocalexpat()
})
/** CONTROLLER MODAL EDIT SICK **/
.controller('modalEditPaternity',function(tablepaternityfactory,$cookieStore,ApiURL,$modalInstance,$timeout,data,time,title,icon){
	var self = this;
	self.handler = tablepaternityfactory;
	self.title = title;
	self.icon = icon;
			//console.log($cookieStore.get('vacLev'));
			data = angular.copy(data);
			data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
			data.Department = data.Department.replace("Department : ","");
			data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
			data.StartFrom = data.StartFrom.replace("StartFrom : ","");
			//self.form.vacLev = $cookieStore.get('vacLev');

			self.form = data;
			self.form = angular.copy(data);
			self.form.vacLev = $cookieStore.get('vacLev');
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close');
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
		})

	// MARRIAGE #############
	/** FACTORY MARRIAGE LEAVE TABLE **/
	.factory('tablemarriagefactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Leave Rules','leave_rules',40],
					['Years In Service','years',30],
					['Entitled Days Leave','entitled_days',30]
					);
				this.setactiveheader(this.headers[0],false)
			},setacvtivebutton:function(active){
				if(active){
					this.mainaccess = permissionfactory.access;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				//////console.log('test',newdata)
				///self.form.vacLev = $cookieStore.get('vacLev');
				//console.log(newdata);
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalMarriage',
					controller: 'modalMarriage',
					controllerAs: 'modalMarriage',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp = 'add'
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalMarriage',
					controller: 'modalEditMarriage',
					controllerAs: 'modalMarriage',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp  = "edit"
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				//console.log(b);
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':

						var t = $cookieStore.get('form_leave_table');
						data.form_for = t.for;
						data.form_leave_type = t.leave_type;

						if(data.submarriage.length == 0 || data.submarriage.length == undefined){
							logger.logError('please fill at least one sub data');
						}else{
							var count = data.submarriage.length;
							var check_data = 0;
							var i = 0
							for(i; i < count; i++){
								if(data.submarriage[i].days == undefined){
									check_data =  check_data + 1
								}
								if(data.submarriage[i].year == undefined){
									check_data =  check_data + 1
								}
									//console.log('ini',i)
								}
								//console.log(self.temp)
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "add"){
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/sickLeave?key=' + $cookieStore.get('key_api'),
											data : data,
										}).then(function(res){
											self.adddata(res.data.data[0]);
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding "+data.form_leave_type+" Failed")
										});
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}
							}


							break;
							case 'edit':
							
							var t = $cookieStore.get('form_leave_table');
							data.b.form_for = t.for;
							data.b.form_leave_type = t.leave_type;

							if(data.b.submarriage == undefined){
								logger.logError('please fill at least one sub data');
							}else{
								var count = data.b.submarriage.length;
								var check_data = 0;
								var i = 0
								for(i; i < count; i++){
									if(data.b.submarriage[i].days == undefined){
										check_data =  check_data + 1
									}
									if(data.b.submarriage[i].year == undefined){
										check_data =  check_data + 1
									}
									//console.log('ini',i)
								}
								//console.log(self.temp)
								var arg = count - 1   
								if(check_data == 0){
									//console.log('arg',check_data)
									if(check_data == 0 && self.temp ==  "edit"){
										//console.log('tes masuk gak');
										$http({
											method : 'POST',
											url : ApiURL.url + '/api/leave/sickUpdate/' + b.id + '?key=' + $cookieStore.get('key_api'),
											data : data.b
										}).then(function(res){
											self.update(res.data.data[0])
											logger.logSuccess(res.data.header.message);
										},function(res){
											res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating "+data.form_leave_type+" Failed")
										});	
									}else{
										logger.logError('please fill all the blank input')
									}
								}else{
									logger.logError('please fill all the blank input')
								}		
								break;
							}}});
			},
			deldata: function(id){

				var t = $cookieStore.get('form_leave_table');

				$http({
					url : ApiURL.url + '/api/leave/sickDelete/' + id + '/'+t.leave_type+'?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					//logger.logSuccess(res.data.header.message);
					logger.logSuccess('Deleting '+t.leave_type+' Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting "+t.leave_type+" Failed");
				})
			},
			/** add new sub sick **/
			addsubmarriage: function(form){
				var a = {};
				a.year ='';
				a.days ='';
				form.submarriage.push(a);
			},removesubmarriage: function(form,a){
				
				if(a > 0){
					form.submarriage.splice(a,1);
				}else{
					logger.logError("can't delete must leaving 1 sub data");
				}
			},getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.jobs = res.data;
				});	
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
						if(self.radio=='local'){return true}
							break;
						case 'expat':
						if(self.radio=='expat'){return true}
							break;
					}
				}
			}
			
		}
	})
/** SICK TABLES CONTROLLER **/
.controller('marriageTableCtrl',function(tablemarriagefactory,ApiURL,$cookieStore){
	var self = this;
	self.handler = tablemarriagefactory;	
})
/** CONTROLLER MODAL SICK **/
.controller('modalMarriage',function(tablemarriagefactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,time,title,icon){
	var t = $cookieStore.get('form_leave_table');
	var self = this;
	self.handler = tablemarriagefactory;
	self.title = t.leave_type;
	self.icon = icon;
	self.form = {};
	self.form.submarriage = [];
	self.form.vacLev = $cookieStore.get('vacLev');

	self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
	self.handler.getlocalexpat()
})
/** CONTROLLER MODAL EDIT SICK **/
.controller('modalEditMarriage',function(tablemarriagefactory,$cookieStore,ApiURL,$modalInstance,$timeout,data,time,title,icon){
	var self = this;
	self.handler = tablemarriagefactory;
	self.title = title;
	self.icon = icon;
			//console.log($cookieStore.get('vacLev'));
			data = angular.copy(data);
			data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
			data.Department = data.Department.replace("Department : ","");
			data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
			data.StartFrom = data.StartFrom.replace("StartFrom : ","");
			//self.form.vacLev = $cookieStore.get('vacLev');

			self.form = data;
			self.form = angular.copy(data);
			self.form.vacLev = $cookieStore.get('vacLev');
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close');
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
		})



/** FACTORY BEREAVEMENT TABLE **/
.factory('tablebereavementfactory',function(filterFilter ,pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,permissionfactory){
	function createheaders(){
		var temp = [];
		for(var i=0;i<arguments.length;i++){
			var x = {};
			x.id = i;
			x.name =arguments[i][0];
			x.sort =arguments[i][1];
			x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
			x.order =false;
			temp.push(x);
		}return temp;	
	}
	var self;
	return self = {
		setheaders: function(){
			this.headers = new createheaders(
				['Leave Rules','leave_rules',25],
				['Relation to Deceased','relation',25],
				['Entitled Days Leave','entitled',25],
				['Days With Pay','day',25]
				);
			this.setactiveheader(this.headers[0],false)
		},setacvtivebutton:function(active){
			if(active){
				this.mainaccess = permissionfactory.access;
			}
		},button:function(a){
			if(this.mainaccess){
				switch (a) {
					case 'create':
					if(this.mainaccess.create == 1){return true}
						break;
					case 'delete':
					if(this.mainaccess.delete == 1){return true}		
						break;
					case 'update':
					if(this.mainaccess.update == 1){return true}		
						break;
				}
			}
		},adddata: function(a){
			this.maindata.push(a);
		},setdata: function(a){
			this.maindata = a;
		},getdata: function(){
			self.filtered = $filter('filter')(this.maindata,{$:self.search});
			self.select(self.currentPage);
			return self.paginated;
		},update: function(newdata){
				//////console.log(newdata)
				for(a in self.maindata){
					if(self.maindata[a].id == newdata[0].id){
						self.maindata[a] = newdata[0];
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalBereavement',
					controller: 'modalBereavement',
					controllerAs: 'modalBereavement',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp =  "add"
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalBereavement',
					controller: 'modalEditBereavement',
					controllerAs: 'modalBereavement',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							self.temp = "edit"
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':
							//console.log(data);
							if(data.subbereavement.length == 0 || data.subbereavement == "undefined"){
								logger.logError('Please insert at least one item')
							}else{
								var check_data = 0
								var i = 0
								var count = data.subbereavement.length;

								for(i; i < count; i++){
									if(data.subbereavement[i].days == ""){
										check_data = check_data + 1
									}
									if(data.subbereavement[i].days_with_pay == ""){
										check_data = check_data + 1
									}
									if(data.subbereavement[i].relation_to_deceased == ""){
										check_data = check_data + 1
									}

								}
								
								var total  = count - 1		
								//console.log(self.temp, check_data)						
								if(self.temp == "add" && check_data == 0){
									$http({
										method : 'POST',
										url : ApiURL.url + '/api/leave/bereavementLeave?key=' + $cookieStore.get('key_api'),
										data : data,
									}).then(function(res){
										self.adddata(res.data.data[0]);
										logger.logSuccess(res.data.header.message);
									},function(res){
										res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Bereavement Failed")
									});
								}else{
									logger.logError('please fill all the blank data')
								}
							}
							break;

							case 'edit':
							//console.log(data);
							if(data.b.subbereavement.length == 0 || data.b.subbereavement == "undefined"){
								logger.logError('Please insert at least one item')
							}else{
								var check_data = 0
								var i = 0
								var count = data.b.subbereavement.length;
								for(i; i < count-1; i++){
									if(data.b.subbereavement[i].days == ""){
										check_data = check_data + 1
									}
									if(data.b.subbereavement[i].days_with_pay == ""){
										check_data = check_data + 1
									}
									if(data.b.subbereavement[i].relation_to_deceased == ""){
										check_data = check_data + 1
									}
								}
								
								var total  = count - 1								
								if(self.temp == "edit" && check_data == 0){
									$http({
										method : 'POST',
										url : ApiURL.url + '/api/leave/bereavementUpdate/' + b.id + '?key=' + $cookieStore.get('key_api'),
										data : data.b
									}).then(function(res){
										self.update(res.data.data)
										logger.logSuccess(res.data.header.message);
									},function(res){
										res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Bereavement Failed")
									});	
								}else{
									logger.logError('please fill all the blank data')
								}
							}		
							break;
						}

					});
			},

			/**	FUNCTION DELETE**/
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/leave/bereavementDelete/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					//logger.logSuccess(res.data.header.message);
					logger.logSuccess('Deleting Bereavement Leave Success');
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Vacation Leave Failed")
				})
			},
			
			addsubbereavement: function(form){
				var a = {};
				a.relation_to_deceased ='';
				a.days ='';
				a.days_with_pay='';
				form.subbereavement.push(a);
			},removesubbereavement: function(form,a){
				if(a > 0){
					form.subbereavement.splice(a,1);
				}
			},
			getdepartment : function(URL){
				$http.get(URL).success(function(res){
					self.data = res.data;
				});	
			},getlocalexpat:function(){
				$http({
					url : ApiURL.url + '/api/leave/viewVacation?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					self.radio = res.data.status;
				})
			},localexpat:function(a){
				if(self.radio){
					switch(a){
						case 'local':
						if(self.radio=='local'){return true}
							break;
						case 'expat':
						if(self.radio=='expat'){return true}
							break;
					}
				}
			}
			
		}
	})
/** BEREAVEMENT TABLES CONTROLLER **/
.controller('bereavementTableCtrl',function(tablebereavementfactory,$http){
	var self = this;
	self.handler = tablebereavementfactory;
	self.handler.setdata([])
})
/** CONTROLLER MODAL BEREAVEMENT **/
.controller('modalBereavement',function(tablebereavementfactory,$modalInstance,$timeout,data,time,title,icon,ApiURL,$cookieStore){
	var self = this;
	self.handler = tablebereavementfactory;
	self.handler.setdata([])

	self.title = title;
	self.icon = icon;
	self.form = {};
	self.form.subbereavement = [];
	self.form.vacLev = $cookieStore.get('vacLev')
	self.save = function(){
		$modalInstance.close(self.form);
				//////console.log(self.form)
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
			self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			self.handler.getlocalexpat()
			
		})
/** CONTROLLER MODAL EDIT BEREAVEMENT **/
.controller('modalEditBereavement',function(tablebereavementfactory,ApiURL,$cookieStore ,$modalInstance,$timeout,data,time,title,icon){
	var self = this;
	self.handler = tablebereavementfactory;
	self.handler.setdata([])

	self.title = title;
	self.icon = icon;

	data = angular.copy(data);
	data.Appliedfor = data.Appliedfor.replace("Applied For : ","");
	data.Department = data.Department.replace("Department : ","");
	data.Entitlementdays = data.Entitlementdays.replace("Entitlementdays : ",""); 
	data.StartFrom = data.StartFrom.replace("StartFrom : ","");

	self.form = data;
	self.form = angular.copy(data);
	self.form.vacLev = $cookieStore.get('vacLev')
			//////console.log(self.form)
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.cancel = function(){
				$modalInstance.dismiss('close');
			}
			$timeout(function(){
				self.handler.getdepartment(ApiURL.url + '/api/leave/vacation?key=' + $cookieStore.get('key_api'));
			},100)
			self.handler.getlocalexpat()
			
		})





/** FACTORY EHANCED TABLE **/
.factory('tableenhancefactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,permissionfactory){
	function createheaders(){
		var temp = [];
		for(var i=0;i<arguments.length;i++){
			var x = {};
			x.id = i;
			x.name =arguments[i][0];
			x.sort =arguments[i][1];
			x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
			x.order =false;
			temp.push(x);
		}
		return temp;	
	}
	var self;
	return self = {
		setheaders: function(){
			this.headers = new createheaders(
				['Job Title','title',50],
				['Entitled Enhance Days','relation',50]
				);
			this.setactiveheader(this.headers[0],false)
		},setacvtivebutton:function(active){
			if(active){
				this.mainaccess = permissionfactory.access;
			}
		},button:function(a){
				//console.log(permissionfactory.access.read)
				if(permissionfactory.access){
					switch (a) {
						case 'create':
						if(permissionfactory.access.create == 1){return true}
							break;
						case 'delete':
						if(permissionfactory.access.delete == 1){return true}		
							break;
						case 'update':
						if(permissionfactory.access.update == 1){return true}		
							break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				//////console.log(newdata)
				for(a in self.maindata){
					if(self.maindata[a].id == newdata[0].id){
						self.maindata[a] = newdata[0];
					}

				}
			},
			setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalenhanced',
					controller: 'AddmodalEnchanced',
					controllerAs: 'modalenhanced',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalenhanced',
					controller: 'modalEditEnchanced',
					controllerAs: 'modalenhanced',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/enchancedLeave?key=' + $cookieStore.get('key_api'),
							data : data,
						}).then(function(res){
									//self.adddata(res.data.data);
									self.setdata(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Enhanced Failed")
								});
						break;
						case 'edit':
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/leave/updateEnchanced/' + b.id + '?key=' + $cookieStore.get('key_api'),
							data : data.b
						}).then(function(res){
							self.setdata(res.data.data)
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Enhanced Failed")
						});			
						break;
					}
					
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/leave/deleteEnchanced/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Enhanced Leave Failed")
				})
			},
			addsubenhanced: function(form){
				//////console.log(form)
				var a = {};
				a.title ='';
				a.entitled_enhanced_days_leave='';
				form.subenhanced.push(a);
			},
			removesubenhanced: function(form,a){
				if(a > 0){
					form.subenhanced.splice(a,1);
				}
			},
			getjobtitle : function(URL){
				$http.get(URL).success(function(res){
					self.data = res.data;
				});	
			},getedit : function(URL,data,form){
				$http.get(URL).success(function(res){
					self.data = res.data;
					data = angular.copy(data)
					form.subenhanced[0] = data;
				});	
			},
			
		}
	})
/** ENCHANCED TABLES CONTROLLER **/
.controller('enchanceTableCtrl',function(tableenhancefactory,$http){
	var self = this;

	self.handler = tableenhancefactory;
	self.handler.setdata([])

})
/** CONTROLLER ENCHANCED LEAVE **/
.controller('AddmodalEnchanced',function(tableenhancefactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,time,title,icon){
	var self = this;
	self.handler = tableenhancefactory;
	self.handler.setdata([])

	self.title = title;
	self.icon = icon;
	self.form = {};
	self.form.subenhanced = [];
	self.handler.getjobtitle(ApiURL.url + '/api/leave/viewJobTitle?key=' + $cookieStore.get('key_api'));
	self.save = function(){
		$modalInstance.close(self.form);
	}
	self.cancel = function(){
		$modalInstance.dismiss('close')
	}
})
/** CONTROLLER MODAL EDIT ENCHANCED **/
.controller('modalEditEnchanced',function(tableenhancefactory,$modalInstance,$timeout,data,time,title,icon,ApiURL,$cookieStore ){
	var self = this;
	self.handler = tableenhancefactory;
	self.handler.setdata([])

	self.title = title;
	self.icon = icon;
	self.form = {};
	self.form.subenhanced = [];
	self.handler.getedit(ApiURL.url + '/api/leave/viewJobTitle?key=' + $cookieStore.get('key_api'),data,self.form);
	self.save = function(){
		$modalInstance.close({a:data,b:self.form});
	}
	self.cancel = function(){
		$modalInstance.dismiss('close');
	}
})


/** FUNCTION SET HEADER TABLES VACATION **/
.run(function(tablevacationfactory){
	tablevacationfactory.setheaders();
})

/** FUNCTION SET HEADER TABLES SICK **/
.run(function(tablesickfactory){
	tablesickfactory.setheaders();
})

/** FUNCTION SET HEADER TABLES SICK **/
.run(function(tablebereavementfactory){
	tablebereavementfactory.setheaders();
})

/** FUNCTION SET HEADER TABLES ENCHANCED **/
.run(function(tableenhancefactory){
	tableenhancefactory.setheaders();
})

/** FUNCTION SET HEADER TABLES MATERNITY **/
.run(function(tablematernityfactory){
	tablematernityfactory.setheaders();
})

/** FUNCTION SET HEADER TABLES PATERNITY **/
.run(function(tablepaternityfactory){
	tablepaternityfactory.setheaders();
})

/** FUNCTION SET HEADER TABLES MARRIAGE **/
.run(function(tablemarriagefactory){
	tablemarriagefactory.setheaders();
})

}())

