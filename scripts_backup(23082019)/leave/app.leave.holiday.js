var appLeaveHoliday = angular.module("app.leave.holiday",["checklist-model"]);

appLeaveHoliday.factory('permissionholiday', function() {
	var self;
		return self = {
			data:function(a){
				self.access = a;
			}
		}
});

appLeaveHoliday.controller("holidayCtrl",function($scope,$filter,$http,$modal,$log,$cookieStore,ApiURL,$routeParams,filterFilter,logger,permissionholiday) {		
		
			/** FUNCTION GET ALL DATA **/

			const dateSetMin = new Date()
			dateSetMin.setMonth(0)
			dateSetMin.setDate(1) 
			$scope.dateMin = new Date(dateSetMin.getFullYear(),dateSetMin.getMonth(),dateSetMin.getDate());
			// console.log(dateMin)
		

			const dateSetMax = new Date()
			dateSetMax.setMonth(11)
			dateSetMax.setDate(31)
			$scope.dateMax = new Date((dateSetMax.getFullYear()+1), dateSetMax.getMonth(), dateSetMax.getDate())
			// console.log(dateMax)

			$http({
				method : 'GET',
				url : ApiURL.url + '/api/timekeeping/holiday-setup?key=' + $cookieStore.get('key_api')
			}).then(function(res){
				if(res.data.header.message == "Unauthorized"){
					$scope.dataaccess(res.data.header.access);
					logger.logError("Access Unauthorized");
				}else{
					$scope.check = {currentPageStores:[]}
					$scope.dataaccess(res.data.header.access);
					permissionholiday.data(res.data.header.access);
					if(res.data.data == null){
						$scope.formData = '';
					}else{
						$scope.formData = res.data.data[0]
					}
					var end, start;
					start = 0;
					$scope.stores = res.data.data;
					$scope.filteredStores = res.data.data;
					if($scope.filteredStores){
							end = start + $scope.numPerPageOpt[2];
							$scope.currentPageStores = $scope.filteredStores.slice(start, end);
							$scope.select($scope.currentPage);
					}else{
							$scope.filteredStores = [];
					}
					logger.logSuccess(res.data.header.message);
				}
			},function(res){
				res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Holidays Failed");
			});
			$scope.dataaccess = function(data){
				this.access = data;
			};
			$scope.button = function(a){
				if(this.access){
					switch(a){
						case 'read':
							if(this.access.read == 1){return true}
						break;
						case 'create':
							if(this.access.create == 1){return true}
						break;
						case 'update':
							if(this.access.update == 1){return true}
						break;
						case 'delete':
							if(this.access.delete == 1){return true}
						break;
					}
				}
			};
			$scope.open = function(size) {
				var modal;
				modal = $modal.open({
					templateUrl: "addHoliday.html",
					controller: "holModalCtrl",
					backdrop : "static",
					size: size,
					resolve: {
						items: function(){
							return;
						}
					}
				});
				modal.result.then(function (newstudent) {
					if($scope.currentPageStores){
						$scope.currentPageStores.push(newstudent);
					}else{
						$scope.currentPageStores = [];
						$scope.currentPageStores.push(newstudent);
					}
				});
			};
			$scope.edit = function(size,store){
				var modal;
				modal = $modal.open({
					templateUrl: "EditHoliday.html",
					controller: "EditHolidayCtrl",
					backdrop : "static",
					size: size,
					resolve: {
						items: function(){
							return store;
						}
					}
				});
			};
				
				
			/** FUNCTION DELETE **/	
			$scope.remove = function(id){
				$http({
					method : 'DELETE',
					url : ApiURL.url +'/api/timekeeping/holiday-setup/' + id + '?key=' + $cookieStore.get('key_api')
				}).success(function(data, status, header){
					var i;
						for( i = 0; i < id.length; i++) {
							$scope.currentPageStores = filterFilter($scope.currentPageStores, function (store) {
								return store.id != id[i];
							});
						};
					$scope.check.currentPageStores = [];
					logger.logSuccess(data.header.message);
				}).error(function(data, status, header) {
					logger.logError(data.header.message);
				});
			};


			/** FUNCTION SWITCH **/
			$scope.checkstuff = function(data){
				////console.log(data)
				if(data  == 'day'){
					$scope.day_x  = true;
					$scope.on_x = false; 
					$scope.repeat_day = ' ';
				}else{
					$scope.day_x  = false;
					$scope.on_x = true;
					$scope.day =  ' ';
					$scope.week = ' ';
				}
			}

			
			
			/** GET ALL DATA HOLIDAYS LIST WITH SEARCHING **/
			$scope.view = function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/timekeeping/search?key=' + $cookieStore.get('key_api'),
					headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
					data : $.param($scope.formData)   
				}).then(function(res){
					$scope.check = {currentPageStores:[]}
					var end, start;
					start =0;
					$scope.stores = res.data.data;
					$scope.filteredStores = res.data.data;
					if($scope.filteredStores){
							end = start + $scope.numPerPageOpt[2];
							$scope.currentPageStores = $scope.filteredStores.slice(start, end);
							$scope.select($scope.currentPage);
					}else{
							$scope.filteredStores = [];
							end = start + $scope.numPerPageOpt[2];
							$scope.currentPageStores = $scope.filteredStores.slice(start, end);
							$scope.select($scope.currentPage);
					}
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Holidays Failed")
				});
			};
		
			/** FUNCTION TABLE BIOMETRIC **/ 
			var init;
            $scope.numPerPageOpt = [3, 5, 10, 20]; 
            $scope.numPerPage = $scope.numPerPageOpt[2]; 
            $scope.currentPage = 1; 
            $scope.currentPageStores = []; 
            $scope.searchKeywords = ""; 
            $scope.filteredStores = []; 
            $scope.row = "" ;
            $scope.stores =[];
            $scope.select = function(page) {
                 var end, start;
                 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
            };
            $scope.onFilterChange = function() {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
            };
            $scope.onNumPerPageChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.onOrderChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.search = function() {
                return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
            };
            $scope.order = function(rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
            };
            init = function() {
                return $scope.search(), $scope.select($scope.currentPage)
            };
			
			
			
			
			
				
});


appLeaveHoliday.controller("holModalCtrl",function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$filter) {
				
				var s = new Date();
				var t =  $filter('date')(s,'yyyy');
				$scope.year = t;
		
				if(items){
					$scope.formData = items;
				}
				$scope.day = 'day';
				
				function getDaysInMonth(month) {
					var s = new Date();
					var t =  $filter('date')(s,'yyyy');
					var date = new Date(t, month, 1);
					var days = [];
					while (date.getMonth() === month) {
						days.push(new Date(date));
						date.setDate(date.getDate() + 1);
					}
						return days;
				}
				$scope.$watch('$scope.testMonth', function() {						
					$scope.testMonth = function(){
						var bln = parseInt($scope.formData.month - 1);
						$scope.datas = getDaysInMonth(bln);
					};
				});
				
				/** FUNCTION SWITCH **/
				$scope.checkstuff = function(data){
					////console.log(data)
					if(data  == 'day'){
						$scope.day_x  = false;
						$scope.on_x = true; 
						$scope.formData.day =  ' ';
						$scope.formData.week = ' ';
					}else{
						$scope.day_x  = true;
						$scope.on_x = false;
						$scope.formData.repeat_day = ' ';
					}
				}




				$scope.save = function(){
					if($scope.formData.repeat_annually == undefined){
						$scope.formData.repeat_annually = 'no'
					}
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/timekeeping/holiday-setup?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData,$scope.formData.repeat_annually)   
					}).success(function(data){
						////console.log(data)
						var temp = {};
							temp = $scope.formData;
							temp.id = data.data.id;
							temp.date = data.data.date;
						$modalInstance.close(temp);
						logger.logSuccess(data.header.message);
					}).error(function(data) {
						////console.log(data,"ERROR")
						data.header ? logger.logError(data.header.message) : logger.logError("Adding Data Holidays Failed");
					});
				};
				
				$scope.cancel = function() {
					$modalInstance.dismiss("cancel")
				}
});

appLeaveHoliday.controller("EditHolidayCtrl", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,Upload,$filter,permissionholiday) {
				
				// DUPLICATE SCOPE
				var a = String(items.name);
				var b = String(items.date);
				var c = String(items.repeat_annually);
				var d = String(items.type);
				var e = String(items.month);
				var f = String(items.day);
				var g = String(items.repeat_day);
				var h = String(items.week);
				if(items){
					$scope.formData = items;
				}
				// FUNCTION GET MONTH WHEN EDIT
				$scope.datas = [];
				$scope.$watch('items', function() {
					if($scope.formData.month){
						var bln = parseInt($scope.formData.month);
						
						var temp = getDaysInMonth(bln);
						var a = [];
						
						for(a in temp){
							var b = temp[a];
						
							var c = $filter('date')(b,'dd');
							//console
							$scope.datas.push(c);
						}
					}

					if($scope.formData.day !==  "" &&  $scope.formData.week !== "")
					{
						//console.log('1')
						$scope.formData.repeat_day = ' ';
					}



				});

				/** FUNCTION SWITCH **/
				$scope.checkstuff = function(data){
					//////console.log(data)
					if(data  == 'day'){
						$scope.day_x  = false;
						$scope.on_x = true; 
						$scope.formData.day =  ' ';
						$scope.formData.week = ' ';
					}else{
						$scope.day_x  = true;
						$scope.on_x = false;
						$scope.formData.repeat_day = ' ';
					}
				}

				// FUNCTION GET YEAR
				var s = new Date();
				var t =  $filter('date')(s,'yyyy');
				$scope.year = t;
				// FUNCTION GET DATE IN MONTH
				function getDaysInMonth(month) {
					var s = new Date();
					var t =  $filter('date')(s,'yyyy');
					
					var date = new Date(t, month, 1);
					var days = [];
					while (date.getMonth() === month) {
						days.push(new Date(date));
						date.setDate(date.getDate() + 1);
					}
					 return days;
				}
				// FUNCTION GET MONTH WHEN INPUT
				$scope.$watch('$scope.formData.repeat_day', function() {						
					$scope.testMonth = function(){
						$scope.datas = [];
						var bln = parseInt($scope.formData.month - 1);
						var temp = getDaysInMonth(bln);
						var a = [];
						for(a in temp){
							var b = temp[a];
							var c = $filter('date')(b,'dd');
							$scope.datas.push(c);
						}
					};
				});
				
				
				/** FUNCTION SAVE **/
				$scope.save = function(){
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/timekeeping/holiday-setup/' + items.id + '?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).then(function(res){
						var temp = {};
							temp = $scope.formData;
							$modalInstance.close(temp);
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header.message ? logger.logError(res.data.header.message) : logger.logError("Adding Work Shift Failed")
					});
			
				};
				/** FUNCTION CANCEL WHEN EDIT **/
				$scope.cancel = function() {
					$scope.formData.name = a;
					$scope.formData.date = b;
					$scope.formData.repeat_annualy = c;
					$scope.formData.device_type = d;
					$scope.formData.month = e;
					$scope.formData.day = f;
					$scope.formData.repeat_day = g;
					$scope.formData.week = h;
					$modalInstance.dismiss("cancel")
				};
				
				$scope.hide = function(a){
					var access  = permissionholiday.access;				
					if(access){
						switch(a){
							case 'update':
								if(access.update == 1){
									return true
								}
							break;
						}
					}
				};
});

/*	
		function getDaysInMonth(month, year) {
         // Since no month has fewer than 28 days
         var date = new Date(year, month, 1);
         var days = [];
         ////console.log('month', month, 'date.getMonth()', date.getMonth())
         while (date.getMonth() === month) {
            days.push(new Date(date));
            date.setDate(date.getDate() + 1);
         }
         return days;
		}
        
		////console.log(getDaysInMonth(8, 2015))
*/    
		

