(function(){
			
	angular.module('app.config.mail',['checklist-model'])

	.factory('mailfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		var self;
			return self = {
				getdataemail:function(URL){
					$http.get(URL).success(function(res){
						if(res.header.message == "Unauthorized"){
							self.getpermission(res.header.access);
							logger.logError("Access Unauthorized");
						}else{
							self.setdata(res.data);
							self.getpermission(res.header.access);
							logger.logSuccess(res.header.message);
						}
					});	
				},getpermission:function(active){
					if(active){
						this.mainaccess = active;
					}
				},hiden:function(a){
					if(this.mainaccess){
						switch (a) {
							case 'update':
								if(this.mainaccess.update == 1){return true}
							break;
						}
					}
				},setdata:function(a){
					self.form = a;
				},save:function(){
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/config/mail/update?key=' + $cookieStore.get('key_api'),
						data : self.form
					}).then(function(res){
						self.form(res.data.data)
						self.button = false;
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Email Setting Failed")
					});	
				},sent:function(){
					self.form.test = 'test';
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/config/mail/update/' + self.form.test + '?key=' + $cookieStore.get('key_api'),
						data : self.form
					}).then(function(res){
						//self.update(res.data.data[0])
						self.button = false;
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Email Setting Failed")
					});
				}	
			}
	})

	.controller('emailSettingCtrl',function(mailfactory,ApiURL,$cookieStore){
		var self = this;
			self.handler = mailfactory;
			self.handler.getdataemail(ApiURL.url + '/api/config/mail?key=' + $cookieStore.get('key_api'))
	})

}())



/* var appConfigMail = angular.module("app.config.mail",[]);
appConfigMail.controller("emailSettingCtrl", ["logger","$cookieStore","ApiURL","$scope","$routeParams","$http", function(logger,$cookieStore,api,$scope,$routeParams,$http) {
		
		$http.get(api.url + '/api/config/mail?key=' + $cookieStore.get('key_api') ).success(function(res){
				
			$scope.formData = res.data;
			
			$scope.save = function(){				
				$http({
					method : 'POST',
					url : api.url + '/api/config/mail/update?key=' + $cookieStore.get('key_api'),
					headers : { 'Content-Type' : 'application/form-data', },
					data : JSON.stringify($scope.formData)     
				}).success(function(data, status, header, config) {
					
						var temp = {};
							temp = $scope.formData;
							$scope.button = false;
							
					logger.logSuccess(data.header.message);
				}).error(function(data, status, headers, config) {
					logger.logError(data.header.message);
				});
					
			};
			
			
			$scope.sent = function(){
				
				$scope.test = 'test';
				
				$http({
					method : 'POST',
					url : api.url + '/api/config/mail/update/' + $scope.test + '?key=' + $cookieStore.get('key_api'),
					headers : { 'Content-Type' : 'application/form-data', },
					data : JSON.stringify($scope.formData)     
				}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData.to;
					logger.logSuccess(data.header.message);
				}).error(function(data, status, headers, config) {
					logger.logError(data.header.message);
				});
					
			};

					
		}).success(function(data, status, header, config) {
				$scope.role = true;
				logger.logSuccess(data.header.message);
		}).error(function(data, status, headers, config) {
				$scope.role = false;
				logger.logError(data.header.message);
		});	
		
		
		
	
		
		
		
		$scope.reset = function(){
			$scope.formData = [];
		}; 
}]);   
 */