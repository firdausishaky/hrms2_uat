(function(){
	
	angular.module("app.config.ldap",[])
	
	.factory('ldapfactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore){
		var self;
		return self = {
			setFormdata: function(URL){
				$http.get(URL).success(function(res){
					if(res.header.message == "Unauthorized"){
						self.getpermission(res.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.setdata(res.data);
						self.getpermission(res.header.access);
						logger.logSuccess(res.header.message);
					}
				});	
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},hiden:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'update':
							if(this.mainaccess.update == 1){return true}
						break;
					}
				}
			},setdata:function(data){
				self.formData = data;
			},save: function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/config/ldap/update?key=' + $cookieStore.get('key_api'),
					data : self.form
				}).then(function(res){
					self.button = false;
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Connection failed check your configure")
				});
			}
		}
	})
	.controller("ldapCtrl",function(ldapfactory,ApiURL,$cookieStore){
		var self = this;
			self.handler = ldapfactory;
			self.handler.setFormdata(ApiURL.url + '/api/config/ldap?key=' + $cookieStore.get('key_api'))
	})
}())
			