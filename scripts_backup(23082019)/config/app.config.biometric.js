(function(){
			
	angular.module('app.config.biometric',['checklist-model'])

	.factory('biometricfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,time,$cookieStore,filterFilter,$route){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;
		}
		
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Device Name','device_name',30],
					['Device Number','device_number',35],
					['Device Location','device_location',35]
				);
				this.setactiveheader(this.headers[0],false)
			},setForm: function(URL,form){
				$http.get(URL).success(function(res){
					if(res.header.message == "Unauthorized"){
						self.getpermission(res.header.access);
						logger.logError("Access Unauthorized");
					}else{
						//console.log(res.data)
						self.setdata(res.data.biometric_setup);
						self.setauthen(res.data.authen_biometric);
						self.dataimport=res.data.import;
						self.databackup=res.data.backup
						if(form){
							form.sub_time = res.data.sub_time;
						}
						self.getpermission(res.header.access);
						logger.logSuccess(res.header.message);
					}
				})		
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},show:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
							if(this.mainaccess.read == 1){return true}
						break;
						case 'update':
							if(this.mainaccess.update == 1){return true}
						break;
					}
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},update: function(dataA,dataB){
				dataA.device_name = dataB.device_name;
				dataA.device_location = dataB.device_location;
				dataA.device_number = dataB.device_number;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},
			setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'modalbio',
					controller: 'modalbioadd',
					controllerAs: 'modalbio',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title : function(){
							return 'Add Biometric'
						},
						icon : function(){
							return 'fa fa-floppy-o'
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modalbio',
					controller: 'modalEditbio',
					controllerAs: 'modalbio',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return 'Edit Biometric'
						},
						icon : function(){
							return 'fa fa-pencil-square-o'
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch (a) {
						case 'add':
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/timekeeping/biometric-setup/insert?key=' + $cookieStore.get('key_api'),
								data :data,
							}).then(function(res){
								self.adddata(res.data.data);
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Biometric  Failed")
							});
							break;
						case 'edit':
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/timekeeping/biometric-setup/update/' + b.device_number + '?key=' + $cookieStore.get('key_api'),
								data : data.b
							}).then(function(res){
								self.update(data.a,res.data.data)
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Biometric Failed")
							});			
							break;
					}
					
				});
			},deldata: function(id){
				$http({
					url : ApiURL.url + '/api/timekeeping/biometric-setup/destroy/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.device_number != id[i];
						})
					}
					self.check = [];
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.message ? logger.logError(res.data.message) : logger.logError("Deleting Failed")
				})
			},
			
			/** function set authentication setup **/
			setauthen:function(a){
				self.form = a;
			},
			
			
			/** function import setting **/
			addtime: function(form){
				var a = {};
				a.time = '';
				form.sub_time.push(a);
			},
			removesubtime: function(form,a){
				form.sub_time.splice(a,1);
			},
			dataimport:function(a){
				console.log(a)
				if(a == ''){
					self.dataimport = '';
				}else{
					self.dataimport =a;
				}
			},databackup:function(a){
				if(a == ''){
					self.databackup = '';
				}else{
					self.databackup =a;
				}
			},saveimportsetting:function(sub){
				console.log(self.form.biometric_A)
				var value = {import:self.dataimport,backup:self.databackup,sub_time:sub};
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/timekeeping/biometric-setup/insert_setting?key=' + $cookieStore.get('key_api'),
					data : value 
				}).then(function(res){
					self.hide=false
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Failed")
				});	
			},save_authen : function(){
				
			
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/timekeeping/biometric-setup/authen?key=' + $cookieStore.get('key_api'),
					data : self.form 
				}).then(function(res){
					console.log('result',res)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Failed")
				});	
			},opendisable:function(a){
				self.button = true;
			}
			
			
		}
	})

	/** PRIMER CONTROLLER **/
	.controller('biometricCtrl',function(biometricfactory,ApiURL,$cookieStore){
		var self = this;
			self.handler = biometricfactory;
			self.formsub = {};
			//self.formsub.sub_time = [];
			//self.handler.addtime(self.formsub);
			self.handler.setForm(ApiURL.url + '/api/timekeeping/biometric-setup/index?key=' + $cookieStore.get('key_api'),self.formsub)	
	})

	
	/** CONTROLLER MODAL ADD **/
	.controller('modalbioadd',function(biometricfactory,$modalInstance,$timeout,data,title,icon){
		var self = this;
			self.handler = biometricfactory;
			self.handler.opendisable()
			self.title = title;
			self.icon = icon;
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss("Close")
			}
			
	})
	
	
	/** CONTROLLER MODAL EDIT **/
	.controller('modalEditbio',function(biometricfactory,$modalInstance,$timeout,data,title,icon){
		
		var self = this;
			self.handler = biometricfactory;
			self.title = title;
			self.icon = icon;
			self.form = data;
			self.form = angular.copy(data);
			
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			
			self.cancel = function(){
				$modalInstance.dismiss("Close")
			}
			
	})

	
	
	// FUNCTION SET HEADER
	.run(function(biometricfactory){
		biometricfactory.setheaders();
	})

}())