(function(){
			
	angular.module('app.att.manualinput',[])

	.factory('manualfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,time,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Name','Name',20],
					['Schedule','Schedule',15],
					['Department','department',15],
					['Job Title','title',15],
					['Time In','timeIn',15],
					['Time Out','timeOut',15]
				);
				this.setactiveheader(this.headers[0],false)
			},getselect : function(URL){
				$http.get(URL).success(function(res){
					if(res.header.message == 'success'){
						self.shift = res.data.shift;
						self.depart = res.data.department;
						self.jobs = res.data.job_title;
						self.mainaccess = res.header.access;
						logger.logSuccess('Access Granted');
					}else{
						self.mainaccess = res.header.access;
						logger.logError('Access Unauthorized');
					}
				});	
			},button:function(a){
				if(self.mainaccess){
					switch(a){
						case 'read':
							if(self.mainaccess.read == 1){return true}
						break;
						case 'update':
							if(self.mainaccess.update == 1){return true}
						break;
					}
				}
			},find : function(){
				self.form.employee = self.label;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/manual-input/search?key=' + $cookieStore.get('key_api'),
					data : self.form
				}).then(function(res){
					self.setdata(res.data.data)
					self.form.employee = self.model;
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.employee = self.model;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Manual Input Attendance Failed")
				});
			},reset:function(){
				self.form = {};
			},save:function(){
				$http({
					method :'POST',
					url : ApiURL.url + '/api/attendance/manual-input/insert?key=' + $cookieStore.get('key_api'),
					data : {data:self.maindata}
				}).then(function(res){
					self.setdata([])
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Save Manual Input  Attendance Failed")
				});
			},cancel:function(){
				self.find()
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},
			setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},onSelect : function ($item, $model, $label) {
				self.model = $item.name;
				self.label = $item.employee_id;
			},getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee +'?&key=' + $cookieStore.get('key_api'),{
				  params:{address : val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						  return a.result.map(function(item){
							return item;
						  });
					}  
				});
			}
			
		}
	})
	.controller('manualInputCtrl',function(manualfactory,ApiURL,$cookieStore){
		var self = this;
			self.handler = manualfactory;
			self.handler.getselect(ApiURL.url + '/api/attendance/manual-input/view?key=' + $cookieStore.get('key_api'));
	})

	
	.controller('modalbio',function(manualfactory,$modalInstance,$timeout,data,title,icon){
		
		var self = this;
			self.title = title;
			self.icon = icon;
			self.form = data;
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss("Close")
			}
			
	})
	
	
	.controller('modalEditbio',function(manualfactory,$modalInstance,$timeout,data,title,icon){
		var self = this;
			self.title = title;
			self.icon = icon;
			self.form = data;
			self.form = angular.copy(data);		
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.cancel = function(){
				$modalInstance.dismiss("Close")
			}
			
	})

	.run(function(manualfactory){
		manualfactory.setheaders();
	})

}())