(function(){
	
	angular.module("app.att.trainingOff",["ngFileUpload"])
	
	.factory('trainingfactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,Upload){
		var self;
		return self = {
			getaccess:function(){
				self.default  = true;
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/training/api_check?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data);
						self.files=false;
						logger.logSuccess('Access Granted');
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
				});
			},callback:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/training/api_check?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setdata(res.data.data);
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
							if(this.mainaccess.read == 1){return true}
						break;
						case 'create':
							if(this.mainaccess.create == 1){return true}
						break;
						case 'update':
							if(this.mainaccess.update == 1){return true}
						break;
					}
				}
			},setdata:function(data){
				if(data.status == 'user'){
					console.log(data.status == 'user')
					this.role = data.status;
					this.iduser = data.employee_id;
					this.nameuser = data.employee_name;
					self.form={};
					self.form.name = this.nameuser;
					self.hide = false;
				}else{
					console.log(data,'hr && spv')
					this.role = data.status;
					self.form={};
					self.form.name = '';
					self.form.name = this.nameuser;
					self.hide = true;
				}
				self.datastatus = data.status;
			},save: function(file){
				if(this.role=='user'){
					self.form.name = this.iduser;
				}else{
					self.form.name = self.label;
				}
				if(file){
					/** function upload **/
					file.upload = Upload.upload({
						method : 'POST',
						url : ApiURL.url + '/api/attendance/training/request?key=' + $cookieStore.get('key_api'),
						data :{data:self.form},
						file: file
					}).then(function(res){
						self.callback()
						self.files=false;
						/*if(self.datastatus=='user'){
							self.form.name = self.nameuser;
						}else{
							self.form.name = self.model;
						}*/
						logger.logSuccess(res.data.header.message);
					},function(res){
						self.callback()
						/*if(self.datastatus=='user'){
							self.form.name = self.nameuser;
						}else{
							self.form.name = self.model;
						}*/
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
					});
				}else{
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/attendance/training/request?key=' + $cookieStore.get('key_api'),
						data : self.form     
					}).then(function(res){
						self.callback()
						/*if(self.datastatus=='user'){
							self.form.name = self.nameuser;
						}else{
							self.form.name = self.model;
						}*/
						logger.logSuccess(res.data.header.message);
					},function(res){
						self.callback()
						/*if(self.datastatus=='user'){
							self.form.name = self.nameuser;
						}else{
							self.form.name = self.model;
						}*/
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
					});
				}
			},cancel : function(file){
				self.getaccess()
				self.files=[];
			},onSelect : function ($item, $model, $label) {
					self.subordinate = $item.name;
					self.model = $item.name;
					self.label = $item.employee_id;
			},
			getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name +'?&key=' + $cookieStore.get('key_api'),{
				  params: {
					address : val,
				  }
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};
							a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else if(response.data != null){
						var a = {};
							a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},
			test  : function(id){
				if(self.form != undefined){
					if(self.form.end != null){
						self.form.end  = {};
					}
				}
				var base =  new Date;
				var year =  base.getFullYear().toString();
				self.default  = false;
				self.injectTo  = id;
				self.getLastDay = year+'-12-31';
			}
		}
	})
	.controller('trainingController',function(trainingfactory,leaverequestfactory,ApiURL,$cookieStore,$scope,$http,Upload,logger,$timeout){
		var self = this;
			self.handler = trainingfactory;
			self.handler_hood = leaverequestfactory;    
			self.handler.getaccess()
			self.handler_hood.getToday();
			self.handler_hood.getLastYear();

			
	})
	
	   
}())
			