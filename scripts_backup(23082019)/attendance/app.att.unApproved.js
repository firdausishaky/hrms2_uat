(function(){

	angular.module('app.att.unApproved',[])
	
	.factory('unappfactory',function(filterFilter,pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore){
		function createheaders(arr){
			var temp = [];
			var arguments = arr;
			arguments ? void 0 : arguments = []
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(header){
				this.headers = new createheaders(header)
			},getHeader: function(){
				return this.headers;
			},getaccess : function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/unapproved-schedule/view?key='  + $cookieStore.get('key_api')
				}).then(function(res){
					self.datadepartment = res.data.data.department;
					self.datayears = res.data.data.year;
					self.access(res.data.header.access)
					logger.logSuccess('Access Granted');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError(" Get Access Failed")
				});		
			},access:function(a){
				if(a){this.mainaccess=a}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
							if(this.mainaccess.read == 1){return true}
						break;
						case 'create':
							if(this.mainaccess.create == 1){return true}
						break;
						case 'update':
							if(this.mainaccess.update == 1){return true}
						break;
					}
				}
			},searchschedule : function(){
				var tmp = parseInt(self.form.month);
				self.form.month = tmp.toString();

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/unapproved-schedule/search?key=' + $cookieStore.get('key_api'),
					data : self.form,
				}).then(function(res){
					
					// var tmp = parseInt(self.form.month)-1;
					// self.form.month = tmp.toString();					
					
					self.setdata(res.data.data)
					self.filterdate()
					self.getlegend()
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Un Approved Schedule Failed")
				});
			},filterdate:function(a){
				var head = [];
				var date = self.getDaysInMonth(parseInt(self.form.month)-1,self.form.years);
				for(b in date){
					var setDate = $filter('date')(date[b],'EEE-dd');
					var getDate =  setDate.slice(0,1) + ' ' +setDate.slice(4,6);
					var headertemp = [getDate,getDate,null];
					head.push(headertemp)
				}
				self.setheaders(head)
			},getDaysInMonth:function(month,year){
				var date = new Date(year, month, 1);
				var days = [];
				while (date.getMonth() === month) {
					days.push(new Date(date));
					date.setDate(date.getDate() + 1);
				}
				return days;
			},getlegend:function(){
				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/attendance/schedule/indexFixShift?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.datalegend = res.data.data;
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Legend Schedule Failed")
				});
			},getworkshift:function(){
				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/attendance/unapproved-schedule/viewWorkShift?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.datashiftcode = res.data.data;
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Legend Schedule Failed")
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				//console.log(a);

				if(a.length == 0){
					self.hideButton = true
				}else{
					self.hideButton = false
				}
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata[0].id){
						self.maindata[a] = newdata[0];
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[3],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				edit:{
					animation: true,
					templateUrl: 'modalunapp',
					controller: 'modalunapp',
					controllerAs: 'modalunapp',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},date:function(){
							return self.date;
						},shift:function(){
							return self.shift;
						}
					}
				},
				approve:{
					animation: true,
					templateUrl: 'modalapprove',
					controller: 'modalapprove',
					controllerAs: 'modalapprove',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},date:function(){
							return self.datemonth;
						}
					}
				}
			},getupdate:function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/unapproved-schedule/search?key=' + $cookieStore.get('key_api'),
					data : self.form,
				}).then(function(res){
					//if(res.data.disabled){
						console.log(res.data.disabled,9090)
						self.hideButton = res.data.disabled
					//}
					self.setdata(res.data.data)
				},function(res){
				});
			},
			openmodal: function(a,b,c,d){
				self.datatoprocess = b;
				self.shift = c;
				self.date = d;
				self.datemonth = c;
				$modal.open(self.modals[a]).result.then(function(data) {
					switch (a) {
						case 'edit':
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/attendance/unapproved-schedule/update?key=' + $cookieStore.get('key_api'),
								data : data
							}).then(function(res){
								self.getupdate()
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Un Approved Schedule Failed")
							});			
						break;
						case 'approve':
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/attendance/unapproved-schedule/approve?key=' + $cookieStore.get('key_api'),
								data : data
							}).then(function(res){
								self.getupdate()
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Approved Schedule Failed ")
							});			
						break;
					}
					
				});
			}
		}
	})
	.controller('unApprovedController',function(unappfactory,$http,ApiURL,$cookieStore){
		var self = this;
		self.handler = unappfactory;
		self.handler.getaccess();
	})
	.controller('modalunapp',function(unappfactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,date,shift){
		var self = this;
			self.handler = unappfactory;
			self.form={};
			self.form.employee = data.employee;
			self.form.date = date;
			self.form.shift = shift;
			self.form.employee_id = data.employee_id;
			self.form = angular.copy(self.form)
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
			/** api get shift code **/
			self.handler.getworkshift()
			
	})

	.controller('modalapprove',function(unappfactory,ApiURL,$cookieStore,$modalInstance,$timeout,data,date,logger){
		var self = this;
			self.handler = unappfactory;
			if(date[0]){
				var objDate = new Date(date[0].datea);
				locale = "en-us";
				month = objDate.toLocaleString(locale, { month: "long" });
				self.form={};
				self.form.month = month;
				self.form.year = data.years;
			}
			self.submit = function(){
				$modalInstance.close(data);
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})	
	.run(function(unappfactory){
		unappfactory.setheaders();
	})
	
}())
