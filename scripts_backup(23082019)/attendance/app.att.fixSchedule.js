(function(){

	angular.module('app.att.fixSchedule',['checklist-model'])

	.factory('fixscheduleFactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Last','last',null],
					['First Name','firstname',null],
					['Department ','department',null],
					['Monday','Monday',null],
					['Tuesday','Tuesday',null],
					['Wednesday','Wednesday',null],
					['Thrusday','Thrusday',null],
					['Friday','Friday',null],
					['Saturday ','Saturday',null],
					['Sunday','Sunday',null]
				);
				this.setactiveheader(this.headers[0],false)
			},find: function(){
				self.form={}
				self.form.Employee = self.label || '';
				self.form.Department = self.form.Department || 0;
				self.form.Job = self.form.Job || 0;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/schedule/getView?key=' + $cookieStore.get('key_api'),
					data : self.form 
				}).then(function(res){
					self.setdata(res.data.data);
					//console.log(res.data.data)
					self.form.Employee = self.model;
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.Employee = self.model;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Fix Schedule Failed")
				});
			},update:function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/schedule/updateFix?key=' + $cookieStore.get('key_api'),
					data : {data:self.maindata} 
				}).then(function(res){
					console.log(res.data.data)
					//self.setdata(res.data.data);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Update Fix Schedule Failed")
				});
			},deldata: function(id){
				console.log(id)
				$http({
					url : ApiURL.url + '/api/attendance/schedule/deleteFix/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata,function(store){
							return store.employee_id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Fix Schedule Success');
				},function(res){
					res.data ? logger.logError(res.data.message) : logger.logError("Deleting  Fix Schedule Failed")
				})
			},adddata: function(a){
				if(this.maindata){
					this.maindata.push(a);
				}else{
					this.maindata = [];
					this.maindata.push(a);
				}
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},modals: {
				add:{
					animation: true,
					templateUrl: 'modalfix',
					controller: 'modalfix',
					controllerAs: 'modalfix',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaleditfix',
					controller: 'modaleditfixed',
					controllerAs: 'modaleditfix',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
						
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'add':
							var sentdata ={'Employee':data.Employee,'Department':data.Department,
								Days:{
										0:{'day':'Monday','shiftcode':data.Monday},
										1:{'day':'Tuesday','shiftcode':data.Tuesday},
										2:{'day':'Wednesday','shiftcode':data.Wednesday},
										3:{'day':'Thursday','shiftcode':data.Thursday},
										4:{'day':'Friday','shiftcode':data.Friday},
										5:{'day':'Saturday','shiftcode':data.Saturday},
										6:{'day':'Sunday','shiftcode':data.Sunday}
										
										}};
							
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/attendance/schedule/insertFix?key=' + $cookieStore.get('key_api'),
								data :sentdata
							}).then(function(res){
								self.adddata(res.data.data[0]);
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Fix Schedule Failed")
							});
						break;
						case 'edit':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/attendance/schedule/updateFix?key=' + $cookieStore.get('key_api'),
									data : {data:data}
								}).then(function(res){
									self.dataupdate(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Vacation Leave Failed")
								});			
						break;
					}
					
				});
			},dataupdate:function(data){
				for(a in self.maindata){
					if(self.maindata[a].employee_id==data.employee_id){
						self.maindata[a] = data;
					}
				}
			},
			onSelect : function($item,$model,$label){self.model = $item.name;self.label = $item.employee_id;},
			getLocation : function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.Employee +'?&key=' + $cookieStore.get('key_api'),{
				  params: {
					address : val,
				  }
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						  return a.result.map(function(item){
							return item;
						  });
					}  
				});
			},
			getselect : function(URL){
				$http.get(URL).success(function(res){
					self.jobs = res.data.job;
					self.depart = res.data.department;
					console.log(res);
					var access = res.header.access.create;
					if(access == 1){
						self.hideForm = false;
					}else{
					    self.hideForm = true;
					}
				});	
			},getselectshift:function(URL){
				$http.get(URL).success(function(res){
					self.shiftcode = res.data;
				});	
			},getselectmodal : function(URL){
				$http.get(URL).success(function(res){
					self.departmodal = res.data;
				});	
			},getformodal:function(URL){
				$http.get(URL).success(function(res){
					self.shift = res.data;
				});
			}	
			
		}
	})
	.controller('fixScheduleCtrl',function(fixscheduleFactory,ApiURL,$cookieStore){
		var self = this;
			self.handler = fixscheduleFactory;
			self.handler.getselect(ApiURL.url + '/api/attendance/schedule/indexFix?key=' + $cookieStore.get('key_api'));
			self.handler.getselectshift(ApiURL.url + '/api/attendance/schedule/indexFixShift?key=' + $cookieStore.get('key_api'));
	})
	.controller('modalfix',function(fixscheduleFactory,$modalInstance,$timeout,data,time,title,icon,ApiURL,$cookieStore,$http){
		var self = this;
			self.handler = fixscheduleFactory;
			self.title = title;
			self.icon = icon;
			self.handler.getformodal(ApiURL.url + '/api/attendance/schedule/indexFixShift?key=' + $cookieStore.get('key_api'));
			self.onSelect = function ($item){self.label = $item.employee_id;},
			self.getLocation = function(val) {return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.Employee +'?&key=' + $cookieStore.get('key_api'),{params: {address : val,}
			}).then(function(response){
				if(response.data.data === null){
					console.log(response);
					logger.logError(response.data.header.message);  
				}else if(response.data != null){
					var a = {};
					a.result = response.data;
					  return a.result.map(function(item){
						return item;
					  });
				}  
			});
			},
			self.save = function(){
				self.form.Employee = self.label;
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
	})

	.controller('modaleditfixed',function(fixscheduleFactory,$modalInstance,$timeout,data,time,title,icon,ApiURL,$cookieStore,$http){
		var self = this;
			self.handler = fixscheduleFactory;
			self.title = title;
			self.icon = icon;
			self.handler.getformodal(ApiURL.url + '/api/attendance/schedule/indexFixShift?key=' + $cookieStore.get('key_api'));
			self.form = data;
			self.form.Employee = data.firstname+' '+data.last
			self.form = angular.copy(data)
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
	})	
	
	.run(function(fixscheduleFactory){
		fixscheduleFactory.setheaders();
	})
	

}())

