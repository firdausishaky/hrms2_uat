(function(){
	
	function schedule(sch,dt){
		/** data schedule **/
		this.employee_id = sch[0] || '';
		this.b = 	sch[1] || '';
		this.c = 	sch[2] || '';
		this.d = 	sch[3] || '';
		this.e = 	sch[4] || '';
		this.f = 	sch[5] || '';
		this.g = 	sch[6] || '';
		this.h = 	sch[7] || '';
		this.i = 	sch[8] || '';
		this.j = 	sch[9] || '';
		this.k = 	sch[10] || '';
		this.l = 	sch[11] || '';
		this.m = 	sch[12] || '';
		this.n = 	sch[13] || '';
		this.o = 	sch[14] || '';
		this.p = 	sch[15] || '';
		this.q = 	sch[16] || '';
		this.r = 	sch[17] || '';
		this.s = 	sch[18] || '';
		this.t = 	sch[19] || '';
		this.u = 	sch[20]|| '';
		this.v =	sch[21] || '';
		this.w = 	sch[22] || '';
		this.x = 	sch[23] || '';
		this.y = 	sch[24] || '';
		this.z = 	sch[25] || '';
		this.aa = 	sch[26] || '';
		this.ab	= 	sch[27] || '';
		this.ac = 	sch[28] || '';
		this.ad = 	sch[29] || '';
		this.ae = 	sch[30] || '';
		this.af	= 	sch[31] || '';
		
		/** data date **/
		this.date1 = 	dt[0 ] || '';
		this.date2 = 	dt[1 ] || '';
		this.date3 = 	dt[2 ] || '';
		this.date4 = 	dt[3 ] || '';
		this.date5 = 	dt[4 ] || '';
		this.date6 = 	dt[5 ] || '';
		this.date7 = 	dt[6 ] || '';
		this.date8 = 	dt[7 ] || '';
		this.date9 = 	dt[8 ] || '';
		this.date10 = 	dt[9 ] || '';
		this.date11 = 	dt[10] || '';
		this.date12 = 	dt[11] || '';
		this.date13 = 	dt[12] || '';
		this.date14 = 	dt[13] || '';
		this.date15 = 	dt[14] || '';
		this.date16 = 	dt[15] || '';
		this.date17 = 	dt[16] || '';
		this.date18 = 	dt[17] || '';
		this.date19 = 	dt[18] || '';
		this.date20 = 	dt[19] || '';
		this.date21 = 	dt[20] || '';
		this.date22 = 	dt[21] || '';
		this.date23 = 	dt[22] || '';
		this.date24 = 	dt[23] || '';
		this.date25 = 	dt[24] || '';
		this.date26 = 	dt[25] || '';
		this.date27 = 	dt[26] || '';
		this.date28 = 	dt[27] || '';
		this.date29 = 	dt[28] || '';
		this.date30 = 	dt[29] || '';
		this.date31 = 	dt[30] || '';
	}


	/** MODULE **/ 
	angular.module('app.att.scheduleimport',['checklist-model'])
	
	
	/** FACTORY **/ 
	.factory('importfactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,pagination,$timeout,$window){
		
		function createheaders(arr){
			var temp = [];
			var arguments = arr;
			arguments ? void 0 : arguments = []
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;
		}
		
		var self;
		return self = {	
			filterLegend : false,
			setheaders: function(header){
				this.headers = new createheaders(header)
			},getHeader: function(){
				return this.headers;
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},sentDate : function(date){
				this.date = date;
				return self.date;
			},getaccess:function(){
				$http({
					url : ApiURL.url + '/api/attendance/schedule-import/api_permission?key=' + $cookieStore.get('key_api'),
					method: 'GET',
				}).then(function(res){
					self.getpermission(res.data.header.access);
					self.getdatadepartment=res.data.data;
					logger.logSuccess('Access Granted');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
				});
			},
			getpermission:function(active){
				this.mainaccess = active;
			},hiden:function(a){
				if(this.mainaccess){
					switch(a){
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},checkdepartment:function(a){
				self.datadepart(a)
			},
			parms : false,
			datatosent:function(a){
				if(self.check==''){
					logger.logError('Please Select Department Before Choose File');
				}else{
					$http({
						url : ApiURL.url + '/api/attendance/schedule-import/importDataXLS?key=' + $cookieStore.get('key_api'),
						method: 'POST',
						data: {data:a,department:self.check}
					}).then(function(res){
						self.setdata(res.data.data)
						
						var t = res.data.data;
						var parm = Object.keys(t[0]);
						self.parms = parm;
						self.filterLegend = [];
						

						self.buttondisabled('hide')
						self.getlegend()

						for (var i = 0; i < t.length; i++) {
							for (var x = 0; x < parm.length; x++) {
								if(parm[x].length <= 2 && t[i][parm[x]] != ''){
									var idx = self.datalegend.findIndex(function(str2){
										var code = str2.shif_code.split(' ');
										return code[0] == t[i][parm[x]];
									});

									if(idx != -1){
										var idx2 = self.filterLegend.findIndex(function(str3){
											return str3.id == self.datalegend[idx].id;
										})

										if(idx2 == -1){
											self.filterLegend.push(self.datalegend[idx]);
										}
									}
								}
								
							};
						};
						//console.log(parm[x],t[i][parm[x]],"FILTERLEGEND")
						console.log(self.filterLegend,"FILTERLEGEND")
						

						self.decideColor()
						if(res.data.header.message=='success'){
							void 0
						}else{
							self.openmodal('show',res.data.header.message);
						}
						logger.logSuccess('Showing Files Scedule');
					},function(res){
						res.data ? logger.logError(res.data.header.message) : logger.logError(res.data.header.message)
					});
				}
			},getdatanotification:function(data){
				self.datanotification = data;
					//console.log(self.datanotification)
				},save : function(){	
					dataLength = self.maindata
					if (dataLength == null || dataLength == undefined) {
						return logger.logError("File Cannot Be Uploaded")
					}
					// console.log(dataLength)
					$http({
						url : ApiURL.url + '/api/attendance/schedule-import/xls?key=' + $cookieStore.get('key_api'),
						method: 'POST',
						data:{data:self.maindata}
					}).then(function(res){
						self.setdata([]);
						self.check=[];
						self.files=[];
						self.disabled=true;
						this.headers = [];
						self.showcancel=true;
						logger.logSuccess(res.data.header.message);
					}
					,function(res){
						// console.log(1)
						// console.log(res.data.header.message);
						res.data.header.message ? logger.logError(res.data.header.message) : logger.logError("Upload File Schedule Failed")
					});
				},getdata: function(){
					self.filtered = $filter('filter')(this.maindata,{$:self.search});
					self.select(self.currentPage);
					return self.paginated;
				},
				
				/*** order by ****/
				setactiveheader:function(a,bool){
					this.activeheader = a;
					this.activeheader.order = bool;	
				},
				/*** Pagination ****/
				numPerPageOpt	: pagination.option,
				numPerPage 		: pagination.option[3],
				currentPage		: 1,
				search			: '',
				select			: function(page){
					if(self.filtered){
						start = (page-1)*self.numPerPage;
						end = start + self.numPerPage;
						self.paginated = self.filtered.slice(start, end) || false;
						self.filteredlength = self.paginated.length || 0;
					}
				},
				modals: {
					edit:{
						animation: true,
						templateUrl: 'modalsecimport',
						controller: 'modalschedule',
						controllerAs: 'modalsecimport',
						size:'',
						backdrop:'static',
						resolve:{
							data:function(){
								return self.datatoprocess;
							},date:function(){
								return self.date;
							},shift:function(){
								return self.shift;
							},index:function(){
								return self.index;
							}
						}
					},
					del:{
						animation: true,
						templateUrl: 'modaldelimport',
						controller: 'deleteimports',
						controllerAs: 'modaldelimport',
						size:'',
						backdrop:'static',
						resolve:{
							data:function(){
								return self.datatoprocess;
							}
						}
					},
					show:{
						animation: true,
						templateUrl: 'modalshowimport',
						controller: 'modalshow',
						controllerAs: 'modalshowimport',
						size:'lg',
						//backdrop:'static',
						resolve:{
							data:function(){
								return self.message;
							}
						}
					}
				},
				openmodal: function(a,b,c,d,e,f){
					self.datatoprocess = b;
					self.shift = c;
					self.date = d;
					self.datemonth = c;
					self.index = e;
					self.message = b;
					$modal.open(self.modals[a]).result.then(function(data){
						switch(a){
							case 'edit':
							var arr = ['x','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','aa','ab','ac','ad','ae','af'];
							var t = arr[data.b.index];
							for(i in data.a){
								if(i == t){
									editData = data.b.shift.split(" ")

									var idx = self.datalegend.findIndex(function(str){
										var split = str.shif_code.split(" ");
										return split[0] == editData[0];
									});

									if(idx != -1){

										var idx2 = self.filterLegend.findIndex(function(str2){
											var split = str2.shif_code.split(" ");
											return split[0] == editData[0];	
										});

										// table								
										data.a[i] = editData[0];
										data.a['color'+i] = self.datalegend[idx].colour;
										console.log(idx2,self.datalegend[idx]);
										if(idx2 == -1){		

											//filter legend
											self.filterLegend.push(self.datalegend[idx]);
										}

										console.log(self.filterLegend,'llllllllllllllllll');
										//console.log(data.a['color'+i],"ppppppppppppppppp")
									}
								}
							}
							logger.logSuccess('Updating Files Success')
							break;
							case 'del':
							for (i = 0; i < self.maindata.length; ++i){
								self.maindata[i]==data ? self.maindata.splice(i,1) : 0
							}
							logger.logSuccess('Deleting File Success');
							break;
							case 'show':

							break;
						}
					});
				},getworkshift:function(){
					$http({
						method : 'GET',
						url : ApiURL.url +  '/api/attendance/unapproved-schedule/viewWorkShift?key=' + $cookieStore.get('key_api')
					}).then(function(res){
						self.datashiftcode = res.data.data;
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching List Schedule Failed")
					});
				},getlegend:function(){
					$http({
						method : 'GET',
						url : ApiURL.url +  '/api/attendance/schedule/indexFixShift?key=' + $cookieStore.get('key_api')
					}).then(function(res){
						self.datalegend = res.data.data;
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Showing Legend Schedule Failed")
					});
				},getexcel:function(e){
					var e  = ApiURL.url + '/api/attendance/schedule-import/getfile?key=' + $cookieStore.get('key_api'); 
					$window.open(e);
				},disabledinput:function(a){
					self.disabled=true;
				},buttondisabled:function(a){
					switch(a){
						case 'hide':
						self.disabled=false;
						self.showcancel=false;							
						break;
						case 'show':
						self.disabled=true;
						this.headers = [];
						self.setdata([]);
						self.check=[];
						file=null;
						self.showcancel=true;
						break;
					}
				},getdataworkshift:function(){
					$http({
						url : ApiURL.url + '/api/timekeeping/work-shift?key=' + $cookieStore.get('key_api'),
						method: 'GET'
					}).then(function(res){
						self.dataWorkShift = res.data.data
					})
				},
				decideColor:function(){
				// self.getdataworkshift()
				// self.getlegend()
				x = self.dataWorkShift
				y = self.datalegend
				tmp = []
				x.forEach(function(e1){
					y.forEach(function(e2){
						yy = e2.shif_code
						yyy = yy.split(" ")
						yy = yyy[0]
						if(e1.shift_code == yy){
							e1.join = e1.shift_code +" = " + e1._from + " - " + e1._to							
							tmp.push(e1)
						}	

					})
				})		


				console.log(tmp)
				self.dataLegendColor = tmp

			},

			/** function read file excel **/
			readXLSX: function(file){
				var X = XLSX;
				var reader = new FileReader();
				var name = file.name;
				var res = [];
				reader.readAsBinaryString(file);
				reader.onload = function(e){
					var data = e.target.result;
					var wb;
					wb = X.read(data, {type: 'binary'});
					res = readWB(wb)
				};
				function readWB(workbook){
					var cell = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF"]
					var result = [];
					var scheduleData=[];
					var dateheader = [];

					for(var i=0;i<workbook.SheetNames.length;i++){
						var sheetName = workbook.SheetNames[i];
						var worksheet = workbook.Sheets[sheetName];

						/** get max xlsx file **/
						var max = worksheet['!ref'];
						if(max==undefined)break;
						var maxfile = max.split(':');
						var c = maxfile[1].split(/ /)[0].replace(/[^\d]/g, '');

						/** get header **/
						var header = [];

						for(j=1; j<=c;j++){
							var scheduleTemp=[];
							for(var h=0; h < cell.length; h++){
									//if(worksheet[(cell[h] + j).toString()] == undefined) break;
									if(j==1){
										if(h==0){
												//var headertemp = [worksheet[(cell[h] + j).toString()].w,worksheet[(cell[h] + j).toString()].w,null];
												//header.push(headertemp)
											}else{
												if(worksheet[(cell[h] + j).toString()] == undefined) break;
												var setDay = [worksheet[(cell[h] + j).toString()].w];
												var date = new Date(setDay);
												var setDate= $filter('date')(date,'yyyy-MM-dd');
												header.push(setDate)
												
												/** for set headers **/
												var date0 = [worksheet[(cell[h] + j).toString()].w];
												var date1 =  new Date(setDay);
												var date2 = $filter('date')(date1,'EEE-dd');
												var date3 = date2.slice(0,1) + ' ' +date2.slice(4,6);
												var date4 = [date3,date3,null];
												dateheader.push(date4)
											}
										}else{
											var number = worksheet[(cell[h] + j).toString()] == undefined ? "" : worksheet[(cell[h] + j).toString()].w;
											scheduleTemp.push(number)
										}
									}
									if(worksheet[(cell[0] + j).toString()] == undefined) break;
									if(j!=1){
										var s = new schedule(scheduleTemp,header)
										scheduleData.push(s);
									}
								}
							//self.setdata(scheduleData)
							self.datatosent(scheduleData)
							$timeout(function(){
								self.setheaders(dateheader)
							}, 100);
						}
						
					}	
				}
			}
			
		})


/** CONTROLLER **/  
.controller('xlsimportController',function(importfactory){

	var self = this;
	self.handler = importfactory;
	self.handler.setdata([])
	self.handler.dataLegendColor = []
	self.handler.filterLegend = []

	self.readFiles = function(file, errFiles){
				//console.log(file[0])
				file[0] ? this.handler.readXLSX(file[0]) : 0
				self.handler.buttondisabled(file[0])
			}
			self.handler.getaccess();
			self.handler.getlegend()

		})

.controller('modalschedule',function(importfactory,$modalInstance,$timeout,data,date,shift,index){
	var self = this;
	self.handler = importfactory;
	self.form={};
	self.form.date = date;
	self.form.shift = shift;
	self.form.employee_id = data.employee_id;
	self.form.index=index;
	self.form = angular.copy(self.form)
	self.save = function(){
		$modalInstance.close({a:data,b:self.form});
		
		/*for (var i = 0; i < this.maindata.length; i++) {
			if(this.maindata[i].employee_id == data.employee_id){
				this.maindata[i][parms[index]] = 
			}
		};
		this.maindata.findIndex(function(str){
			str.employee_id = data.employee_id
		});*/

		console.log(self.form)
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
	/** api get shift code **/
	self.handler.getworkshift()

})

.controller('deleteimports',function(importfactory,$modalInstance,data){
	var self = this;
	self.handler=importfactory;
	self.form={};
	self.form=angular.copy(data)
	self.submit = function(){
		$modalInstance.close(data);
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}			
})

.controller('modalshow',function(importfactory,$modalInstance,data){
	var self = this;
	self.handler=importfactory;
	self.handler.getdatanotification(data);
	self.close = function(){
		$modalInstance.dismiss('close')
	}			
})

.run(function(importfactory){
	importfactory.setheaders();
})


}())
