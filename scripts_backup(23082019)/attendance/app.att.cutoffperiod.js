(function(){
			
	angular.module("app.att.cutoffperiod",['checklist-model'])

	.factory('periodfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		function isEmpty(obj){
	// 		for(var prop in obj){
 // 					
	// 		}

			return JSON.stringify(obj) === JSON.stringify({});
		}
		var NAME = '_maaveran';
		var s = localStorage.getItem(NAME);
		var items = s ? JSON.parse(s) : {};
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Payroll Period','payroll_period',30],
					['Attendance Cut-Off','title',30],
					['Pay Out Date','payout_date',20]
				);
				this.setactiveheader(this.headers[0],false)
			},getaccessperiod:function(){
				$http({
					url : ApiURL.url + '/api/attendance/cutOff/vPeriode?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					localStorage.setItem(NAME,JSON.stringify(items));
				
					self.setform(res.data.data)
					self.getpermission(res.data.header.access);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Cant Get Access Cutt Off Period")
				})
			},setform:function(data){
				self.form=data;
			},manipulation :  function(data){
				var count = items.length;
				if(count > 0){
					for(var i = 0;i < count-1; i++){
						if(items[i].id == data.id ){
							items[i].dateFrom = data.dateFrom;
							items[i].dateTo = data.dateTo;
							items[i].payout_date = data.payoutDate;
						}
					}
				}
				self.returnitems(items)
				return items;
			},returnitems:function(data){
				localStorage.setItem(NAME,JSON.stringify(data))
				self.setdata(data)
			},sendDate:function(data){
				$http({
					url : ApiURL.url +  '/api/attendance/cutOff/checkDate?key=' + $cookieStore.get('key_api'),
					method: 'POST',
					data : data
				}).then(function(res){
					self.manipulation2(res.data.data,self.datautama)
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Cant Get Access Cutt Off Period");
				})
				
			},manipulation2:function(data,datautama){
				for(a in datautama){
					if(datautama[a].id == data.id){
						datautama[a].dateFrom = data.dateFrom;
						datautama[a].dateTo = data.dateTo;
						datautama[a].payout_date = "";
						//datautama[a].payout_date = data.payoutDate;
					}
				}
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
						case 'read':
							if(this.mainaccess.read == 1){return true}		
						break;
					}
				}
			},viewperiod:function(){

				$http({
					url : ApiURL.url + '/api/attendance/cutOff/sPeriode?key=' + $cookieStore.get('key_api'),
					method: 'POST',
					data:self.form
				}).then(function(res){

					localStorage.setItem(NAME,JSON.stringify(res.data.data));
					self.setdata(res.data.data)
					self.datautama = res.data.data
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Cutt Off Period Failed")
				})
			},generateToExcel : function(){
				
				var arr = [];
				var count = 1;
				for(var prop in items){
					if(items[prop].payroll_period){
						var t = items[prop].payroll_period;
						var split = t.split('-');
						items[prop].payroll_period = t.replace("-"+split[3]," - "+split[3]);
					}
					arr[prop] =  {Payroll_Period : items[prop].dateFrom+' - '+items[prop].dateTo,Attendance_Cut_Off : items[prop].payroll_period, Pay_Out_Date : items[prop].payout_date  } 
					count =  count + 1;
				}
				var counts = arr.length;
				var mystyle = {
					//sheetid: 'My Big Table Sheet',
			        headers: true,
			        // caption: {
			        //   title:'My Big Table',
			        //   style:'font-size: 50px; color:blue;' // Sorry, styles do not works
			        // },
			        column: {
			          style:'font-size:40px',
			          width : 400
			        }
			        ,
			        columns: [
			          {columnid:'Payroll_Period',title: 'Payroll Period', width:300 },
			          {columnid:'Attendance_Cut_Off', title: 'Attendance Cut Off', width:300},
			          {columnid: 'Pay_Out_Date', title: 'Pay Out Date', width:300}
			        ]
			    };
			    var t = alasql('SELECT * INTO XLSX("cutoffperiod.xlsx",?) FROM ?',[mystyle,arr]);
			    console.log(t,"GENERATE XLSX",arr)
			    
				 //alasql('SELECT * INTO XLSXML("stephenDev.xlsx",?) FROM ?',[mystyle,arr]);
				// if(count == counts){ 
				// 	console.log('test masuk')
						
				// }
			},generate:function(){
				console.log(items,'GENERATE');
					// var count = items.length;
					// var from = items[0].payroll_period;
					// var transformFrom = from.split('-');
					// var newFrom = transformFrom[0]+'-'+transformFrom[1]+'-'+transformFrom[2];
					// var to = items[count-1].payroll_period;
					// var transformTo = to.split('-');
					// var newTo = transformTo[3]+'-'+transformTo[4]+'-'+transformTo[5];
					// self.jogress = {'from' : newFrom, 'to' : newTo} 
					// $http({
					// 	url : ApiURL.url + '/api/attendance/cutOff/generate?key=' + $cookieStore.get('key_api'),
					// 	method: 'POST',
					// 	data:{data:self.maindata,date:self.jogress}
					// }).then(function(res){
					// 	self.setdata(res.data.data)
					// 	logger.logSuccess("Generate to Word");
					// },function(res){
					// 	res.data.header ? logger.logError("Generate to Word Failed") : logger.logError("Generate to Word Failed")
					// })
			},save:function(){
					var count = items.length;
					var from = items[0].payroll_period;
					var transformFrom = from.split('-');
					var newFrom = transformFrom[0]+'-'+transformFrom[1]+'-'+transformFrom[2];
					var to = items[count-1].payroll_period;
					var transformTo = to.split('-');
					var newTo = transformTo[3]+'-'+transformTo[4]+'-'+transformTo[5];
					self.jogress = {'from' : newFrom, 'to' : newTo} 
					$http({
						url : ApiURL.url + '/api/attendance/cutOff/ePeriode?key=' + $cookieStore.get('key_api'),
						method: 'POST',
						data:{data:self.maindata,date:self.jogress}
					}).then(function(res){
						self.setdata(res.data.data)
						self.hide = false;
						logger.logSuccess(res.data.header.message);
					},function(res){
						console.log(res,"LOGGER ERRROR ++++++++++++++++++++++++++++++++++++++++++++++")
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Data Cutt Off Period Failed")
					})
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			getdata: function(){
				////console.log(this.maindata);
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				////console.log(self.currentPage);
				var test = angular.equals(self.currentPage,{})
				if(test != true){
					self.select(self.currentPage);
					return self.paginated;		
				}
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			select			: function(page){
				if(self.filtered){
					////console.log(pagination);
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					////console.log('paginate',start,end);
					
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			}
		}
	}).controller('cutOffPeriodCtrl',function(periodfactory){
		var self = this;
			self.handler = periodfactory;
			self.handler.getaccessperiod()
	}).run(function(periodfactory){
		periodfactory.setheaders();
	})
}())

//default form 
// (function(){

// }())

/* var appCutOff = angular.module("app.att.cutoffperiod",[]);


appCutOff.controller("cutOffPeriodCtrl", ["$scope","$filter","$http","$modal","$log","$cookieStore","ApiURL","$routeParams","filterFilter","logger","dateFilter", function($scope,$filter,$http,$modal,$log,$cookieStore,api,$routeParams,filterFilter,logger,dateFilter) {		
		

		
		
			$http.get(api.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api') ).success(function(res){
				
				FUNCTION GT ID FOR DELETE
				$scope.check = {
					currentPageStores: []
				}
				
				var end, start;
				start =0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
				
				
			}).success(function(data, status, header, config) {
					$scope.role = true;
					logger.logSuccess(data.header.message);
			}).error(function(data, status, headers, config) {
					$scope.role = false;
					logger.logError(data.header.message);
			});
			
		
		
			$http.get("json/json/cutOff.json").success(function(res){
				
				FUNCTION GT ID FOR DELETE
				$scope.check = {
					currentPageStores: []
				}
				
				var end, start;
				start =0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
				
				
				
			}).success(function(data, status, header, config) {
					//$scope.role = true;
					//logger.logSuccess(data.header.message);
			}).error(function(data, status, headers, config) {
					//$scope.role = false;
					//logger.logError(data.header.message);
			});
		
		
		
			// FUNCTION TABLE BIOMETRIC 
			
			var init;
            $scope.numPerPageOpt = [3, 5, 10, 20]; 
            $scope.numPerPage = $scope.numPerPageOpt[2]; 
            $scope.currentPage = 1; 
            $scope.currentPageStores = []; 
            $scope.searchKeywords = ""; 
            $scope.filteredStores = []; 
            $scope.row = "" ;
            $scope.stores =[];
            $scope.select = function(page) {
                 var end, start;
                 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
            };
            $scope.onFilterChange = function() {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
            };
            $scope.onNumPerPageChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.onOrderChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.search = function() {
                return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
            };
            $scope.order = function(rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
            };
            init = function() {
                return $scope.search(), $scope.select($scope.currentPage)
            };
			
			
			
}]);

		appCutOff.directive(
			'dateInput',
			function(dateFilter){
				return {
					require: 'ngModel',
					template: '<input type="date"></input>',
					replace: true,
					link: function(scope, elm, attrs, ngModelCtrl) {
						ngModelCtrl.$formatters.unshift(function (modelValue) {
							return dateFilter(modelValue, 'yyyy-MM-dd');
						});
						
						ngModelCtrl.$parsers.unshift(function(viewValue) {
							return new Date(viewValue);
						});
					},
				};
		});

 */