(function(){
	
	angular.module("app.att.attendancecutoff",[])
	
	.factory('attCutOffFactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,$route){
		
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['name','Name'],
					['Date','date'],
					['Schedule Time','schedule'],
					['Time In','timein'],
					['Time Out','time_out'],
					['Work Hours (Hrs)','workhour'],
					['Overtime (Hrs)','overtime'],
					['Overtime Rest Day (Hrs)','retsday'],
					['Short (Mins)','short'],
					['Late (Mins)','late'],
					['Early Out (Mins)','early_out']
					);
				this.setactiveheader(this.headers[0],false)	
			},getaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/cutOff/view?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setform(res.data.data);
						logger.logSuccess('Access Granted');
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
						if(this.mainaccess.read == 1){return true}
							break;
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}
							break;
					}
				}
			},setform:function(data){
				self.depart = data.department;
				self.jobs = data.job;
				self.dataperiod=data.period;
			},
			view : function(){

				console.log("COBA YAA1")
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					if(res.data.data  ==  []){
						
						self.cutoff_disabled   = true;
						self.talesnotale =    res.data.data
					}else{	
						console.log(3)
						self.cutoff_calculate = res.data.cutoff_calculate;
						self.cutoff_disabled   =  false;
					}
					

					var tmp=[];
					if(res.data.data.length > 0){
						for(var i=0;i<res.data.data.length;i++){
							var obj = Object.keys(tmp);
							var t = obj.findIndex((str)=>{
								
								return str==res.data.data[i].date;
							});
							if(t != -1){
								if(res.data.data[i].employee_id){
									tmp[obj[t]].push(res.data.data[i]);
								}
							}else{
								if(res.data.data[i].employee_id){
									tmp[res.data.data[i].date] = [res.data.data[i]];
								}
							}
						}
					}
					console.log(tmp,"asdsadsad");
					self.keyss = Object.keys(tmp);
					self.group=tmp;
					$cookieStore.put('option',self.form);

					console.log($cookieStore.get('option'))
					
					self.setdata(self.group[self.keyss[0]]);
					self.setrange(self.form)
					//self.setdata(res.data.data);
					//$http.post('/data.json',res.data.data);

					logger.logSuccess(res.data.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
					if(res.data.header.status == 504) {
						logger.logError("Please Input Job Title & Department")
					}
				});
			},setrange:function(form){
				//console.log(JSON.stringify(form,null,4),"+++++++++++++++++++++++++++++++")
				var base_a = form.attendance.slice(0,10);
				var base_b = form.attendance.slice(11,21);
				
				var a = new Date(base_a);
				var  b  =  new Date(base_b)
				
				var n1 = b.getDate();
				var split_a = base_a.split('/');
				var split_b = base_b.split('/');

				var lastDay = new Date(parseInt(base_a[0]),parseInt(base_a[1]), 0);
				var n = lastDay.getDate();
				
				var str = '';

				if(split_a[1] != split_b[1]){
					for(var i = parseInt(split_a[2]);  i <= parseInt(n); i++){
						var ix =  i
						str = str+'.'+ ix
						if(i == n){
							for(var j = parseInt(1);  j <= parseInt(split_b[2]); j++){
								var ij =  j
								str = str +'.'+ ij
							}
						}
					} 
				}else{
					for(var i = parseInt(split_a[2]); i <= parseInt(split_b[2]); i++){
						var ix =  i
						str = str +'.'+ ix 
					}
				}

				var getDatess = function(startDate, endDate) {
					var dates = [],
					currentDate = startDate,
					addDays = function(days) {
						var date = new Date(this.valueOf());
						date.setDate(date.getDate() + days);
						return date;
					};
					while (currentDate <= endDate) {
						dates.push(currentDate);
						currentDate = addDays.call(currentDate, 1);
					}
					return dates;
				};

				// Usage
				var tmp2=[];
				var dates = getDatess(new Date(base_a), new Date(base_b));
				dates.forEach(function(date) {
					tmp2.push(date.getDate().toString());
				});

				self.dataTabs = []
				self.datarange = /*str.split('.')*/ tmp2;
				//delete self.datarange[0];
				var arr = [];
				var len = self.datarange.length;
				console.log(len);
				var j=0;var keys=self.keyss;
				for (var i = 0; i < len; i++) {

					try{
						var tmp_tgl = keys.findIndex((idx)=>{

							var tgl  	= idx.split('-');
							tgl[2]  = parseInt(tgl[2]);
							if(tgl[2].toString() == self.datarange[i]){ return true; }
							else{ return false; }
						})

						if(tmp_tgl < 0){
							arr.push({
								tab:self.datarange[i],
								date:[],
							});
						}else{
							arr.push({
								tab:self.datarange[i],
								date:keys[tmp_tgl],
							});
						}
						/*var tgl  = keys[j].split('-');
						if(tgl[2] != self.datarange[i] && self.datarange[i] == '31'){
							
						}else{
						    arr.push({
						        tab:self.datarange[i],
						        date:keys[j],
						    });
						    j++;
						}*/
					}catch(e){

					}
				}
				self.dataRange = arr
				console.log(self.dataRange,'range')
				
			},tabIndex:0,
			setIndex:function(index){
				console.log(index)
				self.tabIndex = index;
				self.setNextData(self.dataRange[self.tabIndex])
				

			},next : function(){
				console.log(self.tabIndex)
				if(self.tabIndex !== self.dataRange.length - 1){
					self.dataRange[self.tabIndex].active = false;
					self.tabIndex++
					self.dataRange[self.tabIndex].active = true;
				}   
			},prev:function(){
				if(self.tabIndex > 0){
					self.dataRange[self.tabIndex].active = false;
					self.tabIndex--
					self.dataRange[self.tabIndex].active = true;
				}else{
					void 0
				}	
			},getdatabyIndex:function(i,j){
				self.tabIndex = j;
				self.form.tab =  i;

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.setdata(res.data.data);
					self.cutoff_calculate = res.data.cutoff_calculate;
					//$http.post('/data.json',res.data.data);
					self.tabIndex = j;
					console.log(res.data.data)
					self.setrange(self.form)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
					if(res.data.header.status == 504) {
						logger.logError("Please Input Job Title & Department")
					}
				});
			},testMasuk : function(data,i){
				
				self.form.tabActive = data;
				$http({		
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.cutoff_calculate = res.data.cutoff_calculate;
					console.log(i)
					self.setIndex =  i
					self.setdata(res.data.data);
					//$http.post('/data.json',res.data.data);

					self.setrange(self.form)
					console.log(res)
					logger.logSuccess(res.data.message);
				},function(res){
					res.data.header ? logger.logError(res.data.message) : logger.logError("View Attendance Cut Off Failed")
				});
			},setNextData:function(data){

				//console.log(self.form.tab)
				// $http({		
				// 	method : 'POST',
				// 	url : ApiURL.url + '/api/attendance/cutOff/searchCutOff?key=' + $cookieStore.get('key_api'),
				// 	data :self.form
				// }).then(function(res){

				// 	self.setdata(res.data.data);
				// 	//$http.post('/data.json',res.data.data);

				// 	self.setrange(self.form)
				// 	logger.logSuccess(res.data.header.message);
				// },function(res){
				// 	res.data.header ? logger.logError(res.data.header.message) : logger.logError("View Attendance Cut Off Failed")
				// });

				/*$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.setdata([]);
					logger.logSuccess('Success find data');
					$route.reload();
				},function(res){
					res.data.header ? logger.logError('Error cut off data') : logger.logError("Error cut off data")
				});*/
			},adddata: function(a){
				this.maindata.push(a);
			},openmodal:function(a){
				//check short exists  
				

				var thanos  =  false;
				var collect  = [];
				var i  =   0;
				for(var prop  in self.maindata){
					if(self.maindata[prop].Short != '-'){

						collect[i]  = self.maindata[prop] ;
						i += 1;
						thanos  =    true;

					}
				}

				if(thanos  ==  true){
					self.collect  =  collect
					//self.context.cancel();
					console.log(self.context)
					$modal.open(self.modals[a]).result.then(function(data){
						switch (a) {
							case 'open':
							self.collect  =  collect
							break;
							case  'open1':


							break;
						}
					});
				}else{
					var a   =   'open1'
					$modal.open(self.modals[a]).result.then(function(data){
						switch (a) {
							case  'open1':


							break;
						}
					});
				}
			},modals : {
				open:{
					animation: true,
					templateUrl: 'open',
					controller: 'cutoffmodal',
					controllerAs: 'open',
					size:'lg',
					resolve:{
						data:function(){
							return self.collect;
						}					
					}
				},
				open1:{
					animation: true,
					templateUrl: 'open1',
					controller: 'cutoffmodal1',
					controllerAs: 'open1',
					size:'lg',
					resolve:{
						data:function(){
							return self.form;
						}					
					}
				}
			},setdata: function(a){
				this.maindata = a
				console.log(a)
			},getdata: function(){
				if(self.dataRange){
					
					var t=self.dataRange.findIndex(function(dt){
						return dt.active==true;
					});
					if(t!=-1){
						this.maindata = self.group[self.dataRange[t].date];
					}else{
						this.maindata = [];
					}
				}
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			}
			,setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},cutOff : function(){
				//console.log(self.form)
				var obj   =  self.maindata;

				//console.log(self.cutoff_calculate[0].employee_id, "=====================",self.maindata);

			
				for(var prop in  obj){

					var t = self.cutoff_calculate.findIndex((str)=>{
						return str.employee_id == self.maindata[prop].employee_id;
					})
					var tmp = null;
					if(t > -1){
						tmp = self.cutoff_calculate[t];
					}

			 		/****if((self.maindata[prop].timeIn == '-' && self.maindata[prop].timeOut == '-') || (self.maindata[prop].timeIn == '-' || self.maindata[prop].timeOut == '-')){
			 			logger.logError(`Error cut off Time In or Time Out doesnt exists  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
			 			break;
			 		}
			 		else if(self.maindata[prop].C_TimeIn ==  'red' || self.maindata[prop].C_TimeIn ==  'orange'){
			 			logger.logError(`Error cut off Time In or Time Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
			 			break;	
			 		}
			 		else if(self.maindata[prop].OvertimeRestDay_color ==  'red' || self.maindata[prop].OvertimeRestDay_color ==  'orange'){
			 			logger.logError(`Error cut off Overtime Rest Day not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
			 			break;	
			 		}
			 		else if(self.maindata[prop].colorLate ==  'red' || self.maindata[prop].colorLate ==  'orange'){
			 			logger.logError(`Error cut off Late not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
			 			break;	
			 		}
			 		else if(self.maindata[prop].early_c ==  'red' || self.maindata[prop].early_c ==  'orange'){
			 			logger.logError(`Error cut off Early Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
			 			break;	
			 		}
			 		else if(self.maindata[prop].new_color_overtime==  'red' || self.maindata[prop].new_color_overtime==  'orange'){
			 			logger.logError(`Error cut off Overtime not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} `);
			 			break;	
			 		}
			 		else{*/
			 			//console.log(self.form);
					//var persons = {'employee_id' : self.maindata[prop].employee_id,  'date' : self.maindata[prop].date, 'short' :  self.maindata[prop].Short };
					var persons = {'employee_id' : self.maindata[prop].employee_id,  'date' : self.maindata[prop].date, 'short' :  self.maindata[prop].Short, 'local_it' : self.maindata[prop].local_it, 'department' : self.maindata[prop].department, 'job' : self.maindata[prop].job };
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
						data :{ form : self.form, short_person : persons, 'cutoff_per_payroll' : tmp }
					}).then(function(res){
							/*var  base_data  = [];
							self.handler.setdata(base_data);
							self.handler.dataRange = [];
							self.handler.form =  []*/
							self.setdata([]);
							logger.logSuccess('Success cut off data');
							$route.reload();
						},function(res){
							res.data.header ? logger.logError('Error cut off data') : logger.logError("Error cut off data")
						});
					//}
				}
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[4],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},

		}
	})
.controller('attCutOffController',function(attCutOffFactory,ApiURL,$cookieStore,$scope,$http){
	var self = this;
	self.form={}
	self.handler = attCutOffFactory;
	self.handler.getaccess()
	self.handler.setdata([])
	// self.handler.group = []
	// self.handler.getdata()
			// self.handler.setdata([])

		})

.controller('cutoffmodal',function(attCutOffFactory,ApiURL,$cookieStore,$modalInstance,$scope,$http,data,$route,logger){
	
	var self = this;
	self.form={}
	self.handler = attCutOffFactory;
	self.handler.context = this
	console.log(self.cutoff_calculate,"===============================================");
	return;
	self.save = function(){

		var final_data =  $cookieStore.get('option');
		var k  =  0;
		var arr  = [];
		for(var prop in self.handler.collect){
			result   =    self.handler.collect[prop]
			arr[k]   = {'employee_id' : result.employee_id,  'date' : result.date, 'short' :  result.Short }
			k  += 1
		}

		console.log(self.handler.maindata)
		var obj   =  self.handler.maindata;
		for(var prop in  obj){
			if((self.maindata[prop].timeIn == '-' && self.maindata[prop].timeOut == '-') || (self.maindata[prop].timeIn == '-' || self.maindata[prop].timeOut == '-')){
				logger.logError("Error cut off Time In or Time Out doesn't exists  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;
			}
			else if(self.maindata[prop].C_TimeIn ==  'red' || self.maindata[prop].C_TimeIn ==  'orange'){
				logger.logError("Error cut off Time In or Time Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else if(self.maindata[prop].OvertimeRestDay_color ==  'red' || self.maindata[prop].OvertimeRestDay_color ==  'orange'){
				logger.logError("Error cut off Overtime Rest Day not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else if(self.maindata[prop].colorLate ==  'red' || self.maindata[prop].colorLate ==  'orange'){
				logger.logError("Error cut off Late not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else if(self.maindata[prop].early_c ==  'red' || self.maindata[prop].early_c ==  'orange'){
				logger.logError("Error cut off Early Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else if(self.maindata[prop].new_color_overtime==  'red' || self.maindata[prop].new_color_overtime==  'orange'){
				logger.logError("Error cut off Overtime not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else{

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data : {form  :  final_data, short_person : arr, cutoff_json :data }
				}).then(function(res){
					var  base_data  = [];
					self.handler.setdata(base_data);
					self.handler.dataRange = [];
					self.handler.form =  []
					// var b = res.data.data;
					// console.log(b)
					// for(s in self.maindata){
					// 	if(b.date === self.maindata[s].date){
					// 		self.maindata[s] = res.data.data;
					// 	}
					// }

					$route.reload();
					logger.logSuccess('Success cut off data');
				},function(res){
					logger.logError('Failed cut off data')
				})
			}
		}


		$modalInstance.close(self.form);
	},
	self.cancel = function(){
		console.log('test_cancle')
		$modalInstance.close()
		self.handler.openmodal('open1')
	}
})


.controller('cutoffmodal1',function(attCutOffFactory,ApiURL,$cookieStore,$modalInstance,$scope,$http,data,$route,logger){
	var self = this;
	self.form={}
	self.handler = attCutOffFactory;
	console.log(self.cutoff_calculate,"===============================================");
	return;
	self.save1 = function(file){
		var final_data =  $cookieStore.get('option');

		var k  =  0;
		var arr  = [];


		console.log(self.handler.maindata)
		var obj   =  self.handler.maindata;
		for(var prop in  obj){
			if((self.maindata[prop].timeIn == '-' && self.maindata[prop].timeOut == '-') || (self.maindata[prop].timeIn == '-' || self.maindata[prop].timeOut == '-')){
				logger.logError("Error cut off Time In or Time Out doesn't exists  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;
			}
			else if(self.maindata[prop].C_TimeIn ==  'red' || self.maindata[prop].C_TimeIn ==  'orange'){
				logger.logError("Error cut off Time In or Time Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else if(self.maindata[prop].OvertimeRestDay_color ==  'red' || self.maindata[prop].OvertimeRestDay_color ==  'orange'){
				logger.logError("Error cut off Overtime Rest Day not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else if(self.maindata[prop].colorLate ==  'red' || self.maindata[prop].colorLate ==  'orange'){
				logger.logError("Error cut off Late not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else if(self.maindata[prop].early_c ==  'red' || self.maindata[prop].early_c ==  'orange'){
				logger.logError("Error cut off Early Out not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}
			else if(self.maindata[prop].new_color_overtime==  'red' || self.maindata[prop].new_color_overtime==  'orange'){
				logger.logError("Error cut off Overtime not yet requested or approved  for ${self.maindata[prop].Name} for date ${self.maindata[prop].date} ");
				break;	
			}else{


				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/cutOff/cutOff_data?key=' + $cookieStore.get('key_api'),
					data : {form  :  final_data, short_person : arr}
				}).then(function(res){
					$route.reload();
					var  base_data  = [];
					self.handler.setdata(base_data);
					self.handler.dataRange = [];
					self.handler.form =  []
					logger.logSuccess("Success cut off data");
				},function(res){
					logger.logError("Failed   cut off   data")
				})
			}
		}

		$modalInstance.close(self.form);
	},
	self.cancel1 = function(){
		console.log('test_cancle')
		$modalInstance.dismiss("Close")
	}
})


/** FUNCTION SET HEADER TABLES ATT CUT-OFF **/
.run(function(attCutOffFactory){
	attCutOffFactory.setheaders();
})

}())
