(function(){
	
	angular.module("app.att.overUnder",["ngFileUpload"])

	.factory('overtimefactory',function($filter,$modal,$http,logger,ApiURL,$cookieStore,time,Upload){
		
		var self;
		return self = {
			getaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/overtime/api_check?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data);
						logger.logSuccess('Access Granted');
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
				});
			},callbackaccess:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/attendance/overtime/api_check?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setdata(res.data.data);
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'read':
							if(this.mainaccess.read == 1){return true}
						break;
						case 'create':
							if(this.mainaccess.create == 1){return true}
						break;
						case 'update':
							if(this.mainaccess.update == 1){return true}
						break;
					}
				}
			},setdata:function(data){
				
				if(data.status == 'user'){
					self.idemp = data.employee_id;
					self.form={};
					self.form.name = this.mainname || data.employee_name;
					self.hide = true;
					//console.log(this.mainname)
				}else{
					self.form={};
					self.form.name = '';
					self.hide = false;
				}
				self.datastatus = data.status;
			},save:function(file){
				if(self.datastatus=='user'){
					self.form.name =  self.idemp;
				}else{
					self.form.name = self.label;
				}
				if(file){
					file.upload = Upload.upload({
						method : 'POST',
						url : ApiURL.url + '/api/attendance/overtime/request?key=' + $cookieStore.get('key_api'),
						data :{data:self.form},
						file: file
					}).then(function(res){
						self.callbackaccess();
						self.files=false;
						logger.logSuccess(res.data.header.message);
					},function(res){
						self.callbackaccess()
						self.files=false;
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
					});
				}else{				
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/attendance/overtime/request?key=' + $cookieStore.get('key_api'),
						data : self.form     
					}).then(function(res){
						self.callbackaccess();
						logger.logSuccess(res.data.header.message);
					},function(res){
						self.callbackaccess();
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Request Failed")
					});
				}
			},
			
			/** FUNCTION GET SCHEDULE WORK HOURS OVERTIME **/
			update : function(){
				self.form.name = self.label;
				self.form.dateRequest =  self.form.dateRequest;
				self.form.test = self.model;
				$http({
					method  :  'POST',
					url  :  ApiURL.url + '/api/attendance/overtime/work-hour?key=' + $cookieStore.get('key_api'),
					data  :  self.form     
				}).success(function(data) {
						self.form.overtime = 0
						self.shiftOver =  data.data.shift_code
						self.works =  data.data.work_hour 
						self.form.name = data.data.name;
						self.form.work_hour = data.data.work_hour;
						logger.logSuccess(data.header.message);
				}).error(function(data,status,header){
						self.form.name = data.data.name;
						logger.logError(data.header.message);
				});
			},
			
			/** FUNCTION GET SCHEDULE WORK HOURS UNDERTIME **/
			updateUnder : function(){	
				self.form.name = self.label;
				self.form.dateRequest =  self.form.dateRequest;
				self.form.test = self.model;
				$http({
					method  :  'POST',
					url  :  ApiURL.url + '/api/attendance/overtime/work-hour?key=' + $cookieStore.get('key_api'),
					headers  :  { 'Content-Type'  :  'application/x-www-form-urlencoded',},
					data  :  $.param(self.form)     
				}).success(function(data) {
						self.shiftUnder =  data.data.shift_code
						self.form.from = data.data.from;
						self.form.to = data.data.to;
						self.form.duration = data.data.duration;
						self.form.name = data.data.name;
						logger.logSuccess(data.header.message);
						//self.findUndertime(self.form)
				}).error(function(data){
						self.form.from = '';
						self.form.to = '';
						self.form.duration = ''
						self.form.name = data.data.name;
						logger.logError(data.header.message);
				});
			},
			
			
			/** FUNCTION GET SCHEDULE WORK HOURS UNDERTIME **/
			// findUndertime  :  function(a){
			// 	var a = self.form.from;
			// 	var b = self.form.shortUnder;
			// 	function parseTime(s){
			// 	   var c = s.split(':');
			// 	   return parseInt(c[0]) * 60 + parseInt(c[1]);
			// 	}
			// 	var minutes = (parseTime(a) - parseTime(b)) * 60;
			// 	var a = minutes.toString();
			// 	String.prototype.toHHMMSS = function () {
			// 		var sec_num = parseInt(this, 10);
			// 		var hours   = Math.floor(sec_num / 3600);
			// 		var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
			// 		var seconds = sec_num - (hours * 3600) - (minutes * 60);

			// 		if (hours   < 10) {hours   = "0"+hours;}
			// 		if (minutes < 10) {minutes = "0"+minutes;}
			// 		if (seconds < 10) {seconds = "0"+seconds;}
			// 		var time    = hours+':'+minutes;
			// 		return time;
			// 	}
			// 	var b = (a.toHHMMSS());
			// 	var c = b.substring(0,5);
			// 	self.form.shortTime = c;

			// 	/*//console.log(c)
			// 	var d = c.split(':');
			// 	var t = d[1].length;
			// 	//console.log(t)
			// 	if(t == 1){
			// 		var k = 12 - d[1]
			// 	}else{
			// 		var k = d[0]
			// 	}

			// 	var j =  d[0]+':'+k;
			// 	//console.log(j)*/
			// },
			findUndertimes : function(a){
				console.log(a)
				self.overUnderButton = false	
				//modify  undertime 
				

				//shortUnder dapet dari  request work  end time  '14:00'
				var modify =  a.shortUnder.split(':');

				//dapet dari from bentuk dasarnya >>   '07:00 to 16:00'
				var basedate   = a.from.split(' to ');


				//ngambil   dari basedate array  pertama   16:00 >>  16				
				var get_base_hour = basedate[1].split(':');
			
				//bandingkan jam   14    dan 16 
				if(modify[1] > get_base_hour[0]){
					logger.logError("More than time of schedule");
					self.overUnderButton =  true 
				}else{
					self.overUnderButton  = false
					
					//manipulation undertime
					

					console.log(get_base_hour)
					var time1 =  (parseInt(get_base_hour[0]) * 60) +  parseInt(get_base_hour[1]);
					console.log(time1) 						
					var time2  = (parseInt(modify[0]) * 60) +   parseInt(modify[1]);
					console.log(time2)
				
					var diff_time     =  parseInt(time1)  - parseInt(time2) 
					console.log(diff_time)		
					if(diff_time  > 60){
					   var mod =  diff_time % 60;
												
					   if(mod == 0){
						var total_time = diff_time / 60 
					   }else{
					  	 var  get_minute  =  diff_time - (mod  * 60);
					   	 var total_time  =  mod+'.'+get_minute   
					   }

					  
					}else{
					   var total_time =  '0.'+diff_time;			
	
					}		

 					logger.logSuccess("Success find   undertime")
				}		
				
				//if(modify[0] != undefined){
				//	var hour = parseInt(modify[0]);
				//} 
				//if(modify[1] != undefined){
				//	var minute = ((parseInt(modify[1]) / 15) * 25) /100;
				//} 

				//self.total =  parseFloat(a.duration) - (parseFloat(minute) + parseFloat(hour)) ;
				self.form.shortTime = total_time;
 				console.log(self.form.shortTime)

				//console.log('date1',self.schedule);
				
				// var count = self.schedule.length;
				// var total = self.schedule[count - 1];

				// var  arg1 = a.split(':');
				// //console.log(hour)
				// var arg2 = a.shortUnder.split(':');
				// //console.log(hours);

				// if(parseInt(arg2[0]) == "00"){
				// 	arg2[0] =  1
				// }

				// if(parseInt(arg1[0]) == "00"){
				// 	arg1[0] =  1
				// }


				// if(parseInt(arg2[1]) == "00"){
				// 	arg2[1] =  60
				// }

				// if(parseInt(arg1[1]) == "00"){
				// 	arg1[1] = 60
				// }



				// console.log(arg1,'->arg1')
				// console.log(arg2,'->arg2')
				// var argFin =  (Math.abs(parseInt(arg1[0]) - parseInt(arg2[0]))).toString().length == 1 ? '0'+(Math.abs(parseInt(arg1[0]) - parseInt(arg2[0]))) : (Math.abs(parseInt(arg1[0]) - parseInt(arg2[0]))) ;
				// var argFin1 = (Math.abs(parseInt(arg1[1]) - parseInt(arg2[1]))).toString().length == 1 ? '0'+(Math.abs(parseInt(arg1[1]) - parseInt(arg2[1]))) : (Math.abs(parseInt(arg1[1]) - parseInt(arg2[1]))) ;
				// self.form.shortTime =  argFin+':'+argFin1;


			},
			findUndertime  :  function(a){
				
				//console.log('test')
				//console.log('time : ',a);

				var hour                = a.from.split(":");
				var to                  = a.to.split(":");
				var manipulateTo_minute = to[0];

					if(parseInt(to[0]) > parseInt(hour[0])){
						
						//console.log('test',1);
						if(hour[0] == "00"){
							var base_hour = parseInt(to[0])-1;
						}else{
							if(parseInt(to[0]) >= 13 && parseInt(to[0]) <= 23 && parseInt(hour[0]) >= 1 && parseInt(hour[0]) <= 23){
								var base_hour = parseInt(to[0]) - parseInt(hour[0]);
							}else if(parseInt(hour[0]) >= 13 && parseInt(hour[0]) <= 23 && parseInt(to[0]) >= 1 && parseInt(to[0]) <= 23){
								var base_hour = (24 - parseInt(to[0])) - parseInt(hour[0]);
							}else if(parseInt(to[0]) >= 13 && parseInt(to[0]) <= 23 && parseInt(hour[0]) >= 13 && parseInt(hour[0]) <= 23){
								var base_hour = parseInt(to[0]) - parseInt(hour[0]);
							}else{
								var base_hour = ( 12 - parseInt(hour[0])) + parseInt(to[0]);
							}
						}
					}else{
						if(hour[0] == "00"){
							var base_hour = parseInt(to[0]);
						}else{
							var base_hour = parseInt(to[0]) - parseInt(hour[0]);
						}
					}

					//console.log('from_minute',to[1]);
					//console.log('to_minute',hour[1]);
					if(parseInt(hour[1]) ==  0){
						var base_minute0 = 0
					}else if(parseInt(hour[1]) > 29){
						var base_minute0 = 60 - parseInt(hour[1]); 
					}else{
						var base_minute0 =  parseInt(hour[1]);
					} 
					//console.log('base_minute0',base_minute0);
					var compare =  parseInt(to[0]) - parseInt(hour[0]);
					if(compare == 0 && to[0] == "00"){
						var base_minute1 = 0;	
					}else{ 
						if(to[1] == "00" && hour[1] == "00"){
							var base_minute1 = 0;
						}else if(to[1] == "00"){
							var base_minute1 = 0;
						}else{
							if(parseInt(to[1]) > 29){
								var base_minute1 = parseInt(to[1]); 
							}else{
								var base_minute1 = 60 - parseInt(to[1]);	
							}
						}
						
						//console.log('base_minute1',base_minute1);
					}
				//console.log('parseMin0',parseInt(base_minute0))
				//console.log('parseMin1',parseInt(base_minute1))
				var base_minute =  (parseInt(base_minute0) + parseInt(base_minute1))/15;
				//console.log('base_hour',base_hour)	

				var manipulate_base = base_hour * 4;
				var hour_base       = "00";
				var based_time      = 0;
				var total           = parseInt(manipulate_base) + parseInt(base_minute)
				//console.log('hour',(manipulate_base))
				//console.log('minutes',(base_minute))
				//console.log(total)
				var arr = [];
				var i   = 0;
				for(i;  i < total; i++){
					//console.log(i);
					if(based_time < 31 ){
							based_time = parseInt(based_time) + 15;
							if(hour_base.toString().length ==  1){
								if(based_time.toString().length ==  1){
									arr[i]     = '0'+hour_base+':'+ based_time+'0';
								}else{
									arr[i]     = '0'+hour_base+':'+ based_time;
								}
							}else{
								arr[i]     = hour_base+':'+ based_time;
							}
					}else{	
						hour_base = (parseInt(hour_base) + 1);
						//console.log(hour_base.length);

						if(hour_base.toString().length ==  1){
							arr[i]    = '0'+hour_base+':'+'00';			
						}else{
							arr[i]    = hour_base+':'+'00';
						}
						based_time = "00";
					}

					if(total != 0){
						if(i == (total-1)){
							self.schedule =  arr
							self.form.arc = arr[total-1];

						}
					}else{
						self.schedule =  [];
					}
					
				}

				////console.log(arr);
			},
			/** SEARCHING NAME FUNCTION **/
			onSelect  :  function ($item, $model, $label) {
				self.subordinate = $item.name;
				self.model = $item.name;
				self.label = $item.employee_id;
			},
			getLocation  :  function(val) {
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name +'?&key=' + $cookieStore.get('key_api'),{
				  params :  {
					address  :  val,
				  }
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						  return a.result.map(function(item){
							return item;
						  });
					}  
				});
			},
			
			
			/** FUNCTION RESET FORM **/

			test : function (){
				console.log('test');
			},
			cancel  :  function(){
				self.callbackaccess()
				self.files = false;
			},
			schedule : function(a){
				//console.log(a)
			},
			/** FORM VARIABLES OVERTIME **/
			schedule : time.schedule
			
			/** FUNCTION GET SCHEDULE WORK HOURS OVERTIME **/
			,schedule_from : function(){
				if(self.works != undefined && self.shiftOver != "DO" ){
				var test =  self.works.split(" ");
				var ti =  time.schedule;
				var i = 0;
				for(var prop in ti){
					if(ti[prop] == test[2]){
						var ind  =  parseInt(prop) != 0 ? parseInt(prop) - 1 : parseInt(prop); 
					}
				}
				var arr = [];
				if(ind){
					for(var get in ti){
						if(get > ind){
							arr[i] = ti[get];
							i = i + 1;
						}
					}					
				}

				return arr;
				console.log('data',arr)
				}

				if(self.works != undefined && self.shiftOver == "DO" ){
					return time.schedule;
				}
			},short : function(){
				console.log(self.form);
			},findOvertime  :  function(a){
				var overto = a.overtimeTo;
				console.log("overto", overto);
			    var overtFrom = a.overtimeFrom;

			    var chk_1  = overto.split(':');
			    console.log("overto.split(':')", overto.split(':'));
			    //console.log("overto",  overto.split(':'));
				var  chk_2 = overtFrom.split(':');
				console.log("overtFrom.split(':')", overtFrom.split(':'));
				//console.log("overtFrom", overtFrom);
				
				var to_hour  =   parseInt(chk_1[0]) * 60;
				console.log("to_hour", to_hour);
				
				var to_minute =  parseInt(chk_1[1])
				console.log("to_minute", to_minute);

				var from_hour  =   parseInt(chk_2[0]) * 60;
				console.log("from_hour", from_hour);
				//console.log("from_hour", from_hour);
				var from_minute =  parseInt(chk_2[1])
				console.log("from_minute", from_minute);


				var  total_1 =  to_hour + to_minute;
				console.log("total_1", total_1);
				var  total_2 =  from_hour + from_minute;
				console.log("total_2", total_2);

				if(total_1 > total_2){
					var get_final_data  =  total_1 -  total_2;
					console.log("get_final_data", get_final_data);
					if(get_final_data % 60  != null){
						var get_divider  = get_final_data  % 60;
						var get_total_divider = get_final_data  - get_divider;
						get_final_data = get_total_divider  / 60;

						var count  =  String(get_divider);
						if(count.length < 2){
							var resul  =  get_final_data;	
						}else{
							var resul  =  get_final_data+' : '+get_divider;
						} 

					}	
				}else{
					var  get_final_data  =  total_2 -  total_1;
					if(get_final_data % 60  != null){
						var get_divider  = get_final_data  % 60;
					 	var get_total_divider = get_final_data  - get_divider;
						get_final_data = get_total_divider  / 60;

						var count  =  String(get_divider);
						if(count.length < 2){
							var resul  =  get_final_data;	
						}else{
							var resul  =  get_final_data+' : '+get_divider;
						}   

					}
				}
				//console.log(overto, overtFrom)
				// String.prototype.toHHMMSS = function () {
				// 	var sec_num = parseInt(this, 10);
				// 	var hours   = Math.floor(sec_num / 3600);
				// 	var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
				// 	var seconds = sec_num - (hours * 3600) - (minutes * 60);

				// 	if (hours   < 10) {hours   = "0"+hours;}
				// 	if (minutes < 10) {minutes = "0"+minutes;}
				// 	if (seconds < 10) {seconds = "0"+seconds;}
				// 	var time    = hours+':'+minutes;
				// 	return time;
				// }
				
				// var overto = a.overtimeTo;
			 //    var overtFrom = a.overtimeFrom;
			 //    var to  = overto.split(':');
			 //    var from = overtFrom.split(':');

			 //    if(parseInt(from[0]) >= 20 && parseInt(to[0]) >= 0  ){
			 //    	var to1 = parseInt(to[0])  * 60;
			 //    	var from1 =  ( 24 - parseInt(from[0])) * 60;
			 //    }else if(parseInt(from[0]) <  parseInt(to[0])){
			 //    	var to1 = parseInt(to[0])  * 60;
			 //    	var from1 = parseInt(from[0]) * 60;
			 //    }else if(parseInt(from[0]) >  parseInt(to[0]) && parseInt(to[0]) < parseInt(from[0]) ){
			 //    	var to1 = (24 - parseInt(to[0]))  * 60;
			 //    	var from1 = parseInt(from[0]) * 60;
			 //    }else{
			 //    	var to1 =  parseInt(to[0])  * 60;
			 //    	var from1 =  parseInt(from[0]) * 60;
			 //    }

			 //    if(parseInt(from[1]) == 0 ){
			 //    	if(from[0] == to[0]){
			 //    		var total_hour =  (parseInt(from1) - parseInt(from[1])) + parseInt(to[1]); 
			 //    	}else{
			 //    		var total_hour = ((parseInt(to1) +  parseInt(from1)) - parseInt(from[1])) + parseInt(to[1]); 
			 //    	}
			 //    }

			 //    if(parseInt(from[1]) != 0){
			 //    	if(from[0] == to[0]){
			 //    		to[1] = parseInt(to[1]) == 0 ? 60 : parseInt(to[1])  
			 //    		var total_hour = parseInt(to[1]) -  parseInt(from[1])
			 //    	}else{
			 //    		var total_hour = ((parseInt(to1) -  parseInt(from1)) - parseInt(from[1])) + parseInt(to[1]);
			 //    	}
			 //    }

			 //    var total_overtime =  total_hour /60;
			    self.form.overtime =  resul;
			 //    self.form.task = self.shiftOver
				// console.log(self.form.overtime)
			},
		
		}
	})
	
	
	/** CONTROLLER **/  
	.controller('overUnderController',function(overtimefactory,ApiURL,$cookieStore,$scope,$http){
		var self = this;
			self.handler = overtimefactory;
			self.handler.getaccess()
			self.handler.short()
	})
	
	   
}())

/*
 FUNCTION SHOW COMPENSATION WITH
 if(self.form.total >= 4){
	self.form.hidecom = true
}else{
	self.form.hidecom = false

				
				

//alert(a.toHHMMSS());
		
		//if(	minutes >= 60 ){
		//	var e = minutes/60;
		//}else{
		//	var e = minutes;
		//}
		
	/*	
		var a = self.maindata.data.to.substring(0,2)
		var b = self.maindata.data.to.substring(3,5)
		var c = self.form.shortUnder.substring(0,2)
		var d = self.form.shortUnder.substring(3,5)
		if(b < d ){
			var e = 60 - d;
		}else{
			var e = b - d;
		}
		var f = a - c ;
		if(e != 0){
			var f = f -1;
		}else{
			var f = f;
		}
		self.form.shortTime = f + ' . ' + e;


var f = Math.round(e);
		$scope.hour_per_day = f;
		
//e % 3600 == 0 ? e : e = ((e - (e % 3600))/3600) + (((e % 3600)/3600)/100)
						
		
var d = [];
var j = 0;
while(j<60){
var e = j+=15;
d.push(e);
}

var a = [];
for(var i =0; i<24; i++){
for(var j = 0; j < d.length; j++) {
	var b = i +":"+ d[j];
	if(b == i +":"+ 60) {
		b = i+1 +":"+ "00";
		if(b == "24:00") {
			b = "00:00";
		}
	}
	a.push(b);
}
}
////console.log(a)

var hours, minutes, ampm;
var time = [];
for(var i = 0; i <= 1440; i += 15){
	hours = Math.floor(i / 60);
	minutes = i % 60;
	if (minutes < 10){
		minutes = '0' + minutes; // adding leading zero
	}
	ampm = hours % 24 < 12 ? 'AM' : 'PM';
	hours = hours % 12;
	if (hours === 0){
		hours = 12;
	}
	time.push(hours + ':' + minutes + ' ' + ampm); 

}
//console.log(time)	
*/
			