(function(){

	function users(a,b,c,d,e,f){
		this.dateRequest = a;
		this.availment_date = b;
		this.employee_name = c;
		this.request_type = d;
		this.status = f;
		
	}

	users.prototype.update = function(a,b,c,d,e,f){
		this.dateRequest = a;
		this.availment_date = b;
		this.employee_name = c;
		this.request_type = d;
		this.status = f;
		
	}
	
	// MODULE LEAVE LIST
	angular.module('app.att.sRequestList',[])

	.factory('requestListFactory',function(pagination,$filter,$modal,$http,logger){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;
		}
		
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Date Request','dateRequest',20],
					['Availment Date','availment_date',20],
					['Employee Name','employee_name',20],
					['Request Type','request_type',20],
					['Status','status',20]
				);
				this.setactiveheader(this.headers[0],false)
			},
			
			
			/*** FUNCTION IN SEARCH LIST ***/
			
			find: function(){
				console.log(self.form)
			},
			
			reset: function(){
				self.form = {};
			},
			
			exp: function(){
				console.log(self.form);
			},
			
			adddata: function(a){
				this.maindata.push(a);
			},
			
			setdata: function(a){
				this.maindata = a;
			},

			getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},
			
			deldata: function(a){
				for (i = 0; i < this.maindata.length; ++i) {
					this.maindata[i]==a ? this.maindata.splice(i,1) : 0
				}
			},
			
			/*** ORDER BY ****/
			setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			
			
			/*** PAGINATION ****/
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			
			/** MODAL HANDLER **/
			modals: {
				time:{
					animation: true,
					templateUrl: 'timeInOut',
					controller: 'timeInOut',
					controllerAs: 'timeInOut',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Time-in / Time-Out"
						}						
					}
				},
				changeShift:{
					animation: true,
					templateUrl: 'timeInOut',
					controller: 'timeInOut',
					controllerAs: 'timeInOut',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Change Shift"
						}
					}
				},
				rej:{
					animation: true,
					templateUrl: 'AppRejModal',
					controller: 'rejLeave',
					controllerAs: 'AppRejModal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Reject"
						}
						
					}
				},
				app:{
					animation: true,
					templateUrl: 'AppRejModal',
					controller: 'appleave',
					controllerAs: 'AppRejModal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Approve"
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function (data) {
					switch (a) {
						case 'time':
							$http.post('/editUserAPI',data.b).success(function(res){
								logger.logSuccess(res.msg)
								data.a.update(data.b.name,data.b.username,data.b.roles)
							}).error(function(){
								logger.logError("Updating User Failed")
								data.a.update(data.b.name,data.b.username,data.b.roles)
							});
							break;
						case 'app':
							$http.post('/addUserAPI',data).success(function(res){
								logger.logSuccess(res.msg)
								self.adddata(new users(data.name,data.username,data.roles));
							}).error(function(){
								logger.logError("Adding User Failed")
								self.adddata(new users(data.name,data.username,data.roles));
							});
							break;
						case 'rej':
							$http.post('/delUserAPI',data).success(function(res){
								logger.logSuccess(res.msg)
								self.deldata(data)
							}).error(function(){
								logger.logError("Updating User Failed")
								self.deldata(data)
							});
							break;	
					}
					
				});
			},
			
			
		}
	})

	// PRIMER CONTROLLER
	.controller('scheduleRequestListCtrl',function(requestListFactory){
		//console.log(requestListFactory, "<-result of post ? if error the apipost request have error");
	
		var self = this;
		self.handler = requestListFactory;
		
		//** should be removed upon api connection
		serverdata = [];
		serverdata.push(new users('2012-11-02','2012-11-02','monika indriyani','Time-in / Time Out','3','Pending'));
		serverdata.push(new users('2013-11-02','2012-11-02','jajang','Change Shift','2','Approval'));
		serverdata.push(new users('2014-11-02','2012-11-02','Agus','Overtime','10','Pending'));
		serverdata.push(new users('2014-11-02','2012-11-02','Adie','Undertime','10','Pending'));
		serverdata.push(new users('2014-11-02','2012-11-02','Stefhen','Late / Early Out Exemption','10','Pending'));
		serverdata.push(new users('2014-11-02','2012-11-02','Dodo','Training','10','Pending'));
		serverdata.push(new users('2014-11-02','2012-11-02','Fasal','Swaft Shift','10','Pending'));
			
		
		requestListFactory.setdata(serverdata);
		
	})

	
	/** CONTROLLER MODAL TIME IN / OUT **/
	.controller('timeInOut',function(requestListFactory,$modalInstance,$timeout,data){
		
		var self = this;
			self.form = data;			
			self.approve = function(){
				$modalInstance.close(self.form);
			},
			self.reject = function(){
				$modalInstance.close(self.form);
			},
			self.back = function(){
				$modalInstance.dismiss("Close")
			}
	})
	
	
	// CONTROLLER MODAL REJECT
	.controller('rejLeave',function(requestListFactory,$modalInstance,$timeout,data,title){
		//console.log(data,"data to delete")
		var self = this;
		self.title = title;
		self.submit = function(){
			$modalInstance.close(data);
		}
		self.form = {};
		self.form = data;
		self.close = function(){
			$modalInstance.dismiss("Close")
		}
	})

	
	
	// CONTROLLER MODAL APPROVE
	.controller('appleave',function(requestListFactory,$modalInstance,$timeout,title,data){
		//console.log(data,'approve');
		var self = this;
		self.title = title;
		self.submit = function(){
			$modalInstance.close({a:data,b:self.form});
		}
		self.form = {};
		self.form = angular.copy(data);
			
		self.close = function(){
			$modalInstance.dismiss("shuwwaaa")
		}
			
	})
	
	// FUNCTION SET HEADER
	.run(function(requestListFactory){
		requestListFactory.setheaders();
	})

}())