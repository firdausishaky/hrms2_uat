var appFormImmig = angular.module("app.dashboard.immigrations", ["checklist-model","ngFileUpload"]);


/** factory permission **/
appFormEmmer.factory('permissionimmigDashboard',function(){
	var self;
	return self = {
		data:function(a){
			self.access = a;
		}
	}
});


appFormImmig.controller("ImmigrationsCtrlDashboard",function($scope,filterFilter,$modal,$http,ApiURL,logger,$cookieStore,$routeParams,$window,permissionimmig) {
	
	/** employee id **/
			// $scope.id = $routeParams.id;
			// $scope.id = $cookieStore.get('NotUser')
			const onlyUser = $cookieStore.get('UserAccess')
			const notUser = $cookieStore.get('NotUser')
			function getID(){
				if (notUser == null) {
					return(onlyUser.id)	
				}else{
					return(notUser)
				}
			}
			$scope.id = getID()

			/** get all data **/
			$http({
				method : 'GET',
				url : ApiURL.url + '/api/employee-details/immigration/' + $scope.id + '?key=' + $cookieStore.get('key_api')
			}).then(function(res){
				console.log(res.data.header.access,'12')
				var temp=res.data.data;
				for(a in temp){
					if(temp[a].filename=='null'){
						temp[a].filename='Not Defined';
					}
				}
				$scope.dataimmigrations=temp;
				$scope.check = {dataimmigrations:[]};
				permissionimmig.data(res.data.header.access);
				logger.logSuccess(res.data.header.message)
			},function(res){
				res.data.header ? permissionimmig.data(res.data.header.access) : permissionimmig.data = [] ; 
				res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Immigrations failed")
			});
			
			
			/** function download file **/ 
			$scope.test = function(a){
				if(a.target.innerText == 'Not Defined'){
					logger.logError("File Not Found");
				}else{	
					var e = ApiURL.url +  '/api/employee-details/immigration/'  + $scope.id +  '/' + a.target.innerText + '?key=' + $cookieStore.get('key_api');
					$window.open(e);
				}
			};
			
			/** function hidden button **/
			$scope.hide = function(a){
				var data = permissionimmig.access;
				if(data){
					switch(a){
						case 'create':
						if(data.create == 1){return true}
							break;
						case 'delete':
						if(data.delete == 1){return true}
							break;
						case 'update':
						if(data.update == 1){return true}
							break;
					}
				}
			};
			
			/** modal handler **/
			$scope.open = function(size) {
				var modal;
				modal = $modal.open({
					templateUrl: "modalImmigrations.html",
					controller: "immigrationCtrlDashboard",
					backdrop : "static",
					size: size,
					resolve: {
						items: function(){
							return;
						}
					}
				});
				modal.result.then(function(newstudent){
					//console.log(newstudent)
					if($scope.dataimmigrations){
						$scope.dataimmigrations.push(newstudent);
					}else{
						$scope.dataimmigrations =[];
						$scope.dataimmigrations.push(newstudent);
					}
				});
			};
			$scope.edit = function(size,student){
				var modal;
				modal = $modal.open({
					templateUrl: "modalEditImmigration.html",
					controller: "EditImmigrationsDashboard",
					backdrop : "static",
					size: size,
					resolve: {
						items: function(){
							return student;
						}
					}
				});
				modal.result.then(function(newdata){
					for(a in $scope.dataimmigrations){	
						if($scope.dataimmigrations[a].id == newdata.id){
							$scope.dataimmigrations[a]=newdata;
						}
					};
				});	
			};
			
			
			/** function remove **/
			$scope.remove = function(id){
				//console.log(id)
				$http({
					method : 'DELETE',
					url : ApiURL.url + '/api/employee-details/immigration/' + id + '?key=' + $cookieStore.get('key_api')
				}).success(function(data){
					var i;
					for( i = 0; i < id.length; i++){
						$scope.dataimmigrations = filterFilter($scope.dataimmigrations,function(student){
							//console.log(student.id)
							return student.id != id[i];
						});
					};
					$scope.check.dataimmigrations = [];
					logger.logSuccess(data.header.message);
				}).error(function(data) {
					data.header ? logger.logError(data.header.message) : logger.logError("Delete Immigration Failed")
				});
			};
			
			
		});

appFormImmig.controller("immigrationCtrlDashboard", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams,Upload) {
	
	/** employee id **/
		// $scope.id = $routeParams.id;
		// $scope.id = $cookieStore.get('NotUser')
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()		
		
		/** copy data **/
		if(items){
			$scope.immig = items;
		}
		
		/** get immigration issuer **/
		$http.get(ApiURL.url+'/api/module/immigration-issuer?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.jobs = res.data;
		});
		
		/** function save **/
		$scope.save = function(file){
			if(file){
				/** function upload **/
				file.upload = Upload.upload({
					method : 'POST',
					url : ApiURL.url + '/api/employee-details/immigration/store1/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
					data :$scope.immig,
					file: file
				}).then(function(res){
					$modalInstance.close(res.data.data[0]); 
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
				});
			}else{				
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/employee-details/immigration/store1/' + $scope.id + '?key=' + $cookieStore.get('key_api'),
					data :$scope.immig     
				}).then(function(res){
					var temp = res.data.data[0];
					temp.filename='Not Defined';
					$modalInstance.close(temp);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
				});
			}
		};
		
		/** close modal **/
		$scope.cancel = function(){
			$modalInstance.dismiss("cancel")
		}
		
	});

appFormImmig.controller("EditImmigrationsDashboard", function(permissionimmig,$routeParams,$modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,Upload) {
	
	/** empoloyee id **/
	// $scope.id = $cookieStore.get('NotUser')
		// $scope.id = $routeParams.id;
		const onlyUser = $cookieStore.get('UserAccess')
		const notUser = $cookieStore.get('NotUser')
		function getID(){
			if (notUser == null) {
				return(onlyUser.id)	
			}else{
				return(notUser)
			}
		}
		$scope.id = getID()			
		
		/** copy data **/
		if(items){
			$scope.immig = items;
			$scope.immig=angular.copy(items);
		}
		
		/** function change filename **/
		$scope.immig.files=[];
		var g = new File([""], items.filename);
		$scope.immig.files.push(g);
		
		
		/** function get immigration issuer **/
		$http.get(ApiURL.url+'/api/module/immigration-issuer?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.checklist = res.data;
		});
		
		/** function save **/
		$scope.save = function(file){
			for(a in $scope.checklist){
				if($scope.checklist[a].title == $scope.immig.issued_by){
					$scope.immig.issued_by = $scope.checklist[a].id;
				};
			};
			if(file){
				if(file[0].name=='Not Defined'){
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/employee-details/immigration/update/'+ items.id +'/'+ $scope.id +'?key='+$cookieStore.get('key_api'),
						data :$scope.immig     
					}).then(function(res){
						var temp=res.data.data[0];
						temp.filename='Not Defined'
						$modalInstance.close(temp);
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
					});
				}else{
					/** function upload **/
					file.upload = Upload.upload({
						method : 'POST',
						url : ApiURL.url + '/api/employee-details/immigration/update/'+ items.id +'/'+ $scope.id +'?key='+$cookieStore.get('key_api'),
						data :$scope.immig,
						file: file
					}).then(function(res){
						$modalInstance.close(res.data.data[0]); 
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
					});
				}
			}else{				
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/employee-details/immigration/update/'+ items.id +'/'+ $scope.id +'?key='+$cookieStore.get('key_api'),
					data :$scope.immig     
				}).then(function(res){
					//console.log(res.data.data)
					var temp=res.data.data[0];
					temp.filename='Not Defined'
						//console.log(temp)
						$modalInstance.close(temp);
						logger.logSuccess(res.data.header.message);
					},function(res){
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Data Immigration Failed")
					});
			}
		};
		
		
		/** function hide delete button upload **/
		$scope.$watch('items', function(){
			if(items.filename=='Not Defined'){
				$scope.delbutton=false;
			}else{
				$scope.delbutton=true;
			}
		});
		
		/** close modal **/
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}
		
		/** function hide button **/
		$scope.hide = function(a){
			var access  = permissionimmig.access;				
			console.log(access)
			if(access){
				switch(a){
					case 'update':
					if(access.update == 1){
						return true
					}
					break;
				}
			}
		};
		
	});