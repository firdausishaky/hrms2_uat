var appTimeInOut = angular.module("app.dashboard.timeInOut", ['ngFileUpload'])

appTimeInOut.factory('timeInOutFactory', function ($filter,$modal,$http,logger,ApiURL,$cookieStore,time,pagination,Upload) {
	function createheaders(){
		var temp = [];
		for(var i=0;i<arguments.length;i++){
			var x = {};
			x.id = i;
			x.name =arguments[i][0];
			x.sort =arguments[i][1];
			x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
			x.order =false;
			temp.push(x);
		}
		return temp;	
	}
	var NAME = '_schedule';
	var s = localStorage.getItem(NAME);
	var items = s ? JSON.parse(s) : {};
	var self;
	return self = {
		setheaders: function(){
			this.headers = new createheaders(
				['Date','date'],
				['Schedule Time','schedule'],
				['Time In','timeIn'],
				['Time Out','timeOut'],
				['Work Hours (Hrs)','WorkHours'],
				['Overtime (Hrs)','total_overtime'],
				['Overtime Rest Day (Hrs)','OvertimeRestDay'],
				['Short (Mins)','Short'],
				['Late (Mins)','Late'],
				['Early Out (Mins)','EarlyOut']
				);
			this.setactiveheader(this.headers[0],false)	
		},adddata: function(a){
			this.maindata.push(a);
		},getaccess:function(){
			$http({
				method : 'GET',
				url : ApiURL.url + '/api/attendance/training/api_check?key=' + $cookieStore.get('key_api')
			}).then(function(res){
				if(res.data.header.message == "Unauthorized"){
					self.getpermission(res.data.header.access);

					logger.logError("Access Unauthorized");
				}else{
					self.getpermission(res.data.header.access);
					self.button = true;
					localStorage.setItem(NAME,JSON.stringify([]));
					self.setform(res.data.data);
					logger.logSuccess('Access Granted');
				}
			},function(res){
				res.data.header ? logger.logError(res.data.header.message) : logger.logError("Get Access Failed")
			});
		},getpermission:function(active){
			if(active){
				this.mainaccess = active;
			}
		},button:function(a){
			if(this.mainaccess){
				switch(a){
					case 'read':
					if(this.mainaccess.read == 1){return true}
						break;
					case 'create':
					if(this.mainaccess.create == 1){return true}
						break;
					case 'update':
					if(this.mainaccess.update == 1){return true}
						break;
				}
			}
		},setform:function(data){
			if(data.status == 'user'){
				this.role = data.status;
				this.iduser = data.employee_id;
				this.nameuser = data.employee_name;
				self.form={};
				self.form.name = this.nameuser;
				self.hide = true;
			}else{
				this.role = data.status;
				self.form={};
				self.form.name = '';
				self.hide = false;
			}
			self.datastatus = data.status;
		},onSelect : function ($item,$model,$label){
			self.model = $item.name;
			self.label = $item.employee_id;
		},getLocation : function(val) {
			return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.name +'?&key=' + $cookieStore.get('key_api'),{
				params:{address : val,}
			}).then(function(response){
				if(response.data.data === null){
					logger.logError(response.data.header.message);
					var a = {};a.result = [];
					return a.result.map(function(item){
						return item;
					});  
				}else if(response.data != null){
					var a = {};
					a.result = response.data;
					return a.result.map(function(item){
						return item;
					});
				}  
			});
		},getYear : function(res){
			var results = res.data.data;
			var getdata = results.length;
			var arr = [];
			var monthArr = [];
			if(getdata > 0){
				var i = 0;
				for(; i < getdata; i++){
					var sub = res.data.data[i].date.substr(0,7);
					var month = res.data.data[i].date.substr(5,2);
					var day = res.data.data[i].date.substr(8,2);

					var subday = day.substr(0,1);
					if(subday == 0){
						day = day.substr(1,1);
					}
					arr[sub] = day;
				}

				var getFinally = [];
				for(key in arr){
					getFinally.push(key+'-'+arr[key]);
				}

				var countFinally = getFinally.length;

				var etc = [{"min" :  getFinally[0]},{"max" :  getFinally[countFinally-1]}];
				return etc;

			}
		},dateEnd : function(data){
				//console.log(data);
				var dates = data.toString();
				var arr = [];
				var arr = dates.split(" ");
				if(arr[1] ==  "Jan"){ arr[1] = 1;}
				if(arr[1] ==  "Feb"){ arr[1] = 2;}
				if(arr[1] ==  "Mar"){ arr[1] = 3;}
				if(arr[1] ==  "Apr"){ arr[1] = 4;}
				if(arr[1] ==  "May"){ arr[1] = 5;}
				if(arr[1] ==  "Jun"){ arr[1] = 6;}
				if(arr[1] ==  "Jul"){ arr[1] = 7;}
				if(arr[1] ==  "Aug"){ arr[1] = 8;}
				if(arr[1] ==  "Sep"){ arr[1] = 9;}
				if(arr[1] ==  "Oct"){ arr[1] = 10;}
				if(arr[1] ==  "Nov"){ arr[1] = 11;}
				if(arr[1] ==  "Dec"){ arr[1] = 12;}
				
				var newdate = arr[3]+'-'+arr[1]+'-'+arr[2];
				//console.log(newdate);
				var result = self.getdateEnd
				//console.log(result);
				var count = result.length;
				var endSearch = [];
				if(count > 0){
					for(var i = 0; i < count; i++){
						
						if(result[i].date ==  newdate){
							endSearch =  result[i];
						}
					}
				}
				//console.log(endSearch);
				if(endSearch == null){
					res.data.header ? logger.logError("data not found") : logger.logError("Data not found")
				}else{
					self.setdata([endSearch]);
				}
				
			},next_paging : function(data,f){
				console.log("f", f);
				console.log("data", data);
				console.log("self.paging", self.paging);
				
				
				if(data > 2 && f  ==  'end' ){
					self.paging[0] = '...'
					self.paging[1] = self.default_paginate[data];

					if(self.default_paginate[data+1] >  self.default_paginate[data]){
						self.paging[2] =  self.default_paginate[data+1] 
					}else{
						delete self.paging[2];
					}
					
					self.paging[3] = '...'
				}else if(data ==  1 && f  == "end"){

				}else{

				}


			},
			setToday : function(datas,type){
				var counts = 1;
				var page = 1;
				var dt = this.maindata;
				console.log("setToday",datas,dt[0].date);
				for(var i in dt){
					if(type=='date'){
						if(dt[i].date == datas){
							//console.log("PAGING",i);
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;
						}
					}else if(type=="month"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getMonth()+1;
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}else if(type=="year"){
						var month_dt = new Date(dt[i].date);
						var m = month_dt.getFullYear();
						if(m==datas){
							bagi = i/10;
							page = Math.floor(bagi);
							if(page < bagi){ page++; }
							break;	
						}
					}
				}
				self.Selectdate 	= datas;
				self.Selectdate_end = dt[dt.length-1].date;
				self.Selectdate_start = dt[0].date;
				
				if(page==0){ page++; }
				console.log(page,"PAGE");
				self.selectedPage 	= page;
				self.currentPage 	= self.selectedPage;
				//self.currentPage = 25;
			},save : function(){
				if(self.label){						
					self.form.name = self.label;
				}else{
					self.form.name = this.iduser;
				}
				/*if(this.role == 'user'){
					self.form.name = this.iduser;
				}else{
					console.log('test')
					self.form.name = self.label;
				}*/
				var dates = new Date();
				var m=dates.getMonth()+1;
				var d = dates.getDate();
				dates = dates.getFullYear()+"-0"+m;
				self.form.datess = dates;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/schedule/record?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					if(res != null){
						self.getYear(res);
					}
					var dates = new Date();
					var m=dates.getMonth()+1;
					var d = dates.getDate();

					if(dates.getDate().toString().length == 1){ d = "0"+dates.getDate(); }
					
					if(m.toString().length == 1){ dates = dates.getFullYear()+"-0"+m+"-"+d; }
					else{ dates = dates.getFullYear()+"-"+m+"-"+d; }

					self.today = dates;

					
					
					self.default_paginate  = Array.apply(null, Array(res.data.paging)).map(function (_, i) {return i;});
					self.paging = Array.apply(null, Array(res.data.paging)).map(function (_, i) {return i;});
					if(self.paging.length > 3){
						self.paging = Array.apply(null, Array(3)).map(function (_, i) {return i;});
						self.paging[3] = '...';
					}
					//console.log("self.paging", self.selectedPage);

					var nonerrs =  JSON.stringify(res.data.data);
					localStorage.setItem(NAME,nonerrs); 
					self.getdateEnd = res.data.data;
					self.min = self.getYear(res)[0].min
					self.max = self.getYear(res)[1].max
					self.button = false;
					if(self.datastatus=='user'){
						self.form.name = self.nameuser;
					}else{
						self.form.name = self.model;
					}

					for(var prop in res.data.data){
						if(res.data.data[prop].Late != "-"){
							try{
								var modify = res.data.data[prop].Late;
								var split = modify.split(":");
								var lastdata = parseInt(split[0]);
								var join = (parseInt(lastdata)*60) + parseInt(split[1]);
								res.data.data[prop].Late = join;
							}catch(e){
								
							}
						 	/*var fc = 0;
						 	if(split[0].charAt(0) == 0){
						 		var lastdata = parseInt(split[0]);
						 		fc = lastdata * 60;
						 	}else{
						 		fc = split[0] * 60
						 	}

						 	var join = parseInt(fc) + parseInt(split[1]);
						 	res.data.data[prop].Late = join;*/
						 }
						}
						self.setdata(res.data.data);
						self.setToday(dates, "date");
						if(res.data.status==500){
							console.log(res)
							logger.logSuccess(res.data.message);
						}else{
							logger.logSuccess(res.data.message);
						}
					},function(res){
						if(self.datastatus=='user'){
							self.form.name = self.nameuser;
						}else{
							self.form.name = self.model;
						}
						res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Employee Attendance Record Failed")
					});
			},restore : function(){
				self.setdata(items);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;
				}
			},
			
			/** set page per period **/
			// getPeriod : function(x,y){
			// 	if(self.maindata    == {}  || self.maindata == undefined){
			// 		//KUDOS
			// 	}else{
			// 		var b = (self.filtered ? self.filtered.length : 0 );
			// 		if(self.maindata){
			// 				while(y % 3 != 0){
			// 					y++
			// 				}
			// 			switch(x){
			// 				case 1:
			// 					(0 + ((y - 3) * 10)) <= self.maindata.length ?  self.period = self.maindata[(0 + ((y - 3) * 10))].between : self.period = '';

			// 					return self.period
			// 					//return (0 + ((y - 3) * 10)) <= self.maindata.length ?  (0 + ((y - 3) * 10)) : '-1';
			// 					break;
			// 				case 2:
			// 					(0 + ((y - 2) * 10)) <= self.maindata.length ?  self.period = self.maindata[(0 + ((y - 2) * 10))].between : self.period = '';
			// 					return self.period
			// 					//return (0 + ((y - 2) * 10)) <= self.maindata.length ?  (0 + ((y - 2) * 10)) : '-1';
			// 					break;
			// 				case 3:
			// 					(0 + ((y - 1) * 10)) <= self.maindata.length ?  self.period = self.maindata[(0 + ((y - 1) * 10))].between : self.period = '';
			// 					return self.period
			// 					//return (0 + ((y - 1) * 10)) <= self.maindata.length ?  (0 + ((y - 1) * 10)) : '-1';
			// 					break;	
			// 			}				
			// 		}
			// 	}

			// },
			/** set page per period **/
			getActive : function(y){
				if(self.maindata){	
					while (true){
						if( y == 1 || y == 2 || y == 3){
							break;
						}
						y = y - 3;
					}
					return y;
				}
			},
			modals: {
				open:{
					animation: true,
					templateUrl: 'open',
					controller: 'opentime',
					controllerAs: 'open',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Time-in / Time-Out"
						}						
					}
				},
				late:{
					animation: true,
					templateUrl: 'late',
					controller: 'modallate',
					controllerAs: 'late',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Late Request"
						}
						
					}
				},
				early:{
					animation: true,
					templateUrl: 'late',
					controller: 'modalearly',
					controllerAs: 'late',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Early Out Exemption Request"
						}
					}
				}
			},
			openmodal:function(b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					

					if(data.file){
						var file = data.file;
						file.upload = Upload.upload({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule/update/detail?key=' + $cookieStore.get('key_api'),
							data :data,
							file: file
						}).then(function(res){
							var b = res.data.data;
							console.log(b)
							for(s in self.maindata){
								if(b.date === self.maindata[s].date){
									self.maindata[s] = res.data.data;
									console.log(self.maindata[s])
								}
							}
							console.log(res);
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Time In / Time Out Request Failed")
						});
					}else{
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule/update/detail?key=' + $cookieStore.get('key_api'),
							data :data
						}).then(function(res){
							var b = res.data.data;
							console.log(b)
							for(s in self.maindata){
								if(b.date === self.maindata[s].date){
									self.maindata[s] = res.data.data;
								}
							}
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.message ? logger.logError(res.data.message) : logger.logError("Saving Time In / Time Out Request Failed")
						});
					}


					
					
				});


			},getActive : function(y){
				if(self.maindata){	
					while (true){
						if( y == 1 || y == 2 || y == 3){
							break;
						}
						y = y - 3;
					}
					return y;
				}
			},
			getdateRequest : function(form){
				a = self.maindata;
				b = form;
				console.log(a)
				console.log(b)
				c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeIn = d.timeIn;
						b.timeOut = d.timeOut;
						b.schedule = d.schedule;
						console.log(d)
						break;
					}else{
						var d = '-';
						form.timeIn = d;
						form.timeOut = d;
						form.schedule = d;
						console.log(d)

					}
				}
			},getlateRequest : function(form){
				var a = self.maindata;
				var b = form;
				console.log(a)
				console.log(b)
				var c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeIn = d.timeIn;
						b.Late = d.Late;
						b.schedule = d.schedule;

						break;
					}else{
						var d = '-';
						form.timeIn = d;
						form.Late = d;
						form.schedule = d;
					}
				}
			},getearlyRequest : function(form){

				var a = self.maindata;
				var b = form;
				console.log(a)
				console.log(b)
				var c = [];
				for(c in a){
					if(a[c].date == b.date){
						var d = a[c];
						b.timeOut = d.timeOut;
						b.EarlyOut = d.EarlyOut;
						b.schedule = d.schedule;
						break;
					}else{
						var d = '-';
						form.timeOut = d;
						form.EarlyOut = d;
						form.schedule = d;
					}
				}
			},setdisabled:function(a){
				if(a.timeIn=='-' || a.timeIn=='00:00'){
					self.formdisabled = false
				}else{
					self.formdisabled = true
				}
			}, getName:function(){
				self.newData()
			}, 
			lastData: function(){

				var dates = new Date();
				var m=dates.getMonth()+1;
				var d = dates.getDate();
				dates = dates.getFullYear()+"-0"+m;
				const dataPost= {name : self.getID(), datess: dates}

				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/schedule/record?key=' + $cookieStore.get('key_api'),
					data : dataPost,
					// file: file
				}).then(function(res){
					var x = res.data.data
					for (var i = 0; i < x.length; i++) {
						var lastdata = x[i]
						if((i+1)==(x.length)){
							return(lastdata)
						}
					}
				})
			},
			newData: function(){
				var dates = new Date();
				var m=dates.getMonth()+1;
				var d = dates.getDate();
				dates = dates.getFullYear()+"-0"+m;
				const dataPost= {name : self.getID(), datess: dates}
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/attendance/schedule/record?key=' + $cookieStore.get('key_api'),
					data : dataPost,
				}).then(function(res){
					return res.data.data
				})
			},
			getID: function(){
				const onlyUser = $cookieStore.get('UserAccess')
				const notUser = $cookieStore.get('NotUser')
				if (notUser == null) {
					return(onlyUser.id)
				}else{
					return(notUser)
				}
				
			}, 
			saveData(a,b){
				self.datatoprocess = b
				result.then(function (data){
					if(data.file){
						var file = data.file;
						file.upload = Upfile.upload = Upload.upload({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule/update/detail?key=' + $cookieStore.get('key_api'),
							data :data,
							file: file
						}).then(function(res){
							var b = res.data.data;
							console.log(b)
							for(s in self.maindata){
								if(b.date === self.maindata[s].date){
									self.maindata[s] = res.data.data;
									console.log(self.maindata[s])
								}
							}
							console.log(res);
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.header ? logger.logError(res.data.header.message) : logger.logError("Saving Failed")
						})
					}else{
						$http({
							method : 'POST',
							url : ApiURL.url + '/api/attendance/schedule/update/detail?key=' + $cookieStore.get('key_api'),
							data :data
						}).then(function(res){
							var b = res.data.data;
							console.log(b)
							for(s in self.maindata){
								if(b.date === self.maindata[s].date){
									self.maindata[s] = res.data.data;
								}
							}
							logger.logSuccess(res.data.header.message);
						},function(res){
							res.data.message ? logger.logError(res.data.message) : logger.logError("Saving Failed")
						});
					}
				})
			}

		}
	})




appTimeInOut.controller('timeInOutController', function (timeInOutFactory,ApiURL,$cookieStore,$scope,$http,$filter) {
	

	var self = this;
	self.handler = timeInOutFactory;
	self.handler.getaccess()


	const data = {C_TimeIn: "green",
	C_TimeOut: "green",
	EarlyOut: "-",
	Late: 5,
	Name: "Sean Carlo Valencia Cachuela",
	OvertimeRestDay: "-",
	OvertimeRestDay_Color: "violet",
	Short: "-",
	WorkHours: "9:50",
	between: "2017-09-01<==>2017-09-10",
	colorLate: "violet",
	date: "2017-09-02",
	early_c: "violet",
	employee_id: "2016015",
	first_name: "Sean Carlo",
	last_name: "Cachuela",
	new_color_overtime: "grey",
	nextday: null,
	noTimeIn: "no",
	noTimeOut: "yes",
	overStatus: "no",
	requestType: "Time-in / Time-Out",
	schedule: "07:00-16:00",
	schedule_shift_code: "A2",
	status: "yes",
	timeIn: "07:05",
	timeInStatus: "no",
	timeOut: "16:55",
	timeOutStatus: "no",
	total_overtime: "-",
	workStatus: "no"}
	self.form = data
	// console.log(self.form)
	// self.form.requestType = title;
	self.form = angular.copy(data);
	self.handler.setdisabled(data)
	self.handler.formdisabled = false;


	/** function get date request **/
	self.dateRequest = function(){
		self.handler.getdateRequest(self.form)
	}


	/** time picker time in **/
	var a = new Date()
	a.setHours(0,0,0,0)
	self.getTimeIn = a
	self.hstep = 1;
	self.mstep = 1;
	self.changed = function(){
		var a = $filter('date')(self.getTimeIn, 'yyyy-MM-dd HH:mm')
		var b = a.substring(11,16)
		self.form.newtimeIn = b;
	}

	/** time picker time out **/
	self.getTimeOut = a
	self.hstepOut = 1
	self.mstepOut = 1
	self.changedOut = function() {
		var a = $filter('date')(self.getTimeOut, 'yyyy-MM-dd HH:mm')
		var b = a.substring(11,16)
		self.form.newtimeOut = b;
	}

	/** function save time in **/
	self.save = function(file){
		self.form.file = file;
		// self.handler.saveData({a:data, b:self.form})
		//$modalInstance.close({a:data,b:self.form});
		// close(self.form);
		self.handler.saveData(self.form)
	}

	self.cancel = function(){
		// dismiss("Close")
		
	}
})


appTimeInOut.controller('lateDashController', function (timeInOutFactory,ApiURL,$cookieStore,$scope,$http,$filter) {
	var self = this;
	const data = {C_TimeIn: "green",
	C_TimeOut: "green",
	EarlyOut: "-",
	Late: 5,
	Name: "Sean Carlo Valencia Cachuela",
	OvertimeRestDay: "-",
	OvertimeRestDay_Color: "violet",
	Short: "-",
	WorkHours: "9:50",
	between: "2017-09-01<==>2017-09-10",
	colorLate: "violet",
	date: "2017-09-02",
	early_c: "violet",
	employee_id: "2016015",
	first_name: "Sean Carlo",
	last_name: "Cachuela",
	new_color_overtime: "grey",
	nextday: null,
	noTimeIn: "no",
	noTimeOut: "yes",
	overStatus: "no",
	requestType: "Time-in / Time-Out",
	schedule: "07:00-16:00",
	schedule_shift_code: "A2",
	status: "yes",
	timeIn: "07:05",
	timeInStatus: "no",
	timeOut: "16:55",
	timeOutStatus: "no",
	total_overtime: "-",
	workStatus: "no"}

	self.form = data;
	// self.form.title = title;
	// self.form.requestType = title;
	self.handler = timeInOutFactory;
	self.form = angular.copy(data);

	self.getlateRequest = function(){
		self.form.requestType = self.form.title;
		self.handler.getlateRequest(self.form)
	}
	self.save = function(file){
		self.form.file = file;
		// $modalInstance.close(self.form);
	},
	self.cancel = function(){
		// $modalInstance.dismiss("Close")
	}



})


appTimeInOut.controller('earlyOutController', function (timeInOutFactory,ApiURL,$cookieStore,$scope,$http,$filter) {
	var self = this;
	const data = {C_TimeIn: "green",
	C_TimeOut: "green",
	EarlyOut: "-",
	Late: 5,
	Name: "Sean Carlo Valencia Cachuela",
	OvertimeRestDay: "-",
	OvertimeRestDay_Color: "violet",
	Short: "-",
	WorkHours: "9:50",
	between: "2017-09-01<==>2017-09-10",
	colorLate: "violet",
	date: "2017-09-02",
	early_c: "violet",
	employee_id: "2016015",
	first_name: "Sean Carlo",
	last_name: "Cachuela",
	new_color_overtime: "grey",
	nextday: null,
	noTimeIn: "no",
	noTimeOut: "yes",
	overStatus: "no",
	requestType: "Time-in / Time-Out",
	schedule: "07:00-16:00",
	schedule_shift_code: "A2",
	status: "yes",
	timeIn: "07:05",
	timeInStatus: "no",
	timeOut: "16:55",
	timeOutStatus: "no",
	total_overtime: "-",
	workStatus: "no"}

	// self.form.title = title;
	// self.form.requestType = title;
	self.handler = timeInOutFactory;
	self.form = angular.copy(data);

	/** function get early request **/
	self.getearlyRequest = function(){
		self.form.requestType = self.form.title; 
		self.handler.getearlyRequest(self.form)
	}
	self.save = function(file){
		self.form.file = file;
		// $modalInstance.close(self.form);
	},
	self.cancel = function(){
		// $modalInstance.dismiss("Close")
	}

})

