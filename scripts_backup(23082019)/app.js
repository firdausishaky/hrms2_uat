var app = angular.module('app',[
	"ngRoute", 
	"ngAnimate",
	"ui.bootstrap",
	"textAngular", 
	"ui.tree",
	"ui.calendar",
	"checklist-model",
	"ngFileUpload",
	"ngMap",
	"ui.mask",
	"app.dashboard",
	"ngCookies",
	"ngTagsInput", 
	"angular-loading-bar",
	"app.controllers",
	"app.localization",
	"app.task",
	"app.directives",
	"app.nav",
	"app.login",
	"app.factory.job",
	"app.factory.report",
	"app.ui.services",
	"app.ui.ctrls",
	"app.ui.directives",
	"app.ui.map",
	"app.form.validation",
	"app.form.csv",
	"app.form.personal",
	"app.form.emmergency",
	"app.form.dependents",
	"app.form.immigrations",
	"app.form.report",
	"app.form.workExp",
	"app.form.skill",
	"app.form.educati",
	"app.form.language",
	"app.form.seminar",
	"app.form.jobHistory",
	"app.form.salary",
	"app.form.org",
	"app.org.depart",
	"app.form.contact",
	"app.ui.form.ctrls",
	"app.ui.form.directives",
	"app.page.ctrls",
	"app.user.management",
	"app.tables.job",
	"app.job.employeestatus",
	"app.job.workshift",
	"app.qual.skill",
	"app.qual.education",
	"app.qual.language",
	"app.tables.national",
	"app.pim.termination",
	"app.pim.immigration",
	"app.pim.document",
	"app.pim.emplist",
	"app.pim.infractionlist",
	"app.pim.addinfraction",
	"app.config.mail",
	"app.config.biometric",
	"app.config.ldap",
	"app.side.menu",
	"app.att.cutoffperiod",
	"app.att.attendancecutoff",
	"app.att.empattrecord",
	"app.att.attrecord",
	
	"app.att.scheduleimport",
	"app.att.scheduleList",
	"app.att.scheduleList2",
	"app.att.fixSchedule",
	"app.att.csvImport",
	"app.att.sRequestList",
	"app.att.unApproved",
	"app.att.trainingOff",
	"app.att.overUnder",
	"app.att.changeShift",
	"app.att.manualinput",
	"app.leave.holiday",
	"app.leave.tables",
	"app.leave.entitlement",
	"app.leave.list",
	"app.leave.report",
	"app.leave.request",
	"app.dashboard.menu",
	"app.dashboard.personal",
	"app.dashboard.contact",
	"app.dashboard.emmergency",
	"app.dashboard.dependents",	
	"app.dashboard.immigrations",
	"app.dashboard.report",
	"app.dashboard.history",
	"app.dashboard.salary",
	"app.dashboard.workExp",
	"app.dashboard.educati",
	"app.dashboard.skill",
	"app.dashboard.language",
	"app.dashboard.seminar",
	"app.dashboard.timeInOut",
	"app.dashboard.LeaveList",
	"app.dashboard.sReqList",
	"app.dashboard.leaveReq",
	"app.dashboard.trainingReq",
	"app.dashboard.overtimeReq",
	"app.dashboard.changeShiftReq",
	"app.dashboard.photograph",
	//'app.dash.emppAttRecordNew',
	////////////SALARY////////////////
	"app.salary.incentive_and_allowance",	
	"app.salary.performance_bonus",
	"app.salary.suspension_rate",
	"app.salary.monthly_tax",
	"app.salary.annual_tax",
	"app.salary.sss",
	"app.salary.hdmf",
	"app.salary.philhealth",
	"app.salary.salaryNewProfile",
	"app.salary.salaryCutOff",
	"app.salary.adjustmentHistory",
	"app.salary.deduction_personal",
	"app.salary.monthIncentiveAllow",
	"app.salary.yearlyBonus",
	"app.salary.serviceIncentiveLeave",
	"app.salary.performanceBonusYear",
	"app.salary.managementBonusYear",
	'app.salary.witholdingTax',
	'app.salary.annualTaxComputation',
	'app.salary.payrollPerCutoff',
	'app.salary.monthlyExpatPayroll',
	'app.salary.otherReports',
	'app.salary.incentiveAllowancePersonal'


	
	]);


// ################################################    API    ###########################################################################

app.factory('ApiURL', function(){
	return {
	 url:'http://hrmsapi2uat.leekie.com:8080'
		//url:'http://hrmsapi2uat.leekie.com:8942'
	//   url:'http://local.dev:4000'
	   //url:'http://laravelsite.com'
//url:'http://hrmsdev.net',
// url:'http://hrmsdev.net:8080'
	   //url:'http://127.0.0.1:8080'

};
});



app.factory('skillURL', function(){
	return {
		url:' /api/qualification/skill/'
	};
});


app.factory('pagination',function(){
	return {
		option:[10,20,50,100,1000]
	}
});






app.factory('Auth', ["ApiURL","$http","$cookieStore",function(api,$http,$cookieStore){
	var keyApi = $cookieStore.get('key_api');

	return {

			// LINK AUTH / LOGIN  
			
			get:function(){
				return $http.get(api.url+'/api/login/get-token');
			},
			
			login_ldap:function(data){
				var authen_ldap =  $http({
					method:'POST',
					url:api.url+'/api/login/authen',
					headers:{ 'Content-Type':'application/x-www-form-urlencoded', },
					data:$.param(data)
				});
				return authen_ldap;
			},
			
			sendToken:function(data,url){
				var send =  $http({
					method:'POST',
					url:url,
					headers:{ 'Content-Type':'application/x-www-form-urlencoded', },
					data:$.param(data)
					// yang dikirim username,password dan auth
				});
				return send;
			},

			getstat:function(){
				return $http.get(api.url+'api/login/status');
			},


			// LINK EMPLOYEES

			getAllEmployees:function(){
				return $http.get(api.url+'employees/get-data');
			},
			
			destroyEmployees:function(id){
				return $http.delete(api.url+'api/comments/'+id);
			},
			
			save:function(data){
				var authen =  $http({
					method:'POST',
					url:api.url+'/api/login/post-auth',
					headers:{ 'Content-Type':'application/x-www-form-urlencoded', },
					data:$.param(data)
				});
				return authen;
			}
		}

	}]);


app.config(["$routeProvider","$locationProvider", function($routeProvider) {
	var routes, setRoutes;
	routes = [
	"dashboard", 
	"user/usermenu",
	"user/costum",
	"job/job",
	"job/employeStatus",
	"job/workShift",
	"organization/general",
	"organization/structure",
	"organization/department",
	"qualification/skills",
	"qualification/education",
	"qualification/language",
	"national/national",
	"config/emailSetting",
	"config/ldap",
	"config/biometric",
	"pim/csv",
	"pim/termination",
	"pim/immigration",
	"pim/docTemplate",
	"pim/employeeList",
	"pim/addInfraction",
	"pim/InfractionList",
	"attendance/cutOff",
	"attendance/scheduleList",
	"attendance/scheduleList2",
	"attendance/scheduleImport",
	"attendance/attCutOff",
	"attendance/attRecord",
	"attendance/unApproved",
	"attendance/fixSchedule",
	"attendance/employeeAttendanceRecord",
	"attendance/manualinputAttRecord",
	"attendance/scheduleRequest",
	"attendance/trainingOff",
	"attendance/overtimeUnder",
	"attendance/changeShift",
	"attendance/ScheduleRequestList",
	"attendance/sRequestDetails",
	"leave/holiday",
	"leave/leaveTable",
	"leave/leaveEntitlement",
	"leave/leaveReport",
	"leave/leaveList",
	"leave/leaveRequest",
	"employee/personalDetail/:id",
	"employee/contact/:id",
	"employee/emergency/:id",
	"employee/immigrations/:id",
	"employee/job/:id",
	"employee/qualification/:id",
	"employee/report/:id",
	"employee/salary/:id",
	"employee/dependents/:id",
	"employee/addEmployee/:id",
	"employee/photograph/:id",
	//	"employee/addEmployee",
	"pages/404", 
	"pages/500", 
	"pages/blank", 
	"pages/forgot-password", 
	"pages/invoice", 
	"pages/lock-screen", 
	"pages/profile", 
	"pages/signin", 
	"schedule/calendar",
	"dashboard2",
	"dashboard/timeInTimeOut",


	//SALARY MASTER
	"salary/incentive_and_allowance",
	"salary/performance_bonus",
	"salary/suspension_rate",
	"salary/monthly_tax",
	"salary/annual_tax",
	"salary/sss",
	"salary/philhealth",
	"salary/hdmf",



	// SALARY ADJUSTMENT

	"salary/adjustmentSalary",
	"salary/personal/deduction_personal",
	"salary/monthIncentiveAllowance",
	"salary/yearlyBonus",
	"salary/serviceIncentiveLeave",
	"salary/management_bonus",
	"salary/annualTaxComputation",
	"salary/payrollPerCutOff",
	"salary/monthlyExpatPayroll",
	"salary/monthlyGovernmentsReport",
	"salary/other_reports",
	// "salary/personalincentiveallowance"
	





	]; 
	setRoutes = function(route) {
		var config, url;

		url = "/" + route;
		config = {
			templateUrl:"views/" + route.replace('/:id','') + ".html"

		}; 
		$routeProvider.when(url, config)
		return $routeProvider
	};
	routes.forEach(function(route) {
		return setRoutes(route)
	}); 
	$routeProvider.when("/", {
		redirectTo:"/dashboard2"
	}).when("/404", {
		templateUrl:"views/pages/404.html"
	}).otherwise({
		redirectTo:"/404"
	})

}]);


app.run(['$location', '$cookies','$cookieStore', '$http','$rootScope',
	function ($location, $cookies, $cookieStore,$http,$rootScope) {
		$rootScope.$on('$routeChangeStart', function (event) {
				// jika lokasi ke pages sign in key api di restart 
				if($location.path() == '/pages/signin'){
					// hapus key api 
					$cookieStore.remove('key_api');
				}else{
					// jika lokasi bukan signin ambil key api  
					var check = $cookieStore.get('key_api');
					// jika di cek ada
					if (check) {
						// masuk ke dashboard
						// $location.path('/dashboard');
					}
					else{
						// jika tidak ada remove key api dan kembali ke signin
						$cookieStore.remove('key_api');
						$location.path('pages/signin');
					}
				};
				
			});

	}]);


app.factory('time',function(){
	return {
		schedule:[ "00:00","00:15", "00:30", "00:45",
		"01:00","01:15", "01:30", "01:45",
		"02:00","02:15", "02:30", "02:45", 
		"03:00","03:15", "03:30", "03:45", 
		"04:00","04:15", "04:30", "04:45",
		"05:00","05:15", "05:30", "05:45", 
		"06:00","06:15", "06:30", "06:45",
		"07:00","07:15", "07:30", "07:45", 
		"08:00","08:15", "08:30", "08:45", 
		"09:00","09:15", "09:30", "09:45", 
		"10:00","10:15", "10:30", "10:45", 
		"11:00","11:15", "11:30", "11:45", 
		"12:00","12:15", "12:30", "12:45", 
		"13:00","13:15", "13:30", "13:45", 
		"14:00","14:15", "14:30", "14:45", 
		"15:00","15:15", "15:30", "15:45", 
		"16:00","16:15", "16:30", "16:45", 
		"17:00","17:15", "17:30", "17:45", 
		"18:00","18:15", "18:30", "18:45", 
		"19:00","19:15", "19:30", "19:45", 
		"20:00","20:15", "20:30", "20:45", 
		"21:00","21:15", "21:30", "21:45", 
		"22:00","22:15", "22:30", "22:45", 
		"23:00","23:15", "23:30", "23:45"]
	}
});
