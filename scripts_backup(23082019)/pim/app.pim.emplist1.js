(function(){
			
	angular.module('app.pim.emplist',['checklist-model','ngFileUpload'])

	.factory('emplistfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,Upload ){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['ID','employee_id',null],
					['First(& Middle) Name ','first_name',null],
					['Last Name','title',null],
					['Date Hired','date_hire',null],
					['Job Title','job',null],
					['Employment Status','status',null],
					['Department','sub_unit',null],
					['Supervisor','supervisor',null]
				);
				this.setactiveheader(this.headers[0],false)
			},getdatalist:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/module/employee-info?lc=' + 0 + '&inc=' + 0 + '&key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setselect()
					self.setactionadd(res.data.data);
					self.setdata(res.data.data.list)
					self.getpermission(res.data.header.access);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Employee List Failed")
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
						case 'read':
							if(this.mainaccess.read == 1){return true}		
						break;
					}
				}
			},searchemp:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/module/employee-info?lc=' + self.form.category + '&inc=' + self.form.include + '&key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setdata(res.data.data.list)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
				});
			},setselect:function(){
				self.form={};
				self.form.category=1;
				self.form.include=1;
			},setactionadd:function(a){
				this.data=a;
			},actionadd:function(a){
				if(this.data){	
					switch (a) {
						case 'action':
							if(this.data.action_add==true){
								return true
							}
						break;
					}
				}
			},getautonumber:function(data){
				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/module/employee-info/add/employee?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setmodaladd(data,res.data.data);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Failed")
				});
			},setmodaladd:function(dataA,dataB){
				dataA.autonumb=dataB.autonumb;
				if(dataB.local_it=='expat'){
					self.hideexpat=false
				}else{
					self.hideexpat=true
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addemployee',
					controller: 'addemployeectrl',
					controllerAs: 'addemployee',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
								if(data.file){
									var file = data.file;
									delete data.file;
									file.upload = Upload.upload({
										method : 'POST',
										url : ApiURL.url + '/api/module/employee-info/add/employee/store?key=' + $cookieStore.get('key_api'),
										data :data,
										file: file
									}).then(function(res){
										console.log(res.data.data)
										self.adddata(res.data.data);
										logger.logSuccess(res.data.header.message);
									},function(res){
										res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
									});
								}else{
									$http({
										method : 'POST',
										url : ApiURL.url + '/api/module/employee-info/add/employee/store?key=' + $cookieStore.get('key_api'),
										data : data
									}).then(function(res){
										console.log(res.data.data)
										self.adddata(res.data.data);
										logger.logSuccess(res.data.header.message);
									},function(res){
										res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
									});
								}
						break;
					}
					
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/module/employee-info/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i){
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.employee_id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Employee Success');
				},function(res){
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Employee Failed")
				})
			}
		}
	})

	.controller('employeListCtrl',function(emplistfactory){
		var self = this;
			self.handler = emplistfactory;
			self.handler.getdatalist();
	})
	
	.controller('addemployeectrl',function(emplistfactory,$modalInstance){	
		var self = this;
			self.handler = emplistfactory;
			self.form={};
			self.handler.getautonumber(self.form);
			self.save = function(file){
				self.form.file=file;
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
	})
	
	.controller('modaleditterm',function(emplistfactory,$modalInstance,$timeout,data,time){	
		var self = this;
			self.handler = emplistfactory;
			self.handler.button();
			self.form = data;
			self.form = angular.copy(data);
			console.log(self.form)
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})
	.run(function(emplistfactory){
		emplistfactory.setheaders();
	})

}())