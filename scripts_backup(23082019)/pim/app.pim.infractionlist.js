(function(){

	angular.module('app.pim.infractionlist',['checklist-model'])
	
	.factory('infractfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,Upload,filterFilter,$window){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;
		}
		var self;
		return self = {
			checklist : [],
			setheaders: function(){
				this.headers = new createheaders(
					['Name','employee_name',20],
					['Department','department',15],
					['Job Title','job_title',17],
					['Date','date',15],
					['Infraction',20],
					['Sanction','sanction',17]
				);
				this.setactiveheader(this.headers[0],false)
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				edit:{
					animation: true,
					templateUrl: 'modaleditinfrac',
					controller: 'modalinfractiondetail',
					controllerAs: 'modaleditinfrac',
					size:'lg',
					//backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title:function(){
							return "Detail Infraction"
						}	
					}
				}
			},openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'edit':
							if(data.file){
								var file = data.file;
								 data.file;
								file.upload = Upload.upload({
									method : 'POST',
									url : ApiURL.url + '/api/jobs/jobs-title?key='  + $cookieStore.get('key_api'),
									data :data,
									file: file
								}).then(function(res){
									self.setdata(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding  Job Title Failed")
								});
							}else{
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/infraction/update/?key='  + $cookieStore.get('key_api'),
									data : data,
								}).then(function(res){
									self.setdata(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding  Job Title Failed")
								});
							}
						break;
					}
				});
			},onSelect:function($item,$model,$label){self.model = $item.name;self.label = $item.employee_id;
			},getLocation:function(val){
				return $http.get(ApiURL.url + '/api/attendance/training/search/' + self.form.employee_id +'?&key=' + $cookieStore.get('key_api'),{
					params:{address:val,}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};a.result = [];
						return a.result.map(function(item){
							return item;
						});  
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},getaccess : function(){
				$http({
					method : 'GET',
					url : ApiURL.url +'/api/infraction/index?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setselect(res.data.data);
					self.mainaccess = res.data.header.access;
					logger.logSuccess('Access Granted');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave List Failed")
				});
			},setselect:function(select){
				self.data = select.department;
				self.jobtitle = select.jobtitle;
				self.form = {};
				self.form.from = select.date.from;
				self.form.to = select.date.to;
				self.form.pending = select.pending;
			},button:function(a){
				if(self.mainaccess){
					switch(a) {
						case 'read':if(self.mainaccess.read == 1){return true}break;
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
					}
				}
			},searchlist: function(){
				self.form.employee_id = self.label;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/infraction/search?key=' + $cookieStore.get('key_api'),
					data :self.form
				}).then(function(res){
					self.checklist = [];
					// if(res.data.data.length > 0){
					// 	for(var i=0; i < res.data.data; i++){
					// 		res.data.data[i].isChecked = { id : res.data.data[i].id, check : false }
					// 	}
					// }
					self.setdata(res.data.data);
					self.form.employee_id = self.model;
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.employee_id = self.model || self.name;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave List Failed")
				});
			},reset: function(){
				self.getaccess()
			},export: function(data){
				if(!data){void 0; logger.logError('Please Searching List Before Export') }
				for(a in self.maindata){
					console.log(self.maindata)
					delete self.maindata[a].id;
					alasql('SELECT * INTO XLSX("Infraction List.xlsx",{headers:true}) FROM ?',[self.maindata]);
					break;
				}
			},getfile:function(file){
				if(file == 'Not Defined'){
					logger.logError("File Not Found");
				}else{
					var e = ApiURL.url +  '/api/attendance/schedule-request-detail/getfile/' + file + '?key=' + $cookieStore.get('key_api');
					$window.open(e);
				}
			},deldatacheck: function(id){
				// var y = ApiURL.url +'/api/infraction/delete/'+ id +'?key=' + $cookieStore.get('key_api');
				// return console.log(id,y,"DELL");
				$http({
					method : 'GET',
					url : ApiURL.url +'/api/infraction/delete/'+ id +'?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					console.log('asdasdasdasdadadasdas')
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.checklist = [];
					logger.logSuccess('Deleting Infraction List Success');
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave List Failed")
				});
				/*$http({
					method : 'DELETE',
					url : ApiURL.url + 'api/infraction/delete/?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data);
					self.form.employee_id = self.model;
					logger.logSuccess(res.data.header.message);
				},function(res){
					self.form.employee_id = self.model || self.name;
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Searching Leave List Failed")
				});*/
				// $http({
				// 	method: 'POST',
				// 	url : ApiURL.url + 'api/infraction/delete?' + id + '?key=' + $cookieStore.get('key_api'),
				// }).then(function(res){
				// 	// var i;
				// 	// for (i = 0; i < id.length; ++i) {
				// 	// 	self.maindata = $filter(self.maindata, function (store) {
				// 	// 		return store.id != id[i];
				// 	// 	})
				// 	// }
				// 	// self.check = [];
				// 	logger.logSuccess('Deleting Infraction List Success');
				// },function(res){
				// 	console.log(res.data.header)
				// 	res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Infraction List Failed")
				// })
			},
			selectList : function(itm){
				if(self.checklist.length == 0){
					self.checklist.push(itm.id);
				}else{
					for(var i=0; i < self.checklist.length; i++){
						if(self.checklist[i] == itm.id){
							self.checklist.pop(i);
						}else{
							self.checklist.push(itm.id);
						}
					}
				}
			}
		}
	})

	.controller('infractionlistcontroller',function(infractfactory){
		var self = this;
			self.handler = infractfactory;
			self.handler.getaccess()
	})
	
	.controller('modalinfractiondetail',function(infractfactory,$modalInstance,data,title){
		var self = this;
			self.handler = infractfactory;
			self.title=title;
			self.form = data;
			self.save = function(file){
				self.form.file = file ? file : ''
				$modalInstance.close(self.form)
			}
			self.close = function(){
				$modalInstance.dismiss("close modal")
			}
	})
	
	.run(function(infractfactory){
		infractfactory.setheaders();
	})

}())