(function(){
			
	angular.module('app.tables.empList',['checklist-model','ngFileUpload'])

	.factory('emplistfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,Upload ){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['ID','employee_id',null],
					['First(& Middle) Name ','first_name',null],
					['Last Name','title',null],
					['Date Hired','date_hire',null],
					['Job Title','job',null],
					['Employment Status','status',null],
					['Department','sub_unit',null],
					['Supervisor','supervisor',null]
				);
				this.setactiveheader(this.headers[0],false)
			},getdatalist:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/module/employee-info?lc=' + 0 + '&inc=' + 0 + '&key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setselect()
					self.setactionadd(res.data.data);
					self.setdata(res.data.data.list)
					self.getpermission(res.data.header.access);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Employee List Failed")
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
						case 'read':
							if(this.mainaccess.read == 1){return true}		
						break;
					}
				}
			},searchemp:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/module/employee-info?lc=' + self.form.category + '&inc=' + self.form.category + '&key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setdata(res.data.data.list)
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
				});
			},setselect:function(){
				self.form={};
				self.form.category=1;
				self.form.include=1;
			},setactionadd:function(a){
				this.data=a;
			},actionadd:function(a){
				if(this.data){	
					switch (a) {
						case 'action':
							if(this.data.action_add==true){
								return true
							}
						break;
					}
				}
			},getautonumber:function(data){
				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/module/employee-info/add/employee?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					self.setmodaladd(data,res.data.data);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Failed")
				});
			},getselectall:function(URL){ 
				$http.get(URL).success(function(res){ console.log(res)
					console.log(res)
					if(res.header.message == 'success'){
						self.selectdepartment = res.data.department;
						self.selectStatus = res.data.leavestatus;
						self.jobTitle = res.data.jobTitle;
						self.mainaccess = res.header.access;
						logger.logSuccess('Access Granted');
					}else{
						self.mainaccess = res.header.access;
						logger.logError('Access Unauthorized');
					}
				});	
			},setmodaladd:function(dataA,dataB){
				dataA.autonumb=dataB.autonumb;
				if(dataB.local_it=='expat'){
					self.hideexpat=false
				}else{
					self.hideexpat=true
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addemployee',
					controller: 'addemployeectrl',
					controllerAs: 'addemployee',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
								if(data.file){
									var file = data.file;
									delete data.file;
									file.upload = Upload.upload({
										method : 'POST',
										url : ApiURL.url + '/api/module/employee-info/add/employee/store?key=' + $cookieStore.get('key_api'),
										data :data,
										file: file
									}).then(function(res){
										var temp=data;
											temp.employee_id=data.autonumb;
											self.adddata(temp)
										logger.logSuccess(res.data.header.message);
									},function(res){
										res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
									});
								}else{
									$http({
										method : 'POST',
										url : ApiURL.url + '/api/module/employee-info/add/employee/store?key=' + $cookieStore.get('key_api'),
										data : data
									}).then(function(res){
										var temp=data;
											temp.employee_id=data.autonumb;
											self.adddata(temp)
										logger.logSuccess(res.data.header.message);
									},function(res){
										res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
									});
								}
						break;
					}
					
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/module/employee-info/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i){
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.employee_id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Employee Success');
				},function(res){
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Employee Failed")
				})
			}
		}
	})

	.controller('employeListCtrl',function(emplistfactory){
		var self = this;
			self.handler = emplistfactory;
			self.handler.getdatalist();
	})
	
	.controller('addemployeectrl',function(emplistfactory,$modalInstance){	
		var self = this;
			self.handler = emplistfactory;
			self.form={};
			self.handler.getautonumber(self.form);
			self.save = function(file){
				self.form.file=file;
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
	})
	
	.controller('modaleditterm',function(emplistfactory,$modalInstance,$timeout,data,time){	
		var self = this;
			self.handler = emplistfactory;
			self.handler.button();
			self.form = data;
			self.form = angular.copy(data);
			console.log(self.form)
			self.save = function(){
				$modalInstance.close({a:data,b:self.form});
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})
	.run(function(emplistfactory){
		emplistfactory.setheaders();
	})

}())


/* var appTablesEmploye = angular.module("app.tables.empList",["ngFileUpload","checklist-model"]);

appTablesEmploye.controller("employeListCtrl",function($scope,$filter,$http,$modal,$log,$cookieStore,ApiURL,$routeParams,logger,filterFilter) {	
		
		$http.get(ApiURL.url + '/api/module/employee-info?lc=' + 0 + '&inc=' + 0 + '&key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.check ={currentPageStores:[]}
			$scope.include = 1;
			$scope.category = 1;
			var end, start;
			start =0;
			if(!res.data.action_add){
				$scope.list  = true;
			}else{
				$scope.list  = false;
			}
			$scope.stores = res.data.list;	
			$scope.filteredStores = res.data.list;
			if($scope.filteredStores){
				end = start + $scope.numPerPageOpt[2];
				$scope.currentPageStores = $scope.filteredStores.slice(start, end);
				$scope.select($scope.currentPage);
			}else{
				$scope.filteredStores = [];
			}	
		}).success(function(data){
				logger.logSuccess(data.header.message);
		}).error(function(data) {
				logger.logError(data.header.message);
		});
		
		
		
		$scope.click = function(){
			var search = $http.get(ApiURL.url + '/api/module/employee-info?lc=' + $scope.category + '&inc=' + $scope.include + '&key=' + $cookieStore.get('key_api') ).success(function(res){
				$scope.check ={currentPageStores: []}
				var end, start;
					start =0;
					$scope.stores = res.data.list;
					$scope.filteredStores = res.data.list;
					if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
					}else{
						$scope.filteredStores = [];
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);	
					}
			}).success(function(data) {
					logger.logSuccess(data.header.message);
			}).error(function(data) {
					logger.logError(data.header.message);
			});
		};	
		
			
			
		
			var init;
            $scope.numPerPageOpt = [3, 5, 10, 20]; 
            $scope.numPerPage = $scope.numPerPageOpt[2]; 
            $scope.currentPage = 1; 
            $scope.currentPageStores = []; 
            $scope.searchKeywords = ""; 
            $scope.filteredStores = []; 
            $scope.row = "" ;
            $scope.stores =[];
            $scope.select = function(page) {
                 var end, start;
                 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
            };
            $scope.onFilterChange = function() {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
            };
            $scope.onNumPerPageChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.onOrderChange = function() {
                return $scope.select(1), $scope.currentPage = 1
            };
            $scope.search = function() {
                return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
            };
            $scope.order = function(rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
            };
            init = function() {
                return $scope.search(), $scope.select($scope.currentPage)
            };
			
			
			
			$scope.open = function(size) {
				var modal;
				modal = $modal.open({
					templateUrl: "AddEmployee.html",
					controller: "AddEmployeeCtrl",
					backdrop : "static",
					size: size,
					resolve: {
						items: function(){
							return;
						}
					}
				});
				modal.result.then(function (newemployee) {
					if($scope.currentPageStores){
						$scope.currentPageStores.push(newemployee);
					}else{
						$scope.currentPageStores = [];
						$scope.currentPageStores.push(newemployee);
					}
				});
			};
			
			
			$scope.remove = function(id){
				$http({
					method : 'DELETE',
					url : ApiURL.url + '/api/module/employee-info/' + id + '?key=' + $cookieStore.get('key_api')
				}).success(function(data){						
					var i;
						for( i = 0; i < id.length; i++) {
							$scope.currentPageStores = filterFilter($scope.currentPageStores, function (store) {
								return store.employee_id != id[i];
							});
						};
					$scope.check.currentPageStores = [];
					logger.logSuccess(data.header.message);
				}).error(function(data){
					logger.logError(data.header.message);
				});
			};
			
});

appFormReport.controller("AddEmployeeCtrl", function($modalInstance,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams,Upload) {
				
		
		$http.get(ApiURL.url + '/api/module/employee-info/add/employee?key=' + $cookieStore.get('key_api') ).success(function(res){
			$scope.formData=res.data;
			if($scope.formData.local_it === "expat"){
				$scope.local = true;
			}else{
				$scope.local = false;
			}
		});		
				
			
		$scope.save = function(){	
				if($scope.id = null){	
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/module/employee-info/add/employee/store?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;
							temp.employee_id=$scope.formData.autonumb;	 						
						$modalInstance.close(temp);
						$scope.formData='';	
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				}else {
					$http({
						method : 'POST',
						url : ApiURL.url + '/api/module/employee-info/add/employee/store?key=' + $cookieStore.get('key_api'),
						headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
						data : $.param($scope.formData,$scope.id)   
					}).success(function(data, status, header, config) {
						var temp = {};
							temp = $scope.formData;
							temp.employee_id=$scope.formData.autonumb;	 						
						$modalInstance.close(temp);
						$scope.formData='';	
						logger.logSuccess(data.header.message);
					}).error(function(data, status, headers, config) {
						logger.logError(data.header.message);
					});
				}
		};
				
					
		$scope.upload = function (files) {
			if (files && files.length) {	
				for (var i = 0; i < files.length; i++) {
					var file = files[i];
					Upload.upload({
						url:  ApiURL.url + '/api/module/employee-info/add/employee?key=' + $cookieStore.get('key_api'),
						file: file
					}).success(function (data, status, header, message) {
						$scope.id = data.data.id;
						logger.logSuccess(data.header.message);
					}).error(function (data, status, header, message) {
						$scope.id = data.data.id;
						logger.logError(data.header.message);
					});
				}
			}
		};
					
					
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}
				
});

 */