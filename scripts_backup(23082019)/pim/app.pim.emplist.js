(function(){

	angular.module('app.pim.emplist',['checklist-model','ngFileUpload'])

	.factory('emplistfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter,Upload){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['ID','employee_id',null],
					['First(& Middle) Name ','first_name',null],
					['Last Name','title',null],
					['Date Hired','date_hire',null],
					['Job Title','job',null],
					['Employment Status','status',null],
					['Department','sub_unit',null],
					['Supervisor','supervisor',null]
					);
				this.setactiveheader(this.headers[0],false)
			},getdatalist:function(){
				$cookieStore.put('setdisabled','')
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/module/employee-info?lc=' + 1 + '&inc=' + 1 + '&key=' + $cookieStore.get('key_api')
				}).then(function(res){
					for (var i = 0; i < res.data.data.list.length; i++) {
						x = res.data.data.list[i].supervisor
						if (Array.isArray(x)) {
							res.data.data.list[i].supervisor = x.toString()

						}
					}
					self.setselect()
					self.setactionadd(res.data.data);
					if(res.data.data.list){						
						self.setdata(res.data.data.list)
					}else{
						self.setdata(null)
						self.maindata = [];
					}
					self.form = res.data.data
					//self.getpermission(res.data.header.access);
					logger.logSuccess(res.data.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Employee List Failed")
				});
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch (a) {
						case 'create':
						if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
						if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
						if(this.mainaccess.update == 1){return true}		
							break;
						case 'read':
						if(this.mainaccess.read == 1){return true}		
							break;
					}
				}
			},searchemp:function(){
				
				var filter = new Object();
				
				if(self.form.category){ filter.category = "&lc="+self.form.category; } else{ filter.category = "&lc=1"; }
				if(self.form.include){ filter.include = "&inc="+self.form.include;	 }else{ filter.include = "&inc=1"; }
				
				
				if(self.form.depart){
					filter.depart = "&depart="+self.form.depart;
				}if(self.form.employeeStatus){
					filter.employeeStatus = "&status="+self.form.employeeStatus;
				}if(self.form.employeeId){
					filter.employeeId = "&empId="+self.form.employeeId;
				}if(self.form.jobTitle){
					filter.jobTitle = "&job="+self.form.jobTitle;
				}if(self.form.spv){
					filter.spv = "&spv="+self.form.spv;
				}if(self.form.idEmployee){
					filter.idEmployee = "&id="+self.form.idEmployee;
				}
				
				
				var addrs = ApiURL.url+ '/api/module/employee-info?key='+ $cookieStore.get('key_api');
				for(var i in filter){
					addrs += filter[i] ;
				}
				
				$http({
					method : 'GET',
					url : addrs
				}).then(function(res){
					console.log(res.data.data.list)
					if(res.data.data.list){						
						self.setdata(res.data.data.list)
					}else{
						self.setdata(null)
						self.maindata = [];
					}
					//self.setdata(res.data.data.list)
					logger.logSuccess(res.data.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
				});
			},searchempnew:function(data){
				
			},setselect:function(){
				self.form={};
				self.form.category=1;
				self.form.include=1;
			},setactionadd:function(a){
				this.data=a;
			},actionadd:function(a){
				if(this.data){	
					switch (a) {
						case 'action':
						if(this.data.action_add==true){
							return true
						}
						break;
					}
				}
			},getautonumber:function(data){
				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/module/employee-info/add/employee?key=' + $cookieStore.get('key_api')
				}).then(function(res){
					// var test = self.getRole();
					// console.log(res);
					if(res.data.data.stat_user == "user"){
						self.employeeRole =  false;
						self.required =  null;
					}else{
						self.required =  "required";
					}
					self.setmodaladd(data,res.data.data);
					logger.logSuccess(res.data.header.message);
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Data Failed")
				});
			},getRole:function(){ 
				$http({
					method : 'GET',
					url : ApiURL.url +  '/api/user-management/system-user-json?key=' + $cookieStore.get('key_api') 
				}).then(function(res){
					var end = [res.data];
					var arr = [];
					for(var prop in end[0]){
						arr.push({"role_id" : end[0][prop].role_id, "role_name" : end[0][prop].role_name});
					}
					self.role_data = arr;
					self.setUserRole(self.role_data);
				},function(res){
					res.data.header ? logger.logError("error get role employee") : logger.logError("Show Data Failed")
				});
			},setmodaladd:function(dataA,dataB){
				dataA.autonumb=dataB.autonumb;
				if(dataB.local_it=='expat'){
					self.hideexpat=false
				}else{
					self.hideexpat=true
				}
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},setUserRole: function(a){
				this.userRole = a;
			},getUserRole: function(){
				return self.userRole;
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addemployee',
					controller: 'addemployeectrl',
					controllerAs: 'addemployee',
					size:'lg',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b

				$modal.open(self.modals[a]).result.then(function(data){
					data.employee_role = self.role_data  
					switch(a){
						case 'add':
						if(data.file){
							var file = data.file;
							delete data.file;
							file.upload = Upload.upload({
								method : 'POST',
								url : ApiURL.url + '/api/module/employee-info/add/employee/store?key=' + $cookieStore.get('key_api'),
								data :data,
								file: file
							}).then(function(res){
								self.adddata(res.data.data);
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
							});
						}else{
							$http({
								method : 'POST',
								url : ApiURL.url + '/api/module/employee-info/add/employee/store?key=' + $cookieStore.get('key_api'),
								data : data
							}).then(function(res){
								self.adddata(res.data.data);
								logger.logSuccess(res.data.header.message);
							},function(res){
								res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding New Employee Failed")
							});
						}
						break;
					}
					
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/module/employee-info/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i){
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.employee_id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Employee Success');
				},function(res){
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Employee Failed")
				})
			},

			/** set disabled form **/
			setdisabled:function(a){
				switch (a) {
					case 'employeeName':
					self.employeeId = true; self.employeeStatus = true; self.include = true; self.supervisor = true; self.jobTitle = true; self.department = true; self.category = true;		 		 		 	
					break;
					case 'employeeId':
					self.employeeName = true; self.employeeStatus = true; self.include = true; self.supervisor = true; self.jobTitle = true; self.department = true; self.category = true;		 		 		 	
					break;
					case 'employeeStatus':
					if($cookieStore.get('setdisabled')==''){
						$cookieStore.put('setdisabled','employeeStatus')
						if($cookieStore.get('setdisabled')=='employeeStatus'){
							self.employeeName = true; self.employeeId = true; self.supervisor = true; self.jobTitle=true;
						}	
					}else{
						void 0
					}
					break;
					case 'include':
					if($cookieStore.get('setdisabled')==''){
						$cookieStore.put('setdisabled','include')
						if($cookieStore.get('setdisabled')=='include'){
							self.employeeName = true; 	self.employeeId = true; 
							self.supervisor = true; 	self.jobTitle=true; 
							self.department=true;		self.category=true;
						}	
					}else{
						void 0
					}
					break;
					case 'supervisor':
					if($cookieStore.get('setdisabled')==''){
						$cookieStore.put('setdisabled','supervisor')
						if($cookieStore.get('setdisabled')=='supervisor'){
							self.employeeId = true; self.employeeName = true; self.jobTitle = true; self.department = true; 
						}	
					}else{
						void 0
					}
					break;
					case 'jobTitle':
					if($cookieStore.get('setdisabled')==''){
						$cookieStore.put('setdisabled','jobTitle')
						if($cookieStore.get('setdisabled')=='jobTitle'){
							self.employeeId = true; self.employeeName = true; self.department = true; self.supervisor = true;
						}	
					}else{
						void 0
					}
					break;
					case 'department':
					if($cookieStore.get('setdisabled')==''){
						$cookieStore.put('setdisabled','department')
						if($cookieStore.get('setdisabled')=='department'){
							self.employeeId = true; self.supervisor = true; self.employeeName = true; self.jobTitle = true; 
						}	
					}else{
						void 0
					}
					break;
					case 'category':
					if($cookieStore.get('setdisabled')==''){
						$cookieStore.put('setdisabled','category')
						if($cookieStore.get('setdisabled')=='category'){
							self.employeeId = true; self.supervisor = true; self.employeeName = true; self.jobTitle = true; 
						}	
					}else{
						void 0
					}
					break;
				}
			},	

			/** get employee name **/
			selectEmployee : function ($item){
				self.form.employeeName = $item.name;
				self.form.idEmployee = $item.employee_id;
			},
			getEmployee : function(val){
				return $http.get(ApiURL.url + '/api/module/employee-info/search-by-name?q='+self.form.employeeName.toLowerCase()+'&key=' + $cookieStore.get('key_api'),{
					params: {
						address : val,
					}
				}).then(function(response){
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};
						a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},

			/** get employee id **/
			selectEmployeeId : function ($item){
				self.form.employeeId = $item.employee_id;
			},
			getEmployeeId : function(val){
				return $http({
					url: ApiURL.url + '/api/module/employee_data/'+self.form.employeeId+'?&key='+$cookieStore.get('key_api'),
					method: 'POST',
					data:{"type": 1}
				}).then(function(response){
					if(response.data.data == ''){
						logger.logError('Not Found, Employee Id');
						var a = {};
						a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else if(response.data != null){
						var a = {};
						a.result = response.data.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},


			/** get supervior id **/
			selectsupervisor : function ($item){
				self.form.spv = $item.employee_id;
				self.form.supervisor = $item.name;
			},
			getsupervisor : function(val){
				return $http.get(ApiURL.url + '/api/module/employee-info/search-by-spv?q='+self.form.supervisor+'&key=' + $cookieStore.get('key_api'),{
					params: {
						address : val,
					}
				}).then(function(response){
					console.log(response.data);
					if(response.data.data === null){
						logger.logError(response.data.header.message);
						var a = {};
						a.result = [];
						return a.result.map(function(item){
							return item;
						});
					}else if(response.data != null){
						var a = {};
						a.result = response.data;
						return a.result.map(function(item){
							return item;
						});
					}  
				});
			},
			

			/** search employee **/
			searchempnew:function(data){
				console.log(data,'postdata')
			},resetdisabled:function(){
				self.employeeId = false; self.employeeName = false; self.employeeStatus = false; self.include = false; self.supervisor = false; self.jobTitle = false; self.department = false; self.category = false;
				$cookieStore.put('setdisabled','')
				self.getdatalist()		 		 		 		
			}
		}
	})

.controller('employeListCtrl',function(emplistfactory,$http,ApiURL,$cookieStore){
	var self = this;
	self.handler = emplistfactory;
	self.handler.getdatalist();
	console.log("test")

	// $http({
	// 	method : 'GET',
	// 	url : ApiURL.url+ '/api/module/employee-info?key='+ $cookieStore.get('key_api'),
	// }).then(function(res){
	// 	console.log(res)
	// })
	// $http({
	// 	method: 'GET',
	// 	url : ApiURL.url + '/api/module/employee-info/search-by-spv?q='+self.form.supervisor+'&key=' + $cookieStore.get('key_api')
	// }).then(function(res){
	// 	console.log(res)
	// })

})

.controller('addemployeectrl',function(emplistfactory,$modalInstance){	
	var self = this;
	self.handler = emplistfactory;
	self.form={};
			//self.roleUser = 
			self.handler.getRole(self);
			self.handler.getautonumber(self.form);
			self.save = function(file){
				self.form.file=file;
				$modalInstance.close(self.form);
			}
			self.cancel = function(){
				$modalInstance.dismiss('close')
			}
		})

.controller('modaleditterm',function(emplistfactory,$modalInstance,$timeout,data,time){	
	var self = this;
	self.handler = emplistfactory;
	self.handler.button();
	self.form = data;
	self.form = angular.copy(data);
	self.save = function(){
		$modalInstance.close({a:data,b:self.form});
	}
	self.close = function(){
		$modalInstance.dismiss('close')
	}
})
.run(function(emplistfactory){
	emplistfactory.setheaders();
})

}())