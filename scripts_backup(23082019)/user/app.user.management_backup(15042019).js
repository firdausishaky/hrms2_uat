	var appUser = angular.module("app.user.management",["checklist-model"]);

	/** controller user management / sysytem user**/
	appUser.factory('permissionuser', function() {
		var self;
			return self = {
				data:function(a){
					self.access = a;
					console.log(self.access)
				}
			}
	});

	appUser.controller("userCtrl", function($scope,$filter,$http,$modal,$log,$cookieStore,ApiURL,$routeParams,filterFilter,logger,permissionuser) {		
				
			$http.get(ApiURL.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api') ).success(function(res){
				if(res.header.message == "Unauthorized"){
					permissionuser.data(res.header.access);
					logger.logError("Access Unauthorized");
				}else{
					permissionuser.data(res.header.access);
					$scope.check = {
						currentPageStores: []
					}
					var end, start;
					start =0;
					$scope.stores = res.data;
					$scope.filteredStores = res.data;
					if($scope.filteredStores){
							end = start + $scope.numPerPageOpt[2];
							$scope.currentPageStores = $scope.filteredStores.slice(start, end);
							$scope.select($scope.currentPage);
					}else{
							$scope.filteredStores = [];
					}
					logger.logSuccess(res.header.message);
				}
			}).success(function(data){
				
			}).error(function(data){
					data.header ? logger.logError(data.header.message) : logger.logError("Show System User Failed")
			});
			$scope.button = function(a){
				var data = permissionuser.access;
				if(data){
					switch(a){
						case 'create':
							if(data.create == 1){return true}
						break;
						case 'delete':
							if(data.delete == 1){return true}
						break;
					}
				}
			};
			$scope.open = function(size) {
					var modal;
					modal = $modal.open({
						templateUrl: "ModalContent.html",
						controller: "modalUserCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return;
							}
						}
					});
					modal.result.then(function (newstudent) {
						if($scope.currentPageStores){
							$scope.currentPageStores.push(newstudent);
						}else{
							$scope.currentPageStores = [];
							$scope.currentPageStores.push(newstudent);
						}
					});
			};	
			$scope.edit = function(size,store) {
					var modal;
					modal = $modal.open({
						templateUrl: "EditModalContent.html",
						controller: "EditUserCtrl",
						backdrop : "static",
						size: size,
						resolve: {
							items: function(){
								return store;
							}
						}
					});
			};	
			$scope.remove = function(id){
				$http({
					method : 'DELETE',
					url : ApiURL.url + '/api/user-management/system-user/' + id + '?key=' + $cookieStore.get('key_api')
				}).success(function(data){
					var i;
						for( i = 0; i < id.length; i++) {
							$scope.currentPageStores = filterFilter($scope.currentPageStores, function (store) {
								return store.id != id[i];
							});
						};
					$scope.check.currentPageStores = [];
					logger.logSuccess(data.header.message);
				}).error(function(data){
					data.header ? logger.logError(data.header.message) : logger.logError("Delete User Failed")
				});
			};
			var init;
			$scope.numPerPageOpt = [3, 5, 10, 20]; 
			$scope.numPerPage = $scope.numPerPageOpt[2]; 
			$scope.currentPage = 1; 
			$scope.currentPageStores = []; 
			$scope.searchKeywords = ""; 
			$scope.filteredStores = []; 
			$scope.row = "" ;
			$scope.stores =[];
			$scope.select = function(page) {
				 var end, start;
				 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
			};
			$scope.onFilterChange = function() {
				return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
			};
			$scope.onNumPerPageChange = function() {
				return $scope.select(1), $scope.currentPage = 1
			};
			$scope.onOrderChange = function() {
				return $scope.select(1), $scope.currentPage = 1
			};
			$scope.search = function() {
				return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
			};
			$scope.order = function(rowName) {
				return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
			};
			init = function() {
				return $scope.search(), $scope.select($scope.currentPage)
			};
				
					
	});

	appUser.controller("modalUserCtrl", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,$routeParams,Upload) {
					
			if(items){
				$scope.user = items;
			}
			$http.get(ApiURL.url + '/api/user-management/system-user-json?key=' + $cookieStore.get('key_api') ).success(function(res){
				$scope.status=res;
			}); 				
			$scope.onSelect = function ($item, $model, $label) {
				$scope.user.employee_id = $item.employee_id;	
			};								
			$scope.getLocation = function(val) {
				return $http.get(ApiURL.url + '/api/user-management/search-domain?search=' +val + '&key=' + $cookieStore.get('key_api'), {
				  params: {
					//address : val,
				  }
				}).then(function(response){
					var a = {};
					a.result = response.data;
				  return a.result.map(function(item){
					return item;
				  });
				});
			};	
			$scope.save = function(){
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/user-management/system-user?key=' + $cookieStore.get('key_api'),
					headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
					data : $.param($scope.user,	$scope.user.employee_id)   
				}).success(function(data, status, header, config) {
					var temp = {};
						temp = $scope.user;
						temp.name=data.data[0].name;
						temp.role_name=data.data[0].role_name;
						temp.id = data.data[0].id;
					//	$scope.user='';				
					$modalInstance.close(temp);
					logger.logSuccess(data.header.message);
				}).error(function(data, status, headers, config) {
					logger.logError(data.header.message);
				});
			};				
			$scope.cancel = function() {
				$modalInstance.dismiss("cancel")
			}		
	});

	appUser.controller("EditUserCtrl", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,ApiURL,Upload,permissionuser) {
						
			$http.get(ApiURL.url + '/api/user-management/system-user-json?key=' + $cookieStore.get('key_api') ).success(function(res){
				$scope.stores=res;
			}); 	
			var a = String(items.name);
			var b = String(items.role_name);
			var c = String(items.status);
			if(items){
				$scope.user = items;
			}	
			$scope.onSelect = function ($item, $model, $label) {
				$scope.user.empoloyee_id = $item.employee_id;	
			};				
			$scope.getLocation = function(val) {
				return $http.get(ApiURL.url + '/api/user-management/search-domain?search=' +val + '&key=' + $cookieStore.get('key_api'), {
				  params: {
					//address : val,
				  }
				}).then(function(response){
					var a = {};
					a.result = response.data;
				  return a.result.map(function(item){
					return item;
				  });
				});
			};	
			$scope.save = function(id){
				$scope.user.name = items.domain_name;
				$http({
					method : 'POST',
					url : ApiURL.url + '/api/user-management/system-user/' + items.id + '?key=' + $cookieStore.get('key_api'),
					headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
					data : $.param($scope.user,$scope.user.empoloyee_id,$scope.user.name,$scope.user.role_name)   
				}).success(function(data, status, header, config) {
						var temp = {};
						temp = $scope.user;
						temp.name=data.data[0].name;
						temp.role_name=data.data[0].role_name;
						temp.status=data.data[0].status;
					//	$scope.user='';		
					$modalInstance.close(temp);
					logger.logSuccess(data.header.message);
				}).error(function(data, status, headers, config) {
					logger.logError(data.header.message);
				});
			};
			$scope.cancel = function(){
				$scope.user.name = a;
				$scope.user.role_name = b;
				$scope.user.status = c;
				$modalInstance.dismiss("cancel")
			};
			$scope.hide = function(a){
				var access  = permissionuser.access;				
				if(access){
					switch(a){
						case 'update':
							if(access.update == 1){
								return true
							}
						break;
					}
				}
			};
	});
		


		
		
		
		
		
		
		

	/** controller costum role menu **/
	appUser.factory('permissioncostum', function() {
		var self;
			return self = {
				data:function(a){
					self.access = a;
					console.log(self.access)
				}
			}
	});

	appUser.controller("costumCtrl",function($scope,$filter,$http,$modal,$log,$cookieStore,ApiURL,$routeParams,filterFilter,logger,permissioncostum) {		
			  
		$http.get(ApiURL.url + '/api/user-management/custome-role?key=' + $cookieStore.get('key_api') ).success(function(res){
		
			if(res.header.message == "Unauthorized"){
				permissioncostum.data(res.header.access);
				logger.logError("Access Unauthorized");
			}else{
				permissioncostum.data(res.header.access);
				$scope.check = {
					currentPageStores: []
				}
				var end, start;
				start =0;
				$scope.stores = res.data;
				$scope.filteredStores = res.data;
				if($scope.filteredStores){
						end = start + $scope.numPerPageOpt[2];
						$scope.currentPageStores = $scope.filteredStores.slice(start, end);
						$scope.select($scope.currentPage);
				}else{
						$scope.filteredStores = [];
				}
				logger.logSuccess(res.header.message);
			}
			}).success(function(data) {
					
			}).error(function(data) {
					data.header ? logger.logError(data.header.message) : logger.logError("Show System User Failed")
		});
		$scope.button = function(a){
			var data = permissioncostum.access;
			if(data){
				switch(a){
					case 'create':
						if(data.create == 1){return true}
					break;
					case 'delete':
						if(data.delete == 1){return true}
					break;
				}
			}
		};
			  	  
		var init;
		$scope.numPerPageOpt = [3, 5, 10, 20]; 
		$scope.numPerPage = $scope.numPerPageOpt[2]; 
		$scope.currentPage = 1; 
		$scope.currentPageStores = []; 
		$scope.searchKeywords = ""; 
		$scope.filteredStores = []; 
		$scope.row = "" ;
		$scope.stores =[];
		$scope.select = function(page) {
			 var end, start;
			 return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
		};
		$scope.onFilterChange = function() {
			return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
		};
		$scope.onNumPerPageChange = function() {
			return $scope.select(1), $scope.currentPage = 1
		};
		$scope.onOrderChange = function() {
			return $scope.select(1), $scope.currentPage = 1
		};
		$scope.search = function() {
			return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
		};
		$scope.order = function(rowName) {
			return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
		};
		init = function() {
			return $scope.search(), $scope.select($scope.currentPage)
		};	
		$scope.open = function(size) {
			var modal;
			modal = $modal.open({
				templateUrl: "modalRole.html",
				controller: "RoleCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return;
					}
				}
				 
			});
			modal.result.then(function (newstudent) {
				if($scope.currentPageStores){
					$scope.currentPageStores.push(newstudent);
				}else{
					$scope.currentPageStores = [];
					$scope.currentPageStores.push(newstudent);
				}
			});
		};
					
		$scope.edit = function(size,store) {
			var modal;
			modal = $modal.open({
				templateUrl: "editRole.html",
				controller: "EditRoleCtrl",
				backdrop : "static",
				size: size,
				resolve: {
					items: function(){
						return store;
					}
				}
			});
		};		
		$scope.remove = function(id){
			$http({
				method : 'DELETE',
				url : ApiURL.url + '/api/user-management/custome-role/' + id + '?key=' + $cookieStore.get('key_api')
			}).success(function(data, status, header, config) {
				
				var i;
					for( i = 0; i < id.length; i++) {
						$scope.currentPageStores = filterFilter($scope.currentPageStores, function (store) {
							return store.role_id != id[i];
						});
					};
				$scope.check.currentPageStores = [];
				
				logger.logSuccess(data.header.message);
			}).error(function(data, status, header) {
				data.header ? logger.logError(data.header.message) : logger.logError("Delete Costum Role Failed")
			});
		};
				
	});


	appUser.controller("RoleCtrl",function($scope,$filter,$http,$modal,$log,$cookieStore,ApiURL,$routeParams,filterFilter,logger,items,$modalInstance) {	
						
		if(items){
			$scope.user = items;
		}					
		/** get data select for base role **/
		$http.get(ApiURL.url + '/api/user-management/custome-role-data?key=' + $cookieStore.get('key_api') ).success(function(res){
			/** scope for select  base role **/ 
			$scope.sizes = res.data;
					/** function get data select checkbox **/
				$scope.update = function() {	
					var role_id = $scope.user.data.role_id;
					$http.get(ApiURL.url + '/api/user-management/custome-role/ '+  role_id +'?key=' + $cookieStore.get('key_api') ).success(function(res){
						$scope.checks = res;
					});
				}
		});		
		$scope.save = function(){
				$scope.checks.push($scope.user.localit);
				$scope.checks[0].role_name = $scope.user.role_name;
				$scope.checks = JSON.stringify($scope.checks);
			$http({
				method : 'POST',
				url : ApiURL.url + '/api/user-management/custome-role?key=' + $cookieStore.get('key_api'),
				headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
				data : {'data' : JSON.parse($scope.checks)}   
			}).success(function(data) {
				var temp = {};
					temp = $scope.user;
					temp.role_id = data.data;	
				$modalInstance.close(temp);
				logger.logSuccess(data.header.message);
			}).error(function(data) {
				logger.logError(data.header.message);
			});
		};	
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel")
		}		
					
	});


	appUser.controller("EditRoleCtrl", ["$modalInstance","items","$scope","filterFilter","$modal","$cookieStore","$http","logger","ApiURL","Upload", function($modalInstance,items,$scope,filterFilter,$modal,$cookieStore,$http,logger,api,Upload) {
				
					var a = String(items.role_name);
					if(items){
						$scope.user = items;					
					}
					console.log($scope.user);
				
					
						// GET DATA SELECT FOR BASE ROLE  
					
					$http.get(api.url + '/api/user-management/custome-role-data?key=' + $cookieStore.get('key_api') ).success(function(res){
						
						
						// SCOPE FOR SELECT  BASE ROLE
						
						$scope.sizes = res.data;
						
						
						// FUNCTION GET DATA SELECT CHECKBOX
						
							$scope.$watch('role_id', function () {
								var role_id = items.role_id;
								$http.get(api.url + '/api/user-management/custome-role/ '+  role_id + '?key=' + $cookieStore.get('key_api') ).success(function(res){
									$scope.checks = res;
									
								
								});
							});
							
							
							$scope.update = function() {						
									var role_id = $scope.user.data.role_id;
									
									$http.get(api.url + '/api/user-management/custome-role/ '+  role_id +'?key=' + $cookieStore.get('key_api') ).success(function(res){
										$scope.checks = res;
									
									});
							}
						
					});
					
					
					
				
					// UPDATE BASE ROLE
					
					$scope.save = function(){
						var role_id = items.role_id;
						console.log(role_id,"ROLE")
						// 	var role_id = $scope.checks[0].role_id ; 	
							$scope.checks[0].role_name = $scope.user.role_name; 
							$scope.checks = JSON.stringify($scope.checks);
						$http({
							method : 'POST',
							url : api.url + '/api/user-management/custome-role-update/' +  role_id  +'?key=' + $cookieStore.get('key_api'),
							headers : { 'Content-Type' : 'application/x-www-form-urlencoded', },
							data : {'data' :JSON.parse($scope.checks)}   
						}).success(function(data, status, header, config) {					
							var temp = {};
								temp.role_id = data.data;
								temp = $scope.user;
								$modalInstance.close(temp);
								logger.logSuccess(data.header.message);
						}).error(function(data, status, headers, config) {
							logger.logError(data.header.message);
						});
					};
					
					
					
					
					// FUNCTION OPEN FORM 
					$scope.edit = function(){
						
						console.log($scope.user.data);
						var a = [];
						for(a in $scope.sizes){	
					
						}
						
					};
					
			
			
					// FUNCTION CLOSE MODAL 
					$scope.cancel = function() {
						$scope.user.role_name = a;
						$modalInstance.dismiss()
					}
					
	}]);

	
