(function(){
			
	angular.module('app.job.employeestatus',['checklist-model'])

	.factory('empstatusfactory',function(pagination,$filter,$modal,$http,logger,ApiURL,$cookieStore,filterFilter){
		function createheaders(){
			var temp = [];
			for(var i=0;i<arguments.length;i++){
				var x = {};
				x.id = i;
				x.name =arguments[i][0];
				x.sort =arguments[i][1];
				x.width =arguments[i][2] ? arguments[i][2]+"%" :'auto';
				x.order =false;
				temp.push(x);
			}
			return temp;	
		}
		var self;
		return self = {
			setheaders: function(){
				this.headers = new createheaders(
					['Employment Status','title',100]
				);
				this.setactiveheader(this.headers[0],false)
			},getdataempstatus:function(){
				$http({
					url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
					method: 'GET'
				}).then(function(res){
					if(res.data.header.message == "Unauthorized"){
						self.getpermission(res.data.header.access);
						logger.logError("Access Unauthorized");
					}else{
						self.getpermission(res.data.header.access);
						self.setdata(res.data.data)
						logger.logSuccess(res.data.header.message);
					}
				},function(res){
					res.data.header ? logger.logError(res.data.header.message) : logger.logError("Show Employment Status Failed")
				})
			},getpermission:function(active){
				if(active){
					this.mainaccess = active;
				}
			},button:function(a){
				if(this.mainaccess){
					switch(a){
						case 'create':
								if(this.mainaccess.create == 1){return true}
							break;
						case 'delete':
								if(this.mainaccess.delete == 1){return true}		
							break;
						case 'update':
							if(this.mainaccess.update == 1){return true}		
						break;
					}
				}
			},callback:function(){
				$http({
					method : 'GET',
					url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
				}).then(function(res){
					self.setdata(res.data.data)
					console.log(res.data.data)
				});
			},adddata: function(a){
				this.maindata.push(a);
			},setdata: function(a){
				this.maindata = a;
			},getdata: function(){
				self.filtered = $filter('filter')(this.maindata,{$:self.search});
				self.select(self.currentPage);
				return self.paginated;
			},update: function(newdata){
				for(a in self.maindata){
					if(self.maindata[a].id == newdata.id){
						self.maindata[a] = newdata;
					}
				}
			},setactiveheader:function(a,bool){
				this.activeheader = a;
				this.activeheader.order = bool;
			},
			numPerPageOpt	: pagination.option,
			numPerPage 		: pagination.option[0],
			currentPage		: 1,
			search			: '',
			select			: function(page){
				if(self.filtered){
					start = (page-1)*self.numPerPage;
					end = start + self.numPerPage;
					self.paginated = self.filtered.slice(start, end) || false;
					self.filteredlength = self.paginated.length || 0;

				}
			},
			modals: {
				add:{
					animation: true,
					templateUrl: 'addmodal',
					controller: 'addmodalemployee',
					controllerAs: 'addmodal',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Add"
						},
						icon: function(){
							return "fa fa-save"
						}
					}
				},
				edit:{
					animation: true,
					templateUrl: 'modaledit',
					controller: 'modaleditemployee',
					controllerAs: 'modaledit',
					size:'',
					backdrop:'static',
					resolve:{
						data:function(){
							return self.datatoprocess;
						},
						title: function(){
							return "Edit"
						},
						icon: function(){
							return "fa fa-pencil-square-o"
						}
					}
				}
			},
			openmodal: function(a,b){
				self.datatoprocess = b
				$modal.open(self.modals[a]).result.then(function(data){
					switch(a){
						case 'add':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/jobs/employment-status?key=' + $cookieStore.get('key_api'),
									data : data,
								}).then(function(res){
									self.adddata(res.data.data[0])
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Adding Employment Status Failed")
								});
							break;
						case 'edit':
								$http({
									method : 'POST',
									url : ApiURL.url + '/api/jobs/employment-status/'  + b.id + '?key=' + $cookieStore.get('key_api'),
									data : data.b
								}).then(function(res){
									self.update(res.data.data)
									logger.logSuccess(res.data.header.message);
								},function(res){
									res.data.header ? logger.logError(res.data.header.message) : logger.logError("Updating Employment Status Failed")
								});			
							break;
					}
				});
			},
			deldata: function(id){
				$http({
					url : ApiURL.url + '/api/jobs/employment-status/' + id + '?key=' + $cookieStore.get('key_api'),
					method: 'DELETE'
				}).then(function(res){
					var i;
					for (i = 0; i < id.length; ++i) {
						self.maindata = filterFilter(self.maindata, function (store) {
							return store.id != id[i];
						})
					}
					self.check = [];
					logger.logSuccess('Deleting Employment Status Success');
				},function(res){
					console.log(res.data.header)
					res.data ? logger.logError(res.data.header.message) : logger.logError("Deleting Employment Status Failed")
				})
			}
		}
	})

	.controller('tableemployeestatusCtrl',function(empstatusfactory,ApiURL,$cookieStore){
		var self = this;
		self.handler = empstatusfactory;
		self.handler.getdataempstatus()
	})
	.controller('addmodalemployee',function(empstatusfactory,$modalInstance,data){	
		var self = this;
			self.save = function(){
				$modalInstance.close(self.form);
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})
	.controller('modaleditemployee',function(empstatusfactory,$modalInstance,data){	
		var self = this;
			self.handler = empstatusfactory;
			self.handler.button();
			self.form = data;
			self.form = angular.copy(data);
			self.save = function(){
				$modalInstance.close(
					{a:data,b:self.form}
				);
			}
			self.close = function(){
				$modalInstance.dismiss('close')
			}
	})
	.run(function(empstatusfactory){
		empstatusfactory.setheaders();
	})

}())